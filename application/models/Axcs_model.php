<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Axcs_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function login_user($email, $password) {        
        $query = $this->db->query("SELECT * FROM user where email='" . $email . "' AND password ='" . $password . "'");

        if ($query->num_rows() > 0) {            
            return $query->row();
        } else {
            return false;
        }
    }

    public function singupuser($data) {
        $this->db->insert("user", $data);
    }


    public function redundant_records_users($email, $passport) {
        $query = $this->db->query("SELECT email, passport FROM user WHERE email ='" . $email . "' OR passport='" . $passport . "';");
        if ($query->num_rows() > 0) {
            return false;
        } else {
            return true;
        }
    }


    private function encryption($password) {

        $setPassword = hash('sha512', $password, true);

        for ($i = 1; $i < 100; $i++) {
            $setPassword = hash('sha512', $setPassword . $password, true);
        }
        $clave = base64_encode($setPassword);
        return $clave;
    }

    public function rol($cond = 'TRUE') {
        $query = $this->db->query("SELECT * FROM roles WHERE $cond and active=1 ORDER BY name");

        if ($query->num_rows() > 0) {
            return $query;
        } else {
            return false;
        }
    }

}
