<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Passengers_model
 *
 * @author carlos
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Excursions_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->database();
    }

    function insertexcursions($data) {
        $fields = "('" . $data['name'] . "','" . $data['highlights'] . "','" . $data['info'] . "'," . $data['active'] . "," . $data['city_idcity'] . "," . $data['max_persons'] . "," . $_SESSION['iduser'] . ")";
        $result = $this->db->query('INSERT INTO `excursions`(`name`, `highlights`, `info`, `active`, `city_idcity`, `max_persons`, `user_iduser`) VALUES ' . $fields);
        if ($result) {
            $result = $this->db->query('select last_insert_id() as last');
            return $result->result()[0]->last;
        } else {
            return false;
        }
    }

    function updateexcursions($data) {
        $fields = "`name`='" . $data['name'] . "',`highlights`='" . $data['highlights'] . "',`info`='" . $data['info'] . "',`active`=" . $data['active'] . ",`city_idcity`=" . $data['city_idcity'] . ",`max_persons`=" . $data['max_persons'];

        $result = $this->db->query('UPDATE excursions SET ' . $fields . ' where idexcursions=' . $data['idexcursions']);
        if ($result) {
            return true;
        } else {
            return false;
        }
    }

    function deleteexcursions($data) {

        $fields = " `active`='" . $data['active'] . "'";

        $result = $this->db->query('UPDATE excursions SET ' . $fields . ' where idexcursions=' . $data['idexcursions']);
        if ($result) {
            return true;
        } else {
            return false;
        }
    }

    function selectexcursions($condi = "TRUE", $orderby = " order by idexcursions desc ", $limit = "", $fields = '*') {
        $query = $this->db->query('select ' . $fields . ' from excursions where ' . $condi . ' ' . $orderby . ' ' . $limit);
        if ($query->num_rows() > 0) {
            return $query;
        }
    }

    function insertimages($name, $idexcursions, $type) {
        $query = $this->db->query('select * from excursions_images where excursions_idexcursions=' . $idexcursions . ' and type=' . $type);
        if ($query->num_rows() > 0) {
            $data = get_object_vars($query->result()[0]);
            $result = $this->db->query('select images_idimages from excursions_images  where excursions_idexcursions=' . $data['excursions_idexcursions'] . ' and type=' . $type);
            if ($result) {
                $dataim = get_object_vars($result->result()[0])['images_idimages'];
                $imagenes = $this->db->query("UPDATE `images` SET `name`='" . $name . "' WHERE `idimages`=" . $dataim);
                if ($imagenes) {
                    return true;
                } else {
                    return false;
                }
            } else {
                return false;
            }
            return true;
        } else {
            $insert = $this->db->query("INSERT INTO `images`(`name`, `active`) values ('" . $name . "',1)");
            if ($insert) {
                $insert = $this->db->query('select last_insert_id() as last');
                $insert2 = $this->db->query("INSERT INTO `excursions_images`( `excursions_idexcursions`, `images_idimages`, `type`) VALUES (" . $idexcursions . "," . $insert->result()[0]->last . "," . $type . ")");
                if ($insert2) {
                    return true;
                }
                return $insert->result()[0]->last;
            } else {
                return false;
            }
        }
    }

    function hotelpricingchart($idexcursions, $startdate, $enddate, $starttime, $endtime, $description, $picukp, $drop_off) {
        //DELETE FROM `hotels_facilities` WHERE 1
        $delete = $this->db->query("DELETE FROM `pricing_chart` WHERE `excursions_idexcursions`=" . $idexcursions . "");
        if ($delete) {
            for ($i = 0; $i < count($startdate); $i++):
                $this->db->query("INSERT INTO `pricing_chart`(`start_date`, `end_date`, `pickup`, `drop_off`, `start_time`, `end_time`, `description`, `active`, `excursions_idexcursions`)"
                        . " VALUES('" . $startdate[$i] . "','" . $enddate[$i] . "','" . $picukp[$i] . "','" . $drop_off[$i] . "','" . $starttime[$i] . "','" . $endtime[$i] . "','" . $description[$i] . "',1," . $idexcursions . ")");
            endfor;
            ;
        }
    }

}
