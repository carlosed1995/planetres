<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Categories_model
 *
 * @author carlos
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Categories_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->database();
    }

    function insertcategories($data) {
        $fields = "'" . $data['name'] . "','" . $data['description'] . "'," . $data['active'];
        $result = $this->db->query('INSERT INTO `category`(`name`, `description`, `active`) VALUES (  ' . $fields . ")");
        if ($result) {
            $result = $this->db->query('select last_insert_id() as last');
            return $result->result()[0]->last;
        } else {
            return false;
        }
    }

    function updatecategories($data) {
        $fields = "`name`='" . $data['name'] . "',`description`='" . $data['description'] . "',`active`=" . $data['active'];
        $result = $this->db->query('UPDATE category SET ' . $fields . ' where idcategory=' . $data['idcategory']);
        if ($result) {
            return true;
        } else {
            return false;
        }
    }
    
    function deletecategories($data) {

        $fields = " `active`='" . $data['active'] . "'";

        $result = $this->db->query('UPDATE category SET ' . $fields . ' where idcategory=' . $data['idcategory']);
        if ($result) {
            return true;
        } else {
            return false;
        }
    }

    function selectcategories($condi = "TRUE", $orderby = " order by idcategory asc ", $limit = "", $fields = '*') {
        $query = $this->db->query('select ' . $fields . ' from category where ' . $condi . ' ' . $orderby . ' ' . $limit);
        if ($query->num_rows() > 0) {
            return $query;
        } else {
            return false;
        }
    }
}
