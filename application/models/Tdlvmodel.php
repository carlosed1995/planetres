<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Tdlvmodel extends CI_Model {

    public function __construct(){
        parent::__construct(); 
        $this->load->database();              
    }       


    function count_room($condi='TRUE'){
        $count = $this->db->query("SELECT COUNT(type_room_idtype_room) as count_room FROM room where ".$condi);
        if($count->num_rows() > 0){    
            return $count;                
        }else{
            return FALSE;
        }
    }

    function capture_room($guests_hotel,$room,$count_room,$num_adults,$num_kids){
        if($guests_hotel < 3){
            $guests = $this->db->query("SELECT idtype_room FROM `type_room` where idtype_room = ".$guests_hotel);
            if($guests->num_rows() > 0){    
                return $guests;                
            }else{
                return FALSE;
            }
        }elseif($room >= $count_room){
            if($guests_hotel > 2 && $num_adults < 4 && $num_kids < 4){
                $guests = $this->db->query("SELECT idtype_room FROM `type_room` where idtype_room = ".$guests_hotel." AND num_adults BETWEEN ".$num_adults." AND 4 AND num_kids BETWEEN ".$num_kids." and 4");
            }elseif($guests_hotel > 2 && $num_adults > 3 && $num_kids > 3){
                $guests = $this->db->query("SELECT idtype_room FROM `type_room` where idtype_room = ".$guests_hotel." AND num_adults BETWEEN 4 AND ".$num_adults." AND num_kids BETWEEN 4 AND ".$num_kids);
            }elseif($guests_hotel > 2 && $num_adults > 3 && $num_kids < 4){
                    $guests = $this->db->query("SELECT idtype_room FROM `type_room` where idtype_room = ".$guests_hotel." AND num_adults BETWEEN 4 AND ".$num_adults." AND num_kids BETWEEN ".$num_kids." AND 4");
            }elseif($guests_hotel > 2 && $num_adults < 4 && $num_kids > 3){
                $guests = $this->db->query("SELECT idtype_room FROM `type_room` where idtype_room = ".$guests_hotel." AND num_adults BETWEEN ".$num_adults." AND 4 AND num_kids BETWEEN 4 AND ".$num_kids);
            }
            if($guests->num_rows() > 0){    
                return $guests;                
            }else{
                return FALSE;
            }
        }else{
            return false;
        }                            
    }

         public function busqueda_hoteles($city,$country,$type_room){ 

            if($type_room < 3){
                $query = $this->db->query("SELECT hotel.*, category.idcategory from hotel INNER JOIN city ON city.idcity = hotel.city_idcity  INNER JOIN country ON country.idcountry = city.country_idcountry INNER JOIN category ON category.idcategory = hotel.category_idcategory where city.idcity = ".$city." and country.idcountry=".$country." and hotel.active = 1 GROUP BY hotel.idhotel");  
            }elseif($type_room > 2){
                $query = $this->db->query("SELECT hotel.*, category.idcategory from hotel INNER JOIN city ON city.idcity = hotel.city_idcity  INNER JOIN country ON country.idcountry = city.country_idcountry INNER JOIN category ON category.idcategory = hotel.category_idcategory where city.idcity = ".$city." and country.idcountry=".$country." and hotel.active = 1 GROUP BY hotel.idhotel");
            }

            if($query->num_rows() > 0){
                return $query;
            }else{
                //$query = $this->db->query();
                return false;
            }                                                                                  
        }

        public function busqueda_tours($destinations){


            $sql="SELECT tours.*,tours_type.*,daily_itinerary.*, tours.name as name_tours from tours INNER JOIN daily_itinerary on tours.idtours=daily_itinerary.tours_idtours INNER JOIN tours_type on tours.tours_type_idtours_type=tours_type.idtours_type INNER JOIN city ON city.idcity=daily_itinerary.city_idcity INNER JOIN country on country.idcountry=city.country_idcountry WHERE country.idcountry = ".$destinations." GROUP BY tours.idtours";

            $query=$this->db->query($sql);           



            if($query->num_rows() > 0){
                return $query;
            }else{
                return FALSE;
            }           
        }

        public function busqueda_excursions($city,$country,$first,$last,$guests_adults,$guests_kids){ 


            $sql="SELECT d.*, a.* from excursions a INNER JOIN city b ON a.city_idcity=b.idcity INNER JOIN country c ON c.idcountry=b.country_idcountry INNER JOIN pricing_chart d ON d.excursions_idexcursions=a.idexcursions where b.idcity=".$city." and c.idcountry=".$country." and d.start_date <= '".$last."' and d.end_date >= '".$first."' and a.guests_adults >= ".$guests_adults." and a.guests_kids >= ".$guests_kids." and a.active = 1";

            $query = $this->db->query($sql);
                            
            if($query->num_rows() > 0){
                return $query;
            }else{
                return FALSE;
            }           
        }

    public function busqueda_transfer($city,$country,$taxis){ 
        $sql="SELECT transfers.* FROM transfers INNER JOIN city ON city.idcity=transfers.city_idcity INNER JOIN country ON country.idcountry = city.country_idcountry INNER JOIN type_transfers ON type_transfers.idtype_transfers=transfers.type_transfers_idtype_transfers WHERE city.idcity=".$city."  and transfers.cars_availables >= '.$taxis.' and transfers.active = 1";
        
        $query= $this->db->query($sql);         
        if($query->num_rows() > 0){
            return $query;
        }else{
            return FALSE;
        }      
    }        
}