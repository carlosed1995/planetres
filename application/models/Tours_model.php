<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Passengers_model
 *
 * @author carlos
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Tours_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->database();
    }

    function inserttours($data) {        
        $fields = "('" . $data['code'] . "','" . $data['name'] ."','" . $data['location'] ."','" . $data['highlights'] . "'," . $data['max_persons'] . "," . $data['active'] . "," . $data['tours_type_idtours_type']."," .$_SESSION['iduser']. ")";
        $result = $this->db->query('INSERT INTO `tours`(`code`, `name`, `location`, `highlights`, `max_persons`, `active`, `tours_type_idtours_type`, `user_iduser`) VALUES  ' . $fields);
        if ($result) {
            $result = $this->db->query('select last_insert_id() as last');
            return $result->result()[0]->last;
        } else {
            return false;
        }
    }

    function updatetours($data) {                
        $fields="`code`='".$data['code']."',`name`='".$data['name']."',`location`='".$data['location']."',`highlights`='".$data['highlights']."',`max_persons`=".$data['max_persons'].",`active`=".$data['active'].",`tours_type_idtours_type`=".$data['tours_type_idtours_type']."";
        $result = $this->db->query('UPDATE tours SET ' . $fields . ' where idtours=' . $data['idtours']);
        if ($result) {
            return true;
        } else {
            return false;
        }
    }

    function deletetours($data) {

        $fields = " `active`='" . $data['active'] . "'";

        $result = $this->db->query('UPDATE tours SET ' . $fields . ' where idtours=' . $data['idtours']);
        if ($result) {
            return true;
        } else {
            return false;
        }
    }

    function selecttours($condi = "TRUE", $orderby = " order by idtours desc ", $limit = "", $fields = '*') {
        $query = $this->db->query('select ' . $fields . ' from tours where ' . $condi . ' ' . $orderby . ' ' . $limit);
        if ($query->num_rows() > 0) {
            return $query;
        }
    }

    function insertimages($name, $idtours, $type) {
        $query = $this->db->query('select * from tours_images where tours_idtours=' . $idtours . ' and type=' . $type);
        if ($query->num_rows() > 0) {
            $data = get_object_vars($query->result()[0]);
            $result = $this->db->query('select images_idimages from tours_images  where tours_idtours=' . $data['tours_idtours'] . ' and type=' . $type);
            if ($result) {
                $dataim = get_object_vars($result->result()[0])['images_idimages'];
                $imagenes = $this->db->query("UPDATE `images` SET `name`='" . $name . "' WHERE `idimages`=" . $dataim);
                if ($imagenes) {
                    return true;
                } else {
                    return false;
                }
            } else {
                return false;
            }
            return true;
        } else {
            $insert = $this->db->query("INSERT INTO `images`(`name`, `active`) values ('" . $name . "',1)");
            if ($insert) {
                $insert = $this->db->query('select last_insert_id() as last');
                $insert2 = $this->db->query("INSERT INTO `tours_images`( `tours_idtours`, `images_idimages`, `type`) VALUES (" . $idtours . "," . $insert->result()[0]->last . "," . $type . ")");
                if ($insert2) {
                    return true;
                }
                return $insert->result()[0]->last;
            } else {
                return false;
            }
        }
    }

    function daily_itinerary($idtours, $date,  $name, $description, $city) {
        //DELETE FROM `hotels_facilities` WHERE 1
        $delete = $this->db->query("DELETE FROM `daily_itinerary` WHERE `tours_idtours`=" . $idtours . "");
        if ($delete) {
            for ($i = 0; $i < count($date); $i++):
                $this->db->query("INSERT INTO `daily_itinerary`(`tours_idtours`, `date`, `name`, `description`, `city_idcity`, `active`)"
                        . " VALUES(". $idtours.",'". $date[$i] . "','" . $name[$i] . "','" . $description[$i] . "','" . $city[$i] . "',1". ")");
            endfor;
            ;
        }
    }

}
