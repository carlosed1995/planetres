<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Typetransfer_model
 *
 * @author carlos
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Typeroom_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->database();
    }

    function inserttyperoom($data) {
        $fields = "'" . $data['name'] . "','" . $data['description'] . "'," . $data['active']."," . $data['num_kids']."," . $data['num_adults'];
        $result = $this->db->query('INSERT INTO `type_room`(`name`, `description`, `active`,`num_kids`,`num_adults`) VALUES (  ' . $fields . ")");
        if ($result) {
            $result = $this->db->query('select last_insert_id() as last');
            return $result->result()[0]->last;
        } else {
            return false;
        }
    }

    function updatetyperoom($data) {
        $fields = "`name`='" . $data['name'] . "',`description`='" . $data['description'] . "',`active`=" . $data['active'].',`num_kids`='.$data['num_kids'].',`num_adults`='. $data['num_adults'];
        $result = $this->db->query('UPDATE type_room SET ' . $fields . ' where idtype_room=' . $data['idtype_room']);
        if ($result) {
            return true;
        } else {
            return false;
        }
    }
    
    function deletetyperoom($data) {

        $fields = " `active`='" . $data['active'] . "'";

        $result = $this->db->query('UPDATE type_room SET ' . $fields . ' where idtype_room=' . $data['idtype_room']);
        if ($result) {
            return true;
        } else {
            return false;
        }
    }

    function selecttyperoom($condi = "TRUE", $orderby = " order by idtype_room asc ", $limit = "", $fields = '*') {
        $query = $this->db->query('select ' . $fields . ' from type_room where ' . $condi . ' ' . $orderby . ' ' . $limit);
        if ($query->num_rows() > 0) {
            return $query;
        } else {
            return false;
        }
    }
}
