<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Typetransfer_model
 *
 * @author carlos
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Typetransfer_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->database();
    }

    function inserttypetransfer($data) {
        $fields = "'" . $data['name'] . "','" . $data['description'] . "'," . $data['active'];
        $result = $this->db->query('INSERT INTO `type_transfers`(`name`, `description`, `active`) VALUES (  ' . $fields . ")");
        if ($result) {
            $result = $this->db->query('select last_insert_id() as last');
            return $result->result()[0]->last;
        } else {
            return false;
        }
    }

    function updatetypetransfer($data) {
        $fields = "`name`='" . $data['name'] . "',`description`='" . $data['description'] . "',`active`=" . $data['active'];
        $result = $this->db->query('UPDATE type_transfers SET ' . $fields . ' where idtype_transfers=' . $data['idtype_transfers']);
        if ($result) {
            return true;
        } else {
            return false;
        }
    }
    
    function deletetypetransfer($data) {

        $fields = " `active`='" . $data['active'] . "'";

        $result = $this->db->query('UPDATE type_transfers SET ' . $fields . ' where idtype_transfers=' . $data['idtype_transfers']);
        if ($result) {
            return true;
        } else {
            return false;
        }
    }

    function selecttypetransfer($condi = "TRUE", $orderby = " order by idtype_transfers asc ", $limit = "", $fields = '*') {
        $query = $this->db->query('select ' . $fields . ' from type_transfers where ' . $condi . ' ' . $orderby . ' ' . $limit);
        if ($query->num_rows() > 0) {
            return $query;
        } else {
            return false;
        }
    }
}
