<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Typetransfer_model
 *
 * @author carlos
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Tourstype_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->database();
    }

    function inserttourstype($data) {
        $fields = "'" . $data['name'] . "','" . $data['description'] . "'," . $data['active'];
        $result = $this->db->query('INSERT INTO `tours_type`(`name`, `description`, `active`) VALUES (  ' . $fields . ")");
        if ($result) {
            $result = $this->db->query('select last_insert_id() as last');
            return $result->result()[0]->last;
        } else {
            return false;
        }
    }

    function updatetourstype($data) {
        $fields = "`name`='" . $data['name'] . "',`description`='" . $data['description'] . "',`active`=" . $data['active'];
        $result = $this->db->query('UPDATE tours_type SET ' . $fields . ' where idtours_type=' . $data['idtours_type']);
        if ($result) {
            return true;
        } else {
            return false;
        }
    }
    
    function deletetourstype($data) {

        $fields = " `active`='" . $data['active'] . "'";

        $result = $this->db->query('UPDATE tours_type SET ' . $fields . ' where idtours_type=' . $data['idtours_type']);
        if ($result) {
            return true;
        } else {
            return false;
        }
    }

    function selecttourstype($condi = "TRUE", $orderby = " order by idtours_type asc ", $limit = "", $fields = '*') {
        
        $query = $this->db->query('select ' . $fields . ' from tours_type where ' . $condi . ' ' . $orderby . ' ' . $limit);
        if ($query->num_rows() > 0) {
            return $query;
        } else {
            return false;
        }
    }
}
