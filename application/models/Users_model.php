<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Users_model
 *
 * @author carlos
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Users_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->database();
    }

    function insertuser($data) {
        $fields = "('" . $data['name'] . "','" . $data['middle_name'] . "','" . $data['last_name'] . "'," . $data['nationality'] . ",'" . $data['passport'] . "','" . ($data['cpyear'] . '-' . $data['cpmonth'] . '-' . $data['cpday']) . "','"
                . ($data['epyear'] . '-' . $data['epmonth'] . '-' . $data['epday']) . "','" . $data['email'] . "','" . $data['phone'] . "','" . $data['genere'] . "','" . ($data['year'] . '-' . $data['month'] . '-' . $data['day']) . "','" . $data['gps'] . "','" . $data['zip_code'] . "','" . md5($data['password'])
                . "','" . $data['url'] . "'," . $data['active'] . "," . $data['role_idrole'] . "," . $_SESSION['iduser'] . "," . $data['city_idcity'] . ")";
        $result = $this->db->query('INSERT INTO `user`( `name`, `middle_name`, `last_name`, `nationality`, `passport`, `expedition_passport`, `expire_passport`, `email`, `phone`, `genere`, `birth`, `gps`, `zip_code`, `password`, `url`, `active`, `role_idrole`, `user_iduser`, `city_idcity`) VALUES  ' . $fields);
        if ($result) {
            $result = $this->db->query('select last_insert_id() as last');
            return $result->result()[0]->last;
        } else {
            return false;
        }
    }

    function updateuser($data) {


        /* `url`=[value-16],`last_login`=[value-17],`active`=[value-18],`role_idrole`=[value-19],`user_iduser`=[value-20],`city_idcity`=[value-21] */
        $fields = "`name`='" . $data['name'] . "',`middle_name`='" . $data['middle_name'] . "',`last_name`='" . $data['last_name']
                . "',`nationality`=" . $data['nationality'] . ",`passport`='" . $data['passport'] . "',`expedition_passport`='" . ($data['cpyear'] . '-' . $data['cpmonth'] . '-' . $data['cpday'])
                . "',`expire_passport`='" . ($data['epyear'] . '-' . $data['epmonth'] . '-' . $data['epday']) . "',`email`='" . $data['email']
                . "',`phone`='" . $data['phone'] . "',`genere`='" . $data['genere'] . "',`birth`='" . $data['year'] . '-' . $data['month'] . '-' . $data['day']
                . "',`gps`='" . $data['gps'] . "',`zip_code`='" . $data['zip_code'] . "',`url`='" . $data['url'] . "',`role_idrole`= " . $data['role_idrole'] . ",`active`=" . $data['active'] . ",`city_idcity`=" . $data['city_idcity'];

        $result = $this->db->query('UPDATE user SET ' . $fields . ' where iduser=' . $data['iduser']);
        if ($result) {
            return true;
        } else {
            return false;
        }
    }

    function update_data_client($data) {


        
        $fields = "`name`='" . $data['name'] . "',`middle_name`='" . $data['middle_name'] . "',`last_name`='" . $data['last_name']
                . "',`nationality`=" . $data['nationality'] . ",`passport`='" . $data['passport'] . "',`expedition_passport`='" . ($data['year_expedition'] . '-' . $data['month_expedition'] . '-' . $data['day_expedition'])
                . "',`expire_passport`='" . ($data['year_expire'] . '-' . $data['month_expire'] . '-' . $data['day_expire']) . "',`phone`='" . $data['phone'] . "',`genere`='" . $data['genere'] . "',`birth`='" . $data['year'] . '-' . $data['month'] . '-' . $data['day']
                . "',`zip_code`='" . $data['zip_code'] . "',`url`='" . $data['url'] . "',`city_idcity`=" . $data['city'];

        $result = $this->db->query('UPDATE user SET ' . $fields . ' where iduser=' . $data['iduser']);
        if ($result) {
            return true;
        } else {
            return false;
        }
    }

    function update_data_support_client($data) {


        
        $fields = "`name`='" . $data['name'] . "',`middle_name`='" . $data['middle_name'] . "',`last_name`='" . $data['last_name']
                . "',`nationality`=" . $data['nationality'] . ",`phone`='" . $data['phone'] . "',`genere`='" . $data['genere'] . "',`birth`='" . $data['year'] . '-' . $data['month'] . '-' . $data['day']
                . "',`zip_code`='" . $data['zip_code'] . "',`url`='" . $data['url'] . "',`city_idcity`=" . $data['city'];

        $result = $this->db->query('UPDATE user SET ' . $fields . ' where iduser=' . $data['iduser']);
        if ($result) {
            return true;
        } else {
            return false;
        }
    }

    function update_email_config_general_client($data) {

        if ($data['new_email'] != '') {
            $fields = "email='" . $data['new_email'] . "'";
            $result = $this->db->query('UPDATE user SET ' . $fields . ' where iduser=' . $data['iduser']);
        } else {
            $result = false;
        }

        if ($result) {
            return true;
        } else {
            return false;
        }
    }

    function update_password_config_general_client($data) {

        if ($data['new_password'] != '') {
            $fields = "password='" . $data['new_password'] . "'";
            $result = $this->db->query('UPDATE user SET ' . $fields . ' where iduser=' . $data['iduser']);
        } else {
            $result = false;
        }

        if ($result) {
            return true;
        } else {
            return false;
        }
    }

    function deleteuser($data) {

        $fields = " `active`='" . $data['active'] . "'";

        $result = $this->db->query('UPDATE user SET ' . $fields . ' where iduser=' . $data['iduser']);
        if ($result) {
            return true;
        } else {
            return false;
        }
    }

    function selectuser($condi = "TRUE", $orderby = " order by iduser desc ", $limit = "", $fields = '*') {
        $query = $this->db->query('select ' . $fields . ' from user where ' . $condi . ' ' . $orderby . ' ' . $limit);
        if ($query->num_rows() > 0) {
            return $query;
        } else {
            return false;
        }
    }

    function selectuserimage($condi = "TRUE", $orderby = " order by iduser desc ", $limit = "", $fields = '*', $name = NULL, $iduser) {
        //echo 'select ' . $fields . ' from user_images where ' . $condi . ' ' . $orderby . ' ' . $limit;die();
        $query = $this->db->query('select ' . $fields . ' from user_images where ' . $condi . ' ' . $orderby . ' ' . $limit);
        if ($query->num_rows() > 0) {
            $data = get_object_vars($query->result()[0]);
            $result = $this->db->query('select images_idimages from user_images  where user_iduser=' . $data['user_iduser'] . ' and type=0');
            if ($result) {
                $dataim = get_object_vars($result->result()[0])['images_idimages'];
                $imagenes = $this->db->query("UPDATE `images` SET `name`='" . $name . "' WHERE `idimages`=" . $dataim);
                if ($imagenes) {
                    return true;
                } else {
                    return false;
                }
            } else {
                return false;
            }
            return true;
        } else {
            $insert = $this->db->query("INSERT INTO `images`(`name`, `active`) values ('" . $name . "',1)");
            if ($insert) {
                $insert = $this->db->query('select last_insert_id() as last');
                $insert2 = $this->db->query("INSERT INTO `user_images`( `user_iduser`, `images_idimages`, `type`) VALUES (" . $iduser . "," . $insert->result()[0]->last . ",0)");
                if ($insert2) {
                    return true;
                }
                return $insert->result()[0]->last;
            } else {
                return false;
            }
        }
    }

    function selectimage($iduser, $type = 0) {
        //die('select * from user_images where type=' . $type . ' and user_iduser=' . $iduser);
        $query = $this->db->query('select user_images.images_idimages,images.name from user_images inner join images on idimages=images_idimages  where type=' . $type . ' and user_iduser=' . $iduser);
        if ($query->num_rows() > 0) {
            return get_object_vars($query->result()[0]);
        } else {
            return NULL;
        }
    }

    function selectbussinesinformation($iduser) {
        $query = $this->db->query("SELECT * FROM `bussines_information` where user_iduser=" . $iduser);
        if ($query->num_rows() > 0) {
            return get_object_vars($query->result()[0]);
        } else {
            return NULL;
        }
    }

    function savebussinesinformation($data) {        
        if ($data['update'] == 'insert') {
            $fields = "'" . $data['paypal_account'] . "','" . $data['bank_name'] . "','" . $data['bank_account_name'] . "','" . $data['bank_account_number'] . "','" . $data['bank_account_swift'] . "'," . $data['active'] . "," . $data['id'];
            $queryexecute = "INSERT INTO `bussines_information`(`paypal_account`, `bank_name`, `bank_account_name`, `bank_account_number`, `bank_account_swift`, `active`, `user_iduser`) VALUES (" . $fields . ")";
        } else {            
            $queryexecute = "UPDATE `bussines_information` SET `paypal_account`='". $data['paypal_account'] ."',`bank_name`='". $data['bank_name'] ."',`bank_account_name`='". $data['bank_account_name'] ."',`bank_account_number`='". $data['bank_account_number'] ."',`bank_account_swift`='". $data['bank_account_swift'] ."',`active`=" . $data['active'] . " WHERE `user_iduser`=".$data['id'];
        }        
        $query = $this->db->query($queryexecute);
        if ($query) {
            return true;
        } else {
            return NULL;
        }
    }

}
