<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Planetresmodel extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    function information_transfers($fields = '* FROM transfers', $condition = 'TRUE'){
        $query = $this->db->query("SELECT ".$fields." where ".$condition);
        if ($query->num_rows() > 0) {
            return $query;
        } else {
            return false;
        }
    }

    function information_excursions($fields = '* FROM excursions', $condition = 'TRUE'){
        $query = $this->db->query("SELECT ".$fields." where ".$condition);
        if ($query->num_rows() > 0) {
            return $query;
        } else {
            return false;
        }
    }

    function reservation_room($condition = 'TRUE'){
        $query = $this->db->query("SELECT * FROM reservations_room where ".$condition);
        if ($query->num_rows() > 0) {
            return $query;
        } else {
            return false;
        }
    }

    function information_tours($fields = '* FROM tours', $condition = 'TRUE'){
        $query = $this->db->query("SELECT ".$fields." where ".$condition);
        if ($query->num_rows() > 0) {
            return $query;
        } else {
            return false;
        }
    }

    function information_hotel($fields = '* FROM hotel', $condition = 'TRUE'){
        $query = $this->db->query("SELECT ".$fields." where ".$condition);
        if ($query->num_rows() > 0) {
            return $query;
        } else {
            return false;
        }
    }

    

    function taxis_availables(){
        $query = $this->db->query("SELECT MAX(cars_availables) as car_available FROM transfers");
        if ($query->num_rows() > 0) {
            return $query;
        } else {
            return false;
        }
    }

    function guests_excursions_adults(){
        $query = $this->db->query("SELECT MAX(guests_adults) as guests_adults from excursions");
        if ($query->num_rows() > 0) {
            return $query;
        } else {
            return false;
        }
    }

    function guests_excursions_kids(){
        $query = $this->db->query("SELECT MAX(guests_kids) as guests_kids from excursions");
        if ($query->num_rows() > 0) {
            return $query;
        } else {
            return false;
        }
    }

    function country($condition='TRUE') {
        $query = $this->db->query("SELECT idcountry,name,abbre FROM country where ".$condition." ORDER BY name");

            if ($query->num_rows() > 0) {
                return $query;
            } else {
                return false;
            }
    }

    function nationality($condicion = TRUE, $limit = '', $orderby = "ORDER BY name") {
        $query = $this->db->query("SELECT idcountry,abbre,name FROM country where ".$condicion." ".$orderby." ".$limit);
        if ($query->num_rows() > 0) {
            return $query;
        } else {
            return false;
        }
    }

    function city_country($condicion = TRUE, $limit = '', $groupby='group by city.abbre') {

        $sql='SELECT city.idcity, country.idcountry, city.name as city, country.name as country, country.abbre FROM city inner join country on country.idcountry=city.country_idcountry WHERE '.$condicion.' and city.active=1 and country.active=1 '.$groupby.'  '.$limit.' ';

        $query = $this->db->query($sql);

        if ($query->num_rows() > 0) {
            return $query;
        } else {
            return false;
        }
    }

    function typetours($condition='TRUE') {
        $query = $this->db->query("SELECT * FROM tours_type where ".$condition." and active = 1");
        if ($query->num_rows() > 0) {
            return $query;
        } else {
            return false;
        }
    }  

    function typeroom($condition='TRUE', $groupby='') {
        $sql = 'SELECT * FROM type_room where '.$condition.' '.$groupby;
        
        $query = $this->db->query($sql);
        
        if ($query->num_rows() > 0) {
            return $query;
        } else {
            return false;
        }
    }  

    function room ($condition='TRUE') {
        $query = $this->db->query("SELECT * FROM room where ".$condition." and active = 1");
        if ($query->num_rows() > 0) {
            return $query;
        } else {
            return false;
        }
    }  

    function count_room($condition='TRUE'){
        $query = $this->db->query("SELECT count(*) as count_room FROM room where ".$condition." and active = 1");
        if ($query->num_rows() > 0) {
            return $query;
        } else {
            return false;
        }
    }

    function guests_excursions($id) {
        $query = $this->db->query("SELECT max_persons FROM excursions where idexcursions= ".$id." and active = 1");
        
        if ($query->num_rows() > 0) {
            return $query;
        } else {
            return false;
        }

    }   

    function guests_transport($id) {
        $query = $this->db->query("SELECT cars_availables FROM transfers where idtrasnfers= ".$id." and active = 1");

        if ($query->num_rows() > 0) {
            return $query;
        } else {
            return false;
        }
    }   

        function  facilities_hotel ($condi = 'TRUE'){

            $sql="SELECT * FROM hotels_facilities inner join facilities on facilities.idfacilities=hotels_facilities.facilities_idfacilities where ".$condi;

            $query = $this->db->query($sql);

            if($query->num_rows() > 0){
                return $query;
            }else{
                return false;
            }
        }


        function  img_hotel ($condi = 'TRUE'){

            $sql="SELECT * FROM hotels_images INNER JOIN images on images.idimages=hotels_images.images_idimages where ".$condi;

            $query = $this->db->query($sql);

            if($query->num_rows() > 0){
                return $query;
            }else{
                return false;
            }
        }

       

        function  details_hotel ($id){

            $sql="SELECT hotel.*, city.name as city FROM hotel INNER JOIN city ON city.idcity=hotel.city_idcity where hotel.idhotel = ".$id;

            $query = $this->db->query($sql);

            if($query->num_rows() > 0){
                return $query;
            }else{
                return false;
            }
        }


        function  details_tours ($id){

         $sql="SELECT tours.*, daily_itinerary.day_of_tours as city , country.name as country from tours INNER JOIN daily_itinerary ON daily_itinerary.tours_idtours=tours.idtours INNER JOIN city ON city.idcity=daily_itinerary.city_idcity INNER JOIN country ON country.idcountry = city.country_idcountry WHERE tours.idtours=$id  GROUP BY tours.idtours";

            $query = $this->db->query($sql);

            if($query->num_rows() > 0){
                return $query;
            }else{
                return false;
            }
        } 

        function  daily_itinerary ($condi = 'TRUE'){

            $sql="select *, (select hotel.name from hotel where idhotel=daily_itinerary.hotel_fc_idhotel) as first_class, (select hotel.name from hotel where idhotel=daily_itinerary.hotel_mfc_idhotel) as moderate_first_class, (select hotel.category_idcategory from hotel where idhotel=daily_itinerary.hotel_fc_idhotel) as class_hotel_1, (select hotel.category_idcategory from hotel where idhotel=daily_itinerary.hotel_mfc_idhotel) as class_hotel_2 from daily_itinerary where ".$condi;

            $query = $this->db->query($sql);

            if($query->num_rows() > 0){
                return $query;
            }else{
                return false;
            }
        }

        function hotel_class_tours($condi = 'TRUE'){
            $sql = "select *, (select hotel.name from hotel where idhotel=daily_itinerary.hotel_fc_idhotel) as first_class, (select hotel.name from hotel where idhotel=daily_itinerary.hotel_mfc_idhotel) as moderate_first_class, (select hotel.category_idcategory from hotel where idhotel=daily_itinerary.hotel_fc_idhotel) as class_hotel_1, (select hotel.category_idcategory from hotel where idhotel=daily_itinerary.hotel_mfc_idhotel) as class_hotel_2 from daily_itinerary where ".$condi." GROUP BY daily_itinerary.tours_idtours";

            $query = $this->db->query($sql);

            if($query->num_rows() > 0){
                return $query;
            }else{
                return false;
            }
        }

        function price_per_person_tours($condi='TRUE'){
            $sql = 'SELECT departure_date_and_price.* FROM `departure_date_and_price` INNER JOIN tours on tours.idtours=departure_date_and_price.tour_idtour where '.$condi;
            $query = $this->db->query($sql);
            if($query->num_rows() > 0){
                return $query;
            }else{
                return false;
            }
        }

        function review_itinerary($fields='*',$condi='TRUE'){
            $sql = 'select '.$fields.' where '.$condi;
            $query = $this->db->query($sql);
            if($query->num_rows() > 0){
                return $query;
            }else{
                return false;
            }
        }

        function departure_date_tours($condi = 'TRUE'){
            $sql="SELECT * FROM departure_date_and_price where ".$condi;
            $query = $this->db->query($sql);
            if($query->num_rows() > 0){
                return $query;
            }else{
                return false;
            }
        }

        function  img_tours ($condi = 'TRUE'){

            $sql="SELECT * FROM tours_images INNER JOIN images on images.idimages=tours_images.images_idimages where ".$condi;

            $query = $this->db->query($sql);

            if($query->num_rows() > 0){
                return $query;
            }else{
                return false;
            }
        }

        function  details_excursion ($id){

           // $query = $this->db->query("SELECT a.name,a.description,a.duration as duration_excursion,b.name as city FROM excursions a INNER JOIN city b ON a.id_city=b.idcity where a.idexcursions = ".$id);

            $sql="SELECT excursions.*,city.name as city FROM excursions INNER JOIN city ON excursions.city_idcity=city.idcity where excursions.idexcursions = $id";

            $query = $this->db->query($sql);

            if($query->num_rows() > 0){
                return $query;
            }else{
                return false;
            }
        }

        function departure_date_excursions($condi = 'TRUE'){
            $sql="SELECT * FROM departure_date_and_price_excursions where ".$condi;
            $query = $this->db->query($sql);
            if($query->num_rows() > 0){
                return $query;
            }else{
                return false;
            }
        }

        function price_per_person_excurions($condi='TRUE'){
            $sql = 'SELECT departure_date_and_price_excursions.*, city.name as city FROM `departure_date_and_price_excursions` INNER JOIN excursions on excursions.idexcursions=departure_date_and_price_excursions.excursions_idexcursions inner join city on city.idcity=excursions.city_idcity where '.$condi;
            $query = $this->db->query($sql);
            if($query->num_rows() > 0){
                return $query;
            }else{
                return false;
            }
        }

        function  pricing_chart ($condi = 'TRUE'){

            $sql="SELECT * FROM pricing_chart where ".$condi;

            $query = $this->db->query($sql);

            if($query->num_rows() > 0){
                return $query;
            }else{
                return false;
            }
        }


        function  img_excursions ($condi = 'TRUE'){

            $sql="SELECT * FROM excursions_images INNER JOIN images on images.idimages=excursions_images.images_idimages where ".$condi;

            $query = $this->db->query($sql);

            if($query->num_rows() > 0){
                return $query;
            }else{
                return false;
            }
        }

        function  details_transfers ($id){

            $query = $this->db->query("SELECT transfers.*, city.name as city, type_transfers.name as type FROM transfers INNER JOIN city ON transfers.city_idcity=city.idcity INNER JOIN type_transfers on type_transfers.idtype_transfers=transfers.type_transfers_idtype_transfers where transfers.idtransfers =".$id);

            if($query->num_rows() > 0){
                return $query;
            }else{
                return false;
            }

        }

        function  departure_transfers (){

            $query = $this->db->query("SELECT * from transfers where type_transfers_idtype_transfers=2");

            if($query->num_rows() > 0){
                return $query;
            }else{
                return false;
            }

        }

        function  count_departure (){

            $query = $this->db->query("SELECT count(*) as count_departure from transfers where type_transfers_idtype_transfers=2");

            if($query->num_rows() > 0){
                return $query;
            }else{
                return false;
            }

        }

        function insert_supplier_person($datos){

            $datos=array(

                'first_name' => $datos['first_name'],
                'middle_name' => $datos['middle_name'],
                'last_name' => $datos['last_name'],
                'address' => $datos['address'],
                'city' => $datos['city'],
                'state' => $datos['state'],
                'country' => $datos['country'],
                'zip' => $datos['zip'],
                'email' => $datos['email'],
                'celular' => $datos['phone'],
                'skype' => $datos['skype'],
                'whatsapp' => $datos['whatsapp']

                );

            $this->db->insert('supplier_personal',$datos);

        }

        function insert_supplier_business($datos){


            $datos=array(
                'name' => $datos['business_name'],
                'registration_name' => $datos['registration_name'],
                'website' => $datos['website'],
                'address' => $datos['business_address'],
                'city' => $datos['business_city'],
                'state' => $datos['business_state'],
                'country' => $datos['business_country'],
                'zip' => $datos['business_zip'],
                'email  ' => $datos['business_email'],
                'phone' => $datos['business_phone'],
                'skype' => $datos['business_skype'],
                'whatsapp' => $datos['business_whatsapp'],
                'hotel_products' => $datos['hotel'],
                'tours_products' => $datos['tours'],
                'excursions_products' => $datos['excursions'],
                'transfers_products' => $datos['transfer'],
                'paypal_account' => $datos['paypal'],
                'bank_name' => $datos['bank_name'],
                'bank_account_name' => $datos['bank_acc_name'],
                'bank_account_numer' => $datos['acc_num'],
                'back_swift_number' => $datos['swift_num']
            );


                 $this->db->insert('supplier_business',$datos);
        }


        function category_hotel(){

           $query = $this->db->query(" SELECT idcategory, name from category");

             if($query->num_rows() > 0){
                return $query;
            }else{
                return false;
            }

        }

        function country_hotel(){

            $query = $this->db->query(" SELECT idcountry, name from country");

            if($query->num_rows() > 0){
                return $query;
            }else{
                return false;
            }



        }

       function city_filter($idcountry){

           $query = $this->db->query(" SELECT a.idcity, a.name from city a INNER JOIN country b ON b.idcountry=a.country_idcountry WHERE idcountry=$idcountry order by a.name");

             if($query->num_rows() > 0){
                return $query;
            }else{
                return false;
            }

        }

        function insert_hotel($datos){

            $datos=array(
                'name' => $datos['name'],
                'ubication' => $datos['address'],
                'description' => $datos['description'],
                'areainformation' => $datos['information'],
                'map' => $datos['map'],
                'location' => $datos['location'],
                'category_idcategory' => $datos['category'],
                'active' => $datos['status'],
                'city_idcity' => $datos['city']
                
            );


                 $this->db->insert('hotel',$datos);

        }

        function type_tours(){

            $query = $this->db->query(" SELECT idtours_type, name from tours_type");

            if($query->num_rows() > 0){
                return $query;
            }else{
                return false;
            }


        }

        function insert_tours(){

            


        }

      
        
}

?>