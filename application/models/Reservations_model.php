<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Passengers_model
 *
 * @author carlos
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Reservations_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->database();
         
    }

    function insertreservations($data) {
        $data['date_reservations'] = $data['year'] . '-' . $data['month'] . '-' . $data['day'] . ' ' . date('H:m:i');        
        $fields = "('" . $data['date_reservations'] . "'," . $data['cost_total'] . ",'" . $data['pay'] . "'," . $data['active'] . ")";
        $result = $this->db->query('INSERT INTO reservations(date_reservations, cost_total,  pay,  active) VALUES ' . $fields);
        if ($result) {
            $result = $this->db->query('select last_insert_id() as last');
            return $result->result()[0]->last;
        } else {
            return false;
        }
    }

    function updatereservations($data) {
$data['date_reservations'] = $data['year'] . '-' . $data['month'] . '-' . $data['day'] . ' ' . date('H:m:i');        
        $fields = "date_reservations='" . $data['date_reservations'] . "',cost=" . $data['cost_total'] . ",pay='" . $data['pay'] . "',active=" . $data['activate'] ."";

        $result = $this->db->query('UPDATE reservations SET ' . $fields . ' where idreservations=' . $data['idreservations']);
        if ($result) {
            return true;
        } else {
            return false;
        }
    }

    function deletereservations($data) {

        $fields = " `active`='" . $data['active'] . "'";

        $result = $this->db->query('UPDATE reservations SET ' . $fields . ' where idreservations=' . $data['idreservations']);
        if ($result) {
            return true;
        } else {
            return false;
        }
    }

    function selectreservations($condi = "TRUE", $orderby = " order by idreservations desc ", $limit = "", $fields = '*') {
        $query = $this->db->query('select ' . $fields . ' from reservations where ' . $condi . ' ' . $orderby . ' ' . $limit);
        if ($query->num_rows() > 0) {
            return $query;
        }
    }

    function selectreservations_client($fields = '*') {
        $query = $this->db->query('select ' . $fields);
        if ($query->num_rows() > 0) {
            return $query;
        }
    }

    function selectreservations_client_travel($fields = '*') {
        $query = $this->db->query('select ' . $fields);
        if ($query->num_rows() > 0) {
            return $query;
        }
    }



}
