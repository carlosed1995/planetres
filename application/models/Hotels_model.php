<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Passengers_model
 *
 * @author carlos
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Hotels_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->database();
    }

    function inserthotels($data) {
        $fields = "('" . $data['name'] . "','" . $data['ubication'] . "','" . $data['description'] . "','" . $data['areainformation'] . "','" . $data['map'] . "','" . $data['location'] . "',"
                . $data['alloment_single'] . "," . $data['alloment_double'] . "," . $data['alloment_family'] . "," . $data['alloment_multifold'] . "," . $data['category_idcategory'] . "," . $data['active'] . "," . $data['city_idcity'] . "," . $_SESSION['iduser'] . ")";
        $result = $this->db->query('INSERT INTO `hotel`(`name`, `ubication`, `description`, `areainformation`, `map`, `location`, `alloment_single`, `alloment_double`, `alloment_family`, `alloment_multifold`, `category_idcategory`, `active`, `city_idcity`, `user_iduser`) VALUES ' . $fields);
        if ($result) {
            $result = $this->db->query('select last_insert_id() as last');
            return $result->result()[0]->last;
        } else {
            return false;
        }
    }

    function updatehotels($data) {
        $fields = "`name`='" . $data['name'] . "',`ubication`='" . $data['ubication'] . "',"
                . "`description`='" . $data['description'] . "',`areainformation`='" . $data['areainformation'] . "',`map`='" . $data['map'] . "',"
                . "`location`='" . $data['location'] . "',`alloment_single`=" . $data['alloment_single'] . ",`alloment_double`=" . $data['alloment_double'] . ","
                . "`alloment_family`=" . $data['alloment_family'] . ",`alloment_multifold`=" . $data['alloment_multifold'] . ","
                . "`category_idcategory`=" . $data['category_idcategory'] . ",`active`=" . $data['active'] . ",`city_idcity`=" . $data['city_idcity'];

        $result = $this->db->query('UPDATE hotel SET ' . $fields . ' where idhotel=' . $data['idhotel']);
        if ($result) {
            return true;
        } else {
            return false;
        }
    }

    function deletehotels($data) {

        $fields = " `active`='" . $data['active'] . "'";

        $result = $this->db->query('UPDATE hotel SET ' . $fields . ' where idhotel=' . $data['idhotel']);
        if ($result) {
            return true;
        } else {
            return false;
        }
    }

    function selecthotels($condi = "TRUE", $orderby = " order by idhotel desc ", $limit = "", $fields = '*') {
        $query = $this->db->query('select ' . $fields . ' from hotel where ' . $condi . ' ' . $orderby . ' ' . $limit);
        if ($query->num_rows() > 0) {
            return $query;
        }
    }

    function insertimages($name, $idhotel, $type) {
        $query = $this->db->query('select * from hotels_images where hotels_idhotel=' . $idhotel . ' and type=' . $type);
        if ($query->num_rows() > 0) {
            $data = get_object_vars($query->result()[0]);
            $result = $this->db->query('select images_idimages from hotels_images  where hotels_idhotel=' . $data['hotels_idhotel'] . ' and type=' . $type);
            if ($result) {
                $dataim = get_object_vars($result->result()[0])['images_idimages'];
                $imagenes = $this->db->query("UPDATE `images` SET `name`='" . $name . "' WHERE `idimages`=" . $dataim);
                if ($imagenes) {
                    return true;
                } else {
                    return false;
                }
            } else {
                return false;
            }
            return true;
        } else {
            $insert = $this->db->query("INSERT INTO `images`(`name`, `active`) values ('" . $name . "',1)");
            if ($insert) {
                $insert = $this->db->query('select last_insert_id() as last');
                $insert2 = $this->db->query("INSERT INTO `hotels_images`( `hotels_idhotel`, `images_idimages`, `type`) VALUES (" . $idhotel . "," . $insert->result()[0]->last . "," . $type . ")");
                if ($insert2) {
                    return true;
                }
                return $insert->result()[0]->last;
            } else {
                return false;
            }
        }
    }

    function hotelfeacilities($idhotel, $facilidades) {
        //DELETE FROM `hotels_facilities` WHERE 1
        $delete = $this->db->query("DELETE FROM `hotels_facilities` WHERE `hotels_idhotel`=" . $idhotel . "");
        if ($delete) {
            foreach ($facilidades as $facil):
                $this->db->query("INSERT INTO `hotels_facilities`(`hotels_idhotel`, `facilities_idfacilities`)  values(" . $idhotel . "," . $facil . ")");
            endforeach;
        }
    }

}
