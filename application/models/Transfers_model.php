<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Passengers_model
 *
 * @author carlos
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Transfers_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->database();
    }

    function inserttransfers($data) {
        $fields = "(" . $data['type_transfers_idtype_transfers'] . "," . $data['city_idcity'] . ",'" . $data['name'] . "','" . $data['description'] . "'," . $data['cars_availables'] . "," . $data['active'] . "," . $_SESSION['iduser'] . ")";
        $result = $this->db->query('INSERT INTO `transfers`( `type_transfers_idtype_transfers`, `city_idcity`, `name`, `description`, `cars_availables`, `active`, `user_iduser`) VALUES ' . $fields);
        if ($result) {
            $result = $this->db->query('select last_insert_id() as last');
            return $result->result()[0]->last;
        } else {
            return false;
        }
    }

    function updatetransfers($data) {


        $fields = "`type_transfers_idtype_transfers`=" . $data['type_transfers_idtype_transfers'] . ",`city_idcity`=" . $data['city_idcity'] . ",`name`='" . $data['name'] . "',`description`='" . $data['description'] . "',`cars_availables`=" . $data['cars_availables'] . ",`active`=" . $data['active'];
        $result = $this->db->query('UPDATE transfers SET ' . $fields . ' where idtrasnfers=' . $data['idtrasnfers']);
        if ($result) {
            return true;
        } else {
            return false;
        }
    }

    function deletetransfers($data) {

        $fields = " `active`='" . $data['active'] . "'";

        $result = $this->db->query('UPDATE transfers SET ' . $fields . ' where idtrasnfers=' . $data['idtrasnfers']);
        if ($result) {
            return true;
        } else {
            return false;
        }
    }

    function selecttransfers($condi = "TRUE", $orderby = " order by idtrasnfers desc ", $limit = "", $fields = '*') {
        $query = $this->db->query('select ' . $fields . ' from transfers where ' . $condi . ' ' . $orderby . ' ' . $limit);
        if ($query->num_rows() > 0) {
            return $query;
        }
    }

}
