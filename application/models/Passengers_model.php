<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Passengers_model
 *
 * @author carlos
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Passengers_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->database();
    }

    function insertpassenger($data) {
        
        $fields = "('" . $data['name'] . "','" . $data['last_name'] . "','" . $data['passport'] . "','" . $data['email'] . "','" . $data['phone'] . "','" . $data['id_city'] . "','" . $data['genere'] . "','" . $data['year'] . '-' . $data['month'] . '-' . $data['day'] . "','" . $data['cpyear'] . '-' . $data['cpmonth'] . '-' . $data['cpday'] . "','" . $data['epyear'] . '-' . $data['epmonth'] . '-' . $data['epday'] . "'," . $data['nationality'] . "," . $data['miles'] . ",'" . $data['miles_program'] . "')";
        //echo 'INSERT INTO passengers( name, last_name, passport,  email, phone,  id_city,  genere, birth,expdedition_passport,expire_passport,nationality,miles,miles_program) VALUES ' . $fields;
        $result = $this->db->query('INSERT INTO passengers( name, last_name, passport,  email, phone,  id_city,  genere, birth,expdedition_passport,expire_passport,nationality,miles,miles_program) VALUES ' . $fields);
        if ($result) {
            $result = $this->db->query('select last_insert_id() as last');
            return $result->result()[0]->last;
        } else {
            return false;
        }
    }

    function updatepassenger($data) {

        $fields = "name='" . $data['name'] . "',last_name='" . $data['last_name'] . "',passport='" . $data['passport'] . "',email='" . $data['email'] . "',phone='" . $data['phone'] . "',id_city='" . $data['id_city'] . "',genere='" . $data['genere'] . "',birth='" . $data['year'] . '-' . $data['month'] . '-' . $data['day'] . "',expdedition_passport='" . $data['cpyear'] . '-' . $data['cpmonth'] . '-' . $data['cpday'] . "',expire_passport='" . $data['epyear'] . '-' . $data['epmonth'] . '-' . $data['epday'] . "',miles=" . $data['miles'] . ",nationality=" . $data['nationality'] . ",miles_program='" . $data['miles_program'] . "'";

        $result = $this->db->query('UPDATE passengers SET ' . $fields . ' where idpassengers=' . $data['idpassengers']);
        if ($result) {
            return true;
        } else {
            return false;
        }
    }

    function deletepassenger($data) {

        $fields = " `active`='" . $data['active'] . "'";

        $result = $this->db->query('UPDATE passengers SET ' . $fields . ' where idpassengers=' . $data['idpassengers']);
        if ($result) {
            return true;
        } else {
            return false;
        }
    }

    function selectpassenger($condi = "TRUE", $orderby = " order by idpassengers desc ", $limit = "", $fields = '*') {
        $query = $this->db->query('select ' . $fields . ' from passengers where ' . $condi . ' ' . $orderby . ' ' . $limit);
        if ($query->num_rows() > 0) {
            return $query;
        }
    }

}
