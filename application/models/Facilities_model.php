<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Facilities_model
 *
 * @author carlos
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Facilities_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->database();
    }

    function insertfacilities($data) {
        $fields = "'" . $data['name'] . "'," . $data['type'] . "," . $data['active'];
        $result = $this->db->query('INSERT INTO `facilities`( `name`, `type`, `active`) VALUES (  ' . $fields . ")");
        if ($result) {
            $result = $this->db->query('select last_insert_id() as last');
            return $result->result()[0]->last;
        } else {
            return false;
        }
    }

    function updatefacilities($data) {
        $fields = "`name`='" . $data['name'] . "',`type`=" . $data['type'] . ",`active`=" . $data['active'];
        $result = $this->db->query('UPDATE facilities SET ' . $fields . ' where idfacilities=' . $data['idfacilities']);
        if ($result) {
            return true;
        } else {
            return false;
        }
    }
    
    function deletefacilities($data) {

        $fields = " `active`='" . $data['active'] . "'";

        $result = $this->db->query('UPDATE facilities SET ' . $fields . ' where idfacilities=' . $data['idfacilities']);
        if ($result) {
            return true;
        } else {
            return false;
        }
    }

    function selectfacilities($condi = "TRUE", $orderby = " order by type asc ", $limit = "", $fields = '*') {
        $query = $this->db->query('select ' . $fields . ' from facilities where ' . $condi . ' ' . $orderby . ' ' . $limit);
        if ($query->num_rows() > 0) {
            return $query;
        } else {
            return false;
        }
    }
    function selectfacilitieshotels($condi = "TRUE", $orderby = " order by type asc ", $limit = "", $fields = '*') {
        $query = $this->db->query('select ' . $fields . ' from hotels_facilities where ' . $condi . ' ' . $orderby . ' ' . $limit);
        if ($query->num_rows() > 0) {
            return $query;
        } else {
            return false;
        }
    }
}
