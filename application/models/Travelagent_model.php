<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Users_model
 *
 * @author carlos
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Travelagent_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->database();
    }

    function inserttravelagent($data) {
        $fields = "('" . $data['name'] . "','" . $data['location'] . "','" . $data['gps'] . "'," ."3" . ",'" . $data['email'] . "','" . $data['phone'] . "','" . md5($data['pwd']) . "','" . $data['city'] . "','" . $data['status'] . "','" . $data['arc_number'] . "'," . $data['zip_code'] . ")";
        $result = $this->db->query('INSERT INTO travel_agent(  name, location, gps, idrole, email, phone, password, id_city,  status, arc_number,  zip_code) VALUES ' . $fields);
        if ($result) {
            $result = $this->db->query('select last_insert_id() as last');
            return $result->result()[0]->last;
        } else {
            return false;
        }
    }

    function updatetravelagent($data) {

        $fields = "name='" . $data['name'] . "',location='" . $data['location'] . "',gps='" . $data['gps'] . "',idrole=" ."3" . ",email='" . $data['email'] . "',phone='" . $data['phone'] . "',password='" . md5($data['pwd']) . "',id_city='" . $data['city'] . "',status='" . $data['status'] . "',arc_number='" . $data['arc_number'] . "',zip_code=" . $data['zip_code'] . "";

        $result = $this->db->query('UPDATE travel_agent SET ' . $fields . ' where idtravel_agent=' . $data['idtravel_agent']);
        if ($result) {
            return true;
        } else {
            return false;
        }
    }

    function deletetravelagent($data) {

        $fields = " `status`='" . $data['status'] . "'";

        $result = $this->db->query('UPDATE travel_agent SET ' . $fields . ' where idtravel_agent=' . $data['idtravel_agent']);
        if ($result) {
            return true;
        } else {
            return false;
        }
    }

    function selecttravelagent($condi = "TRUE", $orderby = " order by idtravel_agent desc ", $limit = "", $fields = '*') {
        $query = $this->db->query('select ' . $fields . ' from travel_agent where ' . $condi . ' ' . $orderby . ' ' . $limit);
        if ($query->num_rows() > 0) {
            return $query;
        }
    }

    public function insert_agent($param){
       
        $array = array(

            'Name' => $param['Name'] ,
            'Web_Site' => $param['Web_Site'],
            'email' =>  $param['email'],
             'phone' => $param['phone'] ,
            'Agency_phone' => $param['Agency_phone'],
            'Slogan_travel' =>   $param['Slogan_travel'],
               'Logo' => $param['Logo']
        );

        $this->db->insert('agent_travel',$array);

        
    }

    function queryExcursion(){
     $query = $this->db->query("SELECT start_date, end_date, pickup, drop_off from excursions INNER JOIN pricing_chart on excursions.idexcursions = pricing_chart.excursions_idexcursions WHERE idexcursions = 1");

     

    }

     function id_update_travel_agent($id) {
    
        $this->db->where('id', $id);
       $query = $this->db->get('agent_travel');

       if($query->num_rows() > 0){
        return $query;
      }else{
        return false;
      }
    }

    function list_agent_trvl(){
      $query =  $this->db->get('agent_travel');

      if($query->num_rows() > 0){
        return $query;
      }else{
        return false;
      }
    }


    
 function list_agent_supp(){
      $query =  $this->db->get('user');

      if($query->num_rows() > 0){
        return $query;
      }else{
        return false;
      }
    }

   function list_reser_hotel(){

       $query =  $this->db->get('hotel');

      if($query->num_rows() > 0){
        return $query;
      }else{
        return false;
      }

   }

 function list_reser_transfer(){
     $query =  $this->db->get('transfers');

      if($query->num_rows() > 0){
        return $query;
      }else{
        return false;
      }


 }

 function list_reser_excursions(){
  $query =  $this->db->get('excursions');

      if($query->num_rows() > 0){
        return $query;
      }else{
        return false;
      }

 }

function on_off_hotel($id, $data){
   $this->db->where('id', $id);
            $this->db->update('hotel', $data);
}

function on_off_transfer($id, $data){
  $this->db->where('idtrasnfers', $id);
            $this->db->update('transfers', $data);

}

function on_off_excursion($id, $data){
  $this->db->where('idexcursions', $id);
            $this->db->update('excursions', $data);

}


 
function on_off_travelsupp($id, $data){
  $this->db->where('idreservations', $id);
            $this->db->update('reservations', $data);

}


      function update_travel_agent($id, $data){
            $this->db->where('id', $id);
            $this->db->update('agent_travel', $data);

      }

}
