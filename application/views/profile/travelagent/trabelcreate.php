<body>
  <div class="row">
   
        
            <!-- Content Header (Page header) -->
<section class="content-header">
<h1 align="center">
        Travel Agency registration
    </h1>                
</section>

            <!-- Main content -->
<section class="content" id="maincontent">

                <!--div class="row">-->
        <div class="col-md-9">
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#Personal_info" data-toggle="tab">Personal Info</a></li>
                    <li><a href="#Business_info" data-toggle="tab">Info of Business</a></li>
                </ul>
                <div class="tab-content">

              
                                
     <form action="insert_agent_travel" method="post" enctype="multipart/form-data" class="form-horizontal">
                                        <div class="form-group">
                                            
<label for="inputName" class="col-sm-2 control-label">
Name of Travel Agency</label>

    <div class="col-sm-10">
<input type="text" name="name" class="form-control" id="" value="<?php echo set_value('name') ?>">

<?php echo form_error('name'); ?>
         </div>
       </div>
 <div class="form-group">
 <label for="inputName" class="col-sm-2 control-label">Web Site</label>

    <div class="col-sm-10">
        <input type="text" name="website" class="form-control" id="" value="<?php echo set_value('website') ?>">
        <?php echo form_error('website'); ?>
    </div>
</div>
     <div class="form-group">
                                            
 <label for="inputName" class="col-sm-2 control-label">Email</label>

<div class="col-sm-10">
                <input type="text" name="email" class="form-control" id="" value="<?php echo set_value('email') ?>">
                <?php echo form_error('email'); ?>
             </div>
         </div>
         <div class="form-group">
                                            
         <label for="inputName" class="col-sm-2 control-label">Phone</label>

             <div class="col-sm-10">
                 <input type="text" name="phone" class="form-control" id="" value="<?php echo set_value('phone') ?>">
                   <?php echo form_error('phone'); ?>
         </div>
        </div>
        <div class="form-group">
                                            
 <label for="inputName" class="col-sm-2 control-label">
Agency phone</label>

 <div class="col-sm-10">
     <input type="text" name="agencyphon" class="form-control" id="" value="<?php echo set_value('agencyphon') ?>">
     <?php echo form_error('agencyphon'); ?>
</div>
</div>

 

        <div class="form-group">
                                            
 <label for="inputName" class="col-sm-2 control-label">
Slogan travel agency</label>

 <div class="col-sm-10">
     <input type="text" name="slogan" class="form-control" id="" value="<?php echo set_value('slogan') ?>">
     <?php echo form_error('slogan'); ?>
</div>
</div>

       <div class="form-group">
                                            
 <label for="inputName" class="col-sm-2 control-label">
Logo</label>

 <div class="col-sm-10">
    
 <input type="file" name="portrait" id="file"  class="file file-loading" data-allowed-file-extensions='["jpg", "png", "jpeg"]' value="<?php echo set_value('portrait') ?>">
 <?php echo form_error('portrait'); ?>
</div>
</div>
                                        
 <div class="form-group">
 <div class="col-sm-offset-2 col-sm-10">
         <div class="checkbox">
         <label>
  <input type="checkbox"> I agree to the <a href="#">terms and conditions</a>
                </label>
             </div>
          </div>
     </div>
     <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                     <input value="Submit" type="submit" class="btn btn-danger">
                </div>
             </div>
         </form>
                               
                                <!-- /.tab-pane -->
</div>
                            <!-- /.tab-content -->
    </div>
                        <!-- /.nav-tabs-custom -->
 </div>
                    <!-- /.col -->
                <!--</div>-->
                <!-- /.row -->

            </section>
            <!-- /.content -->
        </div>
</body>