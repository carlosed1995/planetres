 <body>
  <div class="row">
       
            <!-- Content Header (Page header) -->
<section class="content-header">
<h1 align="center">
        Travel Agency edit
    </h1>                
</section>

            <!-- Main content -->
<section class="content" id="maincontent">

                <!--div class="row">-->
        <div class="col-md-9">
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#Personal_info" data-toggle="tab">Personal Info</a></li>
                    <li><a href="#Business_info" data-toggle="tab">Info of Business</a></li>
                </ul>
                <div class="tab-content">
            
<form action="<?php echo base_url()?>travelagency/Travel_agency_functions/update_agent_trvel/<?php echo $id ?>" class="form-horizontal" method="post" enctype='multipart/form-data'>



<div class="form-group">
<label for="inputName" class="col-sm-2 control-label">
Name of Travel Agency</label>

    <div class="col-sm-10">
<input name="name" type="text" value="<?php echo $Name ?>" class="form-control" id="" value="">

<?php echo form_error('name'); ?>
         </div>
       </div>
      
 <div class="form-group">
 <label for="inputName" class="col-sm-2 control-label">Web Site</label>

    <div class="col-sm-10">
        <input name="website" type="text" value="<?= $Web_Site ?>" class="form-control" id="">
        <?php echo form_error('website'); ?>
    </div>
</div>
     <div class="form-group">
                                            
 <label for="inputName" class="col-sm-2 control-label">Email</label>

<div class="col-sm-10">
                <input name="email" type="email" class="form-control" id="" value="<?= $email?>">
                 <?php echo form_error('email'); ?>
             </div>
         </div>
         <div class="form-group">
                                            
         <label for="inputName" class="col-sm-2 control-label">Phone</label>

             <div class="col-sm-10">
                 <input name="phone" type="text" class="form-control" id="" value="<?= $phone ?>">
                   <?php echo form_error('phone'); ?>
         </div>
        </div>
        <div class="form-group">
                                            
 <label for="inputName" class="col-sm-2 control-label">
Agency phone</label>

 <div class="col-sm-10">
     <input type="text" name="agencyphon" class="form-control" id="" value="<?= $Agency_phone ?>">
       <?php echo form_error('agencyphon'); ?>
</div>
</div>
 <div class="form-group">
                                            
 <label for="inputName" class="col-sm-2 control-label">
Slogan travel agency</label>

 <div class="col-sm-10">
     <input type="text" name="slogan" value="<?= $Slogan_travel ?>" class="form-control" id="" value="">
     <?php echo form_error('slogan'); ?>
</div>
</div>
         <div class="form-group">
                                            
 

 <!--<div class="col-sm-10">-->
     <!--<input type="file" name="logo" id="" value="" class="file file-loading" data-allowed-file-extensions='["jpg", "png", "jpeg"]'>
     -->
<!--</div>-->
</div>

<div align="center">Logo</div>
<div align="center">
<img src="../../../web/images/profiles/<?= $Logo ?>" class="img img-responsive" style="width: 100px;height: 100px;">
</div>



     <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                     <button type="submit" class="btn btn-danger">Submit</button>
                </div>
             </div>
         </form>
                               
                                <!-- /.tab-pane -->
</div>
                            <!-- /.tab-content -->
    </div>
                        <!-- /.nav-tabs-custom -->
 </div>
                    <!-- /.col -->
                <!--</div>-->
                <!-- /.row -->

            </section>
            <!-- /.content -->
        </div>
</body>

    
