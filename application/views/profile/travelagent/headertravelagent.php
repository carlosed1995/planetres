<!doctype html>

<html>
<head>
<meta charset="utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Planetres</title>

<link href="https://fonts.googleapis.com/css?family=Comfortaa" rel="stylesheet">
<link href="<?=base_url()?>web/css/styles.css" rel="stylesheet" type="text/css">
<link href="<?=base_url()?>web/css/backend.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.0.47/jquery.fancybox.min.css" />
<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>  
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.0.47/jquery.fancybox.min.js"></script>
  <script src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>   
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>  
  <script src="<?=base_url()?>web/js/sortable.min.js" type="text/javascript"></script>
<script src="<?=base_url()?>web/js/purify.min.js" type="text/javascript"></script>
<script src="<?=base_url()?>web/js/fileinput.min.js"></script>  
<script src="<?=base_url()?>web/js/ckeditor.js"></script>
<link href="<?=base_url()?>web/css/fileinput.min.css" media="all" rel="stylesheet" type="text/css" />
  <link href="<?=base_url()?>web/css/bootstrap-chosen.css" media="all" rel="stylesheet" type="text/css" />
    <link href="<?=base_url()?>web/css/multiple.css" rel="stylesheet" type="text/css" />
     
<script src="<?=base_url()?>web/js/piexif.min.js" type="text/javascript"></script>
 <script src="http://harvesthq.github.io/chosen/chosen.jquery.js"></script>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.5/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.5/css/bootstrap-theme.min.css">
    <link rel="stylesheet" href="<?=base_url()?>web/css/bootstrap-tagsinput.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/rainbow/1.2.0/themes/github.css">
    <!--<link rel="stylesheet" href="/../public/assets/app.css"> -->
          
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
        <!-- Ionicons -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
        <!-- Theme style -->
        <link rel="stylesheet" href="<?= base_url(); ?>web/dist/css/AdminLTE.min.css">
        <!-- AdminLTE Skins. Choose a skin from the css/skins
             folder instead of downloading all of them to reduce the load. -->
        <link rel="stylesheet" href="<?= base_url(); ?>web/dist/css/skins/_all-skins.min.css">
        <!-- iCheck -->
        <link rel="stylesheet" href="<?= base_url(); ?>web/plugins/iCheck/flat/blue.css">
        <!-- Morris chart -->
        <link rel="stylesheet" href="<?= base_url(); ?>web/plugins/morris/morris.css">
        <!-- jvectormap -->
        <link rel="stylesheet" href="<?= base_url(); ?>web/plugins/jvectormap/jquery-jvectormap-1.2.2.css">
        <!--   Date Picker -->
        <link rel="stylesheet" href="<?= base_url(); ?>web/plugins/datepicker/datepicker3.css">
        <!--    Daterange picker -->
        <link rel="stylesheet" href="<?= base_url(); ?>web/plugins/daterangepicker/daterangepicker.css">
        <!-- bootstrap wysihtml5 - text editor -->
        <link rel="stylesheet" href="<?= base_url(); ?>web/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">      
        

    
 
   
  <link rel="stylesheet" href="<?=base_url()?>web/css/bootstrap.min.css">
  <link rel="stylesheet" href="<?=base_url()?>web/css/bootstrap.css">
  <script src="<?=base_url()?>web/js/bootstrap.js"></script>
  <link rel="stylesheet" href="<?=base_url()?>web/css/sb-admin-2.css">

  <script type="text/javascript" src="<?=base_url()?>web/js/jquery.min.js"></script>
  <script type="text/javascript" src="<?=base_url()?>web/js/moment.min.js"></script>
  <script type="text/javascript" src="<?=base_url()?>web/js/bootstrap.min.js"></script>
  <script type="text/javascript" src="<?=base_url()?>web/js/bootstrap-datetimepicker.min.js"></script>
  <link rel="stylesheet" href="<?=base_url()?>web/css/bootstrap-datetimepicker.min.css" />
  
  <script src="http://harvesthq.github.io/chosen/chosen.jquery.js"></script>
  <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css" rel='stylesheet' type='text/css'>
  <link href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700" rel='stylesheet' type='text/css'>
  <link href="<?=base_url()?>web/css/metisMenu.min.css" rel="stylesheet" type="text/css" media="all" />
</head>
<div id="page_client">
  <div id="wrapper">  
    <div class="header-bg">
      <div class="row">
        <div id="logo_client" class="col-xs-12 col-lg-12 col-md-12 col-sm-12">
          <a href="<?=base_url();?>"><img src="<?=base_url();?>web/images/logo.png" alt="Planetres"></a>
        </div>
      </div>
      <div class="header-in_client">
        <div class="header-1r_client">          
          <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
              <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                  <span class="sr-only">Toggle navigation</span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#">You are a Client</a>
              </div>

              <!-- Collect the nav links, forms, and other content for toggling -->
              <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                  <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Planetres <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                      <li><a href="#">Services</a></li>
                      <li role="separator" class="divider"></li>
                      <li><a href="#">Especials</a></li>
                      <li role="separator" class="divider"></li>
                      <li><a href="#">Contact Us</a></li>                      
                    </ul>
                  </li>
                  <li><a href="#">Reservations</a></li>
                  <li><a href="#">Destinations</a></li>
                  <li>
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                    <span class="caret"></span>Agent travel
                    </a>
                    <ul class="dropdown-menu">
                        <li><a href="<?=base_url('travelagency/Travel_agency_functions/profile_travel_agent_create') ?>">Create</a></li>
                        <li role="separator" class="divider"></li>
                        <li><a href="<?=base_url('travelagency/Travel_agency_functions/profile_travel_agent_list') ?>">Search</a></li>
                        <li role="separator" class="divider"></li>
                        
                    </ul> 
                  </li>
                </ul>  
                <ul class="nav navbar-nav navbar-right">
                  <li><a href="#"><span class="glyphicon glyphicon-shopping-cart" aria-hidden="true"></span></a></li>
                  <li class="active"><a href="<?=base_url('Planetres')?>"><span class="glyphicon glyphicon-home" aria-hidden="true"></span></a></li>
                  <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><span class="glyphicon glyphicon-align-justify" aria-hidden="true"></span><span class="glyphicon glyphicon-menu-down" aria-hidden="true"></span></a>
                    <ul class="dropdown-menu">
                      <li><a href="<?=base_url('travelagency/Profile
                      ')?>"><span class="glyphicon glyphicon-user" aria-hidden="true"></span>Profile</a></li>
                      <li role="separator" class="divider"></li>
                      <li><a href="<?=base_url('travelagency/Config')?>"><span class="glyphicon glyphicon-cog" aria-hidden="true"></span> Settings</a></li>
                      <li role="separator" class="divider"></li>
                      <li><a href="<?=base_url('Login/destroySession')?>"><span class="glyphicon glyphicon-log-out" aria-hidden="true"></span> Logout</a></li>                  
                    </ul>
                  </li>
                </ul>
              </div><!-- /.navbar-collapse -->
            </div><!-- /.container-fluid -->
          </nav>
        </div>
      </div>
    </div>

     

