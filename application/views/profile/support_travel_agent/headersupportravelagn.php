<!doctype html>

<html>
<head>
<meta charset="utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Planetres</title>

<link href="https://fonts.googleapis.com/css?family=Comfortaa" rel="stylesheet">
<link href="<?=base_url()?>web/css/styles.css" rel="stylesheet" type="text/css">
<link href="<?=base_url()?>web/css/backend.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.0.47/jquery.fancybox.min.css" />
<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>  
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.0.47/jquery.fancybox.min.js"></script>
<script src="<?=base_url()?>web/js/ckeditor.js"></script>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.5/css/bootstrap.min.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.5/css/bootstrap-theme.min.css">
  <link rel="stylesheet" href="<?= base_url(); ?>web/dist/css/AdminLTE.min.css">
    
  <link rel="stylesheet" href="<?=base_url()?>web/css/bootstrap.min.css">
  <link rel="stylesheet" href="<?=base_url()?>web/css/bootstrap.css">
  <script src="<?=base_url()?>web/js/bootstrap.js"></script>
  
</head>

<div id="page_client">
  <div id="wrapper">  
    <div class="header-bg">
      <div class="row">
        <div id="logo_client" class="col-xs-12 col-lg-12 col-md-12 col-sm-12">
          <a href="<?=base_url();?>"><img src="<?=base_url();?>web/images/logo.png" alt="Planetres"></a>
        </div>
      </div>
      <div class="header-in_client">
        <div class="header-1r_client">          
          <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
              <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                  <span class="sr-only">Toggle navigation</span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#">Welcome <?=$this->session->userdata('name');?></a>
              </div>
          
              <!-- Collect the nav links, forms, and other content for toggling -->
              <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                  <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Planetres <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                      <li><a href="#">Services</a></li>
                      <li role="separator" class="divider"></li>
                      <li><a href="#">Especials</a></li>
                      <li role="separator" class="divider"></li>
                      <li><a href="#">Contact Us</a></li>                      
                    </ul>
                  </li>
                  <li class="dropdown">
                  	<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Functions<span class="caret"></span></a>
                  	<ul class="dropdown-menu">

                   
                      <li><a href="#">Create a new support Agent travel</a></li>

                      <li role="separator" class="divider"></li>
                      <li><a href="<?=base_url('travelagency_support/Travel_agency_support_functions/reservations_travel_support_agency')?>">View all reservations of the clients</a></li>
                      <li role="separator" class="divider"></li>
                      <li><a href="#">Assist travel agent with any reservations issue</a></li>                      
                    </ul>
                  </li>
                </ul>      
                <ul class="nav navbar-nav navbar-right">
                  <li><a href="<?=base_url('Planetres')?>"><span class="glyphicon glyphicon-home" aria-hidden="true"></span></a></li>
                  <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><span class="glyphicon glyphicon-align-justify" aria-hidden="true"></span><span class="glyphicon glyphicon-menu-down" aria-hidden="true"></span></a>
                    <ul class="dropdown-menu">

                  <li><a href="<?=base_url('travelagency_support/Profile')?>" aria-hidden="true"></span>Profiles</a></li>


                      <li role="separator" class="divider"></li>
                      <li><a href="<?=base_url('travelagency_support/Config')?>"><span class="glyphicon glyphicon-cog" aria-hidden="true"></span> Settings</a></li>
                      <li role="separator" class="divider"></li>
                      <li><a href="<?=base_url('Login/destroySession')?>"><span class="glyphicon glyphicon-log-out" aria-hidden="true"></span> Logout</a></li>                  
                    </ul>
                  </li>
                </ul>
              </div><!-- /.navbar-collapse -->
            </div><!-- /.container-fluid -->
          </nav>
        </div>
      </div>
    </div>