<?
	$attributes = array(
      'class' => 'control-label col-sm-2'
    );
?>
<link rel="stylesheet" href="<?=base_url('web/css/lightslider.css');?>"/>
<script src="<?=base_url('web/js/lightslider.js');?>"></script> 

<script type="text/javascript">
$(document).ready(function() {
$("#tabs").tabs();

$('#image-gallery').lightSlider({
	gallery:true,
	item:1,
	//thumbItem:9,
	slideMargin: 0,
	speed:1000,
	pause: 4500,
	auto:true,
	loop:true,
	onSliderLoad: function() {
		$('#image-gallery').removeClass('cS-hidden');
	}  
});

$("a.login").fancybox({
	iframe : {
		css : {
			'width': '460px',
			'height': '340px',
			'max-width': '90%',
			'max-height': '90%',	
			'margin': '0'
		}
	}
	});

$("a.register").fancybox({
	iframe : {
		css : {
			'width': '460px',
			'height': '570px',
			'max-width': '90%',
			'max-height': '90%',	
			'margin': '0'
		}
	}
	});

$("a.form").fancybox({
	iframe : {
		css : {
			'width': '460px',
			'height': '570px',
			'max-width': '90%',
			'max-height': '90%',	
			'margin': '0'
		}
	}
	});
	
});
	$(function() {
    $("#chk-in,#chk-out").datepicker({
        maxDate: new Date(2017,12,31), 
        changeYear: true, 
        changeMonth: true,
        minDate: new Date(),
        dateFormat: 'yy-mm-dd',
        constrainInput: true});    
      var start = "<?=$_GET['start_transfer'] ?>";
      var end = "<?=$_GET['end_transfer'] ?>";    
      if(start != "" && end != ""){
       $("#chk-in").datepicker('setDate',start);
       $("#chk-out").datepicker('setDate',end);     
      }
  });
</script>
</head>

<body>
<div id="page_details_transfers">
<section id="content">

<div class="head-details h-detail-l">
<div class="a-half">
<h1 class="title-details">
	<?=$name_transfers?>
	<br>&nbsp;	
 </h1>
<a href="<?=base_url('Planetres')?>"><strong>&laquo;</strong> Back to Results</a>

</div>
</div>

<div id="tabs">
  <ul>
  	<li><a href="#tabs-0">Description</a></li>
  	<li><a href="#tabs-1">Go to reservations</a></li>
  </ul>
<div id="tabs-0">
	<h1 class="tit3">Description</h1>
	<p style="margin-top:20px; margin-bottom:0;">
		<?=$description_transfers?>
	</p>
</div>
<div id="tabs-1">
	<form class="form-horizontal" id="formulario">
	<div id="change" class="row">
	  <h1 class="tit3">Persons Availability and Prices</h1>
	  <p style="margin-top:20px; margin-bottom:0;">
	  	
	  	<div class="form-group">
		    <?=form_label('Arrival transfers','chk-in',$attributes)?>
		    <div class="col-sm-10">
		    	<input name="chk-in" type="text" class="field2b rounded" id="chk-in" placeholder="Check-in *"> 
		   	</div>
	    </div>
	    </br>
	    <?=$arrival_transfers?>
	    <div class="form-group">
	    	<?=form_label('Departure transfers','chk-out',$attributes)?>
	    	<div class="col-sm-10">
	    		<input type="text" name="chk-out" id="chk-out" class="field2b rounded" placeholder="Check-out *">
	    	</div>
	    </div>
	    </br>
	    <?=$departure_transfers?>
	  </p>
	<p><button type="button" name="submit" id="submit" value="add to cart" class="field4 rounded">Add to cart</button></p>
	</div>
	</form>
</div>

<script>
    $( function() {
    	var count = '<?=$_GET["guests_transfer"]?>';
    	$( ".hr_arrival,.min_arrival,.am_pm_arrival" ).change(function() {  
    		for(var i=1; i<=count;i++){
	    		var hr = $('#hr_arrival'+i).val();   
	    		var min = $('#min_arrival'+i).val();
	    		var am_pm = $('#am_pm_arrival'+i).val();
	        	if(hr != ''  && min != '' && am_pm != ''){
	        		$("#active_arrival"+i).prop("checked", true);  // para poner la marca
	        		$("#active_arrival"+i).removeAttr("disabled");
	        	}else if(hr == '' || min == '' || am_pm == ''){
	        		$("#active_arrival"+i).attr("disabled", true);
	        		$("#active_arrival"+i).prop("checked", false);
	        	}
        	}
      	});
  	});

  	$( function() {
  		var count = '<?=$count?>';
    	$( ".hr_departure,.min_departure,.am_pm_departure" ).change(function() {  
    		for(var i=1; i<=count;i++){
	    		var hr = $('#hr_departure'+i).val();   
	    		var min = $('#min_departure'+i).val();
	    		var am_pm = $('#am_pm_departure'+i).val();
	        	if(hr != ''  && min != '' && am_pm != ''){
	        		$("#active_departure"+i).prop("checked", true);  // para poner la marca
	        		$("#active_departure"+i).removeAttr("disabled");
	        	}else if(hr == '' || min == '' || am_pm == ''){
	        		$("#active_departure"+i).attr("disabled", true);
	        		$("#active_departure"+i).prop("checked", false);
	        	}
        	}
      	});
  	});
   

       $(document).ready(function(){
         $("#submit").click(function(){

                 $.post("<?=base_url('support_clientPlanetres/form_transfers_result')?>", $("#formulario").serialize(), 
                 function(htmlexterno){
                    $("#change").html(htmlexterno);

          });
        });
      });

</script>

</section>
</div>
</body>
</html>
