<link rel="stylesheet" href="<?=base_url('web/css/lightslider.css');?>"/>
<script src="<?=base_url('web/js/lightslider.js');?>"></script> 

<script type="text/javascript">
$(document).ready(function() {
$("#tabs").tabs();

$('#image-gallery').lightSlider({
	gallery:true,
	item:1,
	//thumbItem:9,
	slideMargin: 0,
	speed:1000,
	pause: 4500,
	auto:true,
	loop:true,
	onSliderLoad: function() {
		$('#image-gallery').removeClass('cS-hidden');
	}  
});

$("a.login").fancybox({
	iframe : {
		css : {
			'width': '460px',
			'height': '340px',
			'max-width': '90%',
			'max-height': '90%',	
			'margin': '0'
		}
	}
	});

$("a.register").fancybox({
	iframe : {
		css : {
			'width': '460px',
			'height': '570px',
			'max-width': '90%',
			'max-height': '90%',	
			'margin': '0'
		}
	}
	});

$("a.form").fancybox({
	iframe : {
		css : {
			'width': '460px',
			'height': '570px',
			'max-width': '90%',
			'max-height': '90%',	
			'margin': '0'
		}
	}
	});
	
});
</script>
<script>

  $(function() {
    $("#chk-in,#chk-out").datepicker({
        maxDate: new Date(2017,12,31), 
        changeYear: true, 
        changeMonth: true,
        minDate: new Date(),
        dateFormat: 'yy-mm-dd',
        constrainInput: true});    
      var start = "<?=$_GET['start_hotel'] ?>";
      var end = "<?=$_GET['end_hotel'] ?>";    
      if(start != "" && end != ""){
       $("#chk-in").datepicker('setDate',start);
       $("#chk-out").datepicker('setDate',end);     
      }
  });

  $(document).ready(function() {
    var currentItem = 2;
     $('#guests_hotel').on('change', function() {
      var optionSelected = $(this).find("option:selected");
      var valueSelected  = optionSelected.val();
      if (valueSelected == 3 || valueSelected == 4) {
        $("#button_hoteles").fadeOut('fast');
        $("#xtra-form").fadeIn('slow');  //.toggle('slow')
        $('#guests_hotel').removeClass('field3');
        $('#guests_hotel').addClass('field1');
       
      }else {                          
        $("#xtra-form").fadeOut('fast');        
        $('#guests_hotel').removeClass('field1');
        $('#guests_hotel').addClass('field3');
        $("#button_hoteles").fadeIn('slow');        
      }
    });

    $('#addnew').click(function addroom(){
     
     if(currentItem <= 4){
     $('#items').val(currentItem);
      var strToAdd = '<div class="rowfields" id="rowf'+currentItem+'">Room <input id="roomcount'+currentItem+'" type="hidden" name="roomcount'+currentItem+'" value="1">'+currentItem+' &nbsp;<select name="adult" id="adult'+currentItem+'" class="field2c rounded"><option value="1">1</option><option value="2">2</option><option value="3">3</option><option value="4">4</option></select><select name="kids" id="kids'+currentItem+'" class="field2c rounded"><option value="0">0</option><option value="1">1</option><option value="2">2</option><option value="3">3</option><option value="4">4</option></select></div>';
        currentItem++;
    }
    $('#pa-grid').append(strToAdd);
       //return false;
    });

    $('#removei').click(function() {
      if(currentItem > 2){
        $('.rowfields:last').remove();
        currentItem--; 
      }
    });
});
</script>
</head>

<body>
<div id="page_details_hotels">
<section id="content">

<div class="head-details h-detail-l">
<div class="a-half">
<h1 class="title-details">
	<?=$name_hotel?>
	<br>&nbsp;
	<?php  
		for ($i=0; $i < $count; ++$i){
	?>
			<img src="<?=base_url();?>web/images/icon-star.png" width="12" height="12">
		
		<?php } ?>
 </h1>
<a href="<?=base_url('Planetres')?>"><strong>&laquo;</strong> Back to Results</a>
<script>


</script>
</div>
</div>

<div id="tabs">
  <ul>
  	<li><a href="#tabs-0">Gallery</a></li>
    <li><a href="#tabs-1">Description</a></li>
    <li><a href="#tabs-2">Facilities</a></li>
    <li><a href="#tabs-3">Locality</a></li>
    <li><a href="#tabs-4">Map</a></li>
    <li><a href="#tabs-5">Make a reservation</a></li>
  </ul>

<div id="tabs-0">
	<?=$images_hotels?>
</div>

<div id="tabs-1">
<p><?=$description?></p>
</div>
  
<div id="tabs-2">
<?=$facilities?>
</div>

<div id="tabs-3">
<p><?=$area_information?></p>
</div>
  

<div id="tabs-4">
<?=$map?>
</div>

<div id="tabs-5">
	<h1 class="tit3">Room Availability and Prices</h1>
  
	<form name="formulario" id="formulario">
  		
  			<p style="margin-top:20px; margin-bottom:0;">
    			<input type="hidden" value="<?=$idhotel?>" name="idhotel" id="idhotel">
    			<input name="chk-in" type="text" class="field2b rounded" id="chk-in" placeholder="Check-in *"> 
    			<input type="text" name="chk-out" id="chk-out" class="field2b rounded" placeholder="Check-out *">
  			</p>
    		<p>
    			<strong style="color:#000;">Nights:</strong><input name="nights" type="text" class="field2d rounded" id="nights" value="<?=$_SESSION['nights']?>" readonly>
  			</p> 
  			<?=$type_room?>
			<div id="xtra-form">
				<div class="add-rows-nav"> 
   					&nbsp; - &nbsp; 
  					<a href="javascript:;" id="addnew" class="rounded"><strong style="color: #090;">+ </strong> Add Room</a> <a href="javascript:;" id="removei" class="rounded"><strong style="color: #C00;">x </strong> Delete Room</a></div>
  					<div id="pa-grid">
    					<div class="rowfields">
    						Room <input id="roomcount1" type="hidden" name="roomcount1" value="1">1 &nbsp;    
    						<select name="adult1" id="adult1" class="field2c rounded">
      							<option>Adults (12+)</option>
      							<option value="1" <? if($_GET['adult1'] == 1){ echo "selected"; } ?> >1</option>
      							<option value="2" <? if($_GET['adult2'] == 2){ echo "selected"; } ?> >2</option>
      							<option value="3" <? if($_GET['adult3'] == 3){ echo "selected"; } ?> >3</option>
      							<option value="4" <? if($_GET['adult4'] == 4){ echo "selected"; } ?> >4</option>
    						</select>   
    						&nbsp;
    						<select name="kids1" id="kids1" class="field2c rounded">
	      						<option>Kids (0-11)</option>
	      						<option value="1" <? if($_GET['kids1'] == 1){ echo "selected"; } ?> >1</option>
	      						<option value="2" <? if($_GET['kids2'] == 2){ echo "selected"; } ?> >2</option>
	      						<option value="3" <? if($_GET['kids3'] == 3){ echo "selected"; } ?> >3</option>
	      						<option value="4" <? if($_GET['kids4'] == 4){ echo "selected"; } ?> >4</option>
    						</select>
  						</div>
					</div>
			</div>
			<p>
				<button type="button" name="submit" id="submit" value="Check Availability" class="field4 rounded">Check Availability</button>
			</p>
		</form>
	
		<form name="formulario1" id="formulario1">
			<div id="change">
				
			</div>
		</form>
</div>

</div>

</section>
</div>
<script>

		$( function() {
		    $( "#guests_hotel,#kids1,#kids2,#kids3,#kids4,#adult1,#adult2,#adult3,#adult4" ).change(function() {     
		         $.ajax({               
		                  url: ' <?=base_url('support_clientPlanetres/destroySession')?>',
		                  type: "POST",
		                  success: function (datos)
		                  {
		                      console.log("");
		                  }
		              });
		      });
		});
   
   		$( function() {
   			var count = '<?=$count_room?>';
	    	$( "#chk-in,#chk-out" ).change(function() {  
	    			var chkin 	= $('#chk-in').val();
	    			var chkout 	= $('#chk-out').val();
		            $.post('<?=base_url('support_clientPlanetres/change_count_nights')?>', {chkin:chkin, chkout:chkout}, function (data) {
		            	$('#nights').val($.trim(data));
		            	for(var i=1;i<=count;i++){
		            		$('#nights_td'+i).val(data);
		            	}
		        });
	        	
	  		});
  		});

       $(document).ready(function(){
         $("#submit").click(function(){
                 $.post("<?=base_url('support_clientPlanetres/form_hotel_result')?>", $("#formulario").serialize(), 
                 function(htmlexterno){
                    $("#change").html(htmlexterno);
          });
        });
      });

        
    
  </script>
</body>

</html>
