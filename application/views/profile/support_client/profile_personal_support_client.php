<?
  foreach ($user_information->result() as $key) {
      $name=$key->name;
      $middle_name=$key->middle_name;
      $last_name=$key->last_name;
      $birth=$key->birth;
      $genere=$key->genere;
      $phone=$key->phone;
      $email=$key->email;
  }
?>
<body>
  <div class="row">
    <div class="col-md-3">
    <table class="table">
      <div class="navbar-collapse collapse sidebar-navbar-collapse">
      <ul class="nav navbar-nav" id="sidenav01">
        <li>
          <h2 align="center">Menu<br></h2>
        </li>
        <li>
          <a href="#" data-toggle="collapse" data-target="#toggleDemo" data-parent="#sidenav01" class="collapsed">
          <span class="glyphicon glyphicon-user" aria-hidden="true"></span> Personal Info <span class="glyphicon glyphicon-menu-down" aria-hidden="true">
          </a>
          <div class="collapse" id="toggleDemo" style="height: 0px;">
            <ul class="nav nav-list">
              <li><a href="<?=base_url('support_client/Profile')?>">View</a></li>
              <li><a href="<?=base_url('support_client/Profile/edit_profile_personal_support_client')?>">Edit</a></li>              
            </ul>
          </div>
        </li>
        
      </ul>
      </div>
    </table>
    </div>
    
        
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <h1 align="center">
                    Personal Info
                </h1>                
            </section>

            <!-- Main content -->
            <section class="content" id="maincontent">

                <!--div class="row">-->
                    <div class="col-md-9">
                        <form clas="form-horizontal">
                          <div class="form-group">
                              <label class="control-label col-sm-2" for="name">Name</label> 
                              <div class="col-sm-2">
                                <input type="text" class="form-control" id="name"  value="<?=$name?>" name="name" placeholder="<?=$name?>" disabled>
                              </div>
                              <label class="control-label col-sm-2" for="middle_name">MiddleName</label>
                              <div class="col-sm-2">
                                <input type="text" class="form-control" id="middle_name"  value="<?=$middle_name?>" name="middle_name" placeholder="<?=$middle_name?>" disabled>
                              </div>
                              <label class="control-label col-sm-2" for="lastname">LastName</label>
                              <div class="col-sm-2">
                                <input type="text" class="form-control" id="lastname"  value="<?=$last_name?>" name="lastname" placeholder="<?=$last_name?>" disabled>
                              </div>
                          </div>
                          <div class="form-group">
                              <label class="control-label col-sm-2" for="birthday">Birthday</label> 
                              <div class="col-sm-4">
                                <input type="text" class="form-control" id="birthday"  value="<?=$birth?>" name="birthday" placeholder="<?=$birth?>" disabled>
                              </div>
                          </div>
                          <div class="form-group">
                              <label class="control-label col-sm-2" for="genere">Genere</label> 
                              <div class="col-sm-4">
                                <input type="text" class="form-control" id="genere"  value="<?=$genere?>" name="genere" placeholder="<?=$genere?>" disabled>
                              </div>
                          </div>
                          <div class="form-group">
                              <label class="control-label col-sm-2" for="phone">Phone</label> 
                              <div class="col-sm-4">
                                <input type="text" class="form-control" id="phone"  value="<?=$phone?>" name="phone" placeholder="<?=$phone?>" disabled>
                              </div>
                          </div>
                          <div class="form-group">
                              <label class="control-label col-sm-2" for="email">Email</label> 
                              <div class="col-sm-4">
                                <input type="text" class="form-control" id="email"  value="<?=$email?>" name="email" placeholder="<?=$email?>" disabled>
                              </div>
                          </div>
                        </form>
                    </div>
                    <!-- /.col -->
                <!--</div>-->
                <!-- /.row -->

            </section>
            <!-- /.content -->
        </div>
</body>

    