<?
$submit1 = array(
                'value'=> 'submit',
                'name' => 'submit1',
                'id' => 'submit1',
                'class' => 'btn btn-block btn-success'
            );
$hidden = array(
        'type'  => 'hidden',
        'name'  => 'iduser',
        'value' => ($iduser != '') ? $iduser : '',
  );

$hidden_role = array(
        'type'  => 'hidden',
        'name'  => 'role',
        'value' => ($role != '') ? $role : '',
  );

$hidden_city = array(
        'type'  => 'hidden',
        'name'  => 'city',
        'value' => $city,
  );

$attributes = array(
      'class' => 'control-label col-sm-2'
  );

$name = array(
        'name'          => 'name',
        'id'            => 'name',
        'value'         => $name,
        'class'         => 'form-control',
        'placeholder'   => 'Insert Name'
    );
$middle_name = array(
        'name'          => 'middle_name',
        'id'            => 'middle_name',
        'value'         => $middle_name,
        'class'         => 'form-control',
        'placeholder'   => 'Insert Middle Name'
    );
$lastname = array(
        'name'          => 'lastname',
        'id'            => 'lastname',
        'value'         => $lastname,
        'class'         => 'form-control',
        'placeholder'   => 'Insert Last Name'
    );

$phone = array(
        'name'          => 'phone',
        'id'            => 'phone',
        'value'         => $phone,
        'class'         => 'form-control',
        'placeholder'   => 'Insert phone'
    );

$email = array(
        'type'          => 'email',
        'name'          => 'email',
        'id'            => 'email',
        'value'         => $email,
        'class'         => 'form-control',
        'placeholder'   => 'Insert email@email.com'
    );

$confirm_email = array(
        'type'          => 'email',
        'name'          => 'confirm_email',
        'id'            => 'confirm_email',
        'value'         => $confirm_email,
        'class'         => 'form-control',
        'placeholder'   => 'Confirm email@email.com'
    );

$pwd = array(
        'type'          => 'password',
        'name'          => 'pwd',
        'id'            => 'pwd',
        'value'         => $pwd,
        'class'         => 'form-control',
        'placeholder'   => 'Insert Password'
    );

$pwd1 = array(
        'name'          => 'pwd1',
        'id'            => 'pwd1',
        'class'         => 'form-control',
        'placeholder'   => 'Confirm Password'
    );
$hidden_lat = array(
        'type'  => 'hidden',
        'name'  => 'gps-lat',
        'id'    => 'gps-lat',
        'value' => $hidden_lat
    );
$hidden_long = array(
        'type'  => 'hidden',
        'name'  => 'gps-long',
        'id'    => 'gps-long',
        'value' => $hidden_long
    );

?>
<head>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maps.googleapis.com/maps/api/js?libraries=places&key=AIzaSyDPZ77RZzHOf4g5hmREiu0pAA9Kdse2BUI"></script>
    <script src="<?=base_url();?>web/js/jquery.mask.min.js"></script>
    <script>
        function initialize() {
        var select = document.getElementById("nationality");
        var address = select.options[select.selectedIndex].value;

        //var address = loc;
        geocoder = new google.maps.Geocoder();
        geocoder.geocode({
        'address': address
        }, function(results, status) {      
            var lat=document.getElementById("gps-lat").value=results[0].geometry.location.lat();    
            var lng=document.getElementById("gps-long").value=results[0].geometry.location.lng();        
        });
        }
        google.maps.event.addDomListener(window, 'load', initialize);
    </script>
<body>
        
	<div class="row">
    <div class="col-md-3">
        <div class="navbar-collapse collapse sidebar-navbar-collapse">
      <ul class="nav navbar-nav" id="sidenav01">
        <li>
          <h2 align="center">Menu<br></h2>
        </li>
        <li>
          <a href="#" data-toggle="collapse" data-target="#toggleDemo" data-parent="#sidenav01" class="collapsed">
          <span class="glyphicon glyphicon-user" aria-hidden="true"></span> Support Client <span class="glyphicon glyphicon-menu-down" aria-hidden="true"></span>
          </a>
          <div class="collapse" id="toggleDemo" style="height: 0px;">
            <ul class="nav nav-list">
              <li><a href="<?=base_url('support_client/Functions_support_client/new_support_client')?>">New support client</a></li>
            </ul>
          </div>
        </li>

      </ul>
      </div>
    </div>
            <!-- Main content -->
            
            <section class="content" id="maincontent">

                
                    <div class="col-md-9">
                        <?php if ($this->session->flashdata('success')) { ?>
                        <div class="alert alert-success">
                            <a href="#" class="close" data-dismiss="alert">&times;</a>
                            <strong>Success!</strong> <?php echo $this->session->flashdata('success'); ?>
                        </div>

                    <?php } else if ($this->session->flashdata('error')) { ?>

                        <div class="alert alert-danger">
                            <a href="#" class="close" data-dismiss="alert">&times;</a>
                            <strong>Error!</strong> <?php echo $this->session->flashdata('error'); ?>
                        </div>

                    <?php } ?>
                        <h1 align="center">
                            Create a new support client 
                        </h1>  
                        <hr>  
                        <?=form_open(base_url('support_client/Functions_support_client/validate'),'class="form-horizontal" id="formulario1"')?>
                        <?=form_input($hidden);?>
                        <?=form_input($hidden_lat)?>
                        <?=form_input($hidden_long)?>
                        <?=form_input($hidden_role);?>
                        <div class="form-group">
                            <?=form_label('Name', 'name', $attributes);?>
                            <div class="col-sm-10">
                                <?=form_input($name)?>
                                <?php echo form_error('name', '<div class="text-danger text-left">', '</div>'); ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <?=form_label('Middle Name', 'middle_name', $attributes);?>
                            <div class="col-sm-10">
                                <?=form_input($middle_name)?>
                                <?php echo form_error('middlename', '<div class="text-danger text-left">', '</div>'); ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <?=form_label('Last Name', 'lastname', $attributes);?>
                            <div class="col-sm-10">
                                <?=form_input($lastname)?>
                                <?php echo form_error('lastname', '<div class="text-danger text-left">', '</div>'); ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <?=form_label('Phone', 'phone', $attributes);?>
                            <div class="col-sm-10">
                                <?=form_input($phone)?>
                                <?php echo form_error('phone', '<div class="text-danger text-left">', '</div>'); ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <?=form_label('Email', 'email', $attributes)?>
                            <div class="col-sm-10">
                                <?=form_input($email)?>
                                <?php echo form_error('email', '<div class="text-danger text-left">', '</div>'); ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <?=form_label('Confirm Email', 'confirm_email', $attributes)?>
                            <div class="col-sm-10">
                                <?=form_input($confirm_email)?>
                                <?php echo form_error('confirm_email', '<div class="text-danger text-left">', '</div>'); ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <?=form_label('Password', 'pwd', $attributes)?>
                            <div class="col-sm-10"> 
                                <?=form_input($pwd)?>
                                <?php echo form_error('pwd', '<div class="text-danger text-left">', '</div>'); ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <?=form_label('Confirm password', 'confirm_password', $attributes)?>
                            <div class="col-sm-10"> 
                                <?=form_input($pwd1)?>
                                <?php echo form_error('pwd1', '<div class="text-danger text-left">', '</div>'); ?>
                                <?php echo form_error('errorpass', '<div class="text-danger text-left">', '</div>'); ?>                              
                            </div>
                        </div>
                        <div class="form-group">
                            <?=form_label('Country', 'country', $attributes)?>
                            <div class= "col-sm-10">
                                <?= $nationality ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <?=form_label('City', 'city_idcity', $attributes);?>
                            <div class="col-sm-10"> 
                                <?=$city_idcity?>
                                <?php echo form_error('city', '<div class="text-danger text-left">', '</div>'); ?>
                            </div>
                        </div>
                        <div class="form-inline text-left">
                            <label class="control-label col-sm-2" for="birth">Birthdate:</label>
                            <div class="">                            
                                <div class="col-sm-10 form-group">
                                    <?= $year . $month . $day; ?>
                                    <?php echo form_error('birth', '<div class="text-danger text-left">', '</div>'); ?>
                                    <?php echo form_error('year', '<div class="text-danger text-left">', '</div>'); ?>
                                    <?php echo form_error('month', '<div class="text-danger text-left">', '</div>'); ?>
                                    <?php echo form_error('day', '<div class="text-danger text-left">', '</div>'); ?>
                                </div>
                            </div>                
                        </div>
                        <div class="form-group"></div>
                        <div class="form-group"> 
                            <div class="col-sm-4 text-right"></div>
                            <div class="col-sm-2 text-left">
                                <?=form_submit($submit1)?>
                            </div>
                            <div class="col-sm-4 text-right"></div>
                        </div>                    
                    <?=form_close()?>
                    </div>
                    

            </section>
           
        </div>
        <script type="text/javascript">

            $(function () {
                 $('#nationality').on('change', function(e) {
                  e.preventDefault();
                  share_city();                        
                });
              });
              function share_city(){
                var country=$('select[id=nationality]').val();
                $.ajax({
                  type: "POST",
                  url: "<?= base_url(); ?>support_client/Planetres/city_filter",
                  data: {country: country }
                }).done(function (response) {
                  $('#city_idcity').html('');
                    if(response){
                      $('#city_idcity').append(response);
                    }                                
                  });
            } 
        </script>
</body>