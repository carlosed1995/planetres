<? 
  foreach ($data->result() as $key) {
    $email_user = $key->email;
    $iduser = $key->iduser;
  }
  $submit1 = array(
                'value'=> 'submit',
                'name' => 'submit1',
                'id' => 'submit1',
                'class' => 'btn btn-block btn-success'
            );
  $submit2 = array(
                'value'=> 'submit2',
                'name' => 'submit2',
                'id' => 'submit2',
                'class' => 'btn btn-block btn-success'
            );
  $hidden = array(
        'type'  => 'hidden',
        'name'  => 'iduser',
        'value' => ($iduser != '') ? $iduser : '',
  );
  $attributes = array(
      'class' => 'control-label col-sm-3'
  );
  $old_email = array(
        'name'          => 'user_email',
        'id'            => 'user_email',
        'value'         => $email_user,
        'class'         => 'form-control',
        'placeholder'   => $email_user,
        'disabled'      => 'disabled'
  );
  $newemail = array(
        'name'          => 'new_email',
        'id'            => 'new_email',
        'value'         => $new_email,
        'class'         => 'form-control',
        'placeholder'   => 'Insert new email'
  );
  $confirmemail = array(
        'name'          => 'confirm_email',
        'id'            => 'confirm_email',
        'value'         => $confirm_email,
        'class'         => 'form-control',
        'placeholder'   => 'Confirm email'
  );
  $old_password = array(
        'type'          => 'password',
        'name'          => 'old_password',
        'id'            => 'old_password',
        'value'         => $old_password,
        'class'         => 'form-control',
        'placeholder'   => 'insert old password'
  );

  $new_password = array(
        'type'          => 'password',
        'name'          => 'new_password',
        'id'            => 'new_password',
        'value'         => $new_password,
        'class'         => 'form-control',
        'placeholder'   => 'insert new password'
  );

  $confirm_password = array(
        'type'          => 'password',
        'name'          => 'confirm_password',
        'id'            => 'confirm_password',
        'value'         => $confirm_password,
        'class'         => 'form-control',
        'placeholder'   => 'Confirm password'
  );
?>
<body>
  <div class="row">
    <div class="col-md-3">
    
      <div class="navbar-collapse collapse sidebar-navbar-collapse">
      <ul class="nav navbar-nav" id="sidenav01">
        <li>
          <h2 align="center">Config menu<br></h2>
        </li>
        <li>
          <a href="#" data-toggle="collapse" data-target="#config_client" data-parent="#sidenav01" class="collapsed">
          <span class="glyphicon glyphicon-user" aria-hidden="true"></span> General 
          </a>
        </li>

      </ul>
      </div>
    
    </div>
    
        
          

            <!-- Main content -->
            <section class="content" id="maincontent">
                    
                    <div class="col-md-9" id="config_client">
                       
                    <h1 class="content-header" align="center">
                    General Config
                    </h1> 
                    <hr>
                    <?=form_open(base_url('support_client/Config/save_config_general'),'class="form-horizontal" id="formulario1"')?>
                    <?php if ($this->session->flashdata('email_update')) { ?>
                        <div class="alert alert-success">
                            <a href="#" class="close" data-dismiss="alert">&times;</a>
                            <strong>Success!</strong> <?php echo $this->session->flashdata('email_update'); ?>
                        </div>

                    <?php } else if ($this->session->flashdata('error')) { ?>

                        <div class="alert alert-danger">
                            <a href="#" class="close" data-dismiss="alert">&times;</a>
                            <strong>Error!</strong> <?php echo $this->session->flashdata('error'); ?>
                        </div>

                    <?php } ?>
                    <?=form_input($hidden);?>       
                            <h4 align="left">Change Email</h4>
                            <div class="form-group">
                              <?=form_label('User email', 'user_email', $attributes);?>
                              <div class="col-sm-9">
                               <?=form_input($old_email)?> 
                              </div>
                            </div>
                            <div class="form-group">
                              <?=form_label('New email', 'new_email', $attributes);?>
                              <div class="col-sm-9">
                                <?=form_input($newemail)?>
                                <span id="msgEmail"></span>
                                <?=form_error('new_email', '<div class="text-danger text-left">', '</div>')?>
                              </div>
                            </div>
                            <div class="form-group">
                              <?=form_label('Confirm email', 'confirm_email', $attributes);?>
                              <div class="col-sm-9">
                                <?=form_input($confirmemail)?>
                                <span id="msgConfirmEmail"></span>
                                <?=form_error('confirm_email', '<div class="text-danger text-left">', '</div>')?>
                                <?=form_error('errorpass', '<div class="text-danger text-left">', '</div>')?>
                              </div>
                            </div>
                            <div class="form-group">
                              <div class="col-sm-5 text-right"></div>
                              <div class="col-sm-2 text-left">
                                <?=form_submit($submit1)?>
                            </div>
                            <div class="col-sm-4 text-right"></div>
                            </div>
                        <?=form_close()?>
                    <hr>
                         
                        <?=form_open(base_url('support_client/Config/save_config_general'),'class="form-horizontal" id="formulario2"')?>
                        <?php if ($this->session->flashdata('success')) { ?>
                        <div class="alert alert-success">
                            <a href="#" class="close" data-dismiss="alert">&times;</a>
                            <strong>Success!</strong> <?php echo $this->session->flashdata('success'); ?>
                        </div>

                    <?php } else if ($this->session->flashdata('error')) { ?>

                        <div class="alert alert-danger">
                            <a href="#" class="close" data-dismiss="alert">&times;</a>
                            <strong>Error!</strong> <?php echo $this->session->flashdata('error'); ?>
                        </div>

                    <?php } ?>
                            <?=form_input($hidden);?>
                            <h4 align="left">Change Password</h4>
                            <div class="form-group">
                              <?=form_label('Old password', 'old_password', $attributes);?>
                              <div class="col-sm-9">
                                <?=form_input($old_password)?>
                                <span id="msgPwd"></span>
                                <?php echo form_error('pwd', '<div class="text-danger text-left">', '</div>'); ?>
                              </div>
                            </div>
                            <div class="form-group">
                              <?=form_label('New password', 'new_password', $attributes);?>
                              <div class="col-sm-9">
                                <?=form_input($new_password)?>
                                <span id="msgPwdnew"></span>
                                <?php echo form_error('new_password', '<div class="text-danger text-left">', '</div>'); ?>
                              </div>
                            </div>
                            <div class="form-group">
                              <?=form_label('Confirm password', 'confirm_password', $attributes);?>
                              <div class="col-sm-9">
                                <?=form_input($confirm_password)?>
                                <span id="msgPwdconfirm"></span>
                                <?php echo form_error('new_password', '<div class="text-danger text-left">', '</div>'); ?>
                                 <?php echo form_error('confirm_password', '<div class="text-danger text-left">', '</div>'); ?>
                              </div>
                            </div>
                            <div class="form-group">
                              <div class="col-sm-5 text-right"></div>
                              <div class="col-sm-2 text-left">
                                <?=form_submit($submit2)?>
                            </div>
                            <div class="col-sm-4 text-right"></div>
                            </div>
                        <?=form_close()?>
                    </div>
            </section>
            <script  type="text/javascript" >
            $(document).ready(function(){
                var emailreg = /^[a-zA-Z0-9_\.\-]+@[a-zA-Z0-9\-]+\.[a-zA-Z0-9\-\.]+$/;
                $('#new_email,#confirm_email').focusout( function(){
                    if( $("#new_email,#confirm_email").val() == "" || !emailreg.test($("#new_email,#confirm_email").val()) )
                    {
                        $('#msgEmail,#msgConfirmEmail').html("<span style='color:#f00'>Enter a correct email</span>");
                    }else{
                        $.ajax({
                            type: "POST",
                            url: "<?=base_url('support_client/Config/check_email_ajax')?>",
                            data: "new_email="+$('#new_email').val(),
                            beforeSend: function(){
                                $('#msgEmail').html('Checking...');
                            },
                            success: function( response ){
                                if(response === '<div style="display:none">1</div>'){
                                    $('#msgEmail').html("<span style='color:#0f0'>Email available</span>");
                                }else{
                                    $('#msgEmail').html("<span style='color:#f00'>Email not available</span>");
                                }
                            }
                        });
                        return false;
                    }
                });
            });

            $(document).ready(function(){
              $('#old_password,#new_password,#confirm_password').focusout( function(){
                  if( $("#old_password,#new_password,#confirm_password").val().length < 2)
                  {
                      $('#msgPwd,#msgPwdnew,#msgPwdconfirm').html("<span style='color:#f00'>Password must contain 2 characters minimum</span>");
                  }else{
                      $.ajax({
                          type: "POST",
                          url: "<?=base_url('support_client/Config/check_password_ajax')?>",
                          data: "old_password="+$('#old_password').val(),
                          beforeSend: function(){
                              $('#msgPwd').html('<span>Checking...</span>');
                          },
                          success: function( respuesta ){
                               if(respuesta == '<div style="display:none">1</div>')
                                  $('#msgPwd').html('<span style="color:#f00">Enter your previous password</span>');
                              else
                                  $('#msgPwd').html("<span style='color:#0f0'>Nice!</span>");
                          }
                     
                      });
                      return false;
                  }
              });
          });
            </script>
        </div>
</body>