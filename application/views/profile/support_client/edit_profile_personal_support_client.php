<?
    foreach ($data->result() as $key) {
        $iduser = $key->iduser;
    }

    $submit1 = array(
                'value'=> 'submit',
                'name' => 'submit1',
                'id' => 'submit1',
                'class' => 'btn btn-block btn-success'
    );

    $hidden = array(
        'type'  => 'hidden',
        'name'  => 'iduser',
        'value' => ($iduser != '') ? $iduser : ''
    );

    $attributes = array(
      'class' => 'control-label col-sm-2'
    );

    $name = array(
        'name'          => 'name',
        'id'            => 'name',
        'value'         => $name,
        'class'         => 'form-control',
        'placeholder'   => 'Insert name'
    );

    $middle_name = array(
        'name'          => 'middle_name',
        'id'            => 'middle_name',
        'value'         => $middle_name,
        'class'         => 'form-control',
        'placeholder'   => 'Insert middlename'
    );

    $last_name = array(
        'name'          => 'last_name',
        'id'            => 'last_name',
        'value'         => $last_name,
        'class'         => 'form-control',
        'placeholder'   => 'Insert lastname'
    );


    $phone = array(
        'name'          => 'phone',
        'id'            => 'phone',
        'value'         => $phone,
        'class'         => 'form-control',
        'placeholder'   => 'Insert phone'
    );

    $url = array(
        'name'          => 'url',
        'id'            => 'url',
        'value'         => $url,
        'class'         => 'form-control',
        'placeholder'   => 'Insert url'
    );

    $zip_code = array(
        'name'          => 'zip_code',
        'id'            => 'zip_code',
        'value'         => $zip_code,
        'class'         => 'form-control',
        'placeholder'   => 'Insert zip code'
    );


?>
<body>
        
	<div class="row">
    <div class="col-md-3">
        <div class="navbar-collapse collapse sidebar-navbar-collapse">
      <ul class="nav navbar-nav" id="sidenav01">
        <li>
          <h2 align="center">Menu<br></h2>
        </li>
        <li>
          <a href="#" data-toggle="collapse" data-target="#toggleDemo" data-parent="#sidenav01" class="collapsed">
          <span class="glyphicon glyphicon-user" aria-hidden="true"></span> Personal Info <span class="glyphicon glyphicon-menu-down" aria-hidden="true"></span>
          </a>
          <div class="collapse" id="toggleDemo" style="height: 0px;">
            <ul class="nav nav-list">
              <li><a href="<?=base_url('support_client/Profile')?>">View</a></li>
              <li><a href="<?=base_url('support_client/Profile/edit_profile_personal_support_client')?>">Edit</a></li>              
            </ul>
          </div>
        </li>

      </ul>
      </div>
    </div>
    
        
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <h1 align="center">
                    Edit personal Info
                </h1>                
            </section>

            <!-- Main content -->
            <section class="content" id="maincontent">

                
                    
                        <?=form_open(base_url('support_client/Profile/save_changes_personal_support_client'),'class="form-horizontal" id="formulario"')?>

                        <div class="col-md-9">
                        <?php if ($this->session->flashdata('success')) { ?>
                        <div class="alert alert-success">
                            <a href="#" class="close" data-dismiss="alert">&times;</a>
                            <strong>Success!</strong> <?php echo $this->session->flashdata('success'); ?>
                        </div>

                        <?php } else if ($this->session->flashdata('error')) { ?>

                        <div class="alert alert-danger">
                            <a href="#" class="close" data-dismiss="alert">&times;</a>
                            <strong>Error!</strong> <?php echo $this->session->flashdata('error'); ?>
                        </div>

                        <?php } ?>

                        <?=form_input($hidden)?>

                        <div class="form-group">
                            <?=form_label('Name','name', $attributes)?>
                            <div class="col-sm-10">
                                <?=form_input($name)?>
                                <?php echo form_error('name', '<div class="text-danger text-left">', '</div>'); ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <?=form_label('Middle name','middle_name', $attributes)?>
                            <div class="col-sm-10">
                                <?=form_input($middle_name)?>
                                <?php echo form_error('middlename', '<div class="text-danger text-left">', '</div>'); ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <?=form_label('Last name','last_name', $attributes)?>
                            <div class="col-sm-10">
                                <?=form_input($last_name)?>
                                <?php echo form_error('lastname', '<div class="text-danger text-left">', '</div>'); ?>
                            </div>
                        </div>

                        <div class="form-group">
                            <?=form_label('Phone','phone', $attributes)?>
                            <div class="col-sm-10">
                                <?=form_input($phone)?>
                                <?php echo form_error('phone', '<div class="text-danger text-left">', '</div>'); ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <?=form_label('Nationality', 'nationality', $attributes)?>
                            <div class= "col-sm-10">
                                
                                    <?= $selectnationality ?>
                                
                            </div>
                        </div>
                        <div class="form-group">
                            <?=form_label('City','city', $attributes)?>
                            <div class="col-sm-10"> 
                                <select name="city" id="city" class="form-control">
                                    <option value="<?=$city?>">City</option>
                                </select>
                                <?php echo form_error('city', '<div class="text-danger text-left">', '</div>'); ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <?=form_label('Zip code','zip_code', $attributes)?>
                            <div class="col-sm-10">
                                <?=form_input($zip_code)?>
                                <?php echo form_error('zip_code', '<div class="text-danger text-left">', '</div>'); ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <?=form_label('Gender',' genere', $attributes)?>
                                <label class="radio-inline col-sm-2">
                                    <input type="radio" id="genere1" name="genere" <?= $female ?> value="female"> Female
                                </label>
                                <label class="checkbox-inline col-sm-2">
                                    <input type="radio" id="genere2" name="genere" <?= $male ?> value="male"> Male
                                </label>
                            </div>      
                            <div class="row">
                                <div class="col-sm-2"></div>
                                <div class="col-sm-10">
                                    <?php echo form_error('genere', '<div class="text-danger text-left">', '</div>'); ?>
                                </div>
                            </div>                            
                        </div>
                        <div class="form-inline text-left">
                            <label class="control-label col-sm-2" for="birth">Birthdate:</label>
                            <div class="">                            
                                <div class="col-sm-10 form-group">
                                    <?= $year . $month . $day; ?>
                                    <?php echo form_error('birth', '<div class="text-danger text-left">', '</div>'); ?>
                                    <?php echo form_error('year', '<div class="text-danger text-left">', '</div>'); ?>
                                    <?php echo form_error('month', '<div class="text-danger text-left">', '</div>'); ?>
                                    <?php echo form_error('day', '<div class="text-danger text-left">', '</div>'); ?>
                                </div>
                            </div>                
                        </div>
                        <div class="form-group"></div>
                        
                        <div class="form-group">
                            <?=form_label('Web site','url', $attributes)?>
                            <div class="col-sm-10">
                                <?=form_input($url)?>
                                <?php echo form_error('url', '<div class="text-danger text-left">', '</div>'); ?>
                            </div>
                        </div>
                        <div class="form-group"> 
                            <div class="col-sm-4 text-right"></div>
                            <div class="col-sm-2 text-left">
                                <?=form_submit($submit1)?>
                            </div>
                            <div class="col-sm-4 text-right"></div>
                        </div>                    
                      <?=form_close()?>
                    </div>
                    

            </section>
           
        </div>
        <script type="text/javascript">

            $(function () {
                $('#nationality').on('change', function(e) {
                    e.preventDefault();
                  busqueda_city();                        
                });
            });

      
            function busqueda_city(){

             var nationality=$('select[id=nationality]').val();

            $.ajax({
                type: "POST",
                url: "<?= base_url('support_client/Profile/city')?>",
                data: {nationality: nationality }
            }).done(function (response) {
                if(response){
                    $('#city').html(response);
                }                                
            });
        }

        </script>
</body>