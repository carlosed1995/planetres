<?
	$submit = array(
                'value'	=> 'Add to cart',
                'name' 	=> 'addcart',
                'id' 	=> 'addcart',
                'class' => 'field4 rounded'
    );
?>
<link rel="stylesheet" href="<?=base_url('web/css/lightslider.css');?>"/>
<script src="<?=base_url('web/js/lightslider.js');?>"></script> 

<script type="text/javascript">
$(document).ready(function() {
$("#tabs").tabs();

$('#image-gallery').lightSlider({
	gallery:true,
	item:1,
	//thumbItem:9,
	slideMargin: 0,
	speed:1000,
	pause: 4500,
	auto:true,
	loop:true,
	onSliderLoad: function() {
		$('#image-gallery').removeClass('cS-hidden');
	}  
});

$("a.login").fancybox({
	iframe : {
		css : {
			'width': '460px',
			'height': '340px',
			'max-width': '90%',
			'max-height': '90%',	
			'margin': '0'
		}
	}
	});

$("a.register").fancybox({
	iframe : {
		css : {
			'width': '460px',
			'height': '570px',
			'max-width': '90%',
			'max-height': '90%',	
			'margin': '0'
		}
	}
	});

$("a.form").fancybox({
	iframe : {
		css : {
			'width': '460px',
			'height': '570px',
			'max-width': '90%',
			'max-height': '90%',	
			'margin': '0'
		}
	}
	});
	
});
</script>
</head>

<body>
<div id="page_details_excursions">
<section id="content">

<div class="head-details h-detail-l">
<div class="a-half">
<h1 class="title-details">
	<?=$name_excursions?>
	<br>&nbsp;

</h1>
<a href="<?=base_url('Planetres')?>"><strong>&laquo;</strong> Back to Results</a>

</div>
</div>

<div id="tabs">
  <ul>
  	<li><a href="#tabs-0">Gallery</a></li>
    <li><a href="#tabs-1">Highlights</a></li>
    <li><a href="#tabs-2">Important info</a></li>
    <li><a href="#tabs-3">Pricing Chart</a></li>
    <li><a href="#tabs-4">Make a Reservation</a></li>
  </ul>

<div id="tabs-0">
<?=$images_excursions?>
</div>

<div id="tabs-1">
<h2 class="title-sub">Highlights</h2></br>
<?=$highlights?>
</div>
  
<div id="tabs-2">
<?=$info?>
</div>

<div id="tabs-3">
<h2 class="title-sub">Pricing Chart</h2>
	
    <?=$pricing_chart?>
   
</div>

<div id="tabs-4">
  <?=form_open(base_url('Planetres'),'class="form-horizontal" id="formulario"')?>
  <div id="change">
  <h1 class="tit3">Persons Availability and Prices</h1>
  <p style="margin-top:20px; margin-bottom:0;">
  	<input name="idexcursions" type="hidden" value="<?=$idexcursions?>">
  	<input name="city_starting" type="hidden" value="<?=$city_starting?>"> 
    <div class="form-group">
	    <label for="departure_date_excursions">Arrival Date</label>
	    <?=$departure_date?>
    </div>
    <div class="form-group">
    	<label for="guests_excursions">Adults</label>
    	<?=$guests_excursions?>
    </div>
  	<div class="form-group">
  		<label for="guests_kids">Kids</label>
    	<?=$guests_kids?>
    </div>
	<button type="button" name="submit" id="submit" value="Get Price" class="field4 rounded">Get Price</button>

  	<?=form_submit($submit)?>

	<div id="price_per_person">

	</div>
	</p>
<?=form_close()?>
 
</div>
  
</div>

</section>
</div>
<script>
   	$( function() {
    $( "#guests_excursions,#guests_kids,#departure_date_excursions" ).change(function() {     
         $.ajax({               
                  url: ' <?php echo base_url('client/Planetres/destroySession'); ?>',
                  type: "POST",
                  success: function (datos)
                  {
                      console.log("");
                  }
              });
      });
  	 });

	$(document).ready(function(){
		$("#addcart").hide();
	    $( "#guests_excursions,#guests_kids,#departure_date_excursions" ).change(function() {     
		    
		    $("#addcart").hide();
		    $("#submit").show();
		   
	    });
	});

    $("#submit").click(function(){
        $.post("<?=base_url('client/Planetres/get_price_excursions')?>", $("#formulario").serialize(), 
        function(htmlexterno){
        	$("#submit").hide();
            $("#price_per_person").html(htmlexterno);
            $("#addcart").show();
        });
    });

</script>
</body>
</html>
