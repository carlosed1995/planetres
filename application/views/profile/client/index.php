<script>
$( function start_hotel() {
  $("#start_hotel").datepicker({

                        maxDate: new Date(2017,12,31), 
                        changeYear: true, 
                        changeMonth: true,
                        minDate: new Date(),
                        dateFormat: 'yy-mm-dd',
                        constrainInput: true
                        });
          });

$( function end_hotel() {
  $("#end_hotel").datepicker({

                        maxDate: new Date(2017,12,31), 
                        changeYear: true, 
                        changeMonth: true,
                        minDate: new Date(),
                        dateFormat: 'yy-mm-dd',
                        constrainInput: true
                        });
          });
        
    

$( function() {
  $("#start_tour,#end_tour").datepicker({

                        maxDate: new Date(2017,12,31),
                        changeYear: true, 
                        changeMonth: true,
                        minDate: new Date(),
                        dateFormat: 'yy-mm-dd',
                        constrainInput: true
                        });
          });

$( function() {
  $("#start_excursions,#end_excursions").datepicker({

                        maxDate: new Date(2017,12,31), 
                        changeYear: true, 
                        changeMonth: true,
                        minDate: new Date(),
                        dateFormat: 'yy-mm-dd',
                        constrainInput: true
                        });
          });

$( function() {
  $("#start_transfer,#end_transfer").datepicker({

                        maxDate: new Date(2017,12,31), 
                        changeYear: true, 
                        changeMonth: true,
                        minDate: new Date(),
                        dateFormat: 'yy-mm-dd',
                        constrainInput: true
                        });
          });

$(document).ready(function() {
$("a.login").fancybox({
  iframe : {
    css : {
      'width': '460px',
      'height': '340px',
      'max-width': '90%',
      'max-height': '90%',  
      'margin': '0'
    }
  }
  });

$("a.register").fancybox({
  iframe : {
    css : {
      'width': '460px',
      'height': '570px',
      'max-width': '90%',
      'max-height': '90%',  
      'margin': '0'
    }
  }
  });
  
});

$(document).ready(function() {
  

$('.field2').on('click', function(){
  $('#ui-datepicker-div').css({"z-index":"1000"});
});

$('#options a').on('click', function(){
  $('#options a.selected').removeClass('selected');
    $(this).addClass('selected');
});

$("#b-hotels").click(function () {
  $(".box-forms").fadeOut('fast');
  $("#box-hotels").fadeIn('slow');
});

$("#b-tours").click(function () {
  $(".box-forms").fadeOut('fast');
  $("#box-tours").fadeIn('slow');
});

$("#b-excursions").click(function () {
  $(".box-forms").fadeOut('fast');
  $("#box-excursions").fadeIn('slow');
});

$("#b-transfer").click(function () {
  $(".box-forms").fadeOut('fast');
  $("#box-transfer").fadeIn('slow');
});

$("#button_hoteles,#button-hotel2").click(function () {
  $("#resultados").fadeOut('fast');
  $("#resultados_de_busqueda").fadeIn('slow');
});

$("#button_excursions").click(function () {
  $("#resultados").fadeOut('fast');
  $("#resultados_de_busqueda").fadeIn('slow');
});
$("#button_tours,#button_tours2").click(function () {
  $("#resultados").fadeOut('fast');
  $("#resultados_de_busqueda").fadeIn('slow');
});
$("#button_transfer,#button-transport2").click(function () {
  $("#resultados").fadeOut('fast');
  $("#resultados_de_busqueda").fadeIn('slow');
});

});

</script>
<section id="reservations">
<div class="box-forms-content">

<div id="box-hotels" class="box-forms" style="display:inline-block;">
  <form name="form1" method="post" action="">
    
      <input name="city_hotel" type="text" class="field1 rounded" id="city_hotel" placeholder="City or Region">
      <input type="hidden" name="city_idcity"  id="city_idcity_hotel" value="">
      <input type="hidden" name="country_idcountry" id="country_idcountry_hotel" value="">
      <div class="containerautocomplete_share"></div>
    
    <input type="text" name="start_hotel" class="field2 rounded" id="start_hotel" placeholder="Start">
    <input type="text" name="end_hotel" class="field2 rounded" id="end_hotel" placeholder="End">
    <?=$type_room?>
    <input name="button_hoteles" type="submit" class="field4 rounded" id="button_hoteles" value="SEARCH">
      <div id="xtra-form">
        <div class="add-rows-nav">
          <a href="javascript:;" id="addnew" class="rounded">
            <strong style="color:#0F0">+ </strong> Add Room
          </a>
          <a href="javascript:;" id="removei" class="rounded">
            <strong style="color: #F90">x </strong> Delete Room
          </a>
        </div>
      <div id="pa-grid">
        <div class="rowfields">
          Room <input id="roomcount1" type="hidden" name="roomcount" value="1">1 &nbsp;
            <select name="adult" id="adult1" class="field6 rounded">      
              <option value="1">1</option>
              <option value="2">2</option>
              <option value="3">3</option>
              <option value="4">4</option>
            </select> 
            <select name="kids" id="kids1" class="field7 rounded">     
              <option value="1">1</option>
              <option value="2">2</option>
              <option value="3">3</option>
              <option value="4">4</option>
            </select>
        </div>
      </div>
      <input name="button-hotel2" type="submit" class="field4 rounded" id="button-hotel2" value="SEARCH">
      </div>
  </form>
</div>

<div id="box-tours" class="box-forms">
  <form name="form1" method="post" action="">
    <?=$country?>
  <input type="text" name="start_tour" class="field2 rounded" id="start_tour" placeholder="Start">
  <input type="text" name="end_tour" class="field2 rounded" id="end_tour" placeholder="End">
  <select name="guests_tours" class="field3 rounded" id="guests_tours">
    <option value="2" selected>2 Adult, 0 Children</option>
    <option value="1">1 Adult, 0 Children</option>
    <option value="0">More option</option>
  </select>
  <input name="button" type="submit" class="field4 rounded" id="button_tours" value="SEARCH">
    <div id="xtra_form_tours">
      <div class="add-rows-nav">
        <a href="javascript:;" id="addnew_room_tours" class="rounded">
          <strong style="color:#0F0">+ </strong> Add Room</a> 
        <a href="javascript:;" id="removei_tours" class="rounded">
          <strong style="color: #F90">x </strong> Delete Room</a>
      </div>
      <div id="pa_grid_tours">
        <div class="rowfields">
          Room <input id="roomcount_tours1" type="hidden" name="roomcount_tours" value="1">1 &nbsp;
            <select name="adult_tours" id="adult_tours1" class="field6 rounded">      
              <option value="1">1</option>
              <option value="2">2</option>
              <option value="3">3</option>
            </select> 
            <select name="kids_tours" id="kids_tours1" class="field7 rounded">
              <option value="0" selected>0</option>
              <option value="1">1</option>
            </select>
        </div>
      </div>
      <input name="button_tours2" type="submit" class="field4 rounded" id="button_tours2" value="SEARCH">
    </div>
  </form>
</div>

<div id="box-excursions" class="box-forms">
  <form name="form1" method="post">
    <input name="destination_exc" type="text" class="field1 rounded" id="destination_exc" placeholder="City or Region to Excursion">
    <input type="hidden" name="city_idcity"  id="city_idcity_excursions" value="">
    <input type="hidden" name="country_idcountry" id="country_idcountry_excursions" value="">
    <div class="containerautocomplete_share"></div>
    <input type="text" name="start_excursions" class="field2 rounded" id="start_excursions" placeholder="Start">
    <input type="text" name="end_excursions" class="field2 rounded" id="end_excursions" placeholder="End">   
    <?=$guests_excursions_adults?>
    <?=$guests_excursions_kids?>
    <input name="button" type="submit" class="field4 rounded" id="button_excursions" value="SEARCH">
  </form>
</div>

<div id="box-transfer" class="box-forms">
  <form name="form1" method="post" action="">
    <input name="citytransf" type="text" class="field1 rounded" id="citytransf" placeholder="City or Region to Transfer">
    <input type="hidden" name="city_idcity"  id="city_idcity_transfer" value="">
    <input type="hidden" name="country_idcountry" id="country_idcountry_transfer" value="">
    <div class="containerautocomplete_share"></div>
    <input type="text" name="start_transfer" class="field2 rounded" id="start_transfer" placeholder="Start">
    <input type="text" name="end_transfer" class="field2 rounded" id="end_transfer" placeholder="End">
    
    <?=$taxies?>

    <input name="button" type="submit" class="field4 rounded" id="button_transfer" value="SEARCH">
    
  </form>
</div>
</div>
<div id="options">
<a href="javascript:;" style="background-image: url(<?= base_url(); ?>web/images/but-key.png)" class="selected" id="b-hotels">Hotels</a> <a href="javascript:;" style="background-image: url(<?= base_url(); ?>web/images/but-suite.png)" id="b-tours">Tours</a> <a href="javascript:;" style="background-image: url(<?= base_url(); ?>web/images/but-bag.png)" id="b-excursions">Excursion</a> <a href="javascript:;" style="background-image: url(<?= base_url(); ?>web/images/but-cars.png)" id="b-transfer">Transfer</a></div>
</section>

<section id="order-rrss">
<div class="half ssnn">
<a href="#"><img src="<?= base_url(); ?>web/images/ico-fb.jpg" width="25" height="25"></a><a href="#"><img src="<?= base_url(); ?>web/images/ico-tw.jpg" width="25" height="25"></a><a href="#"><img src="<?= base_url(); ?>web/images/ico-03.jpg" width="25" height="25"></a><a href="#"><img src="<?= base_url(); ?>web/images/ico-pint.jpg" width="25" height="25"></a><a href="#"><img src="<?= base_url(); ?>web/images/ico-g.jpg" width="25" height="25"></a><a href="#"><img src="<?= base_url(); ?>web/images/ico-lin.jpg" width="25" height="25"></a><a href="#"><img src="<?= base_url(); ?>web/images/ico-yt.jpg" width="25" height="25"></a> &nbsp; &laquo; Compartir
</div>
<div class="half" style="text-align:right;">
  <select name="order" class="field5 rounded" id="order">
      <option>Order by:</option>
      <option>Order 1</option>
      <option>Order 2</option>
      <option>Order 3</option>
    </select>
</div>
</section>

<section id="resultados_de_busqueda">
  

</section>


<section id="resultados">  
    <div class="r-row rounded">
      <a href="#"><img src="<?= base_url(); ?>web/images/thumbnail.jpg" class="thumb rounded"></a>      
      <div class="description">
      <h3>Sunny Suites</h3>
      <p>This stunning hotel boasts a spectacular setting in London, amidst the sophistication of Seven spectacular setting in London Dials village, amidst the sophistication of Seven Dials village a spectacular setting in London.</p>
      <p><img src="<?= base_url(); ?>web/images/icon-star.png" width="12" height="12"> <img src="<?= base_url(); ?>web/images/icon-star.png" width="12" height="12"> <img src="<?= base_url(); ?>web/images/icon-star.png" width="12" height="12"></p>
      </div>
      <div class="price rounded">
      <strong>$255.31</strong>
      <p>Per person</p>
      <a href="#" class="rounded">Reserve</a> </div>
    </div>

    <div class="r-row rounded">
      <a href="#"><img src="<?= base_url(); ?>web/images/thumbnail.jpg" class="thumb rounded"></a>
      <div class="description">
      <h3>Sunny Suites</h3>
      <p>This stunning hotel boasts a spectacular setting in London, amidst the sophistication of Seven spectacular setting in London Dials village, amidst the sophistication of Seven Dials village a spectacular setting in London.</p>
      <p><img src="<?= base_url(); ?>web/images/icon-star.png" width="12" height="12"> <img src="<?= base_url(); ?>web/images/icon-star.png" width="12" height="12"> <img src="<?= base_url(); ?>web/images/icon-star.png" width="12" height="12"></p>
      </div>
      <div class="price rounded">
      <strong>$255.31</strong>
      <p>Per person</p>
      <a href="#" class="rounded">Reserve</a> </div>
    </div>

    <div class="r-row rounded">
      <a href="#"><img src="<?= base_url(); ?>web/images/thumbnail.jpg" class="thumb rounded"></a>
      <div class="description">
      <h3>Sunny Suites</h3>
      <p>This stunning hotel boasts a spectacular setting in London, amidst the sophistication of Seven spectacular setting in London Dials village, amidst the sophistication of Seven Dials village a spectacular setting in London.</p>
      <p><img src="<?= base_url(); ?>web/images/icon-star.png" width="12" height="12"> <img src="<?= base_url(); ?>web/images/icon-star.png" width="12" height="12"> <img src="<?= base_url(); ?>web/images/icon-star.png" width="12" height="12"></p>
    </div>
    <div class="price rounded">
      <strong>$255.31</strong>
      <p>Per person</p>
      <a href="#" class="rounded">Reserve</a> </div>
    </div>

    <div class="r-row rounded">
      <a href="#"><img src="<?= base_url(); ?>web/images/thumbnail.jpg" class="thumb rounded"></a>
      <div class="description">
      <h3>Sunny Suites</h3>
      <p>This stunning hotel boasts a spectacular setting in London, amidst the sophistication of Seven spectacular setting in London Dials village, amidst the sophistication of Seven Dials village a spectacular setting in London.</p>
      <p><img src="<?= base_url(); ?>web/images/icon-star.png" width="12" height="12"> <img src="<?= base_url(); ?>web/images/icon-star.png" width="12" height="12"> <img src="<?= base_url(); ?>web/images/icon-star.png" width="12" height="12"></p>
    </div>
    <div class="price rounded">
      <strong>$255.31</strong>
      <p>Per person</p>
      <a href="#" class="rounded">Reserve</a> </div>
    </div>

    <div class="r-row rounded">
      <a href="#"><img src="<?= base_url(); ?>web/images/thumbnail.jpg" class="thumb rounded"></a>
      <div class="description">
        <h3>Sunny Suites</h3>
        <p>This stunning hotel boasts a spectacular setting in London, amidst the sophistication of Seven spectacular setting in London Dials village, amidst the sophistication of Seven Dials village a spectacular setting in London.</p>
        <p><img src="<?= base_url(); ?>web/images/icon-star.png" width="12" height="12"> <img src="<?= base_url(); ?>web/images/icon-star.png" width="12" height="12"> <img src="<?= base_url(); ?>web/images/icon-star.png" width="12" height="12"></p>
      </div>
    <div class="price rounded">
      <strong>$255.31</strong>
      <p>Per person</p>
      <a href="#" class="rounded">Reserve</a> </div>
    </div>

    <div class="r-row rounded">
      <a href="#"><img src="<?= base_url(); ?>web/images/thumbnail.jpg" class="thumb rounded"></a>
      <div class="description">
        <h3>Sunny Suites</h3>
        <p>This stunning hotel boasts a spectacular setting in London, amidst the sophistication of Seven spectacular setting in London Dials village, amidst the sophistication of Seven Dials village a spectacular setting in London.</p>
        <p><img src="<?= base_url(); ?>web/images/icon-star.png" width="12" height="12"> <img src="<?= base_url(); ?>web/images/icon-star.png" width="12" height="12"> <img src="<?= base_url(); ?>web/images/icon-star.png" width="12" height="12"></p>
      </div>
      <div class="price rounded">
        <strong>$255.31</strong>
        <p>Per person</p>
        <a href="#" class="rounded">Reserve</a> 
      </div>
    </div>

    <div class="r-row rounded">
      <a href="#"><img src="<?= base_url(); ?>web/images/thumbnail.jpg" class="thumb rounded"></a>
      <div class="description">
      <h3>Sunny Suites</h3>
      <p>This stunning hotel boasts a spectacular setting in London, amidst the sophistication of Seven spectacular setting in London Dials village, amidst the sophistication of Seven Dials village a spectacular setting in London.</p>
      <p><img src="<?= base_url(); ?>web/images/icon-star.png" width="12" height="12"> <img src="<?= base_url(); ?>web/images/icon-star.png" width="12" height="12"> <img src="<?= base_url(); ?>web/images/icon-star.png" width="12" height="12"></p>
      </div>
      <div class="price rounded">
      <strong>$255.31</strong>
      <p>Per person</p>
      <a href="#" class="rounded">Reserve</a> </div>
    </div>        
</section>

<script>

  $("#city_hotel").keyup(function () {
    var info = $(this).val();
    $.post('<?=base_url('Autocomplete/autocomplete_hotel')?>', {info: info}, function (data) {
    if (data != ''){
      $(".containerautocomplete_share").html(data);
    } else {
       $(".containerautocomplete_share").html('');
    }
    })
  });
  
  function asignar_hotel(idcity,idcountry,nombre_city,nombre_country,abbre) {
    var result = nombre_city+', '+nombre_country+' '+abbre;
    $("#city_idcity_hotel").val(idcity);
    $("#country_idcountry_hotel").val(idcountry);
    $("#city_hotel").val(result);
    $(".containerautocomplete_share").html('');
  }

  $("#destination_exc").keyup(function () {
    var info = $(this).val();
    $.post('<?=base_url('Autocomplete/autocomplete_excursion')?>', {info: info}, function (data) {
    if (data != ''){
      $(".containerautocomplete_share").html(data);
    } else {
       $(".containerautocomplete_share").html('');
    }
    })
  });
  
  function asignar_excursion(idcity,idcountry,nombre_city,nombre_country,abbre) {
    var result = nombre_city+', '+nombre_country+' '+abbre;
    $("#city_idcity_excursions").val(idcity);
    $("#country_idcountry_excursions").val(idcountry);
    $("#destination_exc").val(result);
    $(".containerautocomplete_share").html('');
  }

  $("#citytransf").keyup(function () {
    var info = $(this).val();
    $.post('<?=base_url('Autocomplete/autocomplete_transfer')?>', {info: info}, function (data) {
    if (data != ''){
      $(".containerautocomplete_share").html(data);
    } else {
       $(".containerautocomplete_share").html('');
    }
    })
  });
  
  function asignar_transfer(idcity,idcountry,nombre_city,nombre_country,abbre) {
    var result = nombre_city+', '+nombre_country+' '+abbre;
    $("#city_idcity_transfer").val(idcity);
    $("#country_idcountry_transfer").val(idcountry);
    $("#citytransf").val(result);
    $(".containerautocomplete_share").html('');
  }

  $( function() {
    $( "#start_hotel,#end_hotel,#guests_hotel" ).change(function() {     
         $.ajax({               
                  url: ' <?php echo base_url('client/Planetres/destroySession'); ?>',
                  type: "POST",
                  success: function (datos)
                  {
                      console.log("");
                  }
              });
      });
  });

  $( function() {
    $( "#start_excursions,#end_excursions,#guests_excursions" ).change(function() {     
         $.ajax({               
                  url: ' <?php echo base_url('client/Planetres/destroySession'); ?>',
                  type: "POST",
                  success: function (datos)
                  {
                      console.log("");
                  }
              });
      });
  });

  $( function() {
    $( "#destinations,#start_tour,#end_tour,#guests_tours" ).change(function() {     
         $.ajax({               
                  url: ' <?php echo base_url('client/Planetres/destroySession'); ?>',
                  type: "POST",
                  success: function (datos)
                  {
                      console.log("");
                  }
              });
      });
  });

  $( function() {
    $( "#citytransf,#start_transfer,#end_transfer,#guests_transfer" ).change(function() {     
         $.ajax({               
                  url: ' <?php echo base_url('client/Planetres/destroySession'); ?>',
                  type: "POST",
                  success: function (datos)
                  {
                      console.log("");
                  }
              });
      });
  });

  $( function() {
    $( "#adult1,#adult2,#adult3,#adult4" ).change(function() {     
         $.ajax({               
                  url: ' <?php echo base_url('client/Planetres/destroySession'); ?>',
                  type: "POST",
                  success: function (datos)
                  {
                      console.log("");
                  }
              });
      });
  });

  $( function() {
    $( "#kids1,#kids2,#kids3,#kids4" ).change(function() {     
         $.ajax({               
                  url: ' <?php echo base_url('client/Planetres/destroySession'); ?>',
                  type: "POST",
                  success: function (datos)
                  {
                      console.log("");
                  }
              });
      });
  });

  $(document).ready(function() {
  var currentItem_tours = 2;
     $('#guests_tours').on('change', function() {
      var optionSelected_tour = $(this).find("option:selected");
      var valueSelected_tour  = optionSelected_tour.val();
      if (valueSelected_tour == 0) {
        $("#button_tours").fadeOut('fast');
        $("#xtra_form_tours").fadeIn('slow');  //.toggle('slow')
        $('#guests_tours').removeClass('field3');
        $('#guests_tours').addClass('field1');
      }else{                          
        $("#xtra_form_tours").fadeOut('fast');        
        $('#guests_tours').removeClass('field1');
        $('#guests_tours').addClass('field3');
        $("#button_tours").fadeIn('slow');        
      }
    });
    $('#addnew_room_tours').click(function addroom_tours(){
     if(currentItem_tours <= 3){
     $('#items_tours').val(currentItem_tours);
      var strToAdd = '<div class="rowfields_tours" id="rowf'+currentItem_tours+'">Room<input id="roomcount_tours'+currentItem_tours+'" type="hidden" name="roomcount_tours" value="1">'+currentItem_tours+' &nbsp;<select name="adult_tours" id="adult_tours'+currentItem_tours+'" class="field6 rounded"><option value="1">1</option><option value="2">2</option><option value="3">3</option></select><select name="kids_tours" id="kids_tours'+currentItem_tours+'" class="field7 rounded"><option value="0">0</option><option value="1">1</option></select></div>';
        currentItem_tours++;
    }
    $('#pa_grid_tours').append(strToAdd);
       //return false;
    });
    $('#removei_tours').click(function() {
      if(currentItem_tours > 2){
        $('.rowfields_tours:last').remove();
        currentItem_tours--; 
      }
    });
  });

  $(document).ready(function() {
    var currentItem = 2;
     $('#guests_hotel').on('change', function() {
      var optionSelected = $(this).find("option:selected");
      var valueSelected  = optionSelected.val();
      if (valueSelected == 3 || valueSelected == 4) {
        $("#button_hoteles").fadeOut('fast');
        $("#xtra-form").fadeIn('slow');  //.toggle('slow')
        $('#guests_hotel').removeClass('field3');
        $('#guests_hotel').addClass('field1');
       
      }else {                          
        $("#xtra-form").fadeOut('fast');        
        $('#guests_hotel').removeClass('field1');
        $('#guests_hotel').addClass('field3');
        $("#button_hoteles").fadeIn('slow');        
      }
    });

    $('#addnew').click(function addroom(){
     
     if(currentItem <= 4){
     $('#items').val(currentItem);
      var strToAdd = '<div class="rowfields" id="rowf'+currentItem+'">Room <input id="roomcount'+currentItem+'" type="hidden" name="roomcount" value="1">'+currentItem+' &nbsp;<select name="adult" id="adult'+currentItem+'" class="field6 rounded"><option value="1">1</option><option value="2">2</option><option value="3">3</option><option value="4">4</option></select><select name="kids" id="kids'+currentItem+'" class="field7 rounded"><option value="1">1</option><option value="2">2</option><option value="3">3</option><option value="4">4</option></select></div>';
        currentItem++;
    }
    $('#pa-grid').append(strToAdd);
       //return false;
    });

    $('#removei').click(function() {
      if(currentItem > 2){
        $('.rowfields:last').remove();
        currentItem--; 
      }
    });
});

  $(function () {
    $('#button_hoteles,#button-hotel2').click(function (e) {
      e.preventDefault();
      busqueda_hoteles();                        
    });
  });

  $(function () {
    $('#button_tours,#button_tours2').click(function (e) {
      e.preventDefault();
      busqueda_tours();                        
    });
  });  


  $(function () {
    $('#button_excursions').click(function (e) {
      e.preventDefault();
      busqueda_excursions();                        
    });
  });  


  $(function () {
    $('#button_transfer').click(function (e) {
      e.preventDefault();
      busqueda_transfer();                        
    });
  });  

  function busqueda_hoteles() {

                    var row1 = parseInt($("#roomcount1").val());
                    var row2 = parseInt($("#roomcount2").val());
                    var row3 = parseInt($("#roomcount3").val());
                    var row4 = parseInt($("#roomcount4").val());
                    
                    if(Number.isNaN(row2)){
                      var row2 = 0;
                    }
                    if(Number.isNaN(row3)){
                      var row3 = 0;
                    }
                    if(Number.isNaN(row4)){
                      var row4 = 0;
                    }

                    var adult1 = parseInt($("#adult1").val());
                    var adult2 = parseInt($("#adult2").val());
                    var adult3 = parseInt($("#adult3").val());
                    var adult4 = parseInt($("#adult4").val());
                    
                    if(Number.isNaN(adult2)){
                      var adult2 = 0;
                    }
                    if(Number.isNaN(adult3)){
                      var adult3 = 0;
                    }
                    if(Number.isNaN(adult4)){
                      var adult4 = 0;
                    }

                    var kids1 = parseInt($("#kids1").val());
                    var kids2 = parseInt($("#kids2").val());
                    var kids3 = parseInt($("#kids3").val());
                    var kids4 = parseInt($("#kids4").val());
                    if(Number.isNaN(kids2)){
                      var kids2 = 0;
                    }
                    if(Number.isNaN(kids3)){
                      var kids3 = 0;
                    }
                    if(Number.isNaN(kids4)){
                      var kids4 = 0;
                    }

                    var city = $("#city_idcity_hotel").val();
                    var country = $("#country_idcountry_hotel").val();
                    var start = $("#start_hotel").val();
                    var end = $("#end_hotel").val();
                    var guests = $("#guests_hotel").val();
                    var room = (row1+row2+row3+row4);
                    var adult = (adult1+adult2+adult3+adult4);
                    var kids = (kids1+kids2+kids3+kids4);
                    $.ajax({
                      type: "POST",
                      url: "<?= base_url(); ?>client/Index_TdLv/busqueda_de_hoteles",
                      data: {city_hotel: city, country_hotel:country, start_hotel: start, end_hotel: end, guests_hotel: guests, room: room, adult: adult, kids: kids, adult1:adult1 ,adult2: adult2 ,adult3: adult3 ,adult4: adult4 ,kids1: kids1, kids2: kids2, kids3: kids3, kids4: kids4 }
                    }).done(function (response) {
                            if(response){
                            $('#resultados_de_busqueda').html(response);
                            }
                    });                                         
                }

  function busqueda_tours() {

                    var row1 = parseInt($("#roomcount_tours1").val());
                    var row2 = parseInt($("#roomcount_tours2").val());
                    var row3 = parseInt($("#roomcount_tours3").val());
                    
                    if(Number.isNaN(row2)){
                      var row2 = 0;
                    }
                    if(Number.isNaN(row3)){
                      var row3 = 0;
                    }
                    
                    var adult1 = parseInt($("#adult_tours1").val());
                    var adult2 = parseInt($("#adult_tours2").val());
                    var adult3 = parseInt($("#adult_tours3").val());
                    
                    
                    if(Number.isNaN(adult2)){
                      var adult2 = 0;
                    }
                    if(Number.isNaN(adult3)){
                      var adult3 = 0;
                    }

                    var kids1 = parseInt($("#kids_tours1").val());
                    var kids2 = parseInt($("#kids_tours2").val());
                    var kids3 = parseInt($("#kids_tours3").val());
                    
                    if(Number.isNaN(kids2)){
                      var kids2 = 0;
                    }
                    if(Number.isNaN(kids3)){
                      var kids3 = 0;
                    }

                    var destinations = $("#destinations").val();
                    var start = $("#start_tour").val();
                    var end = $("#end_tour").val();
                    var guests = $("#guests_tours").val();
                    var room = (row1+row2+row3);
                    var adult = (adult1+adult2+adult3);
                    var kids = (kids1+kids2+kids3);

                    $.ajax({
                        type: "POST",
                        url: "<?= base_url(); ?>client/Index_TdLv/busqueda_de_tours",
                        data: {destinations: destinations, start_tour: start, end_tour: end, guests_tours: guests, room_tours: room, adult_tours: adult, kids_tours: kids, adult_tours1:adult1 ,adult_tours2: adult2 ,adult_tours3: adult3 ,kids_tours1: kids1, kids_tours2: kids2, kids_tours3: kids3 }
                    }).done(function (response) {
                            if(response){
                            $('#resultados_de_busqueda').html(response);
                            }
                    });                                         
                }

  function busqueda_excursions() {

                    var city = $("#city_idcity_excursions").val();
                    var country = $("#country_idcountry_excursions").val();
                    var start = $("#start_excursions").val();
                    var end = $("#end_excursions").val();
                    var guests=parseInt($("#guests_excursions_adults").val());
                    var guests_kids=parseInt($("#guests_excursions_kids").val());
                    var guests_total = (guests+guests_kids);

                    $.ajax({
                        type: "POST",
                        url: "<?= base_url(); ?>client/Index_TdLv/busqueda_de_excursions",
                        data: {city_excursions: city, country_excursions: country, start_excursions: start, end_excursions: end, guests_excursions:guests, guests_kids:guests_kids, guests_total:guests_total}
                    }).done(function (response) {
                            if(response){
                            $('#resultados_de_busqueda').html(response);
                            }
                    });                                         
                }

  function busqueda_transfer() {
                    var row1 = parseInt($("#transportcount1").val());
                    var row2 = parseInt($("#transportcount2").val());            
                    var row3 = parseInt($("#transportcount3").val());
                    var row4 = parseInt($("#transportcount4").val());

                    if(Number.isNaN(row2)){
                      var row2 = 0;
                    }
                    if(Number.isNaN(row3)){
                      var row3 = 0;
                    }
                    if(Number.isNaN(row4)){
                      var row4 = 0;
                    }

                    var person1 = parseInt($("#person1").val());
                    var person2 = parseInt($("#person2").val());            
                    var person3 = parseInt($("#person3").val());
                    var person4 = parseInt($("#person4").val());

                    if(Number.isNaN(person2)){
                      var person2 = 0;
                    }
                    if(Number.isNaN(person3)){
                      var person3 = 0;
                    }
                    if(Number.isNaN(person4)){
                      var person4 = 0;
                    }

                    var city = $("#city_idcity_transfer").val();
                    var country = $("#country_idcountry_transfer").val();
                    var start = $("#start_transfer").val();
                    var end = $("#end_transfer").val();
                    var guests=$("#guests_transfer").val();
                    var count_transport = (row1+row2+row3+row4);
                    var person = (person1+person2+person3+person4);
                    $.ajax({
                        type: "POST",
                        url: "<?= base_url(); ?>client/Index_TdLv/busqueda_de_transfer",
                        data: {citytransf: city, country_transfer:country, start_transfer: start, end_transfer:end, guests_transfer:guests, count_transport:count_transport, person: person}
                    }).done(function (response) {
                        if(response){
                          $('#resultados_de_busqueda').html(response);
                        }
                    });                                         
                }                

                
</script>


</div>
</body>