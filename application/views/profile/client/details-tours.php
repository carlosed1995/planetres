<?
	$submit = array(
                'value'	=> 'Next',
                'name' 	=> 'next',
                'id' 	=> 'next',
                'class' => 'field4 rounded'
    );
?>
<link rel="stylesheet" href="<?=base_url('web/css/lightslider.css');?>"/>
<script src="<?=base_url('web/js/lightslider.js');?>"></script> 

<script type="text/javascript">
	
$(document).ready(function() {
$("#tabs").tabs();

$('#image-gallery').lightSlider({
	gallery:true,
	item:1,
	//thumbItem:9,
	slideMargin: 0,
	speed:1000,
	pause: 4500,
	auto:true,
	loop:true,
	onSliderLoad: function() {
		$('#image-gallery').removeClass('cS-hidden');
	}  
});

$("a.login").fancybox({
	iframe : {
		css : {
			'width': '460px',
			'height': '340px',
			'max-width': '90%',
			'max-height': '90%',	
			'margin': '0'
		}
	}
	});

$("a.register").fancybox({
	iframe : {
		css : {
			'width': '460px',
			'height': '570px',
			'max-width': '90%',
			'max-height': '90%',	
			'margin': '0'
		}
	}
	});

$("a.form").fancybox({
	iframe : {
		css : {
			'width': '460px',
			'height': '570px',
			'max-width': '90%',
			'max-height': '90%',	
			'margin': '0'
		}
	}
	});
	
});
</script>
</head>

<body>
<div id="page_details_tours">
<section id="content">

<div class="head-details h-detail-l">
<div class="a-half">
<h1 class="title-details">
<?=$name_tours?>
<br>&nbsp;

	 <?php 
		if($type_tours == 1){
	?>
			
			<img src="<?=base_url();?>web/images/tourism_aventure.png">

		
	<?php }else if($type_tours == 2){ ?>

			<img src="<?=base_url();?>web/images/tourism_ecologic.jpg">
			
	<?php } ?>
	
 </h1>
<a href="<?=base_url('Planetres')?>"><strong>&laquo;</strong> Back to Results</a>	

</div>
</div>

<div id="tabs">
  <ul>
  	<li><a href="#tabs-0">Gallery</a></li>
    <li><a href="#tabs-1">Highlights</a></li>
    <li><a href="#tabs-2">Daily Itinerary</a></li>
    <li><a href="#tabs-3">Make a Reservation</a></li>
  </ul>

<div id="tabs-0">
	<?=$images_tours?>
</div>

<div id="tabs-1">
<h2 class="title-sub">Highlights</h2>
	<?=$highlights?>
</div>
  
<div id="tabs-2">
<h2 class="title-sub">Daily Itinerary</h2>
	<?=$itinerary?>
</div>

<div id="tabs-3">
	<?=form_open(base_url('Planetres/customize_tours'),'class="form-horizontal" id="formulario"')?>
	  <h1 class="tit3">Persons Availability and Prices</h1>
	    <p style="margin-top:20px; margin-bottom:0;">
	      <input name="idtours" type="hidden" value="<?=$idtours?>"> 
	      <div class="form-group">
	      	<label>Departure Date</label>
	      	<?=$departure_date?>
	      </div>
	      <div class="form-group">
	      	<label>Departure City</label>
	      	<?=$departure_city?>
	      </div>
	      <div class="form-group">
	      	<label>Hotel Class</label>
	      	<select name="hotel_class_tours" class="field3 rounded" id="hotel_class_tours">
	      		<option value="1">Moderate First Class</option>
	      		<option value="2">First Class</option>
	      	</select>
	      </div>
	      <div class="form-group">
	      	<input type="checkbox" value="1" name="private" id="private">&nbsp;Private
	      </div>
	      <div class="form-group">
	      	<label>Guests Tours</label>
	        <select name="guests_tours" class="field3 rounded" id="guests_tours"> 
	        	<option value="2" selected>2 Adult, 0 Children</option>
			    <option value="1">1 Adult, 0 Children</option>
			    <option value="0">More option</option>     
	  		</select>
		    <div id="xtra_form_tours">
					<div class="add-rows-nav"> 
	   					&nbsp; - &nbsp; 
	  					<a href="javascript:;" id="addnew_room_tours" class="rounded">
	  						<strong style="color: #090;">+</strong> Add Room
	  					</a> 
	  					<a href="javascript:;" id="removei_tours" class="rounded">
	  						<strong style="color: #C00;">x</strong> Delete Room
	  					</a>
	  				</div>
	  				<div id="pa_grid_tours">
	    				
	    					<div class="rowfields">
	    						Room <input id="roomcount_tours1" type="hidden" name="roomcount_tours1" value="1">1 &nbsp;    
	    						<select name="adult_tours1" id="adult_tours1" class="field6 rounded">
	      							<option value="0" selected>Adults (12+)</option>
	      							<option value="1" >1</option>
	      							<option value="2">2</option>
	      							<option value="3">3</option>
	    						</select>   
	    							&nbsp;
	    						<select name="kids_tours1" id="kids_tours1" class="field7 rounded">
		      						<option value="0" selected>Kids (0-11)</option>
		      						<option value="1">1</option>
	    						</select>
	  						</div>
	  				
					</div>
			</div>
		</div>
		
		<button type="button" name="submit" id="submit" value="Get Price" class="field4 rounded">Get Price</button>

		<?=form_submit($submit)?>
		

		<div id="price_per_person">

		</div>
		
	    </p>
	<?=form_close()?>
</div>
</div>
</section>
</div>
<script>
$( function() {
    $( "#departure_date_tours,#departure_city_tours,#hotel_class_tours,#private,#guests_tours" ).change(function() {     
         $.ajax({               
                  url: ' <?php echo base_url('client/Planetres/destroySession'); ?>',
                  type: "POST",
                  success: function (datos)
                  {
                      console.log("");
                  }
              });
      });
  });
$(document).ready(function(){
	$("#next").hide();
    $( "#departure_date_tours,#departure_city_tours,#hotel_class_tours,#private,#guests_tours,#adult_tours1,#adult_tours2,#adult_tours3,#kids_tours1,#kids_tours2,#kids_tours3" ).change(function() {     
	    
	    $("#next").hide();
	    $("#submit").show();
	   
    });
});

   	$(document).ready(function() {
  		var currentItem_tours = 2;
     	$('#guests_tours').on('change', function() {
      		var optionSelected_tour = $(this).find("option:selected");
      		var valueSelected_tour  = optionSelected_tour.val();
      		if (valueSelected_tour == 0) {
        		$("#xtra_form_tours").fadeIn('slow');  //.toggle('slow')
        		$('#guests_tours').removeClass('field3');
        		$('#guests_tours').addClass('field1');
      		}else{                          
        		$("#xtra_form_tours").fadeOut('fast');        
        		$('#guests_tours').removeClass('field1');
        		$('#guests_tours').addClass('field3');
      		}
    	});
    	$('#addnew_room_tours').click(function addroom_tours(){
     		if(currentItem_tours <= 3){
     			$('#items_tours').val(currentItem_tours);
      				var strToAdd = '<div class="rowfields">Room<input id="roomcount_tours'+currentItem_tours+'" type="hidden" name="roomcount_tours'+currentItem_tours+'" value="1">'+currentItem_tours+' &nbsp;<select name="adult_tours'+currentItem_tours+'" id="adult_tours'+currentItem_tours+'" class="field6 rounded"><option value="0">Adults (12+)</option><option value="1">1</option><option value="2">2</option><option value="3">3</option></select><select name="kids_tours'+currentItem_tours+'" id="kids_tours'+currentItem_tours+'" class="field7 rounded"><option value="0">Kids (0-11)</option><option value="1">1</option></select></div>';
        			currentItem_tours++;
    		}
    		$('#pa_grid_tours').append(strToAdd);
       	});
    	$('#removei_tours').click(function() {
      		if(currentItem_tours > 2){
        	$('.rowfields:last').remove();
        	currentItem_tours--; 
      		}
    	});
  	});
   
	$("#submit").click(function(){
        $.post("<?=base_url('client/Planetres/get_price_tours')?>", $("#formulario").serialize(), 
        function(htmlexterno){
        	$("#submit").hide();
            $("#price_per_person").html(htmlexterno);
            $("#next").show();
        });
    });

</script>
</body>
</html>
