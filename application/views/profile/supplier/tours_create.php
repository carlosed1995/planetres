<body>
  <div class="row">
   
   
        
            <!-- Content Header (Page header) -->
<section class="content-header">
<h1 align="center">
        Tours Create
    </h1>                
</section>

            <!-- Main content -->
<section class="content" id="maincontent">

                <!--div class="row">-->
        <div class="col-md-9">
            <div class="nav-tabs-custom">
            
                <div class="tab-content">
                                
                                    <form class="form-horizontal" action="insert_tours" method="post">
                                        <div class="form-group">
                                            
<label for="inputName" class="col-sm-2 control-label">
 Name Tour</label>

    <div class="col-sm-10">
<input type="text" class="form-control" id="name" name="name">
         </div>
       </div>
 <div class="form-group">
 <label for="inputName" class="col-sm-2 control-label">Code</label>

    <div class="col-sm-10">
        <input type="text" class="form-control" id="code" name="code">
    </div>
</div>
     <div class="form-group">
                                            
 <label for="inputName" class="col-sm-2 control-label">Location</label>

<div class="col-sm-10">
                <input type="text" class="form-control" id="location" name="location">
             </div>
         </div>
         <div class="form-group">
                                            
         <label for="inputName" class="col-sm-2 control-label">Highlights</label>

             <div class="col-sm-10">
                 <input type="text" class="form-control" id="highlights" name="highlights">
         </div>
        </div>
        <div class="form-group">
                                            
 <label for="inputName" class="col-sm-2 control-label">
Max persons</label>

 <div class="col-sm-10">
     <input type="text" class="form-control" id="max_persons" name="max_persons">
</div>
</div>
<div class="form-group">
                                            
 <label for="inputName" class="col-sm-2 control-label">
Tours type</label>

 <div class="col-sm-10">
 <select class="form-control" id="type" name="type">
     <option>Selected</option>

      <?php foreach ($tours_type->result() as $key) {  ?>
        <option value="<?=$key->idtours_type;?>"><?=$key->name;?></option>
  
       <? }  ?>
 </select>

</div>
</div>

 <div class="form-group">
<label for="inputName" class="col-sm-2 control-label">Active</label>


         <div class="checkbox">
         <label>
 <label class="radio-inline"> <input type="radio" name="status" value="1">Yes</label>

 <label class="radio-inline"><input type="radio" name="status" value="0"> No</label>
 
                </label>
             </div>
          </div>



</div>
<div class="form-group">

    
     <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                     <button type="submit" class="btn btn-danger">Submit</button>
                </div>
             </div>
         </form>
                               
                                <!-- /.tab-pane -->
</div>
                            <!-- /.tab-content -->
    </div>
                        <!-- /.nav-tabs-custom -->
 </div>
                    <!-- /.col -->
                <!--</div>-->
                <!-- /.row -->

            </section>
            <!-- /.content -->
        </div>
</body>