

  <body>
  <div class="row">
   
    
        
            <!-- Content Header (Page header) -->
<section class="content-header">
<h1 align="center">
       
    </h1>                
</section>

            <!-- Main content -->
<section class="content" id="maincontent">

                <!--div class="row">-->
        <div class="col-md-9">
            <div class="nav-tabs-custom">
                
                <div class="tab-content">
                                
                                    <form class="form-horizontal" action="insert_hotel" method="post">
                                        <div class="form-group">
                                            
<label for="inputName" class="col-sm-2 control-label">
Name Hotel</label>

    <div class="col-sm-10">
<input type="text" name="name" class="form-control" id="" value="" >
         </div>
       </div>
 <div class="form-group">
 <label for="inputName" class="col-sm-2 control-label">Address hotel</label>

    <div class="col-sm-10">
        <input type="text" name="address" class="form-control" id="" value="">
    </div>
</div>
     <div class="form-group">
                                            
 <label for="inputName" class="col-sm-2 control-label">
Hotel description</label>

<div class="col-sm-10">
                <input type="text" name="description" class="form-control" id="" value="">
             </div>
         </div>
    
        <div class="form-group">
                                            
 <label for="inputName" class="col-sm-2 control-label">
Area information</label>

 <div class="col-sm-10">
     <input type="text" name="information" class="form-control" id="" value="">
</div>
</div>
<div class="form-group">
                                            
 <label for="inputName" class="col-sm-2 control-label">
Map</label>

 <div class="col-sm-10">
     <input type="text" name="map" class="form-control" id="" value="">
</div>
</div>

<div class="form-group">
                                            
 <label for="inputName" class="col-sm-2 control-label">
Location</label>

 <div class="col-sm-10">
     <input type="text" name="location" class="form-control" id="" value="">
</div>
</div>

<div class="form-group">
                                            
 <label for="inputName" class="col-sm-2 control-label">
Country</label>

 <div class="col-sm-10">
     <select id="country" name="country" class="form-control" >
    
     <option value="">Country</option>

       <?php foreach ($country->result() as $key) {  ?>
        <option value="<?=$key->idcountry;?>"><?=$key->name;?></option>
  
       <? }  ?>

  
  </select>
</div>
</div>
<div class="form-group">
                                            
 <label for="inputName" class="col-sm-2 control-label">
City</label>

 <div class="col-sm-10">
     <select name="city" id="city" class="form-control">
    
     <option value="">City</option>
    
  
  
  </select>
</div>
</div>

<div class="form-group">
                                            
 <label for="inputName" class="col-sm-2 control-label">
Category</label>

 <div class="col-sm-10">
      <select name="category" class="form-control" id="category">
    
     <option value="">Category</option>
       <?php foreach ($category->result() as $key) {  ?>
        <option value="<?=$key->idcategory;?>"><?=$key->name;?></option>
  
       <? }  ?>
  
  
  </select>
</div>
</div>

 <div class="form-group">
                                            
 <label for="inputName" class="col-sm-2 control-label">
Hotel image</label>

 <div class="col-sm-10">
     <input type="file" id="" value="" multiple class="file file-loading" data-allowed-file-extensions='["jpg", "png", "jpeg"]'>
</div>
</div>


<div class="form-group">
<label for="inputName" class="col-sm-2 control-label">
Status hotel  </label>


         <div class="checkbox">
         <label>
 <label class="radio-inline"> <input type="radio" name="status" value="1">Active</label>

 <label class="radio-inline"><input type="radio" name="status" value="0">Inactive</label>

                </label>
             </div>
          </div>



     <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                     <button type="submit" class="btn btn-danger">Submit</button>
                </div>
             </div>
         </form>
                               
                                <!-- /.tab-pane -->
</div>
                            <!-- /.tab-content -->
    </div>
                        <!-- /.nav-tabs-custom -->
 </div>
                    <!-- /.col -->
                <!--</div>-->
                <!-- /.row -->

            </section>
            <!-- /.content -->
        </div>

        <div id="resultados_de_busqueda">
            
        </div>
</body>
<script>
    
 $(function () {
     $('#country').on('change', function(e) {
      e.preventDefault();
      busqueda_city();                        
    });
  });

      
      function busqueda_city(){

             var country=$('select[id=country]').val();

       $.ajax({
                        type: "POST",
                        url: "<?= base_url(); ?>Profiles_planetres/city_hotel",
                        data: {country: country }
                    }).done(function (response) {
                            if(response){
                                $('#city').html(response);
                            
                  }                                
         });
      
      }
     

</script>
