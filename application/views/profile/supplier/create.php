<body>
  <div class="row">
        
            <!-- Content Header (Page header) -->
<section class="content-header">
<h1 align="center">
        Supplier registration
    </h1>                
</section>

            <!-- Main content -->
<section class="content" id="maincontent">

                <!--div class="row">-->
        <div class="col-md-9">
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#Personal_info" data-toggle="tab">Personal Info</a></li>
                    <li><a href="#Business_info" data-toggle="tab">Info of Business</a></li>
                </ul>
                <div class="tab-content">
                                
                                    <form action="insert_supplier" method="post" id="" class="form-horizontal">
                                        <div align="center"><h3>Supplier Personal Info </h3></div>
 <div class="form-group">
 <label for="inputName" class="col-sm-2 control-label">First name</label>

    <div class="col-sm-10">
        <input type="text" name="first_name" class="form-control" id="" value="">
    </div>
</div>
     <div class="form-group">
                                            
 <label for="inputName" class="col-sm-2 control-label">Middle name</label>

<div class="col-sm-10">
                <input type="text" name="middle_name" class="form-control" id="" value="">
             </div>
         </div>
         <div class="form-group">
                                            
         <label for="inputName" class="col-sm-2 control-label">Last name</label>

             <div class="col-sm-10">
                 <input type="text" name="last_name" class="form-control" id="" value="">
         </div>
        </div>
        <div class="form-group">
                                            
 <label for="inputName" class="col-sm-2 control-label">
Personal Address</label>

 <div class="col-sm-10">
     <input type="text" name="address" class="form-control" id="" value="">
</div>
</div>
<div class="form-group">
                                            
 <label for="inputName" class="col-sm-2 control-label">
Personal city</label>

 <div class="col-sm-10">
     <input type="text" name="city" class="form-control" id="" value="">
</div>
</div>
<div class="form-group">
                                            
 <label for="inputName" class="col-sm-2 control-label">
Personal state</label>

 <div class="col-sm-10">
     <input type="text" name="state" class="form-control" id="" value="">
</div>
</div>
<div class="form-group">
                                            
 <label for="inputName" class="col-sm-2 control-label">
Personal country</label>

 <div class="col-sm-10">
     <input type="text" name="country" class="form-control" id="" value="">
</div>
</div>
<div class="form-group">
                                            
 <label for="inputName" class="col-sm-2 control-label">
Personal zip</label>

 <div class="col-sm-10">
     <input type="text" name="zip" class="form-control" id="" value="">
</div>
</div>
<div class="form-group">
                                            
 <label for="inputName" class="col-sm-2 control-label">
Personal email</label>

 <div class="col-sm-10">
     <input type="text" name="email" class="form-control" id="" value="">
</div>
</div>
<div class="form-group">
                                            
 <label for="inputName" class="col-sm-2 control-label">
Personal phone</label>

 <div class="col-sm-10">
     <input type="text" name="phone" class="form-control" id="" value="">
</div>
</div>
<div class="form-group">
                                            
 <label for="inputName" class="col-sm-2 control-label">
Personal skype</label>

 <div class="col-sm-10">
     <input type="text" name="skype" class="form-control" id="" value="">
</div>
</div>
<div class="form-group">
                                            
 <label for="inputName" class="col-sm-2 control-label">
Personal whatsapp</label>

 <div class="col-sm-10">
     <input type="text" name="whatsapp" class="form-control" id="" value="">
</div>
</div>
<div align="center"><h3>Supplier Business </h3></div>
<div class="form-group">
                                            
 <label for="inputName" class="col-sm-2 control-label">
Supplier Business info</label>
<br>

 <div class="col-sm-10">
     <input type="text" name="" class="form-control" id="" value="">
</div>
</div>
<div class="form-group">
                                            
 <label for="inputName" class="col-sm-2 control-label">
Business name</label>

 <div class="col-sm-10">
     <input type="text" name="business_name" class="form-control" id="" value="">
</div>
</div>
<div class="form-group">
                                            
 <label for="inputName" class="col-sm-2 control-label">
Business registration name</label>

 <div class="col-sm-10">
     <input type="text" name="registration_name" class="form-control" id="" value="">
</div>
</div>
<div class="form-group">
                                            
 <label for="inputName" class="col-sm-2 control-label">
Business web site</label>

 <div class="col-sm-10">
     <input type="text" name="website" class="form-control" id="" value="">
</div>
</div>

<div class="form-group">
                                            
 <label for="inputName" class="col-sm-2 control-label">
Business address</label>

 <div class="col-sm-10">
     <input type="text" name="business_address" class="form-control" id="" value="">
</div>
</div>

<div class="form-group">
                                            
 <label for="inputName" class="col-sm-2 control-label">
Business city</label>

 <div class="col-sm-10">
     <input type="text" name="business_city" class="form-control" id="" value="">
</div>
</div>
<div class="form-group">
                                            
 <label for="inputName" class="col-sm-2 control-label">
Business state</label>

 <div class="col-sm-10">
     <input type="text" name="business_state" class="form-control" id="" value="">
</div>
</div>
<div class="form-group">
                                            
 <label for="inputName" class="col-sm-2 control-label">
Business country</label>

 <div class="col-sm-10">
     <input type="text" name="business_country" class="form-control" id="" value="">
</div>
</div>

<div class="form-group">
                                            
 <label for="inputName" class="col-sm-2 control-label">
Business zip</label>

 <div class="col-sm-10">
     <input type="text" name="business_zip"  class="form-control" id="" value="">
</div>
</div>
<div class="form-group">
                                            
 <label for="inputName" class="col-sm-2 control-label">
Business email</label>

 <div class="col-sm-10">
     <input type="text" name="business_email"  class="form-control" id="" value="">
</div>
</div>

<div class="form-group">
                                            
 <label for="inputName" class="col-sm-2 control-label">
Business phone</label>

 <div class="col-sm-10">
     <input type="text" name="business_phone" class="form-control" id="" value="">
</div>
</div>

<div class="form-group">
                                            
 <label for="inputName" class="col-sm-2 control-label">
Business skype</label>

 <div class="col-sm-10">
     <input type="text" name="business_skype" class="form-control" id="" value="">
</div>
</div>
<div class="form-group">
                                            
 <label for="inputName" class="col-sm-2 control-label">
Business whatsapp</label>

 <div class="col-sm-10">
     <input type="text" name="business_whatsapp" class="form-control" id="" value="">
</div>
</div>

                                            
 
<div class="form-group">
<label for="inputName" class="col-sm-2 control-label">
Tour products</label>


         <div class="checkbox">
         <label>
 <label class="radio-inline"> <input type="checkbox" name="tours" value="yes">Yes</label>

 <label class="radio-inline"><input type="checkbox" name="tours" value="no"> No</label>
 
                </label>
             </div>
          </div>
     <div class="form-group">
<label for="inputName" class="col-sm-2 control-label">Excursions   products </label>


         <div class="checkbox">
         <label>
 <label class="radio-inline"> <input type="checkbox" name="excursions" value="yes">Yes</label>

 <label class="radio-inline"><input type="checkbox" name="excursions" value="no"> No</label>
 
                </label>
             </div>
          </div>
          <div class="form-group">
<label for="inputName" class="col-sm-2 control-label">
Transfer   products  </label>


         <div class="checkbox">
         <label>
 <label class="radio-inline"> <input type="checkbox" name="transfer" value="yes">Yes</label>

 <label class="radio-inline"><input type="checkbox" name="transfer" value="no"> No</label>

                </label>
             </div>
          </div>
          <div class="form-group">
<label for="inputName" class="col-sm-2 control-label">
 Hotel products</label>


         <div class="checkbox">
         <label>
 <label class="radio-inline"> <input type="checkbox" name="hotel" value="yes">Yes</label>

 <label class="radio-inline"><input type="checkbox" name="hotel" value="no"> No</label>
 
                </label>
             </div>
          </div>

          <div class="form-group">
                                            
 <label for="inputName" class="col-sm-2 control-label">
Paypal account</label>

 <div class="col-sm-10">
     <input type="text" name="paypal" class="form-control" id="" value="">
</div>
</div>

<div class="form-group">
                                            
 <label for="inputName" class="col-sm-2 control-label">
Bank name</label>

 <div class="col-sm-10">
     <input type="text" name="bank_name" class="form-control" id="" value="">
</div>
</div>

<div class="form-group">
                                            
 <label for="inputName" class="col-sm-2 control-label">
Bank account name</label>

 <div class="col-sm-10">
     <input type="text" name="bank_acc_name" class="form-control" id="" value="">
</div>
</div>
<div class="form-group">
                                            
 <label for="inputName" class="col-sm-2 control-label">
Bank account number</label>

 <div class="col-sm-10">
     <input type="text" name="acc_num" class="form-control" id="" value="">
</div>
</div>

<div class="form-group">
                                            
 <label for="inputName" class="col-sm-2 control-label">
Bank swift number</label>

 <div class="col-sm-10">
     <input type="text" name="swift_num" class="form-control" id="" value="">
</div>
</div>
                                      
 <div class="form-group">
 <div class="col-sm-offset-2 col-sm-10">
         <div class="checkbox">
         <label>
  <input type="checkbox"> I agree to the <a href="#">terms and conditions</a>
                </label>
             </div>
          </div>
     </div>
     <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                     <button type="submit" class="btn btn-danger">Submit</button>
                </div>
             </div>
         </form>
                               
                                <!-- /.tab-pane -->
</div>
                            <!-- /.tab-content -->
    </div>
                        <!-- /.nav-tabs-custom -->
 </div>
                    <!-- /.col -->
                <!--</div>-->
                <!-- /.row -->

            </section>
            <!-- /.content -->
        </div>
</body>