<?
	/*echo '<pre>';
	print_r ($data);
	echo '</pre>';*/
?>
<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
<body>
	<div id="page_customize">
		<section id="content">
			<div class="row">
				<div class="col-sm-12">
					<strong>Names exactly as they appear on your passports</strong>
					<?=$table_passengers?>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-6">
					<?=$price_chart?>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-12">
				<strong>Review Itinerary</strong>
					<?=$review_itinerary?>
				</div>
			</div>
		</section>
	</div>
	<script>
	$( function start_hotel() {
		var count = '<?=$person_total?>';
		for(var i=1;i<=count;i++){
  		$("#birthday"+i).datepicker({

                        maxDate: new Date(), 
                        changeYear: true, 
                        changeMonth: true,
                        minDate: new Date(1962,12,31),
                        dateFormat: 'yy-mm-dd',
                        constrainInput: true
                        });
  		}
    });
	$(document).ready(function(){
		var count = '<?=$person_total?>';
			$('#active1').click( function() {
	    		//alert($(this).is(":checked"));
	    			if($(this).is(":checked")){
	    				name = $('#name1').val();
						last_name = $('#last_name1').val();
						$('#passenger1').append('<option>'+name+' '+last_name+'</option>');
	    			}else{
	    				$('#passenger1').html('');
	    			}
			});

			$('#active2').click( function() {
	    		//alert($(this).is(":checked"));
	    			if($(this).is(":checked")){
	    				name = $('#name2').val();
						last_name = $('#last_name2').val();
						$('#passenger2').append('<option>'+name+' '+last_name+'</option>');
	    			}else{
	    				$('#passenger2').html('');
	    			}
			});

			$('#active3').click( function() {
	    		//alert($(this).is(":checked"));
	    			if($(this).is(":checked")){
	    				name = $('#name3').val();
						last_name = $('#last_name3').val();
						$('#passenger3').append('<option>'+name+' '+last_name+'</option>');
	    			}else{
	    				$('#passenger3').html('');
	    			}
			});

			$('#active4').click( function() {
	    		//alert($(this).is(":checked"));
	    			if($(this).is(":checked")){
	    				name = $('#name4').val();
						last_name = $('#last_name4').val();
						$('#passenger4').append('<option>'+name+' '+last_name+'</option>');
	    			}else{
	    				$('#passenger4').html('');
	    			}
			});

			$('#active5').click( function() {
	    		//alert($(this).is(":checked"));
	    			if($(this).is(":checked")){
	    				name = $('#name5').val();
						last_name = $('#last_name5').val();
						$('#passenger5').append('<option>'+name+' '+last_name+'</option>');
	    			}else{
	    				$('#passenger5').html('');
	    			}
			});

			$('#active6').click( function() {
	    		//alert($(this).is(":checked"));
	    			if($(this).is(":checked")){
	    				name = $('#name6').val();
						last_name = $('#last_name6').val();
						$('#passenger6').append('<option>'+name+' '+last_name+'</option>');
	    			}else{
	    				$('#passenger6').html('');
	    			}
			});

			$('#active7').click( function() {
	    		//alert($(this).is(":checked"));
	    			if($(this).is(":checked")){
	    				name = $('#name7').val();
						last_name = $('#last_name7').val();
						$('#passenger7').append('<option>'+name+' '+last_name+'</option>');
	    			}else{
	    				$('#passenger7').html('');
	    			}
			});

			$('#active8').click( function() {
	    		//alert($(this).is(":checked"));
	    			if($(this).is(":checked")){
	    				name = $('#name8').val();
						last_name = $('#last_name8').val();
						$('#passenger8').append('<option>'+name+' '+last_name+'</option>');
	    			}else{
	    				$('#passenger8').html('');
	    			}
			});

			$('#active9').click( function() {
	    		//alert($(this).is(":checked"));
	    			if($(this).is(":checked")){
	    				name = $('#name9').val();
						last_name = $('#last_name9').val();
						$('#passenger9').append('<option>'+name+' '+last_name+'</option>');
	    			}else{
	    				$('#passenger9').html('');
	    			}
			});

			$('#active10').click( function() {
	    		//alert($(this).is(":checked"));
	    			if($(this).is(":checked")){
	    				name = $('#name10').val();
						last_name = $('#last_name10').val();
						$('#passenger10').append('<option>'+name+' '+last_name+'</option>');
	    			}else{
	    				$('#passenger10').html('');
	    			}
			});

			$('#active11').click( function() {
	    		//alert($(this).is(":checked"));
	    			if($(this).is(":checked")){
	    				name = $('#name11').val();
						last_name = $('#last_name11').val();
						$('#passenger11').append('<option>'+name+' '+last_name+'</option>');
	    			}else{
	    				$('#passenger11').html('');
	    			}
			});

			$('#active12').click( function() {
	    		//alert($(this).is(":checked"));
	    			if($(this).is(":checked")){
	    				name = $('#name12').val();
						last_name = $('#last_name12').val();
						$('#passenger12').append('<option>'+name+' '+last_name+'</option>');
	    			}else{
	    				$('#passenger12').html('');
	    			}
			});

	});
	</script>
</body>