<?
    $submit = array(
      'value' => 'submit',
      'name'  => 'submit',
      'id'    => 'submit',
      'class' => 'field4 rounded'
    );
    $hidden_lat = array(
        'type'  => 'hidden',
        'name'  => 'gps-lat',
        'id'    => 'gps-lat',
        'value' => $hidden_lat
    );
    $hidden_long = array(
        'type'  => 'hidden',
        'name'  => 'gps-long',
        'id'    => 'gps-long',
        'value' => $hidden_long
    );

    $name = array(
        'name'          => 'name',
        'id'            => 'name',
        'value'         => $name,
        'class'         => 'field1b rounded',
        'placeholder'   => 'Insert Name'
    );
    $middle_name = array(
        'name'          => 'middle_name',
        'id'            => 'middle_name',
        'value'         => $middle_name,
        'class'         => 'field1b rounded',
        'placeholder'   => 'Insert Middle Name'
    );
    $last_name = array(
        'name'          => 'last_name',
        'id'            => 'last_name',
        'value'         => $last_name,
        'class'         => 'field1b rounded',
        'placeholder'   => 'Insert Last Name'
    );
    $email = array(
        'type'          => 'email',
        'name'          => 'email',
        'id'            => 'email',
        'value'         => $email,
        'class'         => 'field1b rounded',
        'placeholder'   => 'Insert email'
    );
    $confirm_email = array(
        'type'          => 'email',
        'name'          => 'confirm_email',
        'id'            => 'confirm_email',
        'value'         => $confirm_email,
        'class'         => 'field1b rounded',
        'placeholder'   => 'Insert confirm email'
    );
    $password = array(
        'type'          => 'password',
        'name'          => 'password',
        'id'            => 'password',
        'value'         => $password,
        'class'         => 'field1b rounded',
        'placeholder'   => 'Insert Password'
    );
    $confirm_password = array(
        'type'          => 'password',
        'name'          => 'confirm_password',
        'id'            => 'confirm_password',
        'value'         => $confirm_password,
        'class'         => 'field1b rounded',
        'placeholder'   => 'Insert confirm password'
    );
    $phone = array(
        'name'          => 'phone',
        'id'            => 'phone',
        'value'         => $phone,
        'class'         => 'field1b rounded',
        'placeholder'   => 'Insert phone'
    );
    $genere1 = array(
        'type'          => 'radio',
        'name'          => 'genere',
        'id'            => 'genere1',
        'value'         => 'Male'
    );
    $genere2 = array(
        'type'          => 'radio',
        'name'          => 'genere',
        'id'            => 'genere2',
        'value'         => 'Female'
    );
    $zip_code = array(
        'name'          => 'zip_code',
        'id'            => 'zip_code',
        'value'         => $zip_code,
        'class'         => 'field1b rounded',
        'placeholder'   => 'Insert zip code'
    );
?>
<!doctype html>
<html>
  <head>
    <meta charset="iso-8859-2">
    <title>Planetres - Register</title>
    <link href="https://fonts.googleapis.com/css?family=Comfortaa" rel="stylesheet">
    <link href="<?=base_url();?>web/css/forms.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="https://maps.googleapis.com/maps/api/js?libraries=places&key=AIzaSyDPZ77RZzHOf4g5hmREiu0pAA9Kdse2BUI"></script>
    <script src="<?=base_url();?>web/js/jquery.mask.min.js"></script>
    <script>
        function initialize() {
        var select = document.getElementById("nationality");
        var address = select.options[select.selectedIndex].value;

        //var address = loc;
        geocoder = new google.maps.Geocoder();
        geocoder.geocode({
        'address': address
        }, function(results, status) {      
        	var lat=document.getElementById("gps-lat").value=results[0].geometry.location.lat();    
        	var lng=document.getElementById("gps-long").value=results[0].geometry.location.lng();        
        });
        }
        google.maps.event.addDomListener(window, 'load', initialize);
    </script>

      <script>
  $(document).ready(function() {
      $('#birth').mask('0000-00-00');
      $('#name,#lastname').mask('SSSSSSSSSSSSSSSSSSS', {'translation': 
        {
          S: {pattern: /[A-Za-z]/},                                        
        }
      });
      $('#email').mask("AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA", {
        translation: {
          "A": { pattern: /[\w@\-.]/, recursive: true }
        }
      });

      $('#zip').mask('0000000000');

      $('#phone').mask('00000000000000000');

      $('#password').mask("AAAAAAAAAAAAAAAA", {
        translation: {
          "A": { pattern: /[A-Za-z0-9]/, recursive: true }
        }
      });

      $('#passport').mask("AAAAAAAAAAAAAAAA", {
        translation: {
          "A": { pattern: /[A-Za-z0-9]/, recursive: true }
        }
      });

      $('#city').mask('SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS', {'translation': 
        {
          S: {pattern: /[A-Za-z, ]/},                                        
        }
      });                     

      $('#email,#passport,#password,#city').bind("cut copy paste",function(e) {
        e.preventDefault();
        alert('We are sorry, but you can not do this action, for security reasons we have blocked it.');
      });
  });
      </script>
  </head>
  <body>      
    <?=form_open(base_url('Register/validate'),'class="form-horizontal" id="formulario"')?>
      <?=form_input($hidden_lat)?>
      <?=form_input($hidden_long)?>
      <table width="90%" border="0" align="center" cellpadding="5" cellspacing="0" style="color:#666;">
        <tr>
          <td>
            <?if ($this->session->flashdata('success')) { ?>
              <div class="alert alert-success">
                <a href="#" class="close" data-dismiss="alert">&times;</a>
                <strong>Success!</strong> <?php echo $this->session->flashdata('success'); ?>
              </div>
            
          </td>
        </tr>
        <tr>
          <td>
            <?}elseif($this->session->flashdata('error')) { ?>
              <div class="alert alert-danger">
                <a href="#" class="close" data-dismiss="alert">&times;</a>
                <strong>Error!</strong> <?php echo $this->session->flashdata('error'); ?>
              </div>
            <?}?>
          </td>
        </tr>
        <tr>
          <td colspan="2" align="center">
            <h1 class="tit2" style="padding-top:10px;">Register</h1>
            <div class="head-details h-detail-l">
              <div class="a-half">
                <a href="<?=base_url('Planetres')?>"><strong>&laquo;</strong> Back to Home</a>
              </div>
            </div>
          </td>
        </tr>
        <tr>
          <td width="25%" align="right">Register as</td>
          <td width="75%" align="left"><?=$register_as?></td>
        </tr>                
        <tr>
          <td width="25%" align="right">Name</td>
          <td width="75%" align="left"><?=form_input($name)?></td>
        </tr>
        <tr>
          <td align="right">Middle Name</td>
          <td align="left"><?=form_input($middle_name)?></td>
        </tr>
        <tr>
          <td align="right">Last Name</td>
          <td align="left"><?=form_input($last_name)?></td>
        </tr>
        <tr>
          <td align="right">Nationality</td>
          <td align="left"><?=$nationality?></td>
        </tr>               
        <tr>
          <td align="right">City</td>
          <td align="left"><?=$city_idcity?></td>
        </tr>
        <tr>
          <td align="right">Email</td>
          <td align="left"><?=form_input($email)?></td>
        </tr>
        <tr>
          <td align="right">Confirm email</td>
          <td align="left"><?=form_input($confirm_email)?></td>
        </tr>
        <tr>
          <td align="right">Password</td>
          <td align="left"><?=form_input($password)?></td>
        </tr>
        <tr>
          <td align="right">Confirm Password</td>
          <td align="left"><?=form_input($confirm_password)?></td>
        </tr>
        <tr>
          <td align="right">Phone</td>
          <td align="left"><?=form_input($phone)?></td>
        </tr>
        <tr>
          <td align="right">Gender</td>
          <td align="left" style="color:#15367b;" id="genere">
            <?=form_input($genere1)?>
              <label for="genere1">Male</label> 
            <?=form_input($genere2)?>
              <label for="genere2">Female</label>
          </td>
        </tr>
        <tr>
          <td align="right">Birth</td>
          <td align="left"><?= $year . $month . $day; ?></td>
        </tr>
        <tr>
          <td align="right">Zip code</td>
          <td align="left"><?=form_input($zip_code)?></td>
        </tr>
        <tr>
          <td colspan="2" align="center">
            <?=form_submit($submit)?>
          </td>
        </tr>
      </table>
    <?=form_close()?>
  </body>
  <script>
  $(function () {
     $('#nationality').on('change', function(e) {
      e.preventDefault();
      share_city();                        
    });
  });
  function share_city(){
    var country=$('select[id=nationality]').val();
    $.ajax({
      type: "POST",
      url: "<?= base_url(); ?>Planetres/city_filter",
      data: {country: country }
    }).done(function (response) {
      $('#city_idcity').html('');
        if(response){
          $('#city_idcity').append(response);
        }                                
      });
  }
  </script>
</html>