<body>
<div id="page">

<header>
<div class="header-bg">
<div id="logo"><a href="#"><img src="<?= base_url(); ?>web/images/logo.jpg" alt="Planetres"></a></div>
<div class="header-in">
<div class="header-1r">
<div class="phone">0800-514-7899</div>
<div class="log"><img src="<?= base_url(); ?>web/images/icon-user.jpg" width="23" height="22"> <a href="<?= base_url(); ?>index.php/planetres/admin/">Login</a> &nbsp; | &nbsp; <a href="#">Register</a> &nbsp; | &nbsp; <a href="#">Cart</a> <img src="<?= base_url(); ?>web/images/icon-cart.jpg" width="29" height="23"></div>
</div>
<div id="menu"><a href="#">Destinations</a> <a href="#">Services</a> <a href="#">Especials</a> <a href="#">Contact Us</a></div>
</div>
</div>
</header>

<section id="reservations">
<div id="form-reserv">
  <form name="form1" method="POST" action="#">
    <input name="search" type="text" class="field1 rounded" id="search" placeholder="Search*">
    <input type="text" name="start" class="field2 rounded" id="start" placeholder="Start">
    <input type="text" name="end" class="field2 rounded" id="end" placeholder="End">
    <select name="guests" class="field3 rounded" id="guests">
      <option>Guest</option>
      <option>Guest 1</option>
      <option>Guest 2</option>
      <option>Guest 3</option>
    </select>
    <input name="button" type="submit" class="field4 rounded" id="button" value="SEARCH">
  </form>
</div>
<div id="options">
<a href="<?=base_url()?>index.php/Planetres/template_hotel" style="background-image: url(<?= base_url(); ?>web/images/but-key.jpg)">Hotels</a> <a href="#" style="background-image: url(<?= base_url(); ?>web/images/but-suite.jpg)">Tours</a> <a href="#" style="background-image: url(<?= base_url(); ?>web/images/but-bag.jpg)">Excursion</a> <a href="#" style="background-image: url(<?= base_url(); ?>web/images/but-cars.jpg)">Transfer</a></div>
</section>

<section id="order-rrss">
<div class="half ssnn">
<a href="#"><img src="<?= base_url(); ?>web/images/ico-fb.jpg" width="25" height="25"></a><a href="#"><img src="<?= base_url(); ?>web/images/ico-tw.jpg" width="25" height="25"></a><a href="#"><img src="<?= base_url(); ?>web/images/ico-03.jpg" width="25" height="25"></a><a href="#"><img src="<?= base_url(); ?>web/images/ico-pint.jpg" width="25" height="25"></a><a href="#"><img src="<?= base_url(); ?>web/images/ico-g.jpg" width="25" height="25"></a><a href="#"><img src="<?= base_url(); ?>web/images/ico-lin.jpg" width="25" height="25"></a><a href="#"><img src="<?= base_url(); ?>web/images/ico-yt.jpg" width="25" height="25"></a> &nbsp; &laquo; Compartir
</div>
<div class="half" style="text-align:right;">
<select name="order" class="field5 rounded" id="order">
      <option>Order by:</option>
      <option>Order 1</option>
      <option>Order 2</option>
      <option>Order 3</option>
    </select>
</div>
</section>

<section id="resultados">
<div class="r-row rounded">
<a href="#"><img src="<?= base_url(); ?>web/images/thumbnail.jpg" class="thumb rounded"></a>      
<div class="description">
<h3>Sunny Suites</h3>
<p>This stunning hotel boasts a spectacular setting in London, amidst the sophistication of Seven spectacular setting in London Dials village, amidst the sophistication of Seven Dials village a spectacular setting in London.</p>
<p><img src="<?= base_url(); ?>web/images/icon-star.png" width="12" height="12"> <img src="<?= base_url(); ?>web/images/icon-star.png" width="12" height="12"> <img src="<?= base_url(); ?>web/images/icon-star.png" width="12" height="12"></p>
</div>
<div class="price rounded">
<strong>$255.31</strong>
<p>Per person</p>
<a href="#" class="rounded">Reserve</a> </div>
</div>

<div class="r-row rounded">
<a href="#"><img src="<?= base_url(); ?>web/images/thumbnail.jpg" class="thumb rounded"></a>
<div class="description">
<h3>Sunny Suites</h3>
<p>This stunning hotel boasts a spectacular setting in London, amidst the sophistication of Seven spectacular setting in London Dials village, amidst the sophistication of Seven Dials village a spectacular setting in London.</p>
<p><img src="<?= base_url(); ?>web/images/icon-star.png" width="12" height="12"> <img src="<?= base_url(); ?>web/images/icon-star.png" width="12" height="12"> <img src="<?= base_url(); ?>web/images/icon-star.png" width="12" height="12"></p>
</div>
<div class="price rounded">
<strong>$255.31</strong>
<p>Per person</p>
<a href="#" class="rounded">Reserve</a> </div>
</div>

<div class="r-row rounded">
<a href="#"><img src="<?= base_url(); ?>web/images/thumbnail.jpg" class="thumb rounded"></a>
<div class="description">
<h3>Sunny Suites</h3>
<p>This stunning hotel boasts a spectacular setting in London, amidst the sophistication of Seven spectacular setting in London Dials village, amidst the sophistication of Seven Dials village a spectacular setting in London.</p>
<p><img src="<?= base_url(); ?>web/images/icon-star.png" width="12" height="12"> <img src="<?= base_url(); ?>web/images/icon-star.png" width="12" height="12"> <img src="<?= base_url(); ?>web/images/icon-star.png" width="12" height="12"></p>
</div>
<div class="price rounded">
<strong>$255.31</strong>
<p>Per person</p>
<a href="#" class="rounded">Reserve</a> </div>
</div>

<div class="r-row rounded">
<a href="#"><img src="<?= base_url(); ?>web/images/thumbnail.jpg" class="thumb rounded"></a>
<div class="description">
<h3>Sunny Suites</h3>
<p>This stunning hotel boasts a spectacular setting in London, amidst the sophistication of Seven spectacular setting in London Dials village, amidst the sophistication of Seven Dials village a spectacular setting in London.</p>
<p><img src="<?= base_url(); ?>web/images/icon-star.png" width="12" height="12"> <img src="<?= base_url(); ?>web/images/icon-star.png" width="12" height="12"> <img src="<?= base_url(); ?>web/images/icon-star.png" width="12" height="12"></p>
</div>
<div class="price rounded">
<strong>$255.31</strong>
<p>Per person</p>
<a href="#" class="rounded">Reserve</a> </div>
</div>

<div class="r-row rounded">
<a href="#"><img src="<?= base_url(); ?>web/images/thumbnail.jpg" class="thumb rounded"></a>
<div class="description">
<h3>Sunny Suites</h3>
<p>This stunning hotel boasts a spectacular setting in London, amidst the sophistication of Seven spectacular setting in London Dials village, amidst the sophistication of Seven Dials village a spectacular setting in London.</p>
<p><img src="<?= base_url(); ?>web/images/icon-star.png" width="12" height="12"> <img src="<?= base_url(); ?>web/images/icon-star.png" width="12" height="12"> <img src="<?= base_url(); ?>web/images/icon-star.png" width="12" height="12"></p>
</div>
<div class="price rounded">
<strong>$255.31</strong>
<p>Per person</p>
<a href="#" class="rounded">Reserve</a> </div>
</div>

<div class="r-row rounded">
<a href="#"><img src="<?= base_url(); ?>web/images/thumbnail.jpg" class="thumb rounded"></a>
<div class="description">
<h3>Sunny Suites</h3>
<p>This stunning hotel boasts a spectacular setting in London, amidst the sophistication of Seven spectacular setting in London Dials village, amidst the sophistication of Seven Dials village a spectacular setting in London.</p>
<p><img src="<?= base_url(); ?>web/images/icon-star.png" width="12" height="12"> <img src="<?= base_url(); ?>web/images/icon-star.png" width="12" height="12"> <img src="<?= base_url(); ?>web/images/icon-star.png" width="12" height="12"></p>
</div>
<div class="price rounded">
<strong>$255.31</strong>
<p>Per person</p>
<a href="#" class="rounded">Reserve</a> </div>
</div>

<div class="r-row rounded">
<a href="#"><img src="<?= base_url(); ?>web/images/thumbnail.jpg" class="thumb rounded"></a>
<div class="description">
<h3>Sunny Suites</h3>
<p>This stunning hotel boasts a spectacular setting in London, amidst the sophistication of Seven spectacular setting in London Dials village, amidst the sophistication of Seven Dials village a spectacular setting in London.</p>
<p><img src="<?= base_url(); ?>web/images/icon-star.png" width="12" height="12"> <img src="<?= base_url(); ?>web/images/icon-star.png" width="12" height="12"> <img src="<?= base_url(); ?>web/images/icon-star.png" width="12" height="12"></p>
</div>
<div class="price rounded">
<strong>$255.31</strong>
<p>Per person</p>
<a href="#" class="rounded">Reserve</a> </div>
</div>
</section>

