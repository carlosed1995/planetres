<!doctype html>
<html>
<head>
<meta charset="utf-8" />
<title>Planetres</title>
<?php include ('includes/header-lib.php') ?>
<script>
  $( function() {
    $("#start,#end").datepicker({ dateFormat: 'yy-mm-dd' });
  } );
  
	$("[data-fancybox]").fancybox({
	iframe : {
		css : {
			width : '480px'
		}
	}
	});
</script>
</head>

<body>
<div id="page">

<?php include ('includes/header.php') ?>

<section id="content">
<h1 class="title-terms">Privacy Policy</h1>
  <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque vestibulum erat sit amet ultricies facilisis. Nam porta sodales purus, blandit vehicula quam imperdiet ac. Nam in risus massa. Nullam feugiat facilisis lorem tempus feugiat. Proin commodo dui vel pharetra molestie. Morbi auctor ut velit a luctus. Aliquam convallis mattis ipsum, non finibus sem porttitor id. Integer sit amet massa ligula. Etiam elementum, elit sed dictum posuere, tellus ante blandit leo, vel tristique sapien est sed lacus. Mauris non tempus nibh. Phasellus vehicula vulputate turpis ornare consequat. In nunc turpis, mattis in cursus a, sagittis non ex. Aenean egestas nisi mattis enim venenatis aliquam.</p>
  <p>Cras quis sodales nisl. Mauris tincidunt neque eget elit tincidunt ullamcorper. Vestibulum eget eros quis urna varius mollis. Donec ac sapien rhoncus urna viverra eleifend. Aliquam viverra, tortor vitae aliquet vehicula, nisl purus imperdiet risus, non tempor quam nisi sed quam. Fusce sem ligula, porta in enim id, tristique commodo neque. Pellentesque et feugiat libero. Aliquam eleifend magna vel fringilla aliquet. Morbi lacinia massa eu urna aliquam posuere. Integer euismod leo nec urna bibendum, eu pulvinar mi pellentesque. In hac habitasse platea dictumst. Nulla fringilla interdum dictum.</p>
  <p>Nulla sodales tellus eget nunc ullamcorper, eu semper ipsum convallis. Mauris dictum non lacus in tempus. Nullam feugiat ipsum sed diam tempor vehicula. Vivamus neque erat, pharetra et tincidunt ac, maximus non neque. Duis eleifend, lorem eget eleifend porta, nisl tortor cursus turpis, lobortis bibendum est nibh accumsan dui. Vestibulum quis magna eget urna efficitur molestie ac nec justo. Sed vitae hendrerit nunc, et auctor ligula. Vivamus congue lectus augue, ut tempus tortor scelerisque molestie. Sed maximus neque ac lacus aliquam iaculis. Aliquam egestas urna at ligula scelerisque accumsan. Etiam convallis eget felis nec tincidunt. Fusce sed libero et nunc auctor suscipit. Donec blandit semper erat ac bibendum. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam egestas, nibh vitae porta fermentum, justo quam viverra felis, in venenatis libero mauris id metus. Aenean tempor sapien eu lectus rhoncus pretium.</p>
  <p>Maecenas eleifend convallis cursus. Praesent nec lorem a neque vehicula semper. Fusce blandit euismod dui, et mattis libero. Integer eget ornare dolor, id faucibus odio. Nullam lacinia quam vel ipsum mattis tincidunt. Donec nunc nulla, elementum in augue ac, placerat interdum purus. Vestibulum euismod ultricies lacus, quis interdum nisl. Proin viverra rhoncus nunc, ac blandit ligula cursus id. Vivamus massa ligula, elementum in faucibus id, vestibulum non orci. Ut dui sapien, convallis nec nisl nec, bibendum vulputate nunc. Duis rhoncus tortor sit amet facilisis semper.</p>
  <p>Phasellus consequat a nulla at laoreet. Morbi in ligula risus. Nulla fringilla ipsum eget metus eleifend, sit amet efficitur nibh hendrerit. In tincidunt arcu fermentum ipsum semper, non consectetur diam placerat. Praesent ac nisl quis enim hendrerit imperdiet. Duis placerat volutpat nulla, at venenatis libero dapibus in. Vestibulum tristique ligula eget leo tempor auctor. Phasellus consequat in dui ut blandit. Nulla facilisi. Mauris eu dignissim dui. Cras venenatis enim velit, condimentum facilisis libero rutrum sed. Nulla mollis mattis eros. Aenean efficitur massa nec mi rhoncus pulvinar. Pellentesque metus massa, mattis tincidunt sagittis dictum, ultrices eu metus. Duis luctus metus non ipsum tincidunt aliquam ut vitae eros. Nulla non mollis odio.</p>
</section>

<?php include ('includes/footer.php') ?>
</div>
</body>
</html>
