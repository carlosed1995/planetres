<!doctype html>
<html>
<head>
<meta charset="iso-8859-2">
<title>Planetres - Login</title>
<link href="https://fonts.googleapis.com/css?family=Comfortaa" rel="stylesheet">
<link href="<?=base_url();?>web/css/forms.css" rel="stylesheet" type="text/css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.0.47/jquery.fancybox.min.js"></script>
</head>

<body>
	<form id="login" method="POST">
	  <h1 class="tit1">Login</h1>
      <div class="head-details h-detail-l">
              <div class="a-half">
                <a onClick="window.parent.location.href = '<?=base_url('Planetres');?>';" href="#"><strong>&laquo;</strong> Back to Home</a>
              </div>
            </div>
	  <p style="margin-top:25px; margin-bottom:0;">
	    <input name="email" type="text" class="field1 rounded" id="email" placeholder="User" required>
	  </p>
	  <p style="margin-top:2px;">
	    <input name="password" type="password" class="field1 rounded" id="password" placeholder="Password" required>
	  </p>
	  <p><a href="#" class="links">Forgot your password?</a></p>
	  <p><input name="button" type="submit" class="field4 rounded" id="button_login" value="Enter"></p>	  
	</form>
</body>
	<script>
		$(function () {
                        
                            $('#button_login').click(function (e) {
                                e.preventDefault();
                               
                                    login();   
                                
                            });
                        
                    });	

		 function login() {	                     
		 	var email = $("#email").val();                             
                    	var password = $("#password").val(); 		                                       	                    	    
                    	$.ajax({                    		
                    		type:"POST",
                    		url:"<?=base_url('Login/signin')?>",
                    		data:{email: email, password: password}
                    		}).done(function (response) {                                    
                            	if(response == "error"){
                                    $('#email').css("border", "5px solid orange");
                                    $('#password').css("border", "5px solid orange");
                            		alert('Los datos ingresados no existen');                                   
                            	}else if(response == "Empty fields"){    
                                    $('#email').css("border", "5px solid red");
                                    $('#password').css("border", "5px solid red");
                                    alert(response);            
                            	}else{
                                    window.parent.location.href = "<?=base_url('Planetres');?>";
                                }
                            	
                    		});                                	
                    }                     
                
  	</script>
</html>