<!doctype html>
<html>
<head>
<meta charset="utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
<meta name="viewport" content="width=device-width" />
<title>Planetres</title>
<link href="https://fonts.googleapis.com/css?family=Comfortaa" rel="stylesheet">
<link href="<?=base_url()?>web/css/styles.css" rel="stylesheet" type="text/css">
<link href="<?=base_url()?>web/css/forms.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.0.47/jquery.fancybox.min.css" />
<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.0.47/jquery.fancybox.min.js"></script>
  <link rel="stylesheet" href="<?=base_url()?>web/css/bootstrap.min.css">
  <link rel="stylesheet" href="<?=base_url()?>web/css/bootstrap.css">
  <script src="<?=base_url()?>web/js/bootstrap.js"></script>
<body>
<div id="page">
<header>
<div class="header-bg">
<div id="logo"><a href="<?=base_url();?>"><img src="<?=base_url();?>web/images/logo.png" alt="Planetres"></a></div>
<div class="header-in">
<div class="header-1r">
<div class="log"><img src="<?=base_url();?>web/images/icon-user.jpg" width="23" height="22"> <a data-fancybox data-src="<?=base_url('login');?>" href="javascript:;" class="login">Login</a> &nbsp; | &nbsp; <a href="<?=base_url('Register')?>" >Register</a> &nbsp; | &nbsp; <a href="#">Cart</a> <img src="<?=base_url();?>web/images/icon-cart.jpg" width="29" height="23"></div>
</div>
<div id="menu"><a href="#">Destinations</a> <a href="#">Services</a> <a href="#">Especials</a> <a href="#">Contact Us</a></div>
</div>
</div>
</header>

