<!doctype html>
<html>
    <head>
        <meta charset="iso-8859-2">
        <title>Planetres - User</title>
        <link href="https://fonts.googleapis.com/css?family=Comfortaa" rel="stylesheet">
        <link href="<?= base_url(); ?>web/css/forms.css" rel="stylesheet" type="text/css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.0.47/jquery.fancybox.min.js"></script>  

    </head>
    <body>
        <div class="col-md-12">
            <div class="panel  text-left">
                <div class="panel-body">  
                    <?php if ($this->session->flashdata('success')) { ?>
                        <div class="alert alert-success">
                            <a href="#" class="close" data-dismiss="alert">&times;</a>
                            <strong>Success!</strong> <?php echo $this->session->flashdata('success'); ?>
                        </div>

                    <?php } else if ($this->session->flashdata('error')) { ?>

                        <div class="alert alert-danger">
                            <a href="#" class="close" data-dismiss="alert">&times;</a>
                            <strong>Error!</strong> <?php echo $this->session->flashdata('error'); ?>
                        </div>

                    <?php } ?>
                    <form class="form-horizontal" id="formulario" autocomplete="off">
                        <div class="form-group">
                            <div class="col-sm-10"><input type="hidden" id="iduser" name="iduser" placeholder="Enter iduser" value="<?= ($iduser != '') ? $iduser : '' ?>"/><?php echo form_error('iduser', '<div class="text-danger text-left">', '</div>'); ?> </div>
                        </div><div class="form-group">
                            <label class="control-label col-sm-2" for="name">Name:</label>
                            <div class="col-sm-10"><input class="form-control" type="text" id="name" name="name" placeholder="Enter name" value="<?= ($name != '') ? $name : '' ?>"/><?php echo form_error('name', '<div class="text-danger text-left">', '</div>'); ?> </div>
                        </div><div class="form-group">
                            <label class="control-label col-sm-2" for="middle_name">Middle name:</label>
                            <div class="col-sm-10"><input class="form-control" type="text" id="middle_name" name="middle_name" placeholder="Enter Middle name" value="<?= ($middle_name != '') ? $middle_name : '' ?>"/><?php echo form_error('middle_name', '<div class="text-danger text-left">', '</div>'); ?> </div>
                        </div><div class="form-group">
                            <label class="control-label col-sm-2" for="last_name">Last name:</label>
                            <div class="col-sm-10"><input class="form-control" type="text" id="last_name" name="last_name" placeholder="Enter Last name" value="<?= ($last_name != '') ? $last_name : '' ?>"/><?php echo form_error('last_name', '<div class="text-danger text-left">', '</div>'); ?> </div>
                        </div>
                        <div class="form-inline text-left">
                            <label class="control-label col-sm-2" for="birth">Birthdate:</label>
                            <div class="">                            
                                <div class="col-sm-10 form-group">
                                    <?= $year . $month . $day; ?>
                                    <?php echo form_error('birth', '<div class="text-danger text-left">', '</div>'); ?>
                                    <?php echo form_error('year', '<div class="text-danger text-left">', '</div>'); ?>
                                    <?php echo form_error('month', '<div class="text-danger text-left">', '</div>'); ?>
                                    <?php echo form_error('day', '<div class="text-danger text-left">', '</div>'); ?>
                                </div>
                            </div>                
                        </div>
                        <div class="form-group"></div>
                        <div class="form-inline text-left">
                            <label class="control-label col-sm-2" for="passport">Passport:</label>
                            <div class="col-sm-10 form-group text-left">                                
                                <div class="col-sm-2 text-left" style="margin-left: -10px">                                
                                    <?= $selectnationality ?>
                                    <?php echo form_error('nationality', '<div class="text-danger text-left">', '</div>'); ?>
                                </div>                                
                                <div class="col-sm-10 text-left" style="margin-left: -10px">                                    
                                    <input type="text" class="form-control" id="passport" name="passport" value="<?= $passport ?>" placeholder="Enter Passport">
                                    <?php echo form_error('passport', '<div class="text-danger text-left">', '</div>'); ?>
                                </div>
                            </div>
                        </div><div class="form-group">

                        </div>
                        <div class="form-inline text-left">
                            <label class="control-label col-sm-2" for="expedition_passport">Passport expedition date:</label>
                            <div class="">                            
                                <div class="col-sm-10 form-group">
                                    <?= $cpyear . $cpmonth . $cpday; ?>                               
                                    <?php echo form_error('cpyear', '<div class="text-danger text-left">', '</div>'); ?>
                                    <?php echo form_error('cpmonth', '<div class="text-danger text-left">', '</div>'); ?>
                                    <?php echo form_error('cpday', '<div class="text-danger text-left">', '</div>'); ?>
                                </div>
                            </div>                
                        </div> 
                        <div class="form-group"></div>
                        <div class="form-inline text-left">
                            <label class="control-label col-sm-2" for="expire_passport">Passport expiration date:</label>
                            <div class="">                            
                                <div class="col-sm-10 form-group">
                                    <?= $epyear . $epmonth . $epday; ?>                               
                                    <?php echo form_error('epyear', '<div class="text-danger text-left">', '</div>'); ?>
                                    <?php echo form_error('epmonth', '<div class="text-danger text-left">', '</div>'); ?>
                                    <?php echo form_error('epday', '<div class="text-danger text-left">', '</div>'); ?>
                                </div>
                            </div>                
                        </div>   
                        <div class="form-group">

                        </div>

                        <div class="form-group">
                            <label class="control-label col-sm-2" for="city">City:</label>
                            <div class="col-sm-10"> 
                                <input type="text" class="form-control" id="citysearch" name="citysearch" value="<?= $citysearch ?>" placeholder="Enter  City">
                                <input type="hidden" class="form-control" name="city_idcity" value="<?= $city_idcity ?>" id="city_idcity">
                                <div class="containerautocomplete"></div>
                                <?php echo form_error('city_idcity', '<div class="text-danger text-left">', '</div>'); ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="email">Email:</label>
                            <div class="col-sm-10"><input class="form-control" type="text" id="email" name="email" placeholder="Enter email" value="<?= ($email != '') ? $email : '' ?>"/><?php echo form_error('email', '<div class="text-danger text-left">', '</div>'); ?> </div>
                        </div><div class="form-group">
                            <label class="control-label col-sm-2" for="phone">Phone:</label>
                            <div class="col-sm-10"><input class="form-control" type="text" id="phone" name="phone" placeholder="Enter phone" value="<?= ($phone != '') ? $phone : '' ?>"/><?php echo form_error('phone', '<div class="text-danger text-left">', '</div>'); ?> </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <label class="control-label col-sm-2" for="genere">Gender:</label>
                                <label class="radio-inline col-sm-2">
                                    <input type="radio" id="genere1" name="genere" <?= (isset($female)) ? $female : "" ?> value="female"> Female
                                </label>
                                <label class="checkbox-inline col-sm-2">
                                    <input type="radio" id="genere2" name="genere" <?= (isset($male)) ? $male : "" ?> value="male"> Male
                                </label>
                            </div>      
                            <div class="row">
                                <div class="col-sm-2"></div>
                                <div class="col-sm-10">
                                    <?php echo form_error('genere', '<div class="text-danger text-left">', '</div>'); ?>
                                </div>
                            </div>                            
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="gps">Gps:</label>
                            <div class="col-sm-10"><input class="form-control" type="text" id="gps" name="gps" placeholder="Enter gps" value="<?= ($gps != '') ? $gps : '' ?>"/><?php echo form_error('gps', '<div class="text-danger text-left">', '</div>'); ?> </div>
                        </div><div class="form-group">
                            <label class="control-label col-sm-2" for="zip_code">Zip    code:</label>
                            <div class="col-sm-10"><input class="form-control" type="text" id="zip_code" name="zip_code" placeholder="Enter zip_code" value="<?= ($zip_code != '') ? $zip_code : '' ?>"/><?php echo form_error('zip_code', '<div class="text-danger text-left">', '</div>'); ?> </div>
                        </div><div class="form-group">
                            <label class="control-label col-sm-2" for="password">Password:</label>
                            <div class="col-sm-10"><input class="form-control" type="password" id="password" name="password" placeholder="Enter password" value="<?= ($password != '') ? $password : '' ?>"/><?php echo form_error('password', '<div class="text-danger text-left">', '</div>'); ?> </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="pwd1">Confirm Password:</label>
                            <div class="col-sm-10"> 
                                <input type="password" class="form-control" id="password1" name="password1" placeholder="Enter  password again" value="<?= ($password != '') ? $password : '' ?>">
                                <?php echo form_error('password1', '<div class="text-danger text-left">', '</div>'); ?>
                                <?php echo form_error('errorpass', '<div class="text-danger text-left">', '</div>'); ?>                              
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="url">Url:</label>
                            <div class="col-sm-10"><input class="form-control" type="text" id="url" name="url" placeholder="Enter url" value="<?= ($url != '') ? $url : '' ?>"/><?php echo form_error('url', '<div class="text-danger text-left">', '</div>'); ?> </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <label class="control-label col-sm-2" for="active">Active:</label>
                                <label class="radio-inline col-sm-2">
                                    <input type="radio" id="active1" name="active" <?= (isset($ina) ? $ina : "") ?> value="0"> Inactive
                                </label>
                                <label class="checkbox-inline col-sm-2">
                                    <input type="radio" id="active2" name="active" <?= (isset($act) ? $act : "") ?> value="1"> Active
                                </label>
                            </div>      
                            <div class="row">
                                <div class="col-sm-2"></div>
                                <div class="col-sm-10">
                                    <?php echo form_error('active', '<div class="text-danger text-left">', '</div>'); ?>
                                </div>
                            </div>                            
                        </div>  
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="role">Role:</label>
                            <div class="col-sm-10">
                                <select class="form-control" id="role_idrole" name="role_idrole" placeholder="Enter Rol">
                                    <option value="">Select role...</option>
                                    <?= $select ?>
                                </select>
                                <?php echo form_error('role_idrole', '<div class="text-danger text-left">', '</div>'); ?>
                            </div>
                        </div>    
                        <div class="form-group"> 
                            <div class="col-sm-4 text-right"></div>
                            <div class="col-sm-2 text-right">
                                <a href="#"><button type="button" name="" id="list" value="list" class="btn btn-block btn-success">List</button></a>
                            </div>
                            <div class="col-sm-2 text-left">
                                <button type="button" name="submit" id="submit" value="submit" class="btn btn-block btn-success">Submit</button>
                            </div>
                            <div class="col-sm-4 text-right"></div>
                        </div>
                    </form>
                </div>
            </div>
        </div>        
    </body>
    <script type="text/javascript">
        $('#submit').click(function () {
            $.ajax({
                data: $("#formulario").serialize(),
                url: '<?= base_url() . 'backend/User/saveuser' ?>',
                type: "POST",
                success: function (datos)
                {
                    $("#maincontent").html(datos);
                }
            });
        });
        $("#citysearch").keyup(function () {
            var info = $(this).val();

            $.post('backend/User/autocomplete', {info: info}, function (data) {
                if (data != '')
                {
                    $(".containerautocomplete").html(data);

                } else {
                    $(".containerautocomplete").html('');
                }
            })


        });
        function asignar(idcity, nombre) {
            $("#city_idcity").val(idcity);
            $("#citysearch").val(nombre);
            $(".containerautocomplete").html('');
        }
        $("#list").click(function () {
            $(".content-header small").html("User List");
            $('#maincontent').load('<?= trim(base_url()) ?>backend/User/listuser');
        });

    </script>