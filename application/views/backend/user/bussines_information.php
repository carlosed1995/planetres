<!doctype html>
<html>
    <head>
        <meta charset="iso-8859-2">
        <title>Planetres - User</title>
        <link href="https://fonts.googleapis.com/css?family=Comfortaa" rel="stylesheet">
        <link href="<?= base_url(); ?>web/css/forms.css" rel="stylesheet" type="text/css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.0.47/jquery.fancybox.min.js"></script>  

    </head>
    <body>
        <div class="col-md-12">
            <div class="panel  text-left">
                <div class="panel-title panel-heading"><div class="text-blue">Business Information</div></div>
                <div class="panel-body">  
                    <?php if ($this->session->flashdata('success')) { ?>
                        <div class="alert alert-success">
                            <a href="#" class="close" data-dismiss="alert">&times;</a>
                            <strong>Success!</strong> <?php echo $this->session->flashdata('success'); ?>
                        </div>

                    <?php } else if ($this->session->flashdata('error')) { ?>

                        <div class="alert alert-danger">
                            <a href="#" class="close" data-dismiss="alert">&times;</a>
                            <strong>Error!</strong> <?php echo $this->session->flashdata('error'); ?>
                        </div>

                    <?php } ?>
                    <form class="form-horizontal" id="formulario" autocomplete="on">
                        <div class="form-group">                            
                            <div class="col-sm-10">
                                <input type="hidden" id="idbussines_information" name="idbussines_information" placeholder="Enter idbussines information" value="<?= ($idbussines_information != '') ? $idbussines_information : '' ?>"/><?php echo form_error('idbussines_information', '<div class="text-danger text-left">', '</div>'); ?> </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="paypal_account">Paypal account:</label>
                            <div class="col-sm-10"><input class="form-control" type="text" id="paypal_account" name="paypal_account" placeholder="Enter paypal account" value="<?= ($paypal_account != '') ? $paypal_account : '' ?>"/><?php echo form_error('paypal_account', '<div class="text-danger text-left">', '</div>'); ?> </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="bank_name">Bank name:</label>
                            <div class="col-sm-10"><input class="form-control" type="text" id="bank_name" name="bank_name" placeholder="Enter bank name" value="<?= ($bank_name != '') ? $bank_name : '' ?>"/><?php echo form_error('bank_name', '<div class="text-danger text-left">', '</div>'); ?> </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="bank_account_name">Bank account name:</label>
                            <div class="col-sm-10"><input class="form-control" type="text" id="bank_account_name" name="bank_account_name" placeholder="Enter bank account name" value="<?= ($bank_account_name != '') ? $bank_account_name : '' ?>"/><?php echo form_error('bank_account_name', '<div class="text-danger text-left">', '</div>'); ?> </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="bank_account_number">Bank account number:</label>
                            <div class="col-sm-10"><input class="form-control" type="text" id="bank_account_number" name="bank_account_number" placeholder="Enter bank account number" value="<?= ($bank_account_number != '') ? $bank_account_number : '' ?>"/><?php echo form_error('bank_account_number', '<div class="text-danger text-left">', '</div>'); ?> </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="bank_account_swift">Bank account swift:</label>
                            <div class="col-sm-10"><input class="form-control" type="text" id="bank_account_swift" name="bank_account_swift" placeholder="Enter bank account swift" value="<?= ($bank_account_swift != '') ? $bank_account_swift : '' ?>"/><?php echo form_error('bank_account_swift', '<div class="text-danger text-left">', '</div>'); ?> </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <label class="control-label col-sm-2" for="active">Active:</label>
                                <label class="radio-inline col-sm-2">
                                    <input type="radio" id="active1" name="active" <?= (isset($ina) ? $ina : "") ?> value="0"> Inactive
                                </label>
                                <label class="checkbox-inline col-sm-2">
                                    <input type="radio" id="active2" name="active" <?= (isset($act) ? $act : "") ?> value="1"> Active
                                </label>
                            </div>      
                            <div class="row">
                                <div class="col-sm-2"></div>
                                <div class="col-sm-10">
                                    <?php echo form_error('active', '<div class="text-danger text-left">', '</div>'); ?>
                                </div>
                            </div>                            
                        </div>  
                        <div class="form-group">
                            <input type="hidden" value="<?= (isset($iduser) ? $iduser : "") ?>" id="user_iduser" name="user_iduser"/>                            
                            <input type="hidden" name="id" id="id" value="<?= $id ?>">
                            <input type="hidden" name="type" id="type" value="<?= $type ?>">
                            <input type="hidden" name="idimage" id="idimage" value="<?= $idimage ?>">
                            <input type="hidden" name="imagename" id="imagename" value="<?= $imagename ?>">                            
                            <input type="hidden" name="update" id="update" value="<?= $update ?>"> 
                        </div>
                        <div class="form-group"> 
                            <div class="col-sm-4 text-right"></div>
                            <div class="col-sm-2 text-right">
                                <a href="#"><button type="button" name="" id="list" value="list"  class="btn btn-block btn-success">Return</button></a>
                            </div>
                            <div class="col-sm-2 text-left">
                                <button type="button" name="submit" id="submit" value="submit" class="btn btn-block btn-success">Submit</button>
                            </div>
                            <div class="col-sm-4 text-right"></div>
                        </div>
                    </form>
                </div>
            </div>
        </div>        
    </body>
    <script>
        $('#submit').click(function () {
            $.ajax({
                data: $("#formulario").serialize(),
                url: '<?= base_url() . 'backend/User/savebussinesinformation' ?>',
                type: "POST",
                success: function (datos)
                {
                    $("#maincontent").html(datos);
                }
            });
        });
        $("#list").click(function () {
            $.ajax({
                data: {iduser: $("#id").val()},
                url: '<?= base_url() .  "backend/User/newuser" ?>',
                type: "POST",
                success: function (datos)
                {
                    $(".content-header small").html("Update User");
                    $('#maincontent').html(datos);
                }
            });
        });
    </script>
<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

