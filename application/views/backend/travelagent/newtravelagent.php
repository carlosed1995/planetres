<!doctype html>
<html>
    <head>
        <meta charset="iso-8859-2">
        <title>Planetres - User</title>
        <link href="https://fonts.googleapis.com/css?family=Comfortaa" rel="stylesheet">
        <link href="<?= base_url(); ?>web/css/forms.css" rel="stylesheet" type="text/css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.0.47/jquery.fancybox.min.js"></script>  
    </head>
    <body>
        <div class="col-md-12">
            <div class="panel  text-left">
                <div class="panel-body">  
                    <?php if ($this->session->flashdata('success')) { ?>
                        <div class="alert alert-success">
                            <a href="#" class="close" data-dismiss="alert">&times;</a>
                            <strong>Success!</strong> <?php echo $this->session->flashdata('success'); ?>
                        </div>

                    <?php } else if ($this->session->flashdata('error')) { ?>

                        <div class="alert alert-danger">
                            <a href="#" class="close" data-dismiss="alert">&times;</a>
                            <strong>Error!</strong> <?php echo $this->session->flashdata('error'); ?>
                        </div>

                    <?php } ?>
                    <form class="form-horizontal" id="formulario" autocomplete="off">
                        <input type='hidden' value="<?= ($idtravel_agent != '') ? $idtravel_agent : '' ?>" name="idtravel_agent">
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="name">Name:</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="name"  value="<?= $name ?>" name="name" placeholder="Enter Name">
                                <?php echo form_error('name', '<div class="text-danger text-left">', '</div>'); ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="email">Email:</label>
                            <div class="col-sm-10">
                                <input type="email" class="form-control" id="email" name="email" value="<?= $email ?>" placeholder="Enter  Email@email.com">
                                <?php echo form_error('email', '<div class="text-danger text-left">', '</div>'); ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="phone">Phone:</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="phone" name="phone" value="<?= $phone ?>" placeholder="Enter  phone number">
                                <?php echo form_error('phone', '<div class="text-danger text-left">', '</div>'); ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="city">City:</label>
                            <div class="col-sm-10"> 
                                <input type="text" class="form-control" id="citysearch" name="citysearch" value="<?= $citysearch ?>" placeholder="Enter  City">
                                <input type="hidden" class="form-control" name="city" value="<?= $city ?>" id="city">
                                <div class="containerautocomplete"></div>
                                <?php echo form_error('city', '<div class="text-danger text-left">', '</div>'); ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="location">Location:</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="location" readonly="readonly" name="location" value="<?= $location ?>" placeholder="Enter Location">
                                <?php echo form_error('location', '<div class="text-danger text-left">', '</div>'); ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="gps">Gps:</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="gps" value="<?= $gps ?>" name="gps" placeholder="Enter Gps">
                                <?php echo form_error('gps', '<div class="text-danger text-left">', '</div>'); ?>
                            </div>
                        </div>                        
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="zip_code">Zip Code:</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="zip_code" value="<?= $zip_code ?>" name="zip_code" placeholder="Enter Zip Code">
                                <?php echo form_error('zip_code', '<div class="text-danger text-left">', '</div>'); ?>
                            </div>
                        </div>                      
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="zip_code">Arc Number:</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="arc_number" value="<?= $arc_number ?>" name="arc_number" placeholder="Enter Arc Number">
                                <?php echo form_error('arc_number', '<div class="text-danger text-left">', '</div>'); ?>
                            </div>
                        </div>                      
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="pwd">Password:</label>
                            <div class="col-sm-10"> 
                                <input type="password" class="form-control" id="pwd"  name="pwd" value="<?= $pwd ?>" placeholder="Enter  password">
                                <?php echo form_error('pwd', '<div class="text-danger text-left">', '</div>'); ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="pwd1">Confirm Password:</label>
                            <div class="col-sm-10"> 
                                <input type="password" class="form-control" id="pwd1" name="pwd1" placeholder="Enter  password again">
                                <?php echo form_error('pwd1', '<div class="text-danger text-left">', '</div>'); ?>
                                <?php echo form_error('errorpass', '<div class="text-danger text-left">', '</div>'); ?>                              
                            </div>
                        </div>                        
                        <div class="form-group">
                            <div class="row">
                                <label class="control-label col-sm-2" for="status">Status:</label>
                                <label class="radio-inline col-sm-2">
                                    <input type="radio" id="status1" name="status" <?= $ina ?> value="0"> Inactive
                                </label>
                                <label class="checkbox-inline col-sm-2">
                                    <input type="radio" id="status2" name="status" <?= $act ?> value="1"> Active
                                </label>
                            </div>      
                            <div class="row">
                                <div class="col-sm-2"></div>
                                <div class="col-sm-10">
                                    <?php echo form_error('status', '<div class="text-danger text-left">', '</div>'); ?>
                                </div>
                            </div>                            
                        </div>                                                
                        <div class="form-group"> 
                            <div class="col-sm-4 text-right"></div>
                            <div class="col-sm-2 text-right">
                                <a href="#"><button type="button" name="" id="list" value="list" class="btn btn-block btn-success">List</button></a>
                            </div>
                            <div class="col-sm-2 text-left">
                                <button type="button" name="submit" id="submit" value="submit" class="btn btn-block btn-success">Submit</button>
                            </div>
                            <div class="col-sm-4 text-right"></div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </body>
    <script type="text/javascript">
        $('#submit').click(function () {
            $.ajax({
                data: $("#formulario").serialize(),
                url: '<?= base_url() . 'backend/Travelagent/savetravelagent' ?>',
                type: "POST",
                success: function (datos)
                {
                    $("#maincontent").html(datos);
                }
            });
        });
        $("#citysearch").keyup(function () {
            var info = $(this).val();

            $.post('backend/User/autocomplete', {info: info}, function (data) {
                if (data != '')
                {
                    $(".containerautocomplete").html(data);

                } else {
                    $(".containerautocomplete").html('');
                }
            })


        });
        function asignar(idcity, nombre) {
            $("#city").val(idcity);
            $.post('backend/Travelagent/searchcountry', {idcity: idcity}, function (data) {
                if (data != '')
                {
                    
                    $("#location").val(data);

                } else {
                    $("#location").val('');
                }
            })

            $("#citysearch").val(nombre);
            $(".containerautocomplete").html('');
        }
        $("#list").click(function () {
            $(".content-header small").html("User List");
            $('#maincontent').load('<?= trim(base_url()) ?>backend/Travelagent/listtravelagent');
        });

    </script>