<!doctype html>
<html>
    <head>
        <meta charset="iso-8859-2">
        <title>Planetres - User</title>
        <link href="https://fonts.googleapis.com/css?family=Comfortaa" rel="stylesheet">
        <link href="<?= base_url(); ?>web/css/forms.css" rel="stylesheet" type="text/css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.0.47/jquery.fancybox.min.js"></script>
        <style>
            #preview{
                width: 171px;
                margin: 0 auto;
                margin-bottom: 10px;
                position: relative;
            }
            #preview a{
                position: absolute;
                bottom: 5px;
                left: 5px;
                right: 5px;
                display: none;
            }
            #file-info{
                display: block;
            }
            input[type=file]{
                position: absolute;
                visibility: hidden;
                width: 0;
                z-index: -9999;
            }
            .buttonfile-save{
                width: 171px;
            }
        </style>
    </head>    
    <body>
        <div class="col-md-12">
            <div class="panel text-center">
                <div class="panel-body">  
                    <div class="container">
                        <div class="row">
                            <div class="form-group col-lg-10">
                                <?php if ($this->session->flashdata('success')) { ?>
                                    <div class="alert alert-success">
                                        <a href="#" class="close" data-dismiss="alert">&times;</a>
                                        <strong>Success!</strong> <?php echo $this->session->flashdata('success'); ?>
                                    </div>

                                <?php } else if ($this->session->flashdata('error')) { ?>

                                    <div class="alert alert-danger">
                                        <a href="#" class="close" data-dismiss="alert">&times;</a>
                                        <strong>Error!</strong> <?php echo $this->session->flashdata('error'); ?>
                                    </div>

                                <?php } ?>
                                <div class="col-lg-12" id="foto-portada">                                                                                                   
                                    <h4>Profile Picture</h4>
                                    <div id="preview" class="thumbnail" style="position: relative">
                                        <input type="hidden" name="id" id="id" value="<?= $id ?>">
                                        <input type="hidden" name="type" id="type" value="<?= $type ?>">
                                        <input type="hidden" name="idimage" id="id" value="<?= $idimage ?>">
                                        <a href="#" class="btn btn-default" id="file-select">Choose File</a>
                                        <img src="<?= base_url(); ?>web/images/profiles/<?= ($imagename || $imagename != '') ? $imagename : "nopicture.jpg" ?>" alt="">
                                    </div>
                                    <span class="alert text-info" id="file-info">No File</span>
                                    <input type="file" name="portrait" id="file">
                                    <a href="#" class="btn btn-success buttonfile-save" id="file-save">Submit</a>
                                    <a href="#"><button type="button" name="" id="list" value="list" class="btn btn-success buttonfile-save">List</button></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>        
    </body>
    <script src="<?= base_url(); ?>web/js/fileupload.js"></script>  
    <script type="text/javascript">
        var global_data = {
            portrait_upload_url: '<?php echo base_url('/backend/fileupload/upload/') ?>'
        };
        $("#list").click(function () {
            $(".content-header small").html("User List");
            $('#maincontent').load('<?= trim(base_url()) ?>backend/User/listuser');
        });
    </script>
    <div class="modal fade" id="upload_errors">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button class="close" data-dismiss="modal" type="button"><span>×</span></button>
                    <h4 class="modal-title">Error uploading image</h4>
                </div>

                <div class="modal-body">
                    <div class="alert alert-danger upload_errors"></div>
                </div>

                <div class="modal-footer">
                    <button class="btn btn-primary" data-dismiss="modal" type="button">To accept</button>
                </div>
            </div>
        </div>
    </div>