<!doctype html>
<html>
    <head>
        <meta charset="iso-8859-2">
        <title>Planetres - User</title>
        <link href="https://fonts.googleapis.com/css?family=Comfortaa" rel="stylesheet">
        <link href="<?= base_url(); ?>web/css/forms.css" rel="stylesheet" type="text/css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.0.47/jquery.fancybox.min.js"></script>  
    </head>
    <body>
        <div class="col-md-12">
            <div class="panel  text-left">
                <div class="panel-body">  
                    <?php if ($this->session->flashdata('success')) { ?>
                        <div class="alert alert-success">
                            <a href="#" class="close" data-dismiss="alert">&times;</a>
                            <strong>Success!</strong> <?php echo $this->session->flashdata('success'); ?>
                        </div>

                    <?php } else if ($this->session->flashdata('error')) { ?>

                        <div class="alert alert-danger">
                            <a href="#" class="close" data-dismiss="alert">&times;</a>
                            <strong>Error!</strong> <?php echo $this->session->flashdata('error'); ?>
                        </div>

                    <?php } ?>
                    <form class="form-horizontal" id="formulario" autocomplete="off">
                        <div class="form-group">                            
                            <div class="col-sm-10"><input type="hidden" id="idfacilities" name="idfacilities" placeholder="Enter idfacilities" value="<?= ($idfacilities != '') ? $idfacilities : '' ?>"/><?php echo form_error('idfacilities', '<div class="text-danger text-left">', '</div>'); ?> </div>
                        </div><div class="form-group">
                            <label class="control-label col-sm-2" for="name">Name:</label>
                            <div class="col-sm-10"><input class="form-control" type="text" id="name" name="name" placeholder="Enter name" value="<?= ($name != '') ? $name : '' ?>"/><?php echo form_error('name', '<div class="text-danger text-left">', '</div>'); ?> </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <label class="control-label col-sm-2" for="type">Type:</label>
                                <label class="radio-inline col-sm-2">
                                    <input type="radio" id="active1" name="type" <?= (isset($service) ? $service : "") ?> value="0"> Services
                                </label>
                                <label class="checkbox-inline col-sm-2">
                                    <input type="radio" id="active2" name="type" <?= (isset($room) ? $room : "") ?> value="1"> Room
                                </label>
                            </div>      
                            <div class="row">
                                <div class="col-sm-2"></div>
                                <div class="col-sm-10">
                                    <?php echo form_error('active', '<div class="text-danger text-left">', '</div>'); ?>
                                </div>
                            </div>                            
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <label class="control-label col-sm-2" for="active">Active:</label>
                                <label class="radio-inline col-sm-2">
                                    <input type="radio" id="active1" name="active" <?= (isset($ina) ? $ina : "") ?> value="0"> Inactive
                                </label>
                                <label class="checkbox-inline col-sm-2">
                                    <input type="radio" id="active2" name="active" <?= (isset($act) ? $act : "") ?> value="1"> Active
                                </label>
                            </div>      
                            <div class="row">
                                <div class="col-sm-2"></div>
                                <div class="col-sm-10">
                                    <?php echo form_error('active', '<div class="text-danger text-left">', '</div>'); ?>
                                </div>
                            </div>                            
                        </div>
                        <div class="form-group"> 
                            <div class="col-sm-4 text-right"></div>
                            <div class="col-sm-2 text-right">
                                <a href="#"><button type="button" name="" id="list" value="list" class="btn btn-block btn-success">List</button></a>
                            </div>
                            <div class="col-sm-2 text-left">
                                <button type="button" name="submit" id="submit" value="submit" class="btn btn-block btn-success">Submit</button>
                            </div>
                            <div class="col-sm-4 text-right"></div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </body>
    <script type="text/javascript">
        $('#submit').click(function () {
            $.ajax({
                data: $("#formulario").serialize(),
                url: '<?= base_url() . 'backend/Facilities/savefacilities' ?>',
                type: "POST",
                success: function (datos)
                {
                    $("#maincontent").html(datos);
                }
            });
        });
        
        $("#list").click(function () {
            $(".content-header small").html("Features List");
            $('#maincontent').load('<?= trim(base_url()) ?>backend/Facilities/listfacilities');
        });

    </script>