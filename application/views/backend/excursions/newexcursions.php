<!doctype html>
<html>
    <head>
        <meta charset="iso-8859-2">
        <title>Planetres - User</title>
        <link href="https://fonts.googleapis.com/css?family=Comfortaa" rel="stylesheet">
        <link href="<?= base_url(); ?>web/css/forms.css" rel="stylesheet" type="text/css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.0.47/jquery.fancybox.min.js"></script>  
        <link href="<?= base_url(); ?>web/css/jasny-bootstrap.min.css" rel="stylesheet" media="screen">
    </head>
    <body>
        <div class="col-md-12">
            <div class="panel  text-left">
                <div class="panel-body">  
                    <?php if ($this->session->flashdata('success')) { ?>
                        <div class="alert alert-success">
                            <a href="#" class="close" data-dismiss="alert">&times;</a>
                            <strong>Success!</strong> <?php echo $this->session->flashdata('success'); ?>
                        </div>

                    <?php } else if ($this->session->flashdata('error')) { ?>

                        <div class="alert alert-danger">
                            <a href="#" class="close" data-dismiss="alert">&times;</a>
                            <strong>Error!</strong> <?php echo $this->session->flashdata('error'); ?>
                        </div>

                    <?php } ?>
                    <form class="form-horizontal" id="formulario" autocomplete="on">
                        <div class="form-group">                            
                            <div class="col-sm-10">
                                <input type="hidden" id="idexcursions" name="idexcursions" placeholder="Enter idexcursions" value="<?= ($idexcursions != '') ? $idexcursions : '' ?>"/><?php echo form_error('idexcursions', '<div class="text-danger text-left">', '</div>'); ?> </div>
                        </div><div class="form-group">
                            <label class="control-label col-sm-2" for="name">Name:</label>
                            <div class="col-sm-10"><input class="form-control" type="text" id="name" name="name" placeholder="Enter name" value="<?= ($name != '') ? $name : '' ?>"/><?php echo form_error('name', '<div class="text-danger text-left">', '</div>'); ?> </div>
                        </div><div class="form-group">                            
                            <label class="control-label col-sm-2" for="highlights">Highlights:</label>
                            <div class="col-sm-10"><textarea class="form-control" id="highlights" cols="20" rows="5" name="highlights" placeholder="Enter highlights" ><?= ($highlights != '') ? $highlights : '' ?></textarea>
                                <?php echo form_error('highlights', '<div class="text-danger text-left">', '</div>'); ?> </div>
                        </div><div class="form-group">
                            <label class="control-label col-sm-2" for="info">Info:</label>                            
                            <div class="col-sm-10"><textarea class="form-control" id="info" cols="20" rows="5" name="info" placeholder="Enter info" ><?= ($info != '') ? $info : '' ?></textarea>
                                <?php echo form_error('info', '<div class="text-danger text-left">', '</div>'); ?> </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <label class="control-label col-sm-2" for="active">Active:</label>
                                <label class="radio-inline col-sm-2">
                                    <input type="radio" id="active1" name="active" <?= (isset($ina) ? $ina : "") ?> value="0"> Inactive
                                </label>
                                <label class="checkbox-inline col-sm-2">
                                    <input type="radio" id="active2" name="active" <?= (isset($act) ? $act : "") ?> value="1"> Active
                                </label>
                            </div>      
                            <div class="row">
                                <div class="col-sm-2"></div>
                                <div class="col-sm-10">
                                    <?php echo form_error('active', '<div class="text-danger text-left">', '</div>'); ?>
                                </div>
                            </div>                            
                        </div>   
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="max_persons">Max persons:</label>
                            <div class="col-sm-10"><input class="form-control" type="number" step="1" min="0" id="max_persons" name="max_persons" placeholder="Enter max persons" value="<?= ($max_persons != '') ? $max_persons : '' ?>"/><?php echo form_error('max_persons', '<div class="text-danger text-left">', '</div>'); ?> </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="city_idcity">City:</label>
                            <div class="col-sm-10"><input type="text" class="form-control" id="citysearch" name="citysearch" value="<?= $citysearch ?>" placeholder="Enter  City">
                                <input type="hidden" class="form-control" name="city_idcity" value="<?= $city_idcity ?>" id="city_idcity">
                                <div class="containerautocomplete"></div>
                                <?php echo form_error('city_idcity', '<div class="text-danger text-left">', '</div>'); ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="">Pricing chart:</label>
                            <div class="col-sm-10">
                                <div class="panel panel-info">
                                    <div class="panel-heading text-left"></div>
                                    <div class="panel-body">                                        
                                        <div class="form-group">
                                            <div class="col-sm-10"><input type="hidden" id="idpricing_chart" name="idpricing_chart[]" placeholder="Enter idpricing_chart" value=""/></div></div>
                                        <div class="form-group"><label class="control-label col-sm-2" for="start_date">Start date:</label>
                                            <div class="col-sm-4"><input class="form-control" type="text" id="start_date" name="start_date[]" placeholder="Enter start_date" value=""/>
                                            </div><label class="control-label col-sm-2" for="end_date">End date:</label><div class="col-sm-4">
                                                <input class="form-control" type="text" id="end_date" name="end_date[]" placeholder="Enter end_date" value=""/></div></div>
                                        <div class="form-group"><label class="control-label col-sm-2" for="start_time">Start time:</label>
                                            <div class="col-sm-4"><input class="form-control" type="time" step="1" id="start_time" name="start_time[]" placeholder="Enter start time" value=""/>
                                            </div>
                                            <label class="control-label col-sm-2" for="end_time">End time:</label>
                                            <div class="col-sm-4"><input class="form-control" type="time" step="1" id="end_time" name="end_time[]" placeholder="Enter end time" value=""/></div></div>
                                        <div class="form-group"><label class="control-label col-sm-2" for="pickup">Pickup:</label><div class="col-sm-4"><input class="form-control" type="text" id="pickup" name="pickup[]" placeholder="Enter pickup" value=""/></div>
                                            <label class="control-label col-sm-2" for="drop_off">Drop off:</label><div class="col-sm-4"><input class="form-control" type="text" id="drop_off" name="drop_off[]" placeholder="Enter drop_off" value=""/></div></div>
                                        <div class="form-group"><label class="control-label col-sm-2" for="description">Description:</label><div class="col-sm-10"><textarea class="form-control" id="descrip" cols="20" rows="2" name="descrip[]" placeholder="Enter description" ></textarea></div>
                                        </div>
                                        <hr>
                                        <div class="row pricing"></div>
                                        <div class="btn btn-primary fa fa-plus" id="add">&nbsp;New</div>
                                    </div>
                                </div>
                            </div>                        
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="">Images:</label>
                            <div class="col-sm-10">
                                <div class="panel panel-info">
                                    <div class="panel-heading text-left"></div>
                                    <div class="panel-body">                                       
                                        <div class="col-sm-6"><label class="row">Front</label>
                                            <div class="row">
                                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                                    <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                                        <img data-src="holder.js/100%x100%" alt="">
                                                    </div>
                                                    <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"></div>
                                                    <div>
                                                        <span class="btn btn-default btn-file"><span class="fileinput-new">Select image</span><span class="fileinput-exists">Change</span><input type="file" name="front"></span>
                                                        <a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                                                    </div>
                                                </div>                                            
                                                <?php echo form_error('front', '<div class="text-danger text-left">', '</div>'); ?>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <label class="row">Images</label>
                                            <div class="row">
                                                <div class="fileinput fileinput-new row" data-provides="fileinput">
                                                    <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                                        <img data-src="holder.js/100%x100%" alt="" >
                                                    </div>
                                                    <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"></div>
                                                    <div>
                                                        <span class="btn btn-default btn-file"><span class="fileinput-new">Select image</span><span class="fileinput-exists">Change</span><input type="file" multiple name="images[]"></span>
                                                        <a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                                                    </div>
                                                </div>
                                            </div>                                                                               
    <!--                                        <input type="file" class="form-control" name="userFiles[]" multiple/>-->
                                        </div>
                                    </div>

                                </div>
                            </div>                        
                        </div>
                        <div class="form-group"> 
                            <div class="col-sm-4 text-right"></div>
                            <div class="col-sm-2 text-right">
                                <a href="#"><button type="button" name="" id="list" value="list" class="btn btn-block btn-success">List</button></a>
                            </div>
                            <div class="col-sm-2 text-left">
                                <button type="button" name="submit" id="submit" value="submit" class="btn btn-block btn-success">Submit</button>
                            </div>
                            <div class="col-sm-4 text-right"></div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </body>
    <script type="text/javascript">
        $('#submit').click(function () {            
            var formData = new FormData(document.getElementById("formulario"));
            formData.append("dato", "valor");
            $.ajax({
                data: formData,
                url: '<?= base_url() . 'backend/Excursions/saveexcursions' ?>',
                type: "POST",
                cache: false,
                contentType: false,
                processData: false,
                success: function (datos)
                {
                    $("#maincontent").html(datos);
                }
            });
        });
        $("#citysearch").keyup(function () {
            var info = $(this).val();
            $.post('backend/Hotels/autocomplete', {info: info}, function (data) {
                if (data != '')
                {
                    $(".containerautocomplete").html(data);
                } else {
                    $(".containerautocomplete").html('');
                }
            })


        });
        function asignar(idcity, nombre) {
            $("#city_idcity").val(idcity);
            $("#citysearch").val(nombre);
            $(".containerautocomplete").html('');
        }
        $("#list").click(function () {
            $(".content-header small").html("Excursions List");
            $('#maincontent').load('<?= trim(base_url()) ?>backend/Excursions/listexcursions');
        });
        $('#add').click(function () {
            var contenido = '<div class="form-group"><div class="col-sm-10"><input type="hidden" id="idpricing_chart" name="idpricing_chart[]" placeholder="Enter idpricing_chart" value=""/></div></div><div class="form-group"><label class="control-label col-sm-2" for="start_date">Start date:</label><div class="col-sm-4"><input class="form-control" type="text" id="start_date" name="start_date[]" placeholder="Enter start_date" value=""/></div><label class="control-label col-sm-2" for="end_date">End date:</label><div class="col-sm-4"><input class="form-control" type="text" id="end_date" name="end_date[]" placeholder="Enter end_date" value=""/></div></div><div class="form-group"><label class="control-label col-sm-2" for="start_time">Start time:</label><div class="col-sm-4"><input class="form-control" type="time" step="1" id="start_time" name="start_time[]" placeholder="Enter start time" value=""/></div><label class="control-label col-sm-2" for="end_time">End time:</label><div class="col-sm-4"><input class="form-control" type="time" step="1" id="end_time" name="end_time[]" placeholder="Enter end time" value=""/></div></div><div class="form-group"><label class="control-label col-sm-2" for="pickup">Pickup:</label><div class="col-sm-4"><input class="form-control" type="text" id="pickup" name="pickup[]" placeholder="Enter pickup" value=""/></div><label class="control-label col-sm-2" for="drop_off">Drop off:</label><div class="col-sm-4"><input class="form-control" type="text" id="drop_off" name="drop_off[]" placeholder="Enter drop_off" value=""/></div></div><div class="form-group"><label class="control-label col-sm-2" for="description">Description:</label><div class="col-sm-10"><textarea class="form-control" id="descrip" cols="20" rows="2" name="descrip[]" placeholder="Enter description" ></textarea></div></div><hr>';
            $('.pricing').append(contenido);
        });
    </script>
    <script src="<?= base_url(); ?>web/js/jasny-bootstrap.min.js"></script>