<!doctype html>
<html>
    <head>
        <meta charset="iso-8859-2">
        <title>Planetres - User</title>
        <link href="https://fonts.googleapis.com/css?family=Comfortaa" rel="stylesheet">
        <link href="<?= base_url(); ?>web/css/forms.css" rel="stylesheet" type="text/css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.0.47/jquery.fancybox.min.js"></script>  
        <link href="<?= base_url(); ?>web/css/jasny-bootstrap.min.css" rel="stylesheet" media="screen">
    </head>
    <body>
        <div class="col-md-12">
            <div class="panel  text-left">
                <div class="panel-body">  
                    <?php if ($this->session->flashdata('success')) { ?>
                        <div class="alert alert-success">
                            <a href="#" class="close" data-dismiss="alert">&times;</a>
                            <strong>Success!</strong> <?php echo $this->session->flashdata('success'); ?>
                        </div>

                    <?php } else if ($this->session->flashdata('error')) { ?>

                        <div class="alert alert-danger">
                            <a href="#" class="close" data-dismiss="alert">&times;</a>
                            <strong>Error!</strong> <?php echo $this->session->flashdata('error'); ?>
                        </div>

                    <?php } ?>
                    <form class="form-horizontal" id="formulario" autocomplete="off">
                        <div class="form-group">                            
                            <div class="col-sm-10"><input type="hidden" id="idtours" name="idtours" placeholder="Enter idtours" value="<?= ($idtours != '') ? $idtours : '' ?>"/><?php echo form_error('idtours', '<div class="text-danger text-left">', '</div>'); ?> </div>
                        </div><div class="form-group">
                            <label class="control-label col-sm-2" for="code">Code:</label>
                            <div class="col-sm-10"><input class="form-control" type="text" id="code" name="code" placeholder="Enter code" value="<?= ($code != '') ? $code : '' ?>"/><?php echo form_error('code', '<div class="text-danger text-left">', '</div>'); ?> </div>
                        </div><div class="form-group">
                            <label class="control-label col-sm-2" for="name">Name:</label>
                            <div class="col-sm-10"><input class="form-control" type="text" id="name" name="name" placeholder="Enter name" value="<?= ($name != '') ? $name : '' ?>"/><?php echo form_error('name', '<div class="text-danger text-left">', '</div>'); ?> </div>
                        </div><div class="form-group">
                            <label class="control-label col-sm-2" for="location">Location:</label>                            
                            <div class="col-sm-10">
                                <textarea class="form-control" id="location" cols="20" rows="5" name="location" placeholder="Enter location" ><?= ($location != '') ? $location : '' ?></textarea>                                
                            </div>
                        </div><div class="form-group">
                            <label class="control-label col-sm-2" for="highlights">Highlights:</label>
                            <div class="col-sm-10"><textarea class="form-control" id="highlights" cols="20" rows="5" name="highlights" placeholder="Enter highlights" ><?= ($highlights != '') ? $highlights : '' ?></textarea></div>
                        </div><div class="form-group">
                            <label class="control-label col-sm-2" for="max_persons">Max persons:</label>
                            <div class="col-sm-10"><input class="form-control" type="number" step="1" min="0" id="max_persons" name="max_persons" placeholder="Enter max_persons" value="<?= ($max_persons != '') ? $max_persons : '' ?>"/><?php echo form_error('max_persons', '<div class="text-danger text-left">', '</div>'); ?> </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <label class="control-label col-sm-2" for="active">Active:</label>
                                <label class="radio-inline col-sm-2">
                                    <input type="radio" id="active1" name="active" <?= (isset($ina) ? $ina : "") ?> value="0"> Inactive
                                </label>
                                <label class="checkbox-inline col-sm-2">
                                    <input type="radio" id="active2" name="active" <?= (isset($act) ? $act : "") ?> value="1"> Active
                                </label>
                            </div>      
                            <div class="row">
                                <div class="col-sm-2"></div>
                                <div class="col-sm-10">
                                    <?php echo form_error('active', '<div class="text-danger text-left">', '</div>'); ?>
                                </div>
                            </div>                            
                        </div>   
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="tours_type_idtours_type">Tours type:</label>
                            <div class="col-sm-10"><select class="form-control" id="tours_type_idtours_type" name="tours_type_idtours_type" placeholder="Enter tours_type_idtours_type">
                                    <option value="">Select tours type...</option>
                                    <?= $tours_type_idtours_type ?>
                                </select><?php echo form_error('tours_type_idtours_type', '<div class="text-danger text-left">', '</div>'); ?> </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="">Daily itinerary:</label>
                            <div class="col-sm-10">
                                <div class="panel panel-info">
                                    <div class="panel-heading text-left"></div>
                                    <div class="panel-body">                                        
                                        <div class="form-group">                                            
                                            <div class="col-sm-10"><input type="hidden" id="iddailyitinerary" name="iddailyitinerary[]" placeholder="Enter iddailyitinerary" /></div>
                                        </div><div class="form-group">
                                            <label class="control-label col-sm-2" for="date">Date:</label>
                                            <div class="col-sm-10"><input class="form-control" type="date" id="datetimepicker1" name="date[]" placeholder="yyyy-mm-dd" /></div>
                                        </div><div class="form-group">
                                            <label class="control-label col-sm-2" for="name">Name:</label>
                                            <div class="col-sm-10"><input class="form-control" type="text" id="name" name="name1[]" placeholder="Enter name"/></div>
                                        </div><div class="form-group">
                                            <label class="control-label col-sm-2" for="description">Description:</label>
                                            <div class="col-sm-10"><input class="form-control" type="text" id="description" name="description[]" placeholder="Enter description"/></div>
                                        </div><div class="form-group">
                                            <label class="control-label col-sm-2" for="city_idcity">City:</label>
                                            <div class="col-sm-10"><input type="text" class="form-control" id="citysearch" name="citysearch[]" placeholder="Enter  City">
                                                <input type="hidden" class="form-control" name="city_idcity[]"  id="city_idcity">
                                                <div class="containerautocomplete"></div>                                                
                                            </div>
                                        </div>
                                        <hr>
                                        <div class="row tourinti"></div>
                                        <div class="btn btn-primary fa fa-plus" id="add">&nbsp;New</div>
                                    </div>
                                </div>
                            </div>                        
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="">Images:</label>
                            <div class="col-sm-10">
                                <div class="panel panel-info">
                                    <div class="panel-heading text-left"></div>
                                    <div class="panel-body">                                       
                                        <div class="col-sm-6"><label class="row">Front</label>
                                            <div class="row">
                                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                                    <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                                        <img data-src="holder.js/100%x100%" alt="">
                                                    </div>
                                                    <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"></div>
                                                    <div>
                                                        <span class="btn btn-default btn-file"><span class="fileinput-new">Select image</span><span class="fileinput-exists">Change</span><input type="file" name="front"></span>
                                                        <a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                                                    </div>
                                                </div>                                            
                                                <?php echo form_error('front', '<div class="text-danger text-left">', '</div>'); ?>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <label class="row">Images</label>
                                            <div class="row">
                                                <div class="fileinput fileinput-new row" data-provides="fileinput">
                                                    <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                                        <img data-src="holder.js/100%x100%" alt="" >
                                                    </div>
                                                    <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"></div>
                                                    <div>
                                                        <span class="btn btn-default btn-file"><span class="fileinput-new">Select image</span><span class="fileinput-exists">Change</span><input type="file" multiple name="images[]"></span>
                                                        <a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                                                    </div>
                                                </div>
                                            </div>                                                                                       
                                        </div>
                                    </div>

                                </div>
                            </div>                        
                        </div>
                        <div class="form-group"> 
                            <div class="col-sm-4 text-right"></div>
                            <div class="col-sm-2 text-right">
                                <a href="#"><button type="button" name="" id="list" value="list" class="btn btn-block btn-success">List</button></a>
                            </div>
                            <div class="col-sm-2 text-left">
                                <button type="button" name="submit" id="submit" value="submit" class="btn btn-block btn-success">Submit</button>
                            </div>
                            <div class="col-sm-4 text-right"></div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </body>   
    <script type="text/javascript">
        $('#submit').click(function () {
            var formData = new FormData(document.getElementById("formulario"));
            formData.append("dato", "valor");
            $.ajax({
                data: formData,
                url: '<?= base_url() . 'backend/Tours/savetours' ?>',
                type: "POST",
                cache: false,
                contentType: false,
                processData: false,
                success: function (datos)
                {
                    $("#maincontent").html(datos);
                }
            });
        });
        $('input[name="citysearch[]"]').keyup(function () {
            var info = $(this).val();
            $.post('backend/Tours/autocomplete', {info: info}, function (data) {
                if (data != '')
                {
                    $(".containerautocomplete").html(data);
                } else {
                    $(".containerautocomplete").html('');
                }
            })
        });
        function asignar(idcity, nombre) {
            $("#city_idcity").val(idcity);
            $("#citysearch").val(nombre);
            $(".containerautocomplete").html('');
        }
        $("#list").click(function () {
            $(".content-header small").html("Tours List");
            $('#maincontent').load('<?= trim(base_url()) ?>backend/Tours/listtours');
        });
        $('#add').click(function () {
            var contenido = ' <div class="form-group">                                                                                        <div class="col-sm-10"><input type="hidden" id="iddailyitinerary" name="iddailyitinerary[]" placeholder="Enter iddailyitinerary" /></div>                                        </div><div class="form-group">  <label class="control-label col-sm-2" for="date">Date:</label>                                            <div class="col-sm-10"><input class="form-control" type="text" id="date" name="date[]" placeholder="yyyy-mm-dd" /></div>                                        </div><div class="form-group">                                            <label class="control-label col-sm-2" for="name">Name:</label>                                            <div class="col-sm-10"><input class="form-control" type="text" id="name" name="name1[]" placeholder="Enter name"/></div>                                        </div><div class="form-group">                                            <label class="control-label col-sm-2" for="description">Description:</label>                                            <div class="col-sm-10"><input class="form-control" type="text" id="description" name="description[]" placeholder="Enter description"/></div>                                        </div><div class="form-group">                                            <label class="control-label col-sm-2" for="city_idcity">City:</label>                                            <div class="col-sm-10"><input type="text" class="form-control" id="citysearch" name="citysearch[]" placeholder="Enter  City">                                                <input type="hidden" class="form-control" name="city_idcity[]"  id="city_idcity">                                                <div class="containerautocomplete"></div>                                                                                            </div>                                        </div>';
            $('.tourinti').append(contenido);
        });

    </script>
    <script src="<?= base_url(); ?>web/js/jasny-bootstrap.min.js"></script>