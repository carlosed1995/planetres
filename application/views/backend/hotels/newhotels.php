<!doctype html>
<html>
    <head>
        <meta charset="iso-8859-2">
        <title>Planetres - User</title>
        <link href="https://fonts.googleapis.com/css?family=Comfortaa" rel="stylesheet">
        <link href="<?= base_url(); ?>web/css/forms.css" rel="stylesheet" type="text/css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.0.47/jquery.fancybox.min.js"></script>          
        <link href="<?= base_url(); ?>web/css/jasny-bootstrap.min.css" rel="stylesheet" media="screen">
    </head>
    <body>
        <div class="col-md-12">
            <div class="panel  text-left">
                <div class="panel-body">  
                    <?php if ($this->session->flashdata('success')) { ?>
                        <div class="alert alert-success">
                            <a href="#" class="close" data-dismiss="alert">&times;</a>
                            <strong>Success!</strong> <?php echo $this->session->flashdata('success'); ?>
                        </div>

                    <?php } else if ($this->session->flashdata('error')) { ?>

                        <div class="alert alert-danger">
                            <a href="#" class="close" data-dismiss="alert">&times;</a>
                            <strong>Error!</strong> <?php echo $this->session->flashdata('error'); ?>
                        </div>

                    <?php } ?>
                    <form class="form-horizontal" id="formulario" autocomplete="on" method="post" enctype="multipart/form-data">
                        <div class="form-group">                            
                            <div class="col-sm-10"><input type="hidden" id="idhotel" name="idhotel" placeholder="Enter idhotel" value="<?= ($idhotel != '') ? $idhotel : '' ?>"/><?php echo form_error('idhotel', '<div class="text-danger text-left">', '</div>'); ?> </div>
                        </div><div class="form-group">
                            <label class="control-label col-sm-2" for="name">Name:</label>
                            <div class="col-sm-10">
                                <input class="form-control" type="text" id="name" name="name" placeholder="Enter name" value="<?= ($name != '') ? $name : '' ?>"/><?php echo form_error('name', '<div class="text-danger text-left">', '</div>'); ?> </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="ubication">Ubication:</label>
                            <div class="col-sm-10"><input class="form-control" type="text" id="ubication" name="ubication" placeholder="Enter ubication" value="<?= ($ubication != '') ? $ubication : '' ?>"/><?php echo form_error('ubication', '<div class="text-danger text-left">', '</div>'); ?> </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="description">Description:</label>
                            <div class="col-sm-10">
                                <textarea class="form-control" id="description" cols="20" rows="5" name="description" placeholder="Enter description" ><?= ($description != '') ? $description : '' ?></textarea>
                                <?php echo form_error('description', '<div class="text-danger text-left">', '</div>'); ?> </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="areainformation">Area information:</label>
                            <div class="col-sm-10">
                                <textarea class="form-control" id="areainformation" cols="20" rows="5" name="areainformation" placeholder="Enter area information" ><?= ($areainformation != '') ? $areainformation : '' ?></textarea>
                                <?php echo form_error('areainformation', '<div class="text-danger text-left">', '</div>'); ?> </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="map">Map:</label>
                            <div class="col-sm-10"><input class="form-control" type="text" id="map" name="map" placeholder="Enter map" value="<?= ($map != '') ? $map : '' ?>"/><?php echo form_error('map', '<div class="text-danger text-left">', '</div>'); ?> </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="location">Location:</label>                            
                            <div class="col-sm-10">
                                <textarea class="form-control" id="location" cols="20" rows="5" name="location" placeholder="Enter location" ><?= ($location != '') ? $location : '' ?></textarea>
                                <?php echo form_error('location', '<div class="text-danger text-left">', '</div>'); ?> </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="alloment_single">Alloment single:</label>
                            <div class="col-sm-10"><input class="form-control" type="number" step="1" min="0" id="alloment_single" name="alloment_single" placeholder="Enter alloment single" value="<?= ($alloment_single != '') ? $alloment_single : '' ?>"/><?php echo form_error('alloment_single', '<div class="text-danger text-left">', '</div>'); ?> </div>
                        </div><div class="form-group">
                            <label class="control-label col-sm-2" for="alloment_double">Alloment double:</label>
                            <div class="col-sm-10"><input class="form-control" type="number" step="1" min="0" id="alloment_double" name="alloment_double" placeholder="Enter alloment double" value="<?= ($alloment_double != '') ? $alloment_double : '' ?>"/><?php echo form_error('alloment_double', '<div class="text-danger text-left">', '</div>'); ?> </div>
                        </div><div class="form-group">
                            <label class="control-label col-sm-2" for="alloment_family">Alloment family:</label>
                            <div class="col-sm-10"><input class="form-control" type="number" step="1" min="0" id="alloment_family" name="alloment_family" placeholder="Enter alloment family" value="<?= ($alloment_family != '') ? $alloment_family : '' ?>"/><?php echo form_error('alloment_family', '<div class="text-danger text-left">', '</div>'); ?> </div>
                        </div><div class="form-group">
                            <label class="control-label col-sm-2" for="alloment_multifold">Alloment multifold:</label>
                            <div class="col-sm-10"><input class="form-control" type="number" step="1" min="0" id="alloment_multifold" name="alloment_multifold" placeholder="Enter alloment multifold" value="<?= ($alloment_multifold != '') ? $alloment_multifold : '' ?>"/><?php echo form_error('alloment_multifold', '<div class="text-danger text-left">', '</div>'); ?> </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="category_idcategory">Category:</label>
                            <div class="col-sm-10"><select class="form-control" id="category_idcategory" name="category_idcategory" placeholder="Enter category_idcategory">
                                    <option value="">Select category...</option>
                                    <?= $category_idcategory ?>
                                </select><?php echo form_error('category_idcategory', '<div class="text-danger text-left">', '</div>'); ?> </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="city">City:</label>
                            <div class="col-sm-10"> 
                                <input type="text" class="form-control" id="citysearch" name="citysearch" value="<?= $citysearch ?>" placeholder="Enter  City">
                                <input type="hidden" class="form-control" name="city_idcity" value="<?= $city_idcity ?>" id="city_idcity">
                                <div class="containerautocomplete"></div>
                                <?php echo form_error('city_idcity', '<div class="text-danger text-left">', '</div>'); ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <label class="control-label col-sm-2" for="active">Active:</label>
                                <label class="radio-inline col-sm-2">
                                    <input type="radio" id="active1" name="active" <?= (isset($ina) ? $ina : "") ?> value="0"> Inactive
                                </label>
                                <label class="checkbox-inline col-sm-2">
                                    <input type="radio" id="active2" name="active" <?= (isset($act) ? $act : "") ?> value="1"> Active
                                </label>
                            </div>      
                            <div class="row">
                                <div class="col-sm-2"></div>
                                <div class="col-sm-10">
                                    <?php echo form_error('active', '<div class="text-danger text-left">', '</div>'); ?>
                                </div>
                            </div>                            
                        </div>   
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="">Facilities:</label>
                            <div class="col-sm-10">
                                <div class="panel panel-info">
                                    <div class="panel-heading text-left"></div>
                                    <div class="panel-body">
                                        <?= $facilidades ?>
                                    </div>
                                </div>
                            </div>                        
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="">Images:</label>
                            <div class="col-sm-10">
                                <div class="panel panel-info">
                                    <div class="panel-heading text-left"></div>
                                    <div class="panel-body">
                                        <div class="col-sm-4"><label class="row">Logo</label>
                                            <div class="row">
                                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                                    <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                                        <img data-src="holder.js/100%x100%" alt="">
                                                    </div>
                                                    <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"></div>
                                                    <div>
                                                        <span class="btn btn-default btn-file"><span class="fileinput-new">Select image</span><span class="fileinput-exists">Change</span><input type="file" name="logo"></span>
                                                        <a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                                                    </div>
                                                </div>                                            
                                                <?php echo form_error('logo', '<div class="text-danger text-left">', '</div>'); ?>
                                            </div>
                                        </div>
                                        <div class="col-sm-4"><label class="row">Front</label>
                                            <div class="row">
                                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                                    <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                                        <img data-src="holder.js/100%x100%" alt="">
                                                    </div>
                                                    <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"></div>
                                                    <div>
                                                        <span class="btn btn-default btn-file"><span class="fileinput-new">Select image</span><span class="fileinput-exists">Change</span><input type="file" name="front"></span>
                                                        <a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                                                    </div>
                                                </div>                                            
                                                <?php echo form_error('front', '<div class="text-danger text-left">', '</div>'); ?>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <label class="row">Images</label>
                                            <div class="row">
                                                <div class="fileinput fileinput-new row" data-provides="fileinput">
                                                    <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                                        <img data-src="holder.js/100%x100%" alt="" >
                                                    </div>
                                                    <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"></div>
                                                    <div>
                                                        <span class="btn btn-default btn-file"><span class="fileinput-new">Select image</span><span class="fileinput-exists">Change</span><input type="file" multiple name="images[]"></span>
                                                        <a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                                                    </div>
                                                </div>
                                            </div>                                                                               
    <!--                                        <input type="file" class="form-control" name="userFiles[]" multiple/>-->
                                        </div>
                                    </div>

                                </div>
                            </div>                        
                        </div>
                        <div class="form-group"> 
                            <div class="col-sm-4 text-right"></div>
                            <div class="col-sm-2 text-right">
                                <a href="#"><button type="button" name="" id="list" value="list" class="btn btn-block btn-success">List</button></a>
                            </div>
                            <div class="col-sm-2 text-left">
                                <button type="button" name="submit" id="submit" value="submit" class="btn btn-block btn-success">Submit</button>
                            </div>
                            <div class="col-sm-4 text-right"></div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </body>
    <script type="text/javascript">
        $('#submit').click(function () {
            var formData = new FormData(document.getElementById("formulario"));
            formData.append("dato", "valor");
            $.ajax({
                data: formData,
                url: '<?= base_url() . 'backend/Hotels/savehotels' ?>',
                type: "POST",
                cache: false,
                contentType: false,
                processData: false,
                success: function (datos)
                {
                    $("#maincontent").html(datos);
                }
            });
        });
        $("#citysearch").keyup(function () {
            var info = $(this).val();

            $.post('backend/Hotels/autocomplete', {info: info}, function (data) {
                if (data != '')
                {
                    $(".containerautocomplete").html(data);

                } else {
                    $(".containerautocomplete").html('');
                }
            })


        });
        function asignar(idcity, nombre) {
            $("#city_idcity").val(idcity);
            $("#citysearch").val(nombre);
            $(".containerautocomplete").html('');
        }
        $("#list").click(function () {
            $(".content-header small").html("Hotels List");
            $('#maincontent').load('<?= trim(base_url()) ?>backend/Hotels/listhotels');
        });



// All this jquery is just used for presentation. Not required at all for the radio buttons to work.
        $(document).ready(function () {
//   Hide the border by commenting out the variable below
            var $on = 'section';
            $($on).css({
                'background': 'none',
                'border': 'none',
                'box-shadow': 'none'
            });
        });
    </script>
    <!-- Latest compiled and minified JavaScript -->
    <script src="<?= base_url(); ?>web/js/jasny-bootstrap.min.js"></script>