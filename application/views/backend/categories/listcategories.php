<?
?>
<!DOCTYPE html>
<html>
    <head> 
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Simple Serverside jQuery Datatable</title>
        <link href="<?php echo base_url('web/datatables/css/dataTables.bootstrap.min.css') ?>" rel="stylesheet">
        <link href="<?php echo base_url('web/datatables/css/datatablefixedcolumns.css') ?>" rel="stylesheet">        
        <link href="<?php echo base_url('web/css/tooltip.css') ?>" rel="stylesheet">
        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head> 

    <body>
        <div class="row">
            <div class="panel">
                <div class="panel-body">  
                    <?php if ($this->session->flashdata('success')) { ?>
                        <div class="alert alert-success">
                            <a href="#" class="close" data-dismiss="alert">&times;</a>
                            <strong>Success!</strong> <?php echo $this->session->flashdata('success'); ?>
                        </div>

                    <?php } else if ($this->session->flashdata('error')) { ?>

                        <div class="alert alert-danger">
                            <a href="#" class="close" data-dismiss="alert">&times;</a>
                            <strong>Error!</strong> <?php echo $this->session->flashdata('error'); ?>
                        </div>

                    <?php } ?>
                    <div class="col-xs-12 table-responsive">
                        <h3>Categories list</h3>
                        <br/>
                        <table id="table" class="display table-striped" cellspacing="0" width="100%">
                            <thead>
                                <tr>   
                                    <th>No</th>
                                    <th>Name</th>
                                    <th>Description</th>                                                                                                                                
                                    <th>active</th>                                                                      
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                            <tfoot>
                                <tr>
                                   <th>No</th>
                                    <th>Name</th>
                                    <th>Description</th>                                                                                                                                
                                    <th>active</th>                                                                      
                                    <th>Actions</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <script src="<?= base_url(); ?>web/plugins/jQuery/jquery-2.2.3.min.js"></script>        
        <script src="<?= base_url(); ?>web/datatables/js/jquery.dataTables.min.js"></script>
        <script src="<?= base_url(); ?>web/datatables/js/dataTables.bootstrap.min.js"></script>
        <script src="<?= base_url(); ?>web/datatables/js/columnFilter.js"></script>
        <script src="<?= base_url(); ?>web/datatables/js/fnSetFilteringDelay.js"></script>
        <script src="<?= base_url(); ?>web/datatables/js/fnFilterClear.js"></script>

        <script type="text/javascript">
            var table;
            $(document).ready(function () {
                //datatables
                table = $('#table').DataTable({
                    "processing": true, //Feature control the processing indicator.
                    "serverSide": true, //Feature control DataTables' server-side processing mode.
                    "order": [], //Initial no order.

                    // Load data for the table's content from an Ajax source
                    "ajax": {
                        "url": "<?= base_url(); ?>backend/Categories/ajax_listcategories",
                        "type": "POST"
                    },

                    //Set column definition initialisation properties.
                    "columnDefs": [
                        {
                            "targets": [0], //first column / numbering column
                            "orderable": true, //set not orderable                            
                        }
                    ]
                });


            });

            $('[data-toggle="tooltip"]').tooltip();

        </script>
