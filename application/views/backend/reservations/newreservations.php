<!doctype html>
<html>
    <head>
        <meta charset="iso-8859-2">
        <title>Planetres - User</title>
        <link href="https://fonts.googleapis.com/css?family=Comfortaa" rel="stylesheet">
        <link href="<?= base_url(); ?>web/css/forms.css" rel="stylesheet" type="text/css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.0.47/jquery.fancybox.min.js"></script>  
    </head>
    <body>
        <div class="col-md-12">
            <div class="panel  text-left">
                <div class="panel-body">  
                    <? if ($this->session->flashdata('success')) { ?>
                        <div class="alert alert-success">
                            <a href="#" class="close" data-dismiss="alert">&times;</a>
                            <strong>Success!</strong> <?=$this->session->flashdata('success')?>
                        </div>

                    <? } else if ($this->session->flashdata('error')) { ?>

                        <div class="alert alert-danger">
                            <a href="#" class="close" data-dismiss="alert">&times;</a>
                            <strong>Error!</strong> <?=$this->session->flashdata('error')?>
                        </div>

                    <? } ?>
                    <form class="form-horizontal" id="formulario" autocomplete="off">
                        <input type='hidden' value="<?= ($idreservations != '') ? $idreservations : '' ?>" name="idreservations">                        
                        <div class="form-inline text-left">
                            <label class="control-label col-sm-2" for="date_reservations">Reservation date:</label>
                            <div class="">                            
                                <div class="col-sm-10 form-group">
                                    <?= $year . $month . $day; ?>
                                    <?php echo form_error('date_reservations', '<div class="text-danger text-left">', '</div>'); ?>
                                    <?php echo form_error('year', '<div class="text-danger text-left">', '</div>'); ?>
                                    <?php echo form_error('month', '<div class="text-danger text-left">', '</div>'); ?>
                                    <?php echo form_error('day', '<div class="text-danger text-left">', '</div>'); ?>
                                </div>
                            </div>                
                        </div>
                        <div class="form-group"></div>
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="cost_total">Cost:</label>
                            <div class="col-sm-10">
                                <input type="cost_total" class="form-control" id="cost_total" name="cost_total" value="<?= $cost_total ?>" placeholder="Enter  Cost">
                                <?php echo form_error('cost_total', '<div class="text-danger text-left">', '</div>'); ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="pay">Pay:</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="pay" name="pay" value="<?= $pay ?>" placeholder="Enter Duration">
                                <?php echo form_error('pay', '<div class="text-danger text-left">', '</div>'); ?>
                            </div>
                        </div>  
                        <div class="form-group">
                            <div class="row">
                                <label class="control-label col-sm-2" for="active">active:</label>
                                <label class="radio-inline col-sm-2">
                                    <input type="radio" id="active1" name="active" <?= $ina ?> value="0"> Inactive
                                </label>
                                <label class="checkbox-inline col-sm-2">
                                    <input type="radio" id="active2" name="active" <?= $act ?> value="1"> Active
                                </label>
                            </div>      
                            <div class="row">
                                <div class="col-sm-2"></div>
                                <div class="col-sm-10">
                                    <?php echo form_error('active', '<div class="text-danger text-left">', '</div>'); ?>
                                </div>
                            </div>                            
                        </div>                                                
                        <div class="form-group"> 
                            <div class="col-sm-4 text-right"></div>
                            <div class="col-sm-2 text-right">
                                <a href="#"><button type="button" name="" id="list" value="list" class="btn btn-block btn-success">List</button></a>
                            </div>
                            <div class="col-sm-2 text-left">
                                <button type="button" name="submit" id="submit" value="submit" class="btn btn-block btn-success">Submit</button>
                            </div>
                            <div class="col-sm-4 text-right"></div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </body>
    <script type="text/javascript">
        $('#submit').click(function () {
            $.ajax({
                data: $("#formulario").serialize(),
                url: '<?= base_url() . 'backend/Reservations/savereservations' ?>',
                type: "POST",
                success: function (datos)
                {
                    $("#maincontent").html(datos);
                }
            });
        });
        $("#list").click(function () {
            $(".content-header small").html("Reservations List");
            $('#maincontent').load('<?= trim(base_url()) ?>backend/Reservations/listreservations');
        });

    </script>