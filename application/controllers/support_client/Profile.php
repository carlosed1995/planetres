<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profile extends CI_Controller {

	function __construct(){
        parent::__construct();
        if(!$this->session->userdata("login")){
            redirect(base_url('Planetres'));
        }
    }

    function index(){
        $data = array();
        $iduser = $this->session->userdata("iduser");
    	$data['user_information'] = $this->Users_model->selectuser("iduser = ".$iduser);
        $this->load->view('profile/support_client/headersupportclient');
        $this->load->view('profile/support_client/profile_personal_support_client',$data);
        $this->load->view('profile/support_client/footersupportclient');
    }

    function city(){
        $city=$this->Planetresmodel->city("country_idcountry=".$_POST['nationality']);
        foreach ($city->result() as $row) { 
            echo "<option value='".$row->idcity."'>".$row->name."</>";
        }
    }

    function edit_profile_personal_support_client(){
        
       
            $data['data'] = $this->Users_model->selectuser("iduser = ".$this->session->userdata('iduser'));;
            $data['name'] = NULL;
            $data['middle_name'] = NULL;
            $data['last_name'] = NULL;
            $data['passport'] = NULL;
            $data['phone'] = NULL;
            $data['genere'] = NULL;
            $data['birth'] = "0000-00-00";
            $data['cyear'] = "0000";
            $data['cmonth'] = "00";
            $data['cday'] = "00";
            $data['nationality'] = NULL;
            $data['city'] = NULL;
            $data['url'] = NULL;
            $data['zip_code'] = NULL;
            $data['male'] = NULL;
            $data['female'] = NULL;
 
        if (isset($data['year'])) {
            $data['cyear'] = $data['year'];
        }
        if (isset($data['month'])) {
            $data['cmonth'] = $data['month'];
        }
        if (isset($data['day'])) {
            $data['cday'] = $data['day'];
        }


        $data['select'] = NULL;
        $data['year'] = '<select name="year" class="form-control"><option value="">Year...</option>';
        foreach (range(date('Y') - 100, date('Y')) as $numero) {
            $data['year'] .= '<option value="' . $numero . '" ' . (isset($data['cyear']) && $data['cyear'] == $numero ? "selected" : "") . '>' . $numero . '</option>';
        }
        $data['year'] .= '</select>';
        $data['month'] = '<select name="month" class="form-control"><option value="">Month...</option>';
        foreach (range(1, 12) as $numero) {
            $numero = str_pad($numero, "2", "0", STR_PAD_LEFT);
            $data['month'] .= '<option value="' . $numero . '" ' . (isset($data['cmonth']) && $data['cmonth'] == $numero ? "selected" : "") . '>' . $numero . '</option>';
        }
        $data['month'] .= '</select>';
        $data['day'] = '<select name="day" class="form-control"><option value="">Day...</option>';
        foreach (range(1, 31) as $numero) {
            $numero = str_pad($numero, "2", "0", STR_PAD_LEFT);
            $data['day'] .= '<option value="' . $numero . '" ' . (isset($data['cday']) && $data['cday'] == $numero ? "selected" : "") . '>' . $numero . '</option>';
        }$data['day'] .= '</select>';
        /* ----------------------------------------  */

        $data['selectnationality'] = '<select class="form-control" id="nationality" name="nationality" placeholder="Enter nationality">
                                    <option value="">Select ... </option>';


        $nationality = $this->Planetresmodel->nationality();
        foreach ($nationality->result() as $row) {
            $data['selectnationality'] .= '<option value="' . $row->idcountry . '" ' . (isset($data['nationality']) && $data['nationality'] == $row->idcountry ? "selected" : "") . '>' .$row->name .", ". $row->abbre . '</option>';
        }
        $data['selectnationality'] .= '</select>';

        $this->load->view('profile/support_client/headersupportclient');
        $this->load->view('profile/support_client/edit_profile_personal_support_client',$data);
        $this->load->view('profile/support_client/footersupportclient');
    }

    function save_changes_personal_support_client(){
        /*echo "<pre>";
        print_r($this->input->post());
        echo "</pre>";
        die();*/
        /*$this->load->library('form_validation');
        $this->form_validation->set_rules('name', 'Name', 'trim|required|xss_clean|max_length[256]|min_length[2]');
        $this->form_validation->set_rules('lastname', 'Lastname', 'trim|required|xss_clean|max_length[256]|min_length[2]');
        $this->form_validation->set_rules('passport', 'Passport', 'trim|required|xss_clean|max_length[45]|min_length[2]');
        $this->form_validation->set_rules('role', 'Role', 'trim|required|xss_clean|max_length[5]|min_length[1]');
        $this->form_validation->set_rules('phone', 'Phone', 'trim|required|xss_clean|max_length[15]|min_length[2]');
        $this->form_validation->set_rules('city', 'City', 'trim|required|xss_clean|max_length[10]|min_length[1]');
        $this->form_validation->set_rules('genere', 'Gender', 'trim|required|xss_clean|max_length[15]|min_length[1]');

        $this->form_validation->set_rules('year', 'Year', 'trim|required|xss_clean|max_length[4]|min_length[4]|numeric');
        $this->form_validation->set_rules('month', 'Month', 'trim|required|xss_clean|max_length[2]|min_length[2]|numeric');
        $this->form_validation->set_rules('day', 'Day', 'trim|required|xss_clean|max_length[2]|min_length[2]|numeric|callback_checkDay[' . $this->input->post('month') . ',' . $this->input->post('day') . ']');
        
        $this->form_validation->set_message('required', 'The field  %s is required');
        
        $this->form_validation->set_message('min_length', 'Field %s must have at least %s characters');
        
        $this->form_validation->set_message('max_length', 'Field %s must have less %s characters');
        $this->form_validation->set_message('numeric', 'The field  %s is numeric');

        $this->form_validation->set_message('checkDay', '%s Does not match the date format');*/

        $data = $this->input->post();

        if ($this->Users_model->update_data_support_client($data)) {
            $this->session->set_flashdata('success', 'Correctly Updated User');
        } else {
            $this->session->set_flashdata('error', 'Ups! An error has occurred');
        }
            $this->index();
    }
}