<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Planetres extends CI_Controller {

	function __construct(){
		parent::__construct();
		if(!$this->session->userdata('login')){
			redirect(base_url('Planetres'));
		}
	}



	public function index()
	{		
			$type_room 				= $this->Planetresmodel->typeroom();
			$country 				= $this->Planetresmodel->country();
			$guests_excursions_adults  = $this->Planetresmodel->guests_excursions_adults();
			$guests_excursions_kids  = $this->Planetresmodel->guests_excursions_kids();
			$taxies 				= $this->Planetresmodel->taxis_availables();
			/***************************************/
			$data['type_room'] = '<select name="guests_hotel" class="field3 rounded" id="guests_hotel">';
			foreach ($type_room->result() as $key) {
				$data['type_room'] .= '<option value="'.$key->idtype_room.'">'.$key->name.'</option>';
			}
			$data['type_room'] .= '</select>';
			/***************************************/
			$data['country'] = '<select name="destinations" class="field1 rounded" id="destinations">
      								<option value="0">Destination</option>';
			foreach ($country->result() as $key) {
				$data['country'] .= '<option value="'.$key->idcountry.'">'.$key->name.'</option>';
			}
			$data['country'] .= '</select>';
			/***************************************/
			$data['guests_excursions_adults'] = '<select name="guests_excursions_adults" class="field3 rounded" id="guests_excursions_adults">';
			foreach ($guests_excursions_adults->result() as $key) {
				for($i=1;$i<=$key->guests_adults;$i++){
					$data['guests_excursions_adults'] .= '<option value="'.$i.'">'.$i.'</option>';
				}
			}
			$data['guests_excursions_adults'] .= '</select>';
			/***************************************/
			$data['guests_excursions_kids'] = '<select name="guests_excursions_kids" class="field3 rounded" id="guests_excursions_kids">';
			$data['guests_excursions_kids'] .= '<option value="0">0</option>';
			foreach ($guests_excursions_kids->result() as $key) {
				for($i=1;$i<=$key->guests_kids;$i++){
					$data['guests_excursions_kids'] .= '<option value="'.$i.'">'.$i.'</option>';
				}
			}
			$data['guests_excursions_kids'] .= '</select>';
			/***************************************/
			$data['type_room'] = '<select name="guests_hotel" class="field3 rounded" id="guests_hotel">';
			foreach ($type_room->result() as $key) {
				$data['type_room'] .= '<option value="'.$key->idtype_room.'">'.$key->name.'</option>';
			}
			$data['type_room'] .= '</select>';
			/***************************************/
			$data['taxies'] = '<select name="guests_transfer" class="field3 rounded" id="guests_transfer">';
			foreach ($taxies->result() as $key) {
				for($i=1;$i<=$key->car_available;$i++){
					$data['taxies'] .= '<option value="'.$i.'">'.$i.' Taxi </option>';
				}
			}
			$data['taxies'] .= '</select>';
			/***************************************/
			$this->load->view('profile/support_client/headersupportclient');
			$this->load->view('profile/support_client/index',$data);
			$this->load->view('profile/support_client/footersupportclient');
			
	}

	function action_reservation(){
		/*echo "<pre>";
		print_r($_GET);
		echo "</pre>";*/
		if($this->session->userdata('login')){
			
			echo "<script>
			alert('Reservations buyed');
			window.parent.location.href = '".base_url('Planetres')."'
				</script>";
		}else{
			echo "<script>
			alert('Reservations buyed');
			window.parent.location.href = '".base_url('Planetres')."'
				</script>";
		}
	}

	function reservation_hotel(){		
		
			$data['count'] 		= $_GET['category'];
			$details 			= $this->Planetresmodel->details_hotel($_GET['id']);
			$idfacilities		= $this->Planetresmodel->facilities_hotel("hotels_idhotel =".$_GET['id']);
			$idimages 			= $this->Planetresmodel->img_hotel("hotels_idhotel =".$_GET['id']);
			$type_room   		= $this->Planetresmodel->typeroom();
			$count_room 		= $this->Planetresmodel->count_room("hotel_idhotel =".$_GET['id']);
			/***************************************/
			foreach ($count_room->result() as $key) {
				$data['count_room'] = $key->count_room;
			}
			/*-------------------------------------*/
			foreach ($details->result() as $key){
				$data['idhotel'] = $key->idhotel;
			 	$data['name_hotel'] = $key->name; 
			    $data['map'] = $key->map;
			    $data['area_information'] = $key->areainformation;
			    $data['description'] = $key->description;
			}
			/*---------------------------------*/
			$i=1;
			$data['type_room'] = '<select name="guests_hotel" class="field3 rounded" id="guests_hotel">';
			foreach ($type_room->result() as $key) {
				if($_GET['guests_hotel'] == $i){
					$data['type_room'] .= '<option value="'.$key->idtype_room.'" selected>'.$key->name.'</option>';
				}else{
					$data['type_room'] .= '<option value="'.$key->idtype_room.'">'.$key->name.'</option>';
				}
				$i++;
			}
			$data['type_room'] .= '</select>';
			/*---------------------------------*/
			$data['images_hotels'] = '<ul id="image-gallery" class="gallery list-unstyled cS-hidden">';
			foreach ($idimages->result() as $key) {
				
				$data['images_hotels'] .='<li data-thumb=""> <img src="'.base_url().'web/images/gallery/hotels/'.$key->hotels_idhotel.'/'.$key->name.'" alt="" tittle=""> </li>';
			}
			$data['images_hotels'] .= '</ul>';
			/*-------------------------------*/
			$data['facilities'] = '<ul class="lst-items">';
			foreach ($idfacilities->result() as $key) {
				$data['facilities'] .='<li>'.$key->name.'</li>';
			}
			$data['facilities'] .= '</ul>';
			/*-------------------------------*/
			$this->load->view('profile/support_client/headersupportclient');
			$this->load->view('profile/support_client/details-hotel',$data);
			$this->load->view('profile/support_client/footersupportclient');
	}	

	function form_hotel_result(){
		$count_room = $this->input->post('roomcount1')+$this->input->post('roomcount2')+$this->input->post('roomcount3')+$this->input->post('roomcount4');
		$room = $this->Planetresmodel->room("hotel_idhotel =".$this->input->post('idhotel')." and type_room_idtype_room = ".$this->input->post('guests_hotel'));
			
		
		if($room){
			$c=1;
			$table_type_room = '<table class="table table-bordered">
													<thead>
														<tr>
															<th>Room type</th>
															<th>Nights</th>
															<th>Price</th>
														</tr>
													</thead>';
			if($this->input->post('guests_hotel') > 2){
				for($i=1;$i<=$count_room;$i++){
					foreach ($room->result() as $key) {
						$table_type_room.= '<tbody>
												<tr>
													<td><input name="active" type="radio">'.$key->description.'</td>
													<td><input name="nights" type="hidden" class="field2d rounded" id="nights_td'.$c.'" value="'.$this->input->post('nights').'">'.$this->input->post('nights').'</td>
													<td>??????</td>
												</tr>
											</tbody>';
											$c++;
					}
				}											
			}else{
				foreach ($room->result() as $key) {
					$table_type_room.= '<tbody>
											<tr>
												<td><input name="active" type="radio">'.$key->description.'</td>
												<td><input name="nights" type="hidden" class="field2d rounded" id="nights_td'.$c.'" value="'.$this->input->post('nights').'">'.$this->input->post('nights').'</td>
												<td>??????</td>
											</tr>
										</tbody>';
										$c++;
				}
			}			
			$table_type_room.='</table>';
			echo $table_type_room;
			echo '<button type="button" name="submit1" id="submit1" value="Add to cart" class="field4 rounded">Add to cart</button>';
		}else{
			echo "Was not found";
		}
	}

	function subtractdays($date1,$date2) {
        $datetime1 = new DateTime($date1);
        $datetime2 = new DateTime($date2);
        $interval = $datetime1->diff($datetime2);
        return $interval->format('%r%a');
    }

	function reservation_tour(){
	
		$details         	= $this->Planetresmodel->details_tours($_GET['id']);
		$iditinerary	 	= $this->Planetresmodel->daily_itinerary("daily_itinerary.tours_idtours =".$_GET['id']." and daily_itinerary.active = 1");
		$idimages 		 	= $this->Planetresmodel->img_tours("tours_idtours =".$_GET['id']);
		$departure_date		= $this->Planetresmodel->departure_date_tours("tour_idtour = ".$_GET['id']." and active = 1");
		$hotel_class_tours 		= $this->Planetresmodel->hotel_class_tours("daily_itinerary.tours_idtours =".$_GET['id']." and daily_itinerary.active = 1");
		/*******************************************/
		foreach ($details->result() as $key) {
			$data['idtours'] = $key->idtours;
			$data['name_tours'] = $key->name;
			$data['starting_city'] = $key->starting_city;
			$data['highlights'] = $key->highlights;
			$data['max_persons'] = $key->max_persons;
			$data['type_tours'] = $key->tours_type_idtours_type;
		}
		/************************************************/
		$data['images_tours'] = '<ul id="image-gallery" class="gallery list-unstyled cS-hidden">';
		foreach ($idimages->result() as $key) {
			$data['images_tours'] .='<li data-thumb=""> <img src="'.base_url().'web/images/gallery/tours/elizabeth_tours/'.$key->name.'" alt="" tittle=""> </li>';
		}
		$data['images_tours'] .= '</ul>';
		/*-------------------------------*/
		$data['itinerary'] = NULL;
		foreach ($iditinerary->result() as $key) {
			$data['itinerary'] .='<p><strong>'.$key->day_of_tours.'</strong></p>';
			$data['itinerary'] .='<p>'.$key->description.'</p>';
			$data['itinerary'] .='<strong>Hoteles</strong>';
			$data['itinerary'] .='<p><strong>First Class:</strong>'.$key->first_class.'</p>';
			$data['itinerary'] .='<p><strong>Moderate First Class:</strong>'.$key->moderate_first_class.'</p>';
		}
		/******************************************/
		$data['departure_date'] = '<select name="departure_date_tours" class="field3 rounded" id="departure_date_tours">';
		foreach ($departure_date->result() as $key) {
			$data['departure_date'] .= '<option value="'.$key->departure_date.'">'.$key->departure_date.'</option>';
		}
		$data['departure_date'] .= '</select>';
		/******************************************/
		$data['departure_city'] = '<select name="departure_city_tours" class="field3 rounded" id="departure_city_tours">';
		foreach ($details->result() as $key) {
			if($key->starting_city != NULL){
				$data['departure_city'] .= '<option value="'.$key->starting_city.'">Land Only</option>';
			}
		}
		$data['departure_city'] .= '</select>';
		/******************************************/
		$this->load->view('profile/support_client/headersupportclient');
		$this->load->view('profile/support_client/details-tours',$data);
		$this->load->view('profile/support_client/footersupportclient');
	}
	
		
	function get_price_tours(){
		/*SI EL $this->input->post('hotel_class_tours') ES IGUAL A 1 ES MFC
		  SI EL $this->input->post('hotel_class_tours') ES IGUAL A 2 ES FC */
		$price_per_person 	= $this->Planetresmodel->price_per_person_tours("departure_date_and_price.tour_idtour = ".$this->input->post('idtours')." and departure_date_and_price.departure_date = '".$this->input->post('departure_date_tours')."' and tours.starting_city = '".$this->input->post('departure_city_tours')."' and departure_date_and_price.active = 1");

		if($this->input->post('private')==1 && $this->input->post('hotel_class_tours')==1){
			
			foreach ($price_per_person->result() as $key) {
				
				echo '<p><input type="hidden" name="price_per_person_tours" value="'.$key->moderate_first_class_single_supplement.'"><strong>Price per Person: </strong>'.$key->moderate_first_class_single_supplement.'$</p>';
				
			}
		}elseif($this->input->post('private')==1 && $this->input->post('hotel_class_tours')==2){
				
			foreach ($price_per_person->result() as $key) {
				
				echo '<p><input type="hidden" name="price_per_person_tours" value="'.$key->first_class_single_supplement.'"><strong>Price per Person: </strong>'.$key->first_class_single_supplement.'$</p>';
				
			}
		}elseif($this->input->post('hotel_class_tours')==1){
			
			foreach ($price_per_person->result() as $key) {
				
				echo '<p><input type="hidden" name="price_per_person_tours" value="'.$key->moderate_first_class_price.'"><strong>Price per Person: </strong>'.$key->moderate_first_class_price.'$</p>';
				
			}
		}elseif($this->input->post('hotel_class_tours')==2) {
			
			foreach ($price_per_person->result() as $key) {
				
				echo '<p><input type="hidden" name="price_per_person_tours" value="'.$key->first_class_price.'"><strong>Price per Person: </strong>'.$key->first_class_price.'$</p>';
				
			}
		}
	}

	function customize_tours(){
		$data['data'] = $this->input->post();
		/***********************************/
		$room_count = $this->input->post('roomcount_tours1')+$this->input->post('roomcount_tours2')+$this->input->post('roomcount_tours3');
		$kids_total = $this->input->post('kids_tours1')+$this->input->post('kids_tours2')+$this->input->post('kids_tours3');
		$adult_total = $this->input->post('adult_tours1')+$this->input->post('adult_tours2')+$this->input->post('adult_tours3');
		$person_total = $kids_total + $adult_total;
		if($this->input->post('guests_tours') != 0){
			$data['person_total'] = $this->input->post('guests_tours');
		}else{
			$data['person_total'] = $person_total;
		}
		/***********************************/
		if($this->input->post('guests_tours') != 0){
			$typeroom = $this->Planetresmodel->typeroom("idtype_room = ".$this->input->post('guests_tours'),"group by active");
			foreach ($typeroom->result() as $key) {
				$tp = $key->description;
			}
		}elseif($adult_total < 4 && $kids_total < 4){
			$typeroom = $this->Planetresmodel->typeroom("num_adults BETWEEN ".$adult_total." and 4 and num_kids BETWEEN ".$kids_total." and 4 and active = 1","group by active");
			foreach ($typeroom->result() as $key) {
				$tp = $key->description;
			}
		}elseif($adult_total > 3 && $kids_total > 3){
			$typeroom = $this->Planetresmodel->typeroom("num_adults BETWEEN 4 AND ".$adult_total." and num_kids BETWEEN 4 and ".$kids_total." and active = 1","group by active");
			foreach ($typeroom->result() as $key) {
				$tp = $key->description;
			}
		}elseif($adult_total > 3 && $kids_total < 4){
			$typeroom = $this->Planetresmodel->typeroom("num_adults BETWEEN 4 and ".$adult_total." and num_kids BETWEEN ".$kids_total." and 4 and active = 1","group by active");
			foreach ($typeroom->result() as $key) {
				$tp = $key->description;
			}
		}elseif($adult_total < 4 && $kids_total > 3){
			$typeroom = $this->Planetresmodel->typeroom("num_adults BETWEEN ".$adult_total." and 4 and num_kids BETWEEN 4 and ".$kids_total." and active = 1","group by active");
			foreach ($typeroom->result() as $key) {
				$tp = $key->description;
			}
		}
		/***********************************/
		if($this->input->post('guests_tours') == 0){
			$price_total = $this->input->post('price_per_person_tours') * $person_total;
		}else{
			$price_total = $this->input->post('price_per_person_tours') * $this->input->post('guests_tours');
		}
		
		/***********************************/
		if($this->input->post('hotel_class_tours') == 1){
			$day_of_tours 	= $this->Planetresmodel->review_itinerary('count(daily_itinerary.iddailyitinerary) as count_day from daily_itinerary inner join hotel on hotel.idhotel=daily_itinerary.hotel_mfc_idhotel inner join city on city.idcity=daily_itinerary.city_idcity inner join departure_date_and_price on departure_date_and_price.tour_idtour=daily_itinerary.tours_idtours','daily_itinerary.tours_idtours ='.$this->input->post('idtours').' and daily_itinerary.active = 1 and hotel.category_idcategory <= 3 and departure_date_and_price.departure_date = "'.$this->input->post('departure_date_tours').'"');
			foreach ($day_of_tours->result() as $key) {
				$day_of_tours_count = $key->count_day;
			}
			$review_itinerary = $this->Planetresmodel->review_itinerary(' departure_date_and_price.departure_date, daily_itinerary.service_types, daily_itinerary.description as service_details , hotel.name as hotel from daily_itinerary inner join hotel on hotel.idhotel=daily_itinerary.hotel_mfc_idhotel inner join departure_date_and_price on departure_date_and_price.tour_idtour=daily_itinerary.tours_idtours','daily_itinerary.tours_idtours ='.$this->input->post('idtours').' and daily_itinerary.active = 1 and hotel.category_idcategory <= 3 and departure_date_and_price.departure_date = "'.$this->input->post('departure_date_tours').'"');
			$i=1;
			foreach ($review_itinerary->result() as $key) {
				$departure_date 	= $key->departure_date;
				$service_types[$i]  	= $key->service_types;
				$service_details[$i] 	= $key->service_details;
				$hotel[$i]				= $key->hotel;
				$i++;
			}
			
		}
		if($this->input->post('hotel_class_tours') == 2){
			$day_of_tours 	= $this->Planetresmodel->review_itinerary('count(daily_itinerary.iddailyitinerary) as count_day from daily_itinerary inner join hotel on hotel.idhotel=daily_itinerary.hotel_fc_idhotel inner join city on city.idcity=daily_itinerary.city_idcity inner join departure_date_and_price on departure_date_and_price.tour_idtour=daily_itinerary.tours_idtours','daily_itinerary.tours_idtours ='.$this->input->post('idtours').' and daily_itinerary.active = 1 and hotel.category_idcategory >= 4 and departure_date_and_price.departure_date = "'.$this->input->post('departure_date_tours').'"');
			foreach ($day_of_tours->result() as $key) {
				$day_of_tours_count = $key->count_day;
			}
			$review_itinerary = $this->Planetresmodel->review_itinerary(' departure_date_and_price.departure_date, daily_itinerary.service_types, daily_itinerary.description as service_details , hotel.name as hotel from daily_itinerary inner join hotel on hotel.idhotel=daily_itinerary.hotel_fc_idhotel inner join departure_date_and_price on departure_date_and_price.tour_idtour=daily_itinerary.tours_idtours','daily_itinerary.tours_idtours ='.$this->input->post('idtours').' and daily_itinerary.active = 1 and hotel.category_idcategory >= 4 and departure_date_and_price.departure_date = "'.$this->input->post('departure_date_tours').'"');
			$i=1;
			foreach ($review_itinerary->result() as $key) {
				$departure_date 	= $key->departure_date;
				$service_types[$i]  	= $key->service_types;
				$service_details[$i] 	= $key->service_details;
				$hotel[$i]				= $key->hotel;
				$i++;
			}
		}
		
		/***********************************/
		$c = 1;
		if($this->input->post('kids_tours'.$c) != 0){
			for($i=0;$i <= $this->input->post('$kids_tours'.$c);$i++){
				$data['table_passengers'] = '<form class="form-horizontal" id="form_guests">
				<table class="table table-bordered">
					<thead>
						<tr>
							<td></td>
							<td>First Name</td>
							<td>Last Name</td>
							<td>Tittle</td>
							<td>Nationality</td>
							<td>Age</td>
							<td>Child</td>
						</tr>
					</thead><tbody>';
				for($i=1; $i <= $person_total; $i++){
				
					$data['table_passengers'] .= '
							<tr>
								<td><input type="checkbox" name="active'.$i.'" value="1" id="active'.$i.'"></td>
								<td><input type="text" id="name'.$i.'" name="name'.$i.'" value="Name '.$i.'"></td>
								<td><input type="text" id="last_name'.$i.'" name="last_name'.$i.'" value="Last Name '.$i.'"></td>
								<td>
									<select name="title'.$i.'" id="title'.$i.'">
										<option>Mr</option>
										<option>Ms</option>
										<option>Mrs</option>
										<option>Miss</option>
										<option>Child</option>
									</select>
								</td>
								<td width="70%"><input type="text" id="nationality'.$i.'" name="nationality'.$i.'" value="Nationality '.$i.'"></td>
								<td width="20%"><input type="text" id="age'.$i.'" name="age'.$i.'"></td>
								<td><input type="checkbox" name="child'.$i.'" value="1" id="child'.$i.'"></td>
							</tr>';
					$data['count_check'] = $i;
				}
				$data['table_passengers'] .= '
						<tr>
							<td></td>
							<td>Room Type:</br>
								<p>'.$tp.'</p>
							</td>';
				for($i=1; $i <= $person_total; $i++){
					if($i<=4){
						$data['table_passengers'] .= '<td>Traveler '.$i.'</br><select name="passenger'.$i.'" id="passenger'.$i.'" class="passenger"></select></td>';
						if($i==4){
							$data['table_passengers'] .= '
							</tr>';
						}
					}
					if($i>=5){
						if($i==5 || $i==9){
							$data['table_passengers'] .= '
							<tr>
								<td></td>
								<td>Room Type:</br>
									<p>'.$tp.'</p>
								</td>';
						}
							$data['table_passengers'] .= '
							<td>Traveler '.$i.'</br><select name="passenger'.$i.'" id="passenger'.$i.'" class="passenger"></select></td>';
						if($i==12){
							$data['table_passengers'] .= '
							</tr>';
						}
					}
				}
				$data['table_passengers'] .='
					</tbody>
				</table>
				</form>';
			}
		}

		if($this->input->post('guests_tours') != 0){
			for($i=1;$i<=$this->input->post('guests_tours');$i++){
			
				$data['table_passengers'] = '<form class="form-horizontal" id="form_guests">
										<table class="table table-bordered">
												<thead>
													<tr>
														<td></td>
														<td>First Name</td>
														<td>Last Name</td>
														<td>Tittle</td>
														<td>Nationality</td>
														<td>Age</td>
													</tr>
												</thead><tbody>';
				for($i=1; $i <= $this->input->post('guests_tours'); $i++){
					$data['table_passengers'] .= '
							<tr>
								<td><input type="checkbox" name="active'.$i.'" value="1" id="active'.$i.'"></td>
								<td><input type="text" id="name'.$i.'" name="name'.$i.'" value="Name '.$i.'"></td>
								<td><input type="text" id="last_name'.$i.'" name="last_name'.$i.'" value="Last Name '.$i.'"></td>
								<td>
									<select name="title'.$i.'" id="title'.$i.'">
										<option>Mr</option>
										<option>Ms</option>
										<option>Mrs</option>
										<option>Miss</option>
										<option>Child</option>
									</select>
								</td>
								<td width="70%"><input type="text" id="nationality'.$i.'" name="nationality'.$i.'" value="Nationality '.$i.'"></td>
								<td width="20%"><input type="text" id="age'.$i.'" name="age'.$i.'"></td>
							</tr>';
					$data['count_check'] = $i;
				}
				$data['table_passengers'] .= '
						<tr>
							<thead>
								<td></td>
								<td>Room Type</br><p>'.$tp.'</p></td>';
				for($i=1; $i <= $this->input->post('guests_tours'); $i++){
					$data['table_passengers'] .= '<td>Traveler '.$i.'</br><select name="passenger'.$i.'" id="passenger'.$i.'" class="passenger"></select></td>';
				}
			
				$data['table_passengers'] .='
							
						</tr>
					</tbody>
				</table></form>';
			}
			$c++;
		}
		/**********************************/
		$data['price_chart'] = '<div class="panel panel-default">
						<div class="panel-heading">Price Chart</div>
 						<div class="panel-body">
 							<form class="form-horizontal" id="form_price">
 								<input type="hidden" name="price_total" id="price_total" value="'.$price_total.'">
 								<input type="hidden" name="price_per_person" id="price_per_person" value="'.$this->input->post('price_per_person_tours').'">
	 							<p>Service Price&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;$'.$price_total.'</p>
	 							<p><strong>Total Price</strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>$'.$price_total.'</strong></p>
	 							<p>Price per person&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;$'.$this->input->post('price_per_person_tours').'</p>
	 							<button class="field4 rounded" id="add_cart" name="add_cart">Add to Cart</button>
	 						</form>
 						</div>
					</div>';
		/**********************************/
		$data['review_itinerary'] = '
 						<table class="table table-bordered">
 							<thead>
 								<tr>
	 								<td>Date</td>
		 							<td>Service Types</td>
		 							<td>Service Details</td>
	 								<td>Hotel Name</td>
	 							</tr>
 							</thead>
 							<tbody>
 								';
 		for($i=1 ;$i <= $day_of_tours_count; $i++){
 			$data['review_itinerary'] .= '<tr><td>'.$this->add_days($departure_date,$i).'</td>
 										<td>'.$service_types[$i].'</td>
 										<td>'.$service_details[$i].'</td>
 										<td>'.$hotel[$i].'</td></tr>';
 		}
 		$data['review_itinerary'] .= '
 				</tbody></table>
 					';
		$this->load->view('profile/support_client/headersupportclient');
		$this->load->view('profile/support_client/customize_tours',$data);
		$this->load->view('profile/support_client/footersupportclient');
	}
	function add_days($date1, $day) {
        $date = new DateTime($date1);
        $date->add(new DateInterval('P' . $day . 'D'));
        return $date->format('Y/M/D');
    }
	function reservation_excursion(){		
		$details 		     = $this->Planetresmodel->details_excursion($_GET['id']);
		$pricing_chart	     = $this->Planetresmodel->pricing_chart("excursions_idexcursions =".$_GET['id']);
		$idimages 		     = $this->Planetresmodel->img_excursions("excursions_idexcursions =".$_GET['id']);
		$departure_date 	 = $this->Planetresmodel->departure_date_excursions("excursions_idexcursions =".$_GET['id']." and active = 1");
		/*--------------------------------*/
		foreach ($details->result() as $key) {
			$data['idexcursions'] = $key->idexcursions;
			$data['name_excursions'] = $key->name;
			$data['info'] = $key->info;
			$data['highlights'] = $key->highlights;
			$data['max_persons'] = $key->guests_adults+$key->guests_kids;
			$data['city_starting'] = $key->city_idcity;
			$guests_adults = $key->guests_adults;
			$guests_kids = $key->guests_kids;
		}
		$data['images_excursions'] = '<ul id="image-gallery" class="gallery list-unstyled cS-hidden">';
		foreach ($idimages->result() as $key) {
			$data['images_excursions'] .='<li data-thumb=""> <img src="'.base_url().'web/images/gallery/excursions/arica_private_city_tour/'.$key->name.'" alt="" tittle=""> </li>';
		}
		$data['images_excursions'] .= '</ul>';
		/*-------------------------------*/
		$data['pricing_chart'] = '<table class="table table-bordered">
    								<thead>
      									<tr>
      										<th>#</th>
        									<th>Pick up</th>
        									<th>Drop off</th>
        									<th>Start time</th>
        									<th>End time</th>
        									<th>Price</th>
      									</tr>
    								</thead>
    								<tbody>';
		foreach ($pricing_chart->result() as $key) {
			$data['pricing_chart'] .=   '<tr>
									        <td>'.$key->idpricing_chart.'</td>
									        <td>'.$key->pickup.'</td>
									        <td>'.$key->drop_off.'</td>
									        <td>'.$key->start_time.'</td>
									        <td>'.$key->end_time.'</td>
									        <td>'.$key->description.'</td>
									      </tr>
									      ';
		}
			$data['pricing_chart'] .=	'</tbody>
									</table>';
		/*-------------------------------*/
		$data['departure_date'] = '<select name="departure_date_excursions" class="field3 rounded" id="departure_date_excursions">';
		foreach ($departure_date->result() as $key) {
			$data['departure_date'].= '<option value="'.$key->departure_date.'">'.$key->departure_date.'</option>';
		}
		$data['departure_date'].= '</select>';
		/*-------------------------------*/
		$data['guests_excursions'] = '<select name="guests_excursions" class="field3 rounded" id="guests_excursions">';
		for($i=1;$i<=$guests_adults;$i++) {
			if($i == $_GET['guests_excursions']){
				$data['guests_excursions'].= '<option value="'.$i.'" selected> '.$i.' </option>';
			}else{
				$data['guests_excursions'].= '<option value="'.$i.'"> '.$i.' </option>';
			}
		}
		$data['guests_excursions'].= '</select>';
		/*-------------------------------*/		
		$data['guests_kids'] = '<select name="guests_kids" class="field3 rounded" id="guests_kids">';
		for($i=0;$i<=$guests_kids;$i++) {
			if($i == $_GET['guests_kids']){
				$data['guests_kids'].= '<option value="'.$i.'" selected> '.$i.' </option>';
			}else{
				$data['guests_kids'].= '<option value="'.$i.'"> '.$i.' </option>';
			}
		}
		$data['guests_kids'].= '</select>';
		/*-------------------------------*/						
			$this->load->view('profile/support_client/headersupportclient');
			$this->load->view('profile/support_client/details-excursion',$data);
			$this->load->view('profile/support_client/footersupportclient');									
	}

	function get_price_excursions(){
		/*SI EL $this->input->post('hotel_class_tours') ES IGUAL A 1 ES MFC
		  SI EL $this->input->post('hotel_class_tours') ES IGUAL A 2 ES FC */
		$price_per_person 	= $this->Planetresmodel->price_per_person_excurions("departure_date_and_price_excursions.excursions_idexcursions = ".$this->input->post('idexcursions')." and departure_date_and_price_excursions.departure_date = '".$this->input->post('departure_date_excursions')."' and excursions.city_idcity = '".$this->input->post('city_starting')."' and departure_date_and_price_excursions.active = 1");

		
			foreach ($price_per_person->result() as $key) {
				
				echo '<p><input type="hidden" name="price_per_person_tours_excursions" value="'.$key->price.'"><strong>Price per Person: </strong>'.$key->price.'$</p>';
				
			}
		
	}

	function reservation_transfers(){		
		$details = $this->Planetresmodel->details_transfers($_GET['id']);
		$departure_transfers = $this->Planetresmodel->departure_transfers();
		$count_departure = $this->Planetresmodel->count_departure();
		foreach ($count_departure->result() as $key) {
			$data['count'] = $key->count_departure;
		}
		foreach ($details->result() as $key) {
			$data['idtransfers'] 			= $key->idtransfers;
			$data['name_transfers'] 		= $key->name;
			$data['type_transfers'] 		= $key->type;
			$data['description_transfers'] 	= $key->description;
			$data['available_cars'] 		= $key->cars_availables;
		}
		$data['arrival_transfers'] = '<table class="table table-bordered">
	    							<thead>
	    								<tr>
	    									<th>Add item</th>
		    								<th>Transfer Name</th>
		    								<th>Adult</th>
		    								<th>Kid</th>
		    								<th>Time</th>
	    									<th>Price per person</th>
	    								</tr>
	    							</thead>
	    							<tbody>';
	   	foreach ($details->result() as $key) {	
	   		for ($i=1;$i<=$_GET['guests_transfer'];$i++){
	   		$data['arrival_transfers'] .= '<tr>
	    			<td><input disabled="true" type="checkbox" name="active_arrival" id="active_arrival'.$i.'" value="0"></td>
	    			<td><input name="idtransfers" type="hidden" value="'.$key->idtransfers.'">'.$key->name.'</td>
	    			<td>
	    				<select name="adults_arrival" id="adults_arrival'.$i.'">
	    					<option value="">-</option>
	    					<option value="1">1</option>
	    					<option value="2" selected>2</option>
	    					<option value="3">3</option>
	    					<option value="4">4</option>
	    					<option value="5">5</option>
	    					<option value="6">6</option>
	    					<option value="7">7</option>
	    					<option value="8">8</option>
	    				</select>
	    			</td>
	    			<td>
	    				<select name="kids_arrival" id="kids_arrival'.$i.'">
	    					<option value="">-</option>
	    					<option value="1" selected>1</option>
	    					<option value="2">2</option>
	    					<option value="3">3</option>
	    					<option value="4">4</option>
	    				</select>
	    			</td>
	    			<td>
	    				<select name="hr_arrival" id="hr_arrival'.$i.'" class="hr_arrival">
	    					<option value="" selected>-</option>
	    					<option value="01">01</option>
	    					<option value="02">02</option>
	    					<option value="03">03</option>
	    					<option value="04">04</option>
	    					<option value="05">05</option>
	    					<option value="06">06</option>
	    					<option value="07">07</option>
	    					<option value="08">08</option>
	    					<option value="09">09</option>
	    					<option value="10">10</option>
	    					<option value="11">11</option>
	    					<option value="12">12</option>
	    				</select>:
	    				<select name="min_arrival" id="min_arrival'.$i.'" class="min_arrival">
	    					<option value="" selected>-</option>
	    					<option value="00">00</option>
	    					<option value="10">10</option>
	    					<option value="20">20</option>
	    					<option value="30">30</option>
	    					<option value="40">40</option>
	    					<option value="50">50</option>
	    				</select>
	    				<select name="am_pm_arrival" id="am_pm_arrival'.$i.'" class="am_pm_arrival">
	    					<option value="" selected>-</option>
	    					<option value="AM">AM</option>
	    					<option value="PM">PM</option>
	    				</select>
	    			</td>
	    		</tr>';
	    	}
	   	}
	   	$data['arrival_transfers'] .= '</tbody>
	   							</table>';

	   	$data['departure_transfers'] = '<table class="table table-bordered">
	    							<thead>
	    								<tr>
	    									<th>Add item</th>
		    								<th>Transfer Name</th>
		    								<th>Adult</th>
		    								<th>Kid</th>
		    								<th>Time</th>
	    									<th>Price per person</th>
	    								</tr>
	    							</thead>
	    							<tbody>';
	   	$c=1;
	   	foreach ($departure_transfers->result() as $key) {	
	   		$data['departure_transfers'] .= '<tr>
	    			<td><input disabled="true" type="checkbox" name="active_departure" id="active_departure'.$c.'" value="0"></td>
	    			<td><input name="idtransfers" type="hidden" value="'.$key->idtransfers.'">'.$key->name.'</td>
	    			<td>
	    				<select name="adults_departure" id="adults_departure">
	    					<option value="">-</option>
	    					<option value="1">1</option>
	    					<option value="2" selected>2</option>
	    					<option value="3">3</option>
	    					<option value="4">4</option>
	    					<option value="5">5</option>
	    					<option value="6">6</option>
	    					<option value="7">7</option>
	    					<option value="8">8</option>
	    				</select>
	    			</td>
	    			<td>
	    				<select name="kids_departure" id="kids_departure">
	    					<option value="">-</option>
	    					<option value="1" selected>1</option>
	    					<option value="2">2</option>
	    					<option value="3">3</option>
	    					<option value="4">4</option>
	    				</select>
	    			</td>
	    			<td>
	    				<select name="hr_departure" id="hr_departure'.$c.'" class="hr_departure">
	    					<option value="" selected>-</option>
	    					<option value="01">01</option>
	    					<option value="02">02</option>
	    					<option value="03">03</option>
	    					<option value="04">04</option>
	    					<option value="05">05</option>
	    					<option value="06">06</option>
	    					<option value="07">07</option>
	    					<option value="08">08</option>
	    					<option value="09">09</option>
	    					<option value="10">10</option>
	    					<option value="11">11</option>
	    					<option value="12">12</option>
	    				</select>:
	    				<select name="min_departure" id="min_departure'.$c.'" class="min_departure">
	    					<option value="" selected>-</option>
	    					<option value="00">00</option>
	    					<option value="10">10</option>
	    					<option value="20">20</option>
	    					<option value="30">30</option>
	    					<option value="40">40</option>
	    					<option value="50">50</option>
	    				</select>
	    				<select name="am_pm_departure" id="am_pm_departure'.$c.'" class="am_pm_departure">
	    					<option value="" selected>-</option>
	    					<option value="AM">AM</option>
	    					<option value="PM">PM</option>
	    				</select>
	    			</td>
	    		</tr>';
	    		++$c;
	   	}
	   	$data['departure_transfers'] .= '</tbody>
	   							</table>';

		$this->load->view('profile/support_client/headersupportclient');
		$this->load->view('profile/support_client/details-transfers',$data);
		$this->load->view('profile/support_client/footersupportclient');
	}

	function city_filter(){
		$city=$this->Planetresmodel->city_filter($this->input->post('country'));
        foreach ($city->result() as $row) { 
         	echo '<option value="'.$row->idcity.'">'.$row->name.'</option>';            
        }
    }
	
	function destroySession(){
		if(!$this->session->userdata('login')){
			/*hotels*/
			unset($_GET['city_hotel']);
			unset($_GET['start_hotel']);
			unset($_GET['end_hotel']);
			unset($_GET['guests_hotel']);
			unset($_GET['room']);
			unset($_GET['adult']);
			unset($_GET['kids']);
			unset($_GET['adult1']);
			unset($_GET['adult2']);
			unset($_GET['adult3']);
			unset($_GET['adult4']);
			unset($_GET['kids1']);
			unset($_GET['kids2']);
			unset($_GET['kids3']);
			unset($_GET['kids4']);	
			/*transfers*/
			unset($_GET['start_transfer']);
			unset($_GET['end_transfer']);
			unset($_GET['citytransf']);
			unset($_GET['guests_transfer']);
			unset($_GET['count_transport']);
			unset($_GET['person']);
			/*excursions*/
			unset($_GET['start_excursions']);
			unset($_GET['end_excursions']);
			unset($_GET['city_excursions']);
			unset($_GET['guests_excursions']);
			unset($_GET['guests_kids']);
			unset($_GET['guests_total']);
			/*tours*/
			unset($_GET['destinations']);
			unset($_GET['guests_tours']);
			unset($_GET['room_tours']);
			unset($_GET['adult_tours']);
			unset($_GET['kids_tours']);
			unset($_GET['adult_tours1']);
			unset($_GET['adult_tours2']);
			unset($_GET['adult_tours3']);
			unset($_GET['kids_tours1']);
			unset($_GET['kids_tours2']);
			unset($_GET['kids_tours3']);
		}
		
	}

	function change_count_nights(){
		echo $this->subtractdays($this->input->post('chkin'),$this->input->post('chkout'));
	}
}
