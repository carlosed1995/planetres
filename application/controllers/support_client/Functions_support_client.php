<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Functions_support_client extends CI_Controller {

	function __construct(){
        parent::__construct();
        if(!$this->session->userdata("login")){
            redirect(base_url('Planetres'));
        }
    }
 
    function new_support_client($data = NULL){
        $nationality = $this->Planetresmodel->country();
        $rol = $this->Axcs_model->rol();

        if($data == NULL){
            $data['iduser']= $this->session->userdata('iduser');
            $data['hidden_lat'] = NULL;
            $data['hidden_long'] = NULL;
            $data['name'] = NULL;
            $data['middle_name'] = NULL;
            $data['lastname'] = NULL;
            $data['email'] = NULL;
            $data['confirm_email'] = NULL;
            $data['pwd'] = NULL;
            $data['pwd1'] = NULL;
            $data['phone'] = NULL;
            $data['birth'] = "0000-00-00";
            $data['cyear'] = "0000";
            $data['cmonth'] = "00";
            $data['cday'] = "00";
            $data['citysearch'] = NULL;
            $data['city'] = NULL;
        }
        
        if (isset($data['year'])) {
            $data['cyear'] = $data['year'];
        }
        if (isset($data['month'])) {
            $data['cmonth'] = $data['month'];
        }
        if (isset($data['day'])) {
            $data['cday'] = $data['day'];
        }

        $data['select'] = NULL;
        $data['year'] = '<select name="year" class="form-control"><option value="">Year...</option>';
        foreach (range(date('Y') - 100, date('Y')) as $numero) {
            $data['year'] .= '<option value="' . $numero . '" ' . (isset($data['cyear']) && $data['cyear'] == $numero ? "selected" : "") . '>' . $numero . '</option>';
        }
        $data['year'] .= '</select>';
        $data['month'] = '<select name="month" class="form-control"><option value="">Month...</option>';
        foreach (range(1, 12) as $numero) {
            $numero = str_pad($numero, "2", "0", STR_PAD_LEFT);
            $data['month'] .= '<option value="' . $numero . '" ' . (isset($data['cmonth']) && $data['cmonth'] == $numero ? "selected" : "") . '>' . $numero . '</option>';
        }
        $data['month'] .= '</select>';
        $data['day'] = '<select name="day" class="form-control"><option value="">Day...</option>';
        foreach (range(1, 31) as $numero) {
            $numero = str_pad($numero, "2", "0", STR_PAD_LEFT);
            $data['day'] .= '<option value="' . $numero . '" ' . (isset($data['cday']) && $data['cday'] == $numero ? "selected" : "") . '>' . $numero . '</option>';
        }$data['day'] .= '</select>';

        foreach ($rol->result() as $row) {
            if ($row->name == 'support_client') {
                $data['role'] = $row->idrole;
            } 
        }

        $data['nationality'] = '<select name="nationality" id="nationality" class ="form-control" onChange="initialize();">';
        $data['nationality'] .= '<option value="">-------</option>';
        foreach ($nationality->result() as $key) {
            $data['nationality'] .= '<option value="'.$key->idcountry.'">'.$key->name.'</option>';
        }
        $data['nationality'] .= '</select>';
        /*************************/
        $data['city_idcity'] = '<select name="city_idcity" id="city_idcity" class="form-control">
                                    <option value="">-------</option>
                                </select>';

        $this->load->view('profile/support_client/headersupportclient');
        $this->load->view('profile/support_client/create_new_support_client',$data);
        $this->load->view('profile/support_client/footersupportclient');
    }

    function validate(){
        echo '<pre>';
        print_r($this->input->post());
        echo '</pre>';
        echo "Validations under construction";
        
        $user_iduser = $this->input->post('iduser');
        $gpslat = $this->input->post('gps-lat');
        $gpslong = $this->input->post('gps-long');
        $role = $this->input->post('role');
        $name = $this->input->post('name');
        $middle_name = $this->input->post('middle_name');
        $last_name = $this->input->post('last_name');
        $nationality = $this->input->post('nationality');
        $city_idcity = $this->input->post('city_idcity');
        $email = $this->input->post('email');
        $password = $this->input->post('password');
        $genere = $this->input->post('genere');
        $phone = $this->input->post('phone');
        $year = $this->input->post('year');
        $month = $this->input->post('month');
        $day = $this->input->post('day');
        $birth ='"'.$year.'-'.$month.'-'.$day.'"';

        $setPassword = hash('sha512', $password, true);

            for ($i = 1; $i < 2500; $i++) {
            $setPassword = hash('sha512', $setPassword . $password, true);
            }
            $pwd = base64_encode($setPassword);
    

              $data = array(
                    "user_iduser" => $user_iduser,
                    "gps-lat" => $gpslat,
                    "gps-long" => $gpslong,
                    "name" => $name,
                    "middle_name" => $middle_name,
                    "last_name" => $last_name,
                    "nationality" => $nationality,
                    "city_idcity" => $city_idcity,
                    "email" => $email,
                    "password" => $pwd,
                    "genere" => $genere,          
                    "phone" => $phone,
                    "birth" => $birth,
                    "role_idrole" => $role,
                    "active"    => '1'
                );

            if($this->Axcs_model->singupuser($data)){
                $this->session->set_flashdata('success', 'User Created Correctly');
            }else{
                $this->session->set_flashdata('error', 'Ups! An error has occurred');
            }
    }
    
    function view_all_reservations_client(){
        $this->load->view('profile/support_client/headersupportclient');
        $this->load->view('profile/support_client/allreservations_clients');
        $this->load->view('profile/support_client/footersupportclient');
    }

    function ajax_list_reservations_client() {

        
        $fields = "reservations.*,roles.`name`,concat(us.`name`,' ',us.`last_name`)as user_name FROM `reservations` inner join user as us on us.iduser=reservations.user_iduser inner join roles on idrole=us.role_idrole where roles.idrole=7";
        $reservations = $this->Reservations_model->selectreservations_client($fields);

        $data = array();
        $no = 0;
        foreach ($reservations->result() as $us) {
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = explode(" ",$us->date)[0];
            $row[] = $us->cost;
            $row[] = $us->pay;
            $row[] = $us->user_name;            
            $row[] = (1 == $us->active) ? "Active" : "Inactive";
            $data[] = $row;
        }

        $output = array(
            "draw" => "",
            "recordsTotal" => $reservations->num_rows(),
            "recordsFiltered" => $reservations->num_rows(),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }
}