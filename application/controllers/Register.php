<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Register extends CI_Controller {

    function __construct(){
        parent::__construct();          
    }

    function index($data = NULL){      
        
        $nationality = $this->Planetresmodel->country();
        $rol = $this->Axcs_model->rol();

        /*************************/
        $data['name'] = NULL;
        $data['middle_name'] = NULL;
        $data['last_name'] = NULL;
        $data['email'] = NULL;
        $data['confirm_email'] = NULL;
        $data['password'] = NULL;
        $data['confirm_password'] = NULL;
        $data['phone'] = NULL;
        $data['zip_code'] = NULL;
        $data['hidden_lat'] = NULL;
        $data['hidden_long'] = NULL;
        $data['cyear'] = "0000";
        $data['cmonth'] = "00";
        $data['cday'] = "00";
        /*************************/
        $data['nationality'] = '<select name="nationality" id="nationality" class ="field1b rounded" onChange="initialize();">';
        $data['nationality'] .= '<option value="">-------</option>';
        foreach ($nationality->result() as $key) {
            $data['nationality'] .= '<option value="'.$key->idcountry.'">'.$key->name.'</option>';
        }
        $data['nationality'] .= '</select>';
        /*************************/
        $data['city_idcity'] = '<select name="city_idcity" id="city_idcity" class="field1b rounded">
                                    <option value="">-------</option>
                                </select>';
        /*************************/
        $data['select'] = NULL;
        $data['year'] = '<select name="year" class="form-control"><option value="">Year...</option>';
        foreach (range(date('Y') - 100, date('Y')) as $numero) {
            $data['year'] .= '<option value="' . $numero . '" ' . (isset($data['cyear']) && $data['cyear'] == $numero ? "selected" : "") . '>' . $numero . '</option>';
        }
        $data['year'] .= '</select>';
        $data['month'] = '<select name="month" class="form-control"><option value="">Month...</option>';
        foreach (range(1, 12) as $numero) {
            $numero = str_pad($numero, "2", "0", STR_PAD_LEFT);
            $data['month'] .= '<option value="' . $numero . '" ' . (isset($data['cmonth']) && $data['cmonth'] == $numero ? "selected" : "") . '>' . $numero . '</option>';
        }
        $data['month'] .= '</select>';
        $data['day'] = '<select name="day" class="form-control"><option value="">Day...</option>';
        foreach (range(1, 31) as $numero) {
            $numero = str_pad($numero, "2", "0", STR_PAD_LEFT);
            $data['day'] .= '<option value="' . $numero . '" ' . (isset($data['cday']) && $data['cday'] == $numero ? "selected" : "") . '>' . $numero . '</option>';
        }$data['day'] .= '</select>';
        /*************************/
        $data['register_as'] = '<select name="register_as" id="register_as" class="field1b rounded">';
        foreach ($rol->result() as $row) {
            if ($row->name == 'client' || $row->name == 'supplier' || $row->name == 'travel_agent') {
                $data['register_as'] .= '<option value="'.$row->idrole.'">'.$row->description.'</option>';
            } 
        }
        $data['register_as'] .= '</select>';
        $this->load->view('frontend/register',$data);
    }

    function validate(){        
        echo '<pre>';
        print_r($this->input->post());
        echo '</pre>';
        echo "Validations under construction";
        
        $gpslat = $this->input->post('gps-lat');
        $gpslong = $this->input->post('gps-long');
        $register_as = $this->input->post('register_as');
        $name = $this->input->post('name');
        $middle_name = $this->input->post('middle_name');
        $last_name = $this->input->post('last_name');
        $nationality = $this->input->post('nationality');
        $city_idcity = $this->input->post('city_idcity');
        $email = $this->input->post('email');
        $password = $this->input->post('password');
        $genere = $this->input->post('genere');
        $phone = $this->input->post('phone');
        $zip_code = $this->input->post('zip_code');
        $year = $this->input->post('year');
        $month = $this->input->post('month');
        $day = $this->input->post('day');
        $birth ='"'.$year.'-'.$month.'-'.$day.'"';

        $setPassword = hash('sha512', $password, true);

            for ($i = 1; $i < 2500; $i++) {
            $setPassword = hash('sha512', $setPassword . $password, true);
            }
            $pwd = base64_encode($setPassword);
    

              $data = array(
                    "gps-lat" => $gpslat,
                    "gps-long" => $gpslong,
                    "name" => $name,
                    "middle_name" => $middle_name,
                    "last_name" => $last_name,
                    "nationality" => $nationality,
                    "city_idcity" => $city_idcity,
                    "email" => $email,
                    "password" => $pwd,
                    "genere" => $genere,          
                    "phone" => $phone,
                    "birth" => $birth,
                    "zip_code" => $zip_code,
                    "role_idrole" => $register_as,
                    "active"    => '1'
                );

            if($this->Axcs_model->singupuser($data)){
                $this->session->set_flashdata('success', 'User Created Correctly');
            }else{
                $this->session->set_flashdata('error', 'Ups! An error has occurred');
            }
        
        /*$validate_email = $this->validate_email($email);
        $redundant_records_user = $this->redundant_records_user($email,$passport);
        $validate_birth = $this->birth($birth,'Y-m-d');
        $variable = 1;
        switch ($variable) {
        case '1':
            if($name == "" || $lastname == "" || $passport =="" || $phone == "" || $password == "" || $city == "" || $email == "" || $genere == "" || $birth == "" || $zip == ""){
                
                return 3;
                break;       

            }elseif(!$validate_email || ctype_space($email)){            
                return 2;
                break;  
            }elseif(!$redundant_records_user){              
                return 0;
                break;  
            }elseif(!ctype_alnum($password) || ctype_space($password)){
                return 4;
                break;
             }elseif(!ctype_alnum($city)){
                return 11;
                break;
            }elseif(!ctype_alpha($name) || ctype_space($name)){
                return 6;
                break;            
            }elseif(!ctype_alpha($lastname) || ctype_space($lastname)){
                return 7;
                break;
            }elseif(!is_numeric($phone) || ctype_space($password)){
                return 8;
                break;
            }elseif(!$validate_birth || ctype_space($validate_birth)){
                return 9;
                break;
            }elseif(strlen($password) < 6 or strlen($password) > 16 ){
                return 5;
                break;
            }elseif(!ctype_digit($zip) || ctype_space($zip)){
                return 10;
                break;    
            }else{
                return 1;
                break;  
            }
        }*/            
        $this->index();
    }

    function birth($birth, $format = 'Y-m-d'){        
        $d = DateTime::createFromFormat($format, $birth);
        return $d && $d->format($format) == $birth;
    }

    function redundant_records_user($email,$passport){
        if($this->Axcs_model->redundant_records_users($email,$passport)){
            return true;
        }else{
            return false;
        }
    }

     function redundant_records_agent($email,$arcnum){
        if($this->Axcs_model->redundant_records_agent($email,$arcnum)){
            return true;
        }else{
            return false;
        }
    }

    function validate_email($email){
        if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
            return true;
        }else{
            return false;
        }
    }
}

