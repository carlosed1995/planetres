<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Support_supplier_functions extends CI_Controller {

	function __construct(){
        parent::__construct();
    $this->load->library('pagination');

        if(!$this->session->userdata("login")){
            redirect(base_url('Planetres'));
        }
    }


    function support_supplier_reservation(){

        $this->load->library('pagination');
        $mostrarpor = 2;

$config['base_url'] = base_url()."Support_supplier_functions/support_supplier_reservation";
$config['total_rows'] = 2;
$config['per_page'] = $mostrarpor;

$this->pagination->initialize($config);

echo $this->pagination->create_links();

      $data = array(
        'list'=>$this->Travelagent_model->list_reser_hotel()
        );

       $this->load->view('profile/support_supplier/headersupportsupplier');
       $this->load->view('profile/support_supplier/reservations_hotel', $data);
       $this->load->view('profile/support_supplier/footersupportsupplier'); 
            
}

public function on_supplier_hotel($id){
$data =  array(
   'active' => 1
   
    );
  $this->Travelagent_model->on_off_hotel($id, $data);


  redirect('Support_supplier_functions/support_supplier_reservation');
  
}

public function off_supplier_hotel($id){
$data =  array(
   'active' => 0
   
    );
  $this->Travelagent_model->on_off_hotel($id, $data);


  redirect('Support_supplier_functions/support_supplier_reservation');
}

public function support_supplier_reservation_transfer(){

 $data = array(
        'list'=>$this->Travelagent_model->list_reser_transfer()
        );

       $this->load->view('profile/support_supplier/headersupportsupplier');
       $this->load->view('profile/support_supplier/reservations_transfer', $data);
       $this->load->view('profile/support_supplier/footersupportsupplier'); 

}

function support_supplier_reservation_excursions(){

    $data = array(
        'list'=>$this->Travelagent_model->list_reser_excursions()
        );

       $this->load->view('profile/support_supplier/headersupportsupplier');
       $this->load->view('profile/support_supplier/reservations_excursions', $data);
       $this->load->view('profile/support_supplier/footersupportsupplier'); 

}

/*functions on off excursion*/

public function off_supplier_excursion($id){
      $data =  array(
   'active' => 0
   
    );
  $this->Travelagent_model->on_off_excursion($id, $data);


  redirect('Support_supplier_functions/support_supplier_reservation_excursions');
}

public function on_supplier_excursion($id){
  $data =  array(
   'active' => 1
   
    );
  $this->Travelagent_model->on_off_excursion($id, $data);


  redirect('Support_supplier_functions/support_supplier_reservation_excursions');
}

/*functions on off transfer*/

public function off_supplier_transfer($id){
    $data =  array(
   'active' => 0
   
    );
  $this->Travelagent_model->on_off_excursion($id, $data);


  redirect('Support_supplier_functions/support_supplier_reservation_transfer');

}

public function on_supplier_transfer ($id){
    $data =  array(
   'active' => 1
   
    );
  $this->Travelagent_model->on_off_transfer($id, $data);


  redirect('Support_supplier_functions/support_supplier_reservation_transfer');
    
}

}