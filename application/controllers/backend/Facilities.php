<?php

defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Passenger
 *
 * @author carlos
 */
class Facilities extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('Axcs_model');
        $this->load->model('Planetresmodel');
        $this->load->model('Facilities_model');
        $this->load->library('Session');
    }

    function newfacilities($data = NULL) {


        if ($this->input->post('idfacilities') != NULL) {
            $facilities = $this->Facilities_model->selectfacilities(" idfacilities=" . $this->input->post('idfacilities'));
            $data = get_object_vars($facilities->result()[0]);
            $data['ina'] = '';
            $data['act'] = '';
            (1 == $data['active']) ? $data['act'] = 'checked' : $data['ina'] = 'checked';
            $data['service'] = '';
            $data['room'] = '';
            (1 == $data['type']) ? $data['room'] = 'checked' : $data['service'] = 'checked';
        }
        if ($data == NULL) {
            $data['idfacilities'] = NULL;
            $data['name'] = NULL;
            $data['active'] = NULL;
            $data['ina'] = NULL;
            $data['service'] = NULL;
            $data['room'] = NULL;
        }

        $this->load->view('backend/facilities/newfacilities', $data);
    }

    function savefacilities() {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('name', 'Name', 'trim|required|xss_clean|max_length[256]|min_length[2]');
        $this->form_validation->set_rules('type', 'Type', 'trim|required|xss_clean|max_length[2]|min_length[1],numeric');
        $this->form_validation->set_rules('active', 'Active', 'trim|required|xss_clean|max_length[2]|min_length[1],numeric');


        $this->form_validation->set_message('required', 'The field  %s is required');
        $this->form_validation->set_message('valid_email', 'The %s is not valid');
        $this->form_validation->set_message('min_length', 'Field %s must have at least %s characters');
        $this->form_validation->set_message('max_length', 'Field %s must have less %s characters');
        $this->form_validation->set_message('numeric', 'The field  %s is numeric');

        if (!$this->form_validation->run()) {
            $return = $this->input->post();
            $return['act'] = '';
            $return['ina'] = '';
            $data['service'] = NULL;
            $data['room'] = NULL;
            (1 == $this->input->post('active')) ? $return['act'] = 'checked' : $return['ina'] = 'checked';
            (1 == $this->input->post('type')) ? $return['room'] = 'checked' : $return['service'] = 'checked';

            $this->newfacilities($return);
        } else {
            $data['mesg'] = 'Ups! An error has occurred';
            if ($this->input->post('idfacilities') != '') {
                $data = $this->input->post();
                $data['act'] = '';
                $data['ina'] = '';
                $data['service'] = NULL;
                $data['room'] = NULL;
                (1 == $this->input->post('active')) ? $return['act'] = 'checked' : (0 == $this->input->post('active')) ? $return['ina'] = 'checked' : $return['ina'] = '';
                (1 == $this->input->post('type')) ? $return['room'] = 'checked' : $return['service'] = 'checked';
                if ($this->Facilities_model->updatefacilities($data)) {
                    $this->session->set_flashdata('success', 'Correctly Updated Feature');
                } else {
                    $this->session->set_flashdata('error', 'Ups! An error has occurred');
                }
            } else {
                $data = $this->input->post();
                $data['act'] = '';
                $data['ina'] = '';
                $data['service'] = NULL;
            $data['room'] = NULL;
                (1 == $this->input->post('active')) ? $data['act'] = 'checked' : $data['ina'] = 'checked';
                (1 == $this->input->post('type')) ? $data['room'] = 'checked' : $data['service'] = 'checked';
                $this->session->set_flashdata('success', 'Features Created Correctly');

                $data['idfacilities'] = $this->Facilities_model->insertfacilities($data);
                if (!$data['idfacilities']) {
                    $this->session->set_flashdata('error', 'Ups! An error has occurred');
                }
            }
            $this->newfacilities($data);
        }
    }

    function listfacilities($data = NULL) {
        $this->load->view('backend/facilities/listfacilities', $data);
    }

    function ajax_listfacilities() {


        $condi = "TRUE";
        $orderby = " order by idfacilities desc ";
        $limit = "";
        $fields = '*';
        $facilities = $this->Facilities_model->selectfacilities($condi, $orderby, $limit, $fields);

        $data = array();
        $no = 0;
        foreach ($facilities->result() as $ps) {
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $ps->name;
            $row[] = (1 == $ps->type) ? "Room" : "Service";
            $row[] = (1 == $ps->active) ? "Active" : "Inactive";

            //$row[] = ;

            $seemore = '<span class="cssToolTip">
                            <button class="btn btn-success btn-xs fa fa-search-plus"></button>
                            <span>Feature: ' . $no . '<br>'
                    . '</span>'
                    . '</span>';

            $hidden = '<input type="hidden" name="idfacilities" value="' . $ps->idfacilities . '">';
            $edit = '<div>' . $hidden . '<button id="updatefacilities" class="btn btn-warning btn-xs fa fa-edit" title="Edit facilities" onclick="udpatefacilities(' . $ps->idfacilities . ',\'' . base_url() . '\')"></button>';
            $color = ($ps->active == 1) ? "btn-danger" : "btn-default";
            $delete = '<div>' . $hidden . '<button class="btn ' . $color . ' btn-xs fa fa-trash-o" title="Delete facilities" onclick="deletefacilities(' . $ps->idfacilities . ',' . $ps->active . ',\'' . base_url() . '\')"></button></div>';

            $row[] = '<table style="border-collapse: separate; border-spacing:  5px;">'
                    . '<tr>'
                    . '<td>' . $seemore . '</td>'
                    . '<td>' . $edit . '</td>'
                    . '<td>' . $delete . '</td>'
                    . '</tr>'
                    . '</table>';

            $data[] = $row;
        }

//        $output = array(
//            "draw" => $_POST['draw'],
//            "recordsTotal" => $this->customers->count_all(),
//            "recordsFiltered" => $this->customers->count_filtered(),
//            "data" => $data,
//        );

        $output = array(
            "draw" => "",
            "recordsTotal" => $facilities->num_rows(),
            "recordsFiltered" => $facilities->num_rows(),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }

    function deletefacilities() {//       
        $data = $this->input->post();
        if ($data['active'] == 0) {
            $data['active'] = 1;
        } else {
            $data['active'] = 0;
        }
        if ($this->Facilities_model->deletefacilities($data)) {
            $this->session->set_flashdata('success', 'Correctly Updated Facilities');
        } else {
            $this->session->set_flashdata('error', 'Ups! An error has occurred');
        }
        $this->listfacilities();
    }

}
