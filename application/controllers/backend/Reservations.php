<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Excursion
 *
 * @author carlos
 */
class Reservations extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('Axcs_model');
        $this->load->model('Planetresmodel');
        $this->load->model('Reservations_model');
        $this->load->library('Session');
    }

    function newreservations($data = NULL) {

        if ($this->input->post('idreservations') != NULL) {
            $reservations = $this->Reservations_model->selectreservations(" idreservations=" . $this->input->post('idreservations'));
            $data = get_object_vars($reservations->result()[0]);
            $data['act'] = '';
            $data['ina'] = '';
            (1 == $data['active']) ? $data['act'] = 'checked' : $data['ina'] = 'checked';
            $data['cyear'] = explode("-", $data['date_reservations'])[0];
            $data['cmonth'] = explode("-", $data['date_reservations'])[1];
            $data['cday'] = explode("-", $data['date_reservations'])[2];
        }

        if ($data == NULL) {
            $data['idreservations'] = NULL;
            $data['date_reservations'] = NULL;
            $data['cost_total'] = NULL;
            $data['pay'] = NULL;
            $data['active'] = NULL;
            $data['act'] = '';
            $data['ina'] = '';
            $data['date_reservations'] = '0000-00-00';
            $data['cmonth'] = '';
            $data['cyear'] = '';
            $data['cday'] = '';
        }
        if (isset($data['year'])) {
            $data['cyear'] = $data['year'];
        }
        if (isset($data['month'])) {
            $data['cmonth'] = $data['month'];
        }
        if (isset($data['day'])) {
            $data['cday'] = $data['day'];
        }
        $data['year'] = '<select name="year" class="form-control"><option value="">Year...</option>';
        foreach (range(date('Y') - 100, date('Y')) as $numero) {
            $data['year'] .= '<option value="' . $numero . '" ' . (isset($data['cyear']) && $data['cyear'] == $numero ? "selected" : "") . '>' . $numero . '</option>';
        }
        $data['year'] .= '</select>';
        $data['month'] = '<select name="month" class="form-control"><option value="">Month...</option>';
        foreach (range(1, 12) as $numero) {
            $numero = str_pad($numero, "2", "0", STR_PAD_LEFT);
            $data['month'] .= '<option value="' . $numero . '" ' . (isset($data['cmonth']) && $data['cmonth'] == $numero ? "selected" : "") . '>' . $numero . '</option>';
        }
        $data['month'] .= '</select>';
        $data['day'] = '<select name="day" class="form-control"><option value="">Day...</option>';
        foreach (range(1, 31) as $numero) {
            $numero = str_pad($numero, "2", "0", STR_PAD_LEFT);
            $data['day'] .= '<option value="' . $numero . '" ' . (isset($data['cday']) && $data['cday'] == $numero ? "selected" : "") . '>' . $numero . '</option>';
        }$data['day'] .= '</select>';

        $this->load->view('backend/reservations/newreservations', $data);
    }

    function listreservations($data = NULL) {
        $this->load->view('backend/reservations/listreservations', $data);
    }

    function ajax_listreservations() {

        $condi = "TRUE";
        $orderby = " order by idreservations desc ";
        $limit = "";
        $fields = ' *';
        $reservations = $this->Reservations_model->selectreservations($condi, $orderby, $limit, $fields);

        $data = array();
        $no = 0;
        foreach ($reservations->result() as $us) {
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = explode(" ",$us->date_reservations)[0];
            $row[] = $us->cost;
            $row[] = $us->pay;            
            $row[] = (1 == $us->active) ? "Active" : "Inactive";


            $seemore = '<span class="cssToolTip">
                            <button class="btn btn-success btn-xs fa fa-search-plus"></button>
                            <span>Excursion: ' . $no . ''
                    . '</span>'
                    . '</span>';
            $hidden = '<input type="hidden" name="idreservations" value="' . $us->idreservations . '">';
            $edit = '<div>' . $hidden . '<button id="updatetarvelagent" class="btn btn-warning btn-xs fa fa-edit" title="Edit Excursion" onclick="udpatereservations(' . $us->idreservations . ',\'' . base_url() . '\')"></button>';
            $color = ($us->active == 1) ? "btn-danger" : "btn-default";
            $delete = '<div>' . $hidden . '<button class="btn ' . $color . ' btn-xs fa fa-trash-o" title="Delete Excursion" onclick="deletereservations(' . $us->idreservations . ',' . $us->active . ',\'' . base_url() . '\')"></button></div>';

            $row[] = '<table style="border-collapse: separate; border-spacing:  5px;">'
                    . '<tr>'
                    . '<td>' . $seemore . '</td>'
                    . '<td>' . $edit . '</td>'
                    . '<td>' . $delete . '</td>'
                    . '</tr>'
                    . '</table>';

            $data[] = $row;
        }

//        $output = array(
//            "draw" => $_POST['draw'],
//            "recordsTotal" => $this->customers->count_all(),
//            "recordsFiltered" => $this->customers->count_filtered(),
//            "data" => $data,
//        );

        $output = array(
            "draw" => "",
            "recordsTotal" => $reservations->num_rows(),
            "recordsFiltered" => $reservations->num_rows(),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }

    function savereservations() {

        $this->load->library('form_validation');


        $this->form_validation->set_rules('year', 'Year', 'trim|required|xss_clean|max_length[4]|min_length[4]|numeric');
        $this->form_validation->set_rules('month', 'Month', 'trim|required|xss_clean|max_length[2]|min_length[2]|numeric');
        $this->form_validation->set_rules('day', 'Day', 'trim|required|xss_clean|max_length[2]|min_length[2]|numeric');
        $this->form_validation->set_rules('pay', 'Pay', 'trim|required|xss_clean|max_length[45]|min_length[2]');
        $this->form_validation->set_rules('cost_total', 'Costo', 'trim|required|xss_clean|max_length[11]|min_length[1]|numeric');
        $this->form_validation->set_rules('active', 'Status', 'trim|required|xss_clean|max_length[15]|min_length[1]');


        $this->form_validation->set_message('required', 'The field  %s is required');
        $this->form_validation->set_message('valid_email', 'The %s is not valid');
        $this->form_validation->set_message('min_length', 'Field %s must have at least %s characters');
        $this->form_validation->set_message('max_length', 'Field %s must have less %s characters');
        $this->form_validation->set_message('numeric', 'The field  %s is numeric');

        if (!$this->form_validation->run()) {
            $return = $this->input->post();
            $return['ina'] = '';
            $return['sta'] = '';
            $return['act'] = '';
            (1 == $this->input->post('active')) ? $return['act'] = 'checked' : $return['ina'] = 'checked';
            $this->newreservations($return);
        } else {
            $data['mesg'] = 'Ups! An error has occurred';
            if ($this->input->post('idreservations') != '') {
                $data = $this->input->post();
                $data['act'] = '';
                $data['ina'] = '';
                $return['act'] = '';
                $data['cyear'] = $this->input->post('year');
                $data['cmonth'] = $this->input->post('month');
                $data['cday'] = $this->input->post('day');
                (1 == $this->input->post('active')) ? $data['act'] = 'checked' : $data['ina'] = 'checked';

                if ($this->Reservations_model->updatereservations($data)) {
                    $this->session->set_flashdata('success', 'Correctly Updated Reservations');
                } else {
                    $this->session->set_flashdata('error', 'Ups! An error has occurred');
                }
            } else {
                $data = $this->input->post();
                $data['act'] = '';
                $data['ina'] = '';
                $data['cyear'] = $this->input->post('year');
                $data['cmonth'] = $this->input->post('month');
                $data['cday'] = $this->input->post('day');
                (1 == $this->input->post('active')) ? $data['act'] = 'checked' : $data['ina'] = 'checked';
                $this->session->set_flashdata('success', 'Reservations Created Correctly');

                $data['idreservations'] = $this->Reservations_model->insertreservations($data);
                if (!$data['idreservations']) {
                    $this->session->set_flashdata('error', 'Ups! An error has occurred');
                }
            }
            $this->newreservations($data);
        }
    }

    function deletereservations() {//       
        $data = $this->input->post();
        if ($data['active'] == 0) {
            $data['active'] = 1;
        } else {
            $data['active'] = 0;
        }
        if ($this->Reservations_model->deletereservations($data)) {
            $this->session->set_flashdata('success', 'Correctly Updated Reservations');
        } else {
            $this->session->set_flashdata('error', 'Ups! An error has occurred');
        }
        $this->listreservations();
    }

    function autocomplete() {
        if ($this->input->is_ajax_request() && $this->input->post('info') && strlen($this->input->post('info')) > 3) {
            $condition = " name like '" . $this->input->post('info') . "%'";
            $search = $this->Planetresmodel->country($condition, " limit 10 ", " order by abbre ");
            if ($search !== FALSE) {
                foreach ($search->result() as $row) {
                    ?>
                    <a onclick="javascript:asignar(<?= $row->idcountry . ',\'' . $row->name . '\''; ?>)" ><?php echo $row->name . ' ' . $row->abbre ?></a><br>
                    <?php
                }
            } else {
                ?>

                <p><?php echo 'No results' ?></p>

                <?php
            }
        }
    }

}
