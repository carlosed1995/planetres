<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Excursion
 *
 * @author carlos
 */
class Excursions extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('Axcs_model');
        $this->load->model('Planetresmodel');
        $this->load->model('Excursions_model');
        $this->load->library('Session');
    }

    function newexcursions($data = NULL) {
        if ($this->input->post('idexcursions') != NULL) {
            $excursions = $this->Excursions_model->selectexcursions(" idexcursions=" . $this->input->post('idexcursions'));
            $data = get_object_vars($excursions->result()[0]);
            //$data['id_country'] = $data['id_city'];
            $city = $search = $this->Planetresmodel->city(" idcity =" . $data['city_idcity'], " limit 10 ", " order by abbre ");
            $data['citysearch'] = get_object_vars($city->result()[0])['name'];
            $data['act'] = '';
            $data['ina'] = '';
            (1 == $data['active']) ? $data['act'] = 'checked' : $data['ina'] = 'checked';
        }

        if ($data == NULL) {
            $data['idexcursions'] = NULL;
            $data['name'] = NULL;
            $data['description'] = NULL;
            $data['city_idcity'] = NULL;
            $data['active'] = NULL;
            $data['highlights'] = NULL;
            $data['info'] = NULL;
            $data['max_persons'] = NULL;
            $data['active'] = NULL;
            $data['act'] = '';
            $data['ina'] = '';
            $data['idpricing_chart'] = NULL;
            $data['start_date'] = NULL;
            $data['end_date'] = NULL;
            $data['pickup'] = NULL;
            $data['drop_off'] = NULL;
            $data['start_time'] = NULL;
            $data['end_time'] = NULL;
            $data['description'] = NULL;

            $data['citysearch'] = NULL;
        }


        $this->load->view('backend/excursions/newexcursions', $data);
    }

    function listexcursions($data = NULL) {
        $this->load->view('backend/excursions/listexcursions', $data);
    }

    function ajax_listexcursions() {

        $condi = "TRUE";
        $orderby = " order by idexcursions desc ";
        $limit = "";
        $fields = ' *,(SELECT concat(cot.name," ",cot.abbre) as city FROM city as cot WHERE cot.idcity=city_idcity) as city ';
        $excursions = $this->Excursions_model->selectexcursions($condi, $orderby, $limit, $fields);

        $data = array();
        $no = 0;
        foreach ($excursions->result() as $us) {
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $us->name;
            $row[] = $us->max_persons;
            $row[] = $us->info;
            $row[] = $us->city;
            $row[] = (1 == $us->active) ? "Active" : "Inactive";


            $seemore = '<span class="cssToolTip">
                            <button class="btn btn-success btn-xs fa fa-search-plus"></button>
                            <span>Excursion: ' . $no . ''
                    . '</span>'
                    . '</span>';
            $hidden = '<input type="hidden" name="idexcursions" value="' . $us->idexcursions . '">';
            $edit = '<div>' . $hidden . '<button id="updatetarvelagent" class="btn btn-warning btn-xs fa fa-edit" title="Edit Excursion" onclick="udpateexcursions(' . $us->idexcursions . ',\'' . base_url() . '\')"></button>';
            $color = ($us->active == 1) ? "btn-danger" : "btn-default";
            $delete = '<div>' . $hidden . '<button class="btn ' . $color . ' btn-xs fa fa-trash-o" title="Delete Excursion" onclick="deleteexcursions(' . $us->idexcursions . ',' . $us->active . ',\'' . base_url() . '\')"></button></div>';

            $row[] = '<table style="border-collapse: separate; border-spacing:  5px;">'
                    . '<tr>'
                    . '<td>' . $seemore . '</td>'
                    . '<td>' . $edit . '</td>'
                    . '<td>' . $delete . '</td>'
                    . '</tr>'
                    . '</table>';

            $data[] = $row;
        }

//        $output = array(
//            "draw" => $_POST['draw'],
//            "recordsTotal" => $this->customers->count_all(),
//            "recordsFiltered" => $this->customers->count_filtered(),
//            "data" => $data,
//        );

        $output = array(
            "draw" => "",
            "recordsTotal" => $excursions->num_rows(),
            "recordsFiltered" => $excursions->num_rows(),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }

    function saveexcursions() {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('name', 'Name', 'trim|required|xss_clean|max_length[256]|min_length[2]');
        $this->form_validation->set_rules('city_idcity', 'City', 'trim|required|xss_clean|max_length[10]|min_length[1]');
        $this->form_validation->set_rules('highlights', 'highlights', 'trim|required|xss_clean|min_length[1]');
        $this->form_validation->set_rules('info', 'Info', 'trim|required|xss_clean|min_length[1]');
        $this->form_validation->set_rules('max_persons', 'Max persons', 'trim|required|xss_clean|max_length[10]|min_length[1]');
        $this->form_validation->set_rules('active', 'Active', 'trim|required|xss_clean|max_length[15]|min_length[1]');


        $this->form_validation->set_message('required', 'The field  %s is required');
        $this->form_validation->set_message('min_length', 'Field %s must have at least %s characters');
        $this->form_validation->set_message('max_length', 'Field %s must have less %s characters');
        $this->form_validation->set_message('numeric', 'The field  %s is numeric');        
        if (!$this->form_validation->run()) {
            echo 'errror';
            $return = $this->input->post();
            $return['ina'] = '';
            $return['sta'] = '';

            /* TEMPORAL */
            $data['idpricing_chart'] = NULL;
            $data['start_date'] = NULL;
            $data['end_date'] = NULL;
            $data['pickup'] = NULL;
            $data['drop_off'] = NULL;
            $data['start_time'] = NULL;
            $data['end_time'] = NULL;
            /* tEMPORAÑ */

            (1 == $this->input->post('active')) ? $return['act'] = 'checked' : $return['ina'] = 'checked';
            $this->newexcursions($return);
        } else {
            
            $data['mesg'] = 'Ups! An error has occurred';
            if ($this->input->post('idexcursions') != '') {
                $data = $this->input->post();
                $data['act'] = '';
                $data['ina'] = '';
                (1 == $this->input->post('active')) ? $data['act'] = 'checked' : $data['ina'] = 'checked';

                if ($this->Excursions_model->updateexcursions($data)) {
                    $this->session->set_flashdata('success', 'Correctly Updated excursions');
                } else {
                    $this->session->set_flashdata('error', 'Ups! An error has occurred');
                }
            } else {               
                $data = $this->input->post();
                $data['act'] = '';
                $data['ina'] = '';

                (1 == $this->input->post('active')) ? $data['act'] = 'checked' : $data['ina'] = 'checked';
                $this->session->set_flashdata('success', 'Excursions Created Correctly');

                $data['idexcursions'] = $this->Excursions_model->insertexcursions($data);
                if (!$data['idexcursions']) {
                    $this->session->set_flashdata('error', 'Ups! An error has occurred');
                }
            }
            if ($data['idexcursions']) {
                $this->Excursions_model->hotelpricingchart($data['idexcursions'], $this->input->post('start_date'), $this->input->post('end_date'), $this->input->post('start_time'), $this->input->post('end_time'), $this->input->post('descrip'), $this->input->post('pickup'), $this->input->post('drop_off'));
                /* Front */
                if (!empty($_FILES['front']['name'])) {
                    $config["upload_path"] = "web/images/gallery/excursions/";
                    $config["allowed_types"] = "gif|jpg|jpeg|png";
                    $config["max_size"] = "2000";
                    $config["remove_spaces"] = TRUE;
                    $config["file_name"] = date('Ymdhis') . rand(1, 10000);
                    $this->load->library('upload', $config);
                    $this->upload->initialize($config);
                    if ($this->upload->do_upload('front')) {
                        $fileData = $this->upload->data();
                        $this->Excursions_model->insertimages($config["file_name"], $data['idexcursions'], 0);
                    } else {
                        $errors = array('error' => $this->upload->display_errors());
                        die(json_encode($errors));
                    }
                }
                /* Front */
                if (!empty($_FILES['images']['name'])) {
                    $filesCount = count($_FILES['images']['name']);
                    for ($i = 0; $i < $filesCount; $i++) {
                        if ($_FILES['images']['name'][$i] != '') {
                            $_FILES['userFile']['name'] = $_FILES['images']['name'][$i];
                            $_FILES['userFile']['type'] = $_FILES['images']['type'][$i];
                            $_FILES['userFile']['tmp_name'] = $_FILES['images']['tmp_name'][$i];
                            $_FILES['userFile']['error'] = $_FILES['images']['error'][$i];
                            $_FILES['userFile']['size'] = $_FILES['images']['size'][$i];
                            $config["upload_path"] = "web/images/gallery/excursions/";
                            $config["allowed_types"] = "gif|jpg|jpeg|png";
                            $config["max_size"] = "2000";
                            $config["remove_spaces"] = TRUE;
                            $config["file_name"] = date('Ymdhis') . rand(1, 10000);
                            $this->load->library('upload', $config);
                            $this->upload->initialize($config);
                            if ($this->upload->do_upload('userFile')) {
                                $fileData = $this->upload->data();
                                $this->Excursions_model->insertimages($fileData['file_name'], $data['idexcursions'], 1);
                            } else {
                                $errors = array('error' => $this->upload->display_errors());
                                die(json_encode($errors));
                            }
                        }
                    }
                }
            }
            $this->newexcursions($data);
        }
    }

    function deleteexcursions() {//       
        $data = $this->input->post();
        if ($data['active'] == 0) {
            $data['active'] = 1;
        } else {
            $data['active'] = 0;
        }
        if ($this->Excursions_model->deleteexcursions($data)) {
            $this->session->set_flashdata('success', 'Correctly Updated Excursions');
        } else {
            $this->session->set_flashdata('error', 'Ups! An error has occurred');
        }
        $this->listexcursions();
    }

    function autocomplete() {
        if ($this->input->is_ajax_request() && $this->input->post('info') && strlen($this->input->post('info')) > 3) {
            $condition = " name like '" . $this->input->post('info') . "%'";
            $search = $this->Planetresmodel->country($condition, " limit 10 ", " order by abbre ");
            if ($search !== FALSE) {
                foreach ($search->result() as $row) {
                    ?>
                    <a onclick="javascript:asignar(<?= $row->idcountry . ',\'' . $row->name . '\''; ?>)" ><?php echo $row->name . ' ' . $row->abbre ?></a><br>
                    <?php
                }
            } else {
                ?>

                <p><?php echo 'No results' ?></p>

                <?php
            }
        }
    }

}
