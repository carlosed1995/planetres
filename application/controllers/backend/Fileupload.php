<?php

defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of fileupload
 *
 * @author carlos
 */
class Fileupload extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('Axcs_model');
        $this->load->model('Planetresmodel');
        $this->load->model('Users_model');
        $this->load->library('Session');
    }

    //put your code here
    public function upload() {
        $config = array();
        $config["upload_path"] = "web/images/profiles/";
        $config["allowed_types"] = "gif|jpg|jpeg|png";
        $config["max_size"] = "2000";
        $config["remove_spaces"] = TRUE;
        $config["file_name"] = date('Ymdhis') . rand(1, 10000);
        $this->load->library('upload', $config);
        if (!$this->upload->do_upload('portrait')) {
            $errors = array('error' => $this->upload->display_errors());
            die(json_encode($errors));            
        } else {
            $data = array('upload_data' => $this->upload->data());
            $idimagen=($this->input->post('idimage')!=NULL)? " and images_idimages=" . $this->input->post('idimage'):'' ;
            $this->Users_model->selectuserimage("user_iduser= " . $this->input->post('id') . $idimagen. " and type=" . $this->input->post('type'), NULL, NULL, '*',$data['upload_data']['file_name'],$this->input->post('id'));            
            die(json_encode(array('file' => $data['upload_data']['file_name'])));            
        }
    }

}
