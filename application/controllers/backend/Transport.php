<?php

defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Passenger
 *
 * @author carlos
 */
class Transport extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('Axcs_model');
        $this->load->model('Planetresmodel');
        $this->load->model('Transfers_model');
        $this->load->model('Typetransfer_model');
        $this->load->library('Session');
    }

    function newtransport($data = NULL) {


        if ($this->input->post('idtrasnfers') != NULL) {
            $transport = $this->Transfers_model->selecttransfers(" idtrasnfers=" . $this->input->post('idtrasnfers'));
            $data = get_object_vars($transport->result()[0]);
            $data['city_idcity'] = $data['city_idcity'];
            $city = $search = $this->Planetresmodel->city(" idcity =" . $data['city_idcity'], " limit 10 ", " order by abbre ");
            $data['citysearch'] = get_object_vars($city->result()[0])['name'];
            $data['ina'] = '';
            $data['act'] = '';
            $data['type_transfers_idtype_transfers1'] =$data['type_transfers_idtype_transfers'] ;
            (1 == $data['active']) ? $data['act'] = 'checked' : $data['ina'] = 'checked';
        }
        if ($data == NULL) {
            $data['idtrasnfers'] = NULL;
            $data['city_idcity'] = NULL;
            $data['name'] = NULL;
            $data['description'] = NULL;
            $data['cars_availables'] = NULL;
            $data['active'] = NULL;
            $data['ina'] = NULL;
            $data['act'] = "0000-00-00";
            $data['city'] = NULL;
            $data['citysearch'] = NULL;
            $data['type_transfers_idtype_transfers'] = NULL;
        }

        $transport = $this->Typetransfer_model->selecttypetransfer();

        foreach ($transport->result() as $tran):
            $data['type_transfers_idtype_transfers'] .= '<option value="' . $tran->idtype_transfers . '" '. (isset($data['type_transfers_idtype_transfers1']) && $data['type_transfers_idtype_transfers1'] == $tran->idtype_transfers ? "selected" : "") .'>' . $tran->name . '</option>';
        endforeach;

        $this->load->view('backend/transports/newtransport', $data);
    }

    function savetransport() {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('name', 'Name', 'trim|required|xss_clean|max_length[256]|min_length[2]');
        $this->form_validation->set_rules('description', 'Description', 'trim|required|xss_clean|min_length[5]');
        $this->form_validation->set_rules('cars_availables', 'Cars availables', 'trim|required|xss_clean|max_length[11]|min_length[1]|numeric');
        $this->form_validation->set_rules('city_idcity', 'City', 'trim|required|xss_clean|max_length[10]|min_length[1]');
        $this->form_validation->set_rules('active', 'Active', 'trim|required|xss_clean|max_length[15]|min_length[1]');


        $this->form_validation->set_message('required', 'The field  %s is required');
        $this->form_validation->set_message('valid_email', 'The %s is not valid');
        $this->form_validation->set_message('min_length', 'Field %s must have at least %s characters');
        $this->form_validation->set_message('max_length', 'Field %s must have less %s characters');
        $this->form_validation->set_message('numeric', 'The field  %s is numeric');

        if (!$this->form_validation->run()) {
            $return = $this->input->post();
            $return['act'] = '';
            $return['ina'] = '';

            (1 == $this->input->post('active')) ? $return['act'] = 'checked' : $return['ina'] = 'checked';

            $this->newtransport($return);
        } else {
            $data['mesg'] = 'Ups! An error has occurred';
            if ($this->input->post('idtrasnfers') != '') {
                $data = $this->input->post();
                $data['act'] = '';
                $data['ina'] = '';
                (1 == $this->input->post('active')) ? $return['act'] = 'checked' : (0 == $this->input->post('active')) ? $return['ina'] = 'checked' : $return['ina'] = '';

                if ($this->Transfers_model->updatetransfers($data)) {
                    $this->session->set_flashdata('success', 'Correctly Updated Transport');
                } else {
                    $this->session->set_flashdata('error', 'Ups! An error has occurred');
                }
            } else {
                $data = $this->input->post();
                $data['act'] = '';
                $data['ina'] = '';
                (1 == $this->input->post('active')) ? $data['act'] = 'checked' : $data['ina'] = 'checked';
                $this->session->set_flashdata('success', 'Transport Created Correctly');

                $data['idtrasnfers'] = $this->Transfers_model->inserttransfers($data);
                if (!$data['idtrasnfers']) {
                    $this->session->set_flashdata('error', 'Ups! An error has occurred');
                }
            }
            $this->newtransport($data);
        }
    }

    function listtransport($data = NULL) {
        $this->load->view('backend/transports/listtransport', $data);
    }

    function ajax_listtransport() {


        $condi = "TRUE";
        $orderby = " order by idtrasnfers desc ";
        $limit = "";
        $fields = ' *,(SELECT concat(cot.name," ",cot.abbre) as city FROM city as cot WHERE cot.idcity=city_idcity) as city ';
        $transport = $this->Transfers_model->selecttransfers($condi, $orderby, $limit, $fields);

        $data = array();
        $no = 0;
        foreach ($transport->result() as $ps) {
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $ps->name;
            $row[] = $ps->description;
            $row[] = $ps->city;
            $row[] = (1 == $ps->active) ? "active" : "Inactivo";
//            $row[] = $ps->cost;            
            //$row[] = ;

            $seemore = '<span class="cssToolTip">
                            <button class="btn btn-success btn-xs fa fa-search-plus"></button>
                            <span>User: ' . $no . '<br>'
                    . '</span>'
                    . '</span>';

            $hidden = '<input type="hidden" name="idtrasnfers" value="' . $ps->idtrasnfers . '">';
            $edit = '<div>' . $hidden . '<button id="updatetransport" class="btn btn-warning btn-xs fa fa-edit" title="Edit transport" onclick="udpatetransport(' . $ps->idtrasnfers . ',\'' . base_url() . '\')"></button>';
            $color = ($ps->active == 1) ? "btn-danger" : "btn-default";
            $delete = '<div>' . $hidden . '<button class="btn ' . $color . ' btn-xs fa fa-trash-o" title="Delete transport" onclick="deletetransport(' . $ps->idtrasnfers . ',' . $ps->active . ',\'' . base_url() . '\')"></button></div>';

            $row[] = '<table style="border-collapse: separate; border-spacing:  5px;">'
                    . '<tr>'
                    . '<td>' . $seemore . '</td>'
                    . '<td>' . $edit . '</td>'
                    . '<td>' . $delete . '</td>'
                    . '</tr>'
                    . '</table>';

            $data[] = $row;
        }

//        $output = array(
//            "draw" => $_POST['draw'],
//            "recordsTotal" => $this->customers->count_all(),
//            "recordsFiltered" => $this->customers->count_filtered(),
//            "data" => $data,
//        );

        $output = array(
            "draw" => "",
            "recordsTotal" => $transport->num_rows(),
            "recordsFiltered" => $transport->num_rows(),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }

    function deletetransport() {//       
        $data = $this->input->post();
        if ($data['active'] == 0) {
            $data['active'] = 1;
        } else {
            $data['active'] = 0;
        }
        if ($this->Transfers_model->deletetransfers($data)) {
            $this->session->set_flashdata('success', 'Correctly Updated Transport');
        } else {
            $this->session->set_flashdata('error', 'Ups! An error has occurred');
        }
        $this->listtransport();
    }

}
