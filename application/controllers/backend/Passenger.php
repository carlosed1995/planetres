<?php

defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Passenger
 *
 * @author carlos
 */
class Passenger extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('Axcs_model');
        $this->load->model('Planetresmodel');
        $this->load->model('Passengers_model');
        $this->load->library('Session');
    }

    function newpassenger($data = NULL) {


        if ($this->input->post('idpassengers') != NULL) {
            $passenger = $this->Passengers_model->selectpassenger(" idpassengers=" . $this->input->post('idpassengers'));
            $data = get_object_vars($passenger->result()[0]);
            $data['id_city'];
            $city = $search = $this->Planetresmodel->city(" idcity =" . $data['id_city'], " limit 10 ", " order by abbre ");
            $data['citysearch'] = get_object_vars($city->result()[0])['name'];
            $data['female'] = '';
            $data['male'] = '';
            ('MALE' == strtoupper($data['genere'])) ? $data['male'] = 'checked' : $data['female'] = 'checked';
            $data['cyear'] = explode("-", $data['birth'])[0];
            $data['cmonth'] = explode("-", $data['birth'])[1];
            $data['cday'] = explode("-", $data['birth'])[2];
            $data['ccpyear'] = explode("-", $data['expdedition_passport'])[0];
            $data['ccpmonth'] = explode("-", $data['expdedition_passport'])[1];
            $data['ccpday'] = explode("-", $data['expdedition_passport'])[2];
            $data['cepyear'] = explode("-", $data['expire_passport'])[0];
            $data['cepmonth'] = explode("-", $data['expire_passport'])[1];
            $data['cepday'] = explode("-", $data['expire_passport'])[2];
        }
        if ($data == NULL) {
            $data['idpassengers'] = NULL;
            $data['name'] = NULL;
            $data['last_name'] = NULL;
            $data['passport'] = NULL;
            $data['phone'] = NULL;
            $data['genere'] = NULL;
            $data['birth'] = "0000-00-00";
            $data['cyear'] = "0000";
            $data['cmonth'] = "00";
            $data['cday'] = "00";
            $data['ccpyear'] = "0000";
            $data['ccpmonth'] = "00";
            $data['ccpday'] = "00";
            $data['cepyear'] = "0000";
            $data['cepmonth'] = "00";
            $data['cepday'] = "00";
            $data['msg'] = NULL;
            $data['email'] = NULL;
            $data['id_city'] = NULL;
            $data['citysearch'] = NULL;
            $data['mesg'] = NULL;
            $data['male'] = NULL;
            $data['female'] = NULL;
            $data['miles_program'] = NULL;
            $data['miles'] = NULL;
        }
        $data['selectnationality'] = NULL;
        $data['year'] = '<select name="year" class="form-control"><option value="">Year...</option>';
        foreach (range(date('Y') - 100, date('Y')) as $numero) {
            $data['year'] .= '<option value="' . $numero . '" ' . (isset($data['cyear']) && $data['cyear'] == $numero ? "selected" : "") . '>' . $numero . '</option>';
        }
        $data['year'] .= '</select>';
        $data['month'] = '<select name="month" class="form-control"><option value="">Month...</option>';
        foreach (range(1, 12) as $numero) {
            $numero = str_pad($numero, "2", "0", STR_PAD_LEFT);
            $data['month'] .= '<option value="' . $numero . '" ' . (isset($data['cmonth']) && $data['cmonth'] == $numero ? "selected" : "") . '>' . $numero . '</option>';
        }
        $data['month'] .= '</select>';
        $data['day'] = '<select name="day" class="form-control"><option value="">Day...</option>';
        foreach (range(1, 31) as $numero) {
            $numero = str_pad($numero, "2", "0", STR_PAD_LEFT);
            $data['day'] .= '<option value="' . $numero . '" ' . (isset($data['cday']) && $data['cday'] == $numero ? "selected" : "") . '>' . $numero . '</option>';
        }$data['day'] .= '</select>';
        /**/
        $data['cpyear'] = '<select name="cpyear" class="form-control"><option value="">Year...</option>';
        foreach (range(date('Y') - 100, date('Y')) as $numero) {
            $data['cpyear'] .= '<option value="' . $numero . '" ' . (isset($data['ccpyear']) && $data['ccpyear'] == $numero ? "selected" : "") . '>' . $numero . '</option>';
        }
        $data['cpyear'] .= '</select>';
        $data['cpmonth'] = '<select name="cpmonth" class="form-control"><option value="">Month...</option>';
        foreach (range(1, 12) as $numero) {
            $numero = str_pad($numero, "2", "0", STR_PAD_LEFT);
            $data['cpmonth'] .= '<option value="' . $numero . '" ' . (isset($data['ccpmonth']) && $data['ccpmonth'] == $numero ? "selected" : "") . '>' . $numero . '</option>';
        }
        $data['cpmonth'] .= '</select>';
        $data['cpday'] = '<select name="cpday" class="form-control"><option value="">Day...</option>';
        foreach (range(1, 31) as $numero) {
            $numero = str_pad($numero, "2", "0", STR_PAD_LEFT);
            $data['cpday'] .= '<option value="' . $numero . '" ' . (isset($data['ccpday']) && $data['ccpday'] == $numero ? "selected" : "") . '>' . $numero . '</option>';
        }$data['cpday'] .= '</select>';
        /**/
        $data['epyear'] = '<select name="epyear" class="form-control"><option value="">Year...</option>';
        foreach (range(date('Y') - 100, date('Y')) as $numero) {
            $data['epyear'] .= '<option value="' . $numero . '" ' . (isset($data['cepyear']) && $data['cepyear'] == $numero ? "selected" : "") . '>' . $numero . '</option>';
        }
        $data['epyear'] .= '</select>';
        $data['epmonth'] = '<select name="epmonth" class="form-control"><option value="">Month...</option>';
        foreach (range(1, 12) as $numero) {
            $numero = str_pad($numero, "2", "0", STR_PAD_LEFT);
            $data['epmonth'] .= '<option value="' . $numero . '" ' . (isset($data['cepmonth']) && $data['cepmonth'] == $numero ? "selected" : "") . '>' . $numero . '</option>';
        }
        $data['epmonth'] .= '</select>';
        $data['epday'] = '<select name="epday" class="form-control"><option value="">Day...</option>';
        foreach (range(1, 31) as $numero) {
            $numero = str_pad($numero, "2", "0", STR_PAD_LEFT);
            $data['epday'] .= '<option value="' . $numero . '" ' . (isset($data['cepday']) && $data['cepday'] == $numero ? "selected" : "") . '>' . $numero . '</option>';
        }$data['epday'] .= '</select>';


        $data['selectnationality'] = '<select class="form-control" id="nationality" name="nationality" placeholder="Enter nationality">
                                    <option value="">Select ... </option>';


        $nationality = $this->Planetresmodel->nationality();
        foreach ($nationality->result() as $row) {
            $data['selectnationality'] .= '<option value="' . $row->idcountry . '" ' . (isset($data['nationality']) && $data['nationality'] == $row->idcountry ? "selected" : "") . '>' . $row->abbre . '</option>';
        }
        $data['selectnationality'] .= '</select>';
        $this->load->view('backend/passengers/newpassenger', $data);
    }

    function savepassenger() {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('name', 'Name', 'trim|required|xss_clean|max_length[256]|min_length[2]');
        $this->form_validation->set_rules('last_name', 'Lastname', 'trim|required|xss_clean|max_length[256]|min_length[2]');
        $this->form_validation->set_rules('passport', 'Passport', 'trim|required|xss_clean|max_length[45]|min_length[2]');

        $this->form_validation->set_rules('email', 'Email', 'trim|valid_email|required|xss_clean');
        $this->form_validation->set_rules('phone', 'Phone', 'trim|required|xss_clean|max_length[15]|min_length[2]');
        $this->form_validation->set_rules('id_city', 'City', 'trim|required|xss_clean|max_length[10]|min_length[1]');
        $this->form_validation->set_rules('genere', 'Gender', 'trim|required|xss_clean|max_length[15]|min_length[1]');

        $this->form_validation->set_rules('year', 'Year', 'trim|required|xss_clean|max_length[4]|min_length[4]|numeric');
        $this->form_validation->set_rules('month', 'Month', 'trim|required|xss_clean|max_length[2]|min_length[2]|numeric');
        $this->form_validation->set_rules('day', 'Day', 'trim|required|xss_clean|max_length[2]|min_length[2]|numeric');
        $this->form_validation->set_rules('cpyear', 'Year', 'trim|required|xss_clean|max_length[4]|min_length[4]|numeric');
        $this->form_validation->set_rules('cpmonth', 'Month', 'trim|required|xss_clean|max_length[2]|min_length[2]|numeric');
        $this->form_validation->set_rules('cpday', 'Day', 'trim|required|xss_clean|max_length[2]|min_length[2]|numeric');
        $this->form_validation->set_rules('epyear', 'Year', 'trim|required|xss_clean|max_length[4]|min_length[4]|numeric');
        $this->form_validation->set_rules('epmonth', 'Month', 'trim|required|xss_clean|max_length[2]|min_length[2]|numeric');
        $this->form_validation->set_rules('epday', 'Day', 'trim|required|xss_clean|max_length[2]|min_length[2]|numeric');
        $this->form_validation->set_rules('miles_program', 'Miles Program Name', 'trim|xss_clean|max_length[250]|min_length[2]');
        $this->form_validation->set_rules('miles', 'Miles', 'trim|xss_clean|max_length[10]|min_length[1]|numeric');

        $this->form_validation->set_rules('nationality', 'Nationality', 'trim|required|xss_clean|max_length[11]|min_length[1]|numeric');

        $this->form_validation->set_message('required', 'The field  %s is required');
        $this->form_validation->set_message('valid_email', 'The %s is not valid');
        $this->form_validation->set_message('min_length', 'Field %s must have at least %s characters');
        $this->form_validation->set_message('max_length', 'Field %s must have less %s characters');
        $this->form_validation->set_message('numeric', 'The field  %s is numeric');

        if (!$this->form_validation->run()) {
            $return = $this->input->post();
            $return['male'] = '';
            $return['female'] = '';
            $return['cyear'] = $this->input->post('year');
            $return['cmonth'] = $this->input->post('month');
            $return['cday'] = $this->input->post('day');
            $return['ccpyear'] = $this->input->post('cpyear');
            $return['ccpmonth'] = $this->input->post('cpmonth');
            $return['ccpday'] = $this->input->post('cpday');
            $return['cepyear'] = $this->input->post('epyear');
            $return['cepmonth'] = $this->input->post('epmonth');
            $return['cepday'] = $this->input->post('epday');
            ('male' == strtolower($this->input->post('genere'))) ? $return['male'] = 'checked' : ('female' == strtolower($this->input->post('genere'))) ? $return['female'] = 'checked' : '';

            $this->newpassenger($return);
        } else {

            $data['mesg'] = 'Ups! An error has occurred';
            if ($this->input->post('idpassengers') != '') {
                $data = $this->input->post();
                $data['male'] = '';
                $data['female'] = '';
                $return['cyear'] = $this->input->post('year');
                $return['cmonth'] = $this->input->post('month');
                $return['cday'] = $this->input->post('day');
                $return['ccpyear'] = $this->input->post('cpyear');
                $return['ccpmonth'] = $this->input->post('cpmonth');
                $return['ccpday'] = $this->input->post('cpday');
                $return['cepyear'] = $this->input->post('epyear');
                $return['cepmonth'] = $this->input->post('epmonth');
                $return['cepday'] = $this->input->post('epday');
                ('male' == strtolower($this->input->post('genere'))) ? $data['male'] = 'checked' : $data['female'] = 'checked';

                if ($this->Passengers_model->updatepassenger($data)) {
                    $this->session->set_flashdata('success', 'Correctly Updated Passenger');
                } else {
                    $this->session->set_flashdata('error', 'Ups! An error has occurred');
                }
            } else {
                $data = $this->input->post();
                $data['male'] = '';
                $data['female'] = '';
                $return['cyear'] = $this->input->post('year');
                $return['cmonth'] = $this->input->post('month');
                $return['cday'] = $this->input->post('day');
                $return['ccpyear'] = $this->input->post('cpyear');
                $return['ccpmonth'] = $this->input->post('cpmonth');
                $return['ccpday'] = $this->input->post('cpday');
                $return['cepyear'] = $this->input->post('epyear');
                $return['cepmonth'] = $this->input->post('epmonth');
                $return['cepday'] = $this->input->post('epday');
                ('male' == strtolower($this->input->post('genere'))) ? $data['male'] = 'checked' : $data['female'] = 'checked';
                $this->session->set_flashdata('success', 'Passenger Created Correctly');

                $data['idpassengers'] = $this->Passengers_model->insertpassenger($data);
                if (!$data['idpassengers']) {
                    $this->session->set_flashdata('error', 'Ups! An error has occurred');
                }
            }
            $this->newpassenger($data);
        }
    }

    function listpassenger($data = NULL) {
        $this->load->view('backend/passengers/listpassenger', $data);
    }

    function ajax_listpassenger() {


        $condi = "TRUE";
        $orderby = " order by idpassengers desc ";
        $limit = "";
        $fields = ' *,(SELECT concat(ct.name," ",ct.abbre) FROM country as ct WHERE ct.idcountry=nationality) as nationality,(SELECT concat(cit.name," ",cit.abbre) as city FROM city as cit WHERE cit.idcity=id_city) as city ';
        $passenger = $this->Passengers_model->selectpassenger($condi, $orderby, $limit, $fields);

        $data = array();
        $no = 0;
        foreach ($passenger->result() as $ps) {
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $ps->name;
            $row[] = $ps->last_name;
            $row[] = $ps->email;
            $row[] = $ps->city;
            $row[] = ucfirst($ps->genere);
            $row[] = $ps->miles_program;
            $row[] = $ps->miles;

            //$row[] = ;

            $seemore = '<span class="cssToolTip">
                            <button class="btn btn-success btn-xs fa fa-search-plus"></button>
                            <span>User: ' . $no . '<br>Nationality:' . $ps->nationality . '<br>Passport: ' . $ps->passport . '<br>Expedition: ' . $ps->expdedition_passport . '<br>Expiration: ' . $ps->expire_passport . '<br>Phone:' . $ps->phone . '<br>Birthdate: ' . $ps->birth . ''
                    . '</span>'
                    . '</span>';

            $hidden = '<input type="hidden" name="idupassengers" value="' . $ps->idpassengers . '">';
            $edit = '<div>' . $hidden . '<button id="updateuser" class="btn btn-warning btn-xs fa fa-edit" title="Edit passenger" onclick="udpatepassenger(' . $ps->idpassengers . ',\'' . base_url() . '\')"></button>';
            $color = ($ps->active == 1) ? "btn-danger" : "btn-default";
            $delete = '<div>' . $hidden . '<button class="btn ' . $color . ' btn-xs fa fa-trash-o" title="Delete passenger" onclick="deletepassenger(' . $ps->idpassengers . ',' . $ps->active . ',\'' . base_url() . '\')"></button></div>';

            $row[] = '<table style="border-collapse: separate; border-spacing:  5px;">'
                    . '<tr>'
                    . '<td>' . $seemore . '</td>'
                    . '<td>' . $edit . '</td>'
                    . '<td>' . $delete . '</td>'
                    . '</tr>'
                    . '</table>';

            $data[] = $row;
        }

//        $output = array(
//            "draw" => $_POST['draw'],
//            "recordsTotal" => $this->customers->count_all(),
//            "recordsFiltered" => $this->customers->count_filtered(),
//            "data" => $data,
//        );

        $output = array(
            "draw" => "",
            "recordsTotal" => $passenger->num_rows(),
            "recordsFiltered" => $passenger->num_rows(),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }

    function deletepassenger() {//       
        $data = $this->input->post();
        if ($data['active'] == 0) {
            $data['active'] = 1;
        } else {
            $data['active'] = 0;
        }
        if ($this->Passengers_model->deletepassenger($data)) {
            $this->session->set_flashdata('success', 'Correctly Updated passenger');
        } else {
            $this->session->set_flashdata('error', 'Ups! An error has occurred');
        }
        $this->listpassenger();
    }

}
