<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Tours
 *
 * @author carlos
 */
class Tours extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('Axcs_model');
        $this->load->model('Planetresmodel');
        $this->load->model('Tours_model');
        $this->load->model('Tourstype_model');
        $this->load->library('Session');
    }

    function newtours($data = NULL) {

        if ($this->input->post('idtours') != NULL) {
            $tours = $this->Tours_model->selecttours(" idtours=" . $this->input->post('idtours'));
            $data = get_object_vars($tours->result()[0]);
            $data['act'] = '';
            $data['ina'] = '';
            (1 == $data['active']) ? $data['act'] = 'checked' : $data['ina'] = 'checked';            
            $tours_type_idtours_type = $this->Tourstype_model->selecttourstype(" idtours_type =" . $data['tours_type_idtours_type'], " limit 10 ");
            $data['tours_type_idtours_type'] = get_object_vars($tours_type_idtours_type->result()[0])['name'];
        }
        if ($data == NULL) {
            $data['idtours'] = NULL;
            $data['code'] = NULL;
            $data['name'] = NULL;
            $data['location'] = NULL;
            $data['highlights'] = NULL;
            $data['max_persons'] = NULL;
            $data['active'] = NULL;
            $data['act'] = '';
            $data['ina'] = '';
            $data['create'] = '0000-00-00';
            $data['cmonth'] = '';
            $data['cyear'] = '';
            $data['cday'] = '';
            $data['description'] = '';
            $data['city'] = '';
            $data['id_city'] = '';
            $data['tours_type_idtours_type'] = NULL;
        }
        if (isset($data['year'])) {
            $data['cyear'] = $data['year'];
        }
        if (isset($data['month'])) {
            $data['cmonth'] = $data['month'];
        }
        if (isset($data['day'])) {
            $data['cday'] = $data['day'];
        }
        $data['year'] = '<select name="year" class="form-control"><option value="">Year...</option>';
        foreach (range(date('Y') - 100, date('Y')) as $numero) {
            $data['year'] .= '<option value="' . $numero . '" ' . (isset($data['cyear']) && $data['cyear'] == $numero ? "selected" : "") . '>' . $numero . '</option>';
        }
        $data['year'] .= '</select>';
        $data['month'] = '<select name="month" class="form-control"><option value="">Month...</option>';
        foreach (range(1, 12) as $numero) {
            $numero = str_pad($numero, "2", "0", STR_PAD_LEFT);
            $data['month'] .= '<option value="' . $numero . '" ' . (isset($data['cmonth']) && $data['cmonth'] == $numero ? "selected" : "") . '>' . $numero . '</option>';
        }
        $data['month'] .= '</select>';
        $data['day'] = '<select name="day" class="form-control"><option value="">Day...</option>';
        foreach (range(1, 31) as $numero) {
            $numero = str_pad($numero, "2", "0", STR_PAD_LEFT);
            $data['day'] .= '<option value="' . $numero . '" ' . (isset($data['cday']) && $data['cday'] == $numero ? "selected" : "") . '>' . $numero . '</option>';
        }$data['day'] .= '</select>';
        $tours_type_idtours_type = $this->Tourstype_model->selecttourstype();
        foreach ($tours_type_idtours_type->result() as $to):
            $data['tours_type_idtours_type'] .= '<option value="' . $to->idtours_type . '">' . $to->name . '</option>';
        endforeach;

        $this->load->view('backend/tours/newtours', $data);
    }

    function listtours($data = NULL) {
        $this->load->view('backend/tours/listtours', $data);
    }

    function ajax_listtours() {

        $condi = "TRUE";
        $orderby = " order by idtours desc ";
        $limit = "";
         $fields = ' *,(SELECT concat(cot.name) as city FROM tours_type as cot WHERE cot.idtours_type=tours_type_idtours_type) as city ';
        $tours = $this->Tours_model->selecttours($condi, $orderby, $limit, $fields);

        $data = array();
        $no = 0;
        foreach ($tours->result() as $us) {
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $us->code;
            $row[] = $us->name;            
            $row[] = $us->max_persons;            
            $row[] = $us->city;
            $row[] = (1 == $us->active) ? "Active" : "Inactive";


            $seemore = '<span class="cssToolTip">
                            <button class="btn btn-success btn-xs fa fa-search-plus"></button>
                            <span>Tours: ' . $no . ''
                    . '</span>'
                    . '</span>';
            $hidden = '<input type="hidden" name="idtours" value="' . $us->idtours . '">';
            $edit = '<div>' . $hidden . '<button id="updatetours" class="btn btn-warning btn-xs fa fa-edit" title="Edit Tours" onclick="udpatetours(' . $us->idtours . ',\'' . base_url() . '\')"></button>';
            $color = ($us->active == 1) ? "btn-danger" : "btn-default";
            $delete = '<div>' . $hidden . '<button class="btn ' . $color . ' btn-xs fa fa-trash-o" title="Delete Tours" onclick="deletetours(' . $us->idtours . ',' . $us->active . ',\'' . base_url() . '\')"></button></div>';

            $row[] = '<table style="border-collapse: separate; border-spacing:  5px;">'
                    . '<tr>'
                    . '<td>' . $seemore . '</td>'
                    . '<td>' . $edit . '</td>'
                    . '<td>' . $delete . '</td>'
                    . '</tr>'
                    . '</table>';

            $data[] = $row;
        }

//        $output = array(
//            "draw" => $_POST['draw'],
//            "recordsTotal" => $this->customers->count_all(),
//            "recordsFiltered" => $this->customers->count_filtered(),
//            "data" => $data,
//        );

        $output = array(
            "draw" => "",
            "recordsTotal" => $tours->num_rows(),
            "recordsFiltered" => $tours->num_rows(),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }

    function savetours() {

        $this->load->library('form_validation');

        $this->form_validation->set_rules('name', 'Name', 'trim|required|xss_clean|min_length[2]');
        $this->form_validation->set_rules('code', 'Code', 'trim|required|xss_clean|min_length[2]');
        $this->form_validation->set_rules('highlights', 'Highlights', 'trim|required|xss_clean|min_length[2]');
        $this->form_validation->set_rules('location', 'Location', 'trim|required|xss_clean|max_length[256]|min_length[2]');
        $this->form_validation->set_rules('max_persons', 'Max Persons', 'trim|required|xss_clean');
        $this->form_validation->set_rules('tours_type_idtours_type', 'Tours Type', 'trim|required|xss_clean');
        $this->form_validation->set_rules('active', 'Active', 'trim|required|xss_clean|max_length[15]|min_length[1]');


        $this->form_validation->set_message('required', 'The field  %s is required');
        $this->form_validation->set_message('valid_email', 'The %s is not valid');
        $this->form_validation->set_message('min_length', 'Field %s must have at least %s characters');
        $this->form_validation->set_message('max_length', 'Field %s must have less %s characters');
        $this->form_validation->set_message('numeric', 'The field  %s is numeric');

        if (!$this->form_validation->run()) {
            $return = $this->input->post();
            $return['ina'] = '';
            $return['act'] = '';
            (1 == $this->input->post('active')) ? $return['act'] = 'checked' : $return['ina'] = 'checked';
            $this->newtours($return);
        } else {
            $data['mesg'] = 'Ups! An error has occurred';
            if ($this->input->post('idtours') != '') {
                $data = $this->input->post();
                $data['act'] = '';
                $data['ina'] = '';
                (1 == $this->input->post('active')) ? $data['act'] = 'checked' : $data['ina'] = 'checked';

                if ($this->Tours_model->updatetours($data)) {
                    $this->session->set_flashdata('success', 'Correctly Updated Tours');
                } else {
                    $this->session->set_flashdata('error', 'Ups! An error has occurred');
                }
            } else {
                $data = $this->input->post();
                $data['act'] = '';
                $data['ina'] = '';
                (1 == $this->input->post('active')) ? $data['act'] = 'checked' : $data['ina'] = 'checked';
                $this->session->set_flashdata('success', 'Tours Created Correctly');

                $data['idtours'] = $this->Tours_model->inserttours($data);
                if (!$data['idtours']) {
                    $this->session->set_flashdata('error', 'Ups! An error has occurred');
                }
            }
            if ($data['idtours']) {
                $this->Tours_model->daily_itinerary($data['idtours'], $this->input->post('date'), $this->input->post('name1'), $this->input->post('description'), $this->input->post('city_idcity'));
                /* Front */
                if (!empty($_FILES['front']['name'])) {
                    $config["upload_path"] = "web/images/gallery/tours/";
                    $config["allowed_types"] = "gif|jpg|jpeg|png";
                    $config["max_size"] = "2000";
                    $config["remove_spaces"] = TRUE;
                    $config["file_name"] = date('Ymdhis') . rand(1, 10000);
                    $this->load->library('upload', $config);
                    $this->upload->initialize($config);
                    if ($this->upload->do_upload('front')) {
                        $fileData = $this->upload->data();
                        $this->Tours_model->insertimages($config["file_name"], $data['idtours'], 0);
                    } else {
                        $errors = array('error' => $this->upload->display_errors());
                        die(json_encode($errors));
                    }
                }
                /* Front */
                if (!empty($_FILES['images']['name'])) {
                    $filesCount = count($_FILES['images']['name']);
                    for ($i = 0; $i < $filesCount; $i++) {
                        if ($_FILES['images']['name'][$i] != '') {
                            $_FILES['userFile']['name'] = $_FILES['images']['name'][$i];
                            $_FILES['userFile']['type'] = $_FILES['images']['type'][$i];
                            $_FILES['userFile']['tmp_name'] = $_FILES['images']['tmp_name'][$i];
                            $_FILES['userFile']['error'] = $_FILES['images']['error'][$i];
                            $_FILES['userFile']['size'] = $_FILES['images']['size'][$i];
                            $config["upload_path"] = "web/images/gallery/tours/";
                            $config["allowed_types"] = "gif|jpg|jpeg|png";
                            $config["max_size"] = "2000";
                            $config["remove_spaces"] = TRUE;
                            $config["file_name"] = date('Ymdhis') . rand(1, 10000);
                            $this->load->library('upload', $config);
                            $this->upload->initialize($config);
                            if ($this->upload->do_upload('userFile')) {
                                $fileData = $this->upload->data();
                                $this->Tours_model->insertimages($fileData['file_name'], $data['idtours'], 1);
                            } else {
                                $errors = array('error' => $this->upload->display_errors());
                                die(json_encode($errors));
                            }
                        }
                    }
                }
            }
            $this->newtours($data);
        }
    }

    function deletetours() {//       
        $data = $this->input->post();
        if ($data['active'] == 0) {
            $data['active'] = 1;
        } else {
            $data['active'] = 0;
        }
        if ($this->Tours_model->deletetours($data)) {
            $this->session->set_flashdata('success', 'Correctly Updated Tours');
        } else {
            $this->session->set_flashdata('error', 'Ups! An error has occurred');
        }
        $this->listtours();
    }

    function autocomplete() {
        if ($this->input->is_ajax_request() && $this->input->post('info') && strlen($this->input->post('info')) > 3) {
            $condition = " name like '" . $this->input->post('info') . "%'";
            $search = $this->Planetresmodel->city($condition, " limit 10 ", " order by abbre ");
            if ($search !== FALSE) {
                foreach ($search->result() as $row) {
                    ?>
                    <a onclick="javascript:asignar(<?= $row->idcity . ',\'' . $row->name . '\''; ?>)" ><?php echo $row->name . ' ' . $row->abbre ?></a><br>
                    <?php
                }
            } else {
                ?>

                <p><?php echo 'No results' ?></p>

                <?php
            }
        }
    }

}
