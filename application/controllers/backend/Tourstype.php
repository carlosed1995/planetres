<?php

defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Passenger
 *
 * @author carlos
 */
class Tourstype extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('Axcs_model');
        $this->load->model('Planetresmodel');
        $this->load->model('Tourstype_model');
        $this->load->library('Session');
    }

    function newtourstype($data = NULL) {


        if ($this->input->post('idtours_type') != NULL) {
            $tourstype = $this->Tourstype_model->selecttourstype(" idtours_type=" . $this->input->post('idtours_type'));
            $data = get_object_vars($tourstype->result()[0]);            
            $data['ina'] = '';
            $data['act'] = '';
            (1 == $data['active']) ? $data['act'] = 'checked' : $data['ina'] = 'checked';
        }
        if ($data == NULL) {
            $data['idtours_type'] = NULL;
            $data['name'] = NULL;
            $data['description'] = NULL;            
            $data['active'] = NULL;
            $data['ina'] = NULL;
            $data['act'] = NULL;            
            
        }

        $this->load->view('backend/tourstype/newtourstype', $data);
    }

    function savetourstype() {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('name', 'Name', 'trim|required|xss_clean|max_length[256]|min_length[2]');
        $this->form_validation->set_rules('description', 'Description', 'trim|required|xss_clean|min_length[10]');
        $this->form_validation->set_rules('active', 'Active', 'trim|required|xss_clean|max_length[15]|min_length[1]');


        $this->form_validation->set_message('required', 'The field  %s is required');
        $this->form_validation->set_message('valid_email', 'The %s is not valid');
        $this->form_validation->set_message('min_length', 'Field %s must have at least %s characters');
        $this->form_validation->set_message('max_length', 'Field %s must have less %s characters');
        $this->form_validation->set_message('numeric', 'The field  %s is numeric');

        if (!$this->form_validation->run()) {
            $return = $this->input->post();
            $return['act'] = '';
            $return['ina'] = '';

            (1 == $this->input->post('active')) ? $return['act'] = 'checked' : $return['ina'] = 'checked';

            $this->newtourstype($return);
        } else {
            $data['mesg'] = 'Ups! An error has occurred';
            if ($this->input->post('idtours_type') != '') {
                $data = $this->input->post();
                $data['act'] = '';
                $data['ina'] = '';
                            (1 == $this->input->post('active')) ? $return['act'] = 'checked' : (0 == $this->input->post('active')) ? $return['ina'] = 'checked':$return['ina'] = '';

                if ($this->Tourstype_model->updatetourstype($data)) {
                    $this->session->set_flashdata('success', 'Correctly Updated Type Transfer');
                } else {
                    $this->session->set_flashdata('error', 'Ups! An error has occurred');
                }
            } else {
                $data = $this->input->post();
                $data['act'] = '';
                $data['ina'] = '';
                (1 == $this->input->post('active')) ? $data['act'] = 'checked' : $data['ina'] = 'checked';
                $this->session->set_flashdata('success', 'Type Transfer Created Correctly');

                $data['idtours_type'] = $this->Tourstype_model->inserttourstype($data);
                if (!$data['idtours_type']) {
                    $this->session->set_flashdata('error', 'Ups! An error has occurred');
                }
            }
            $this->newtourstype($data);
        }
    }

    function listtourstype($data = NULL) {
        $this->load->view('backend/tourstype/listtourstype', $data);
    }

    function ajax_listtourstype() {


        $condi = "TRUE";
        $orderby = " order by idtours_type desc ";
        $limit = "";
        $fields = '*';
        $tourstype = $this->Tourstype_model->selecttourstype($condi, $orderby, $limit, $fields);

        $data = array();
        $no = 0;
        foreach ($tourstype->result() as $ps) {
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $ps->name;
            $row[] = $ps->description;                        
            $row[] = (1==$ps->active)? "Active":"Inactive";            

            //$row[] = ;

            $seemore = '<span class="cssToolTip">
                            <button class="btn btn-success btn-xs fa fa-search-plus"></button>
                            <span>Tours Type: ' . $no . '<br>'
                    . '</span>'
                    . '</span>';

            $hidden = '<input type="hidden" name="idtours_type" value="' . $ps->idtours_type . '">';
            $edit = '<div>' . $hidden . '<button id="updatetourstype" class="btn btn-warning btn-xs fa fa-edit" title="Edit tourstype" onclick="udpatetourstype(' . $ps->idtours_type . ',\'' . base_url() . '\')"></button>';
            $color = ($ps->active == 1) ? "btn-danger" : "btn-default";
            $delete = '<div>' . $hidden . '<button class="btn ' . $color . ' btn-xs fa fa-trash-o" title="Delete tourstype" onclick="deletetourstype(' . $ps->idtours_type . ',' . $ps->active . ',\'' . base_url() . '\')"></button></div>';

            $row[] = '<table style="border-collapse: separate; border-spacing:  5px;">'
                    . '<tr>'
                    . '<td>' . $seemore . '</td>'
                    . '<td>' . $edit . '</td>'
                    . '<td>' . $delete . '</td>'
                    . '</tr>'
                    . '</table>';

            $data[] = $row;
        }

//        $output = array(
//            "draw" => $_POST['draw'],
//            "recordsTotal" => $this->customers->count_all(),
//            "recordsFiltered" => $this->customers->count_filtered(),
//            "data" => $data,
//        );

        $output = array(
            "draw" => "",
            "recordsTotal" => $tourstype->num_rows(),
            "recordsFiltered" => $tourstype->num_rows(),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }

    function deletetourstype() {//       
        $data = $this->input->post();
        if ($data['active'] == 0) {
            $data['active'] = 1;
        } else {
            $data['active'] = 0;
        }
        if ($this->Tourstype_model->deletetourstype($data)) {
            $this->session->set_flashdata('success', 'Correctly Updated Tours Type');
        } else {
            $this->session->set_flashdata('error', 'Ups! An error has occurred');
        }
        $this->listtourstype();
    }

}
