<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Hotel
 *
 * @author carlos
 */
class Hotels extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('Axcs_model');
        $this->load->model('Planetresmodel');
        $this->load->model('Hotels_model');
        $this->load->model('Categories_model');
        $this->load->model('Facilities_model');
        $this->load->library('Session');
    }

    function newhotels($data = NULL) {
        if ($this->input->post('idhotel') != NULL) {
            $hotels = $this->Hotels_model->selecthotels(" idhotel=" . $this->input->post('idhotel'));
            $data = get_object_vars($hotels->result()[0]);
            $city = $search = $this->Planetresmodel->city(" idcity =" . $data['city_idcity'], " limit 10 ", " order by abbre ");
            $data['citysearch'] = get_object_vars($city->result()[0])['name'];            
            $data['act'] = '';
            $data['ina'] = '';
            (1 == $data['active']) ? $data['act'] = 'checked' : $data['ina'] = 'checked';
            $facili = $this->Facilities_model->selectfacilitieshotels(' hotels_idhotel =' . $data['idhotel'], NULL, NULL, 'facilities_idfacilities');
            foreach ($facili->result() as $facil):
                $data['facilities'][] = $facil->facilities_idfacilities;
            endforeach;
        }

        if ($data == NULL) {
            $data['idhotel'] = NULL;
            $data['name'] = NULL;
            $data['description'] = NULL;
            $data['areainformation'] = NULL;
            $data['map'] = NULL;
            $data['location'] = NULL;
            $data['ubication'] = NULL;
            $data['city_idcity'] = NULL;
            $data['citysearch'] = NULL;
            $data['active'] = NULL;
            $data['image'] = NULL;
            $data['logo'] = NULL;
            $data['alloment_single'] = NULL;
            $data['alloment_multifold'] = NULL;
            $data['alloment_double'] = NULL;
            $data['alloment_family'] = NULL;
            $data['act'] = '';
            $data['ina'] = '';
            $data['facilities'] = NULL;
            $data['category_idcategory'] = NULL;
        }
        $data['idcategory'] = NULL;
        $data['facilidades'] = NULL;
        $data['user_iduser'] = $_SESSION['iduser'];
        $category = $this->Categories_model->selectcategories();
        $data['cat'] = $data['category_idcategory'];
        foreach ($category->result() as $row) {
            $data['category_idcategory'] .= '<option value="' . $row->idcategory . '" ' . (isset($data['cat']) && $data['cat'] == $row->idcategory ? "selected" : "") . '>' . $row->name . '</option>';
        }
        $facilities = $this->Facilities_model->selectfacilities();
        $type = -1;
        $otro = 0;
        foreach ($facilities->result() as $row) {

            if ($type != $row->type) {
                if ($otro == 1) {
                    $data['facilidades'] .= '</div>';
                    $otro = 0;
                }
                $data['facilidades'] .= '<div class="row text-left text-info"><small>';
                if ($row->type == 0) {
                    $data['facilidades'] .= 'Services';
                } else {
                    $data['facilidades'] .= 'Room';
                }
                $data['facilidades'] .= '</small></div>';
                if ($otro == 0) {
                    $data['facilidades'] .= '<div class="row text-left">';
                    $otro = 1;
                }
                $data['facilidades'] .= '<div class="col-md-3">
                <input type="checkbox" value="' . $row->idfacilities . '" id="facilities*' . $row->idfacilities . '" name="facilities[]" ' . (($data['facilities'] != null) ? ((in_array($row->idfacilities, $data['facilities'])) ? 'checked' : '') : '') . ' />
            ' . $row->name . '</div>';
            } else {
                $data['facilidades'] .= '<div class="col-md-3">
                <input type="checkbox" value="' . $row->idfacilities . '" id="facilities*' . $row->idfacilities . '" name="facilities[]" ' . (($data['facilities'] != null) ? ((in_array($row->idfacilities, $data['facilities'])) ? 'checked' : '') : '') . ' />
            ' . $row->name . '</div>';
            }


            $type = $row->type;
        }

        $this->load->view('backend/hotels/newhotels', $data);
    }

    function listhotels($data = NULL) {
        $this->load->view('backend/hotels/listhotels', $data);
    }

    function ajax_listhotels() {

        $condi = "TRUE";
        $orderby = " order by idhotel desc ";
        $limit = "";
        $fields = ' *,(SELECT concat(cot.name," ",cot.abbre) as city FROM city as cot WHERE cot.idcity=city_idcity) as city ';
        $hotels = $this->Hotels_model->selecthotels($condi, $orderby, $limit, $fields);

        $data = array();
        $no = 0;
        foreach ($hotels->result() as $us) {
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $us->name;
            $row[] = $us->description;
            $row[] = $us->ubication;
            $row[] = $us->city;
            $row[] = (1 == $us->active) ? "Active" : "Inactive";


            $seemore = '<span class="cssToolTip">
                            <button class="btn btn-success btn-xs fa fa-search-plus"></button>
                            <span>Hotel: ' . $no . ''
                    . '</span>'
                    . '</span>';
            $hidden = '<input type="hidden" name="idhotel" value="' . $us->idhotel . '">';
            $edit = '<div>' . $hidden . '<button id="updatehotel" class="btn btn-warning btn-xs fa fa-edit" title="Edit Hotel" onclick="udpatehotels(' . $us->idhotel . ',\'' . base_url() . '\')"></button>';
            $color = ($us->active == 1) ? "btn-danger" : "btn-default";
            $delete = '<div>' . $hidden . '<button class="btn ' . $color . ' btn-xs fa fa-trash-o" title="Delete Hotel" onclick="deletehotels(' . $us->idhotel . ',' . $us->active . ',\'' . base_url() . '\')"></button></div>';

            $row[] = '<table style="border-collapse: separate; border-spacing:  5px;">'
                    . '<tr>'
                    . '<td>' . $seemore . '</td>'
                    . '<td>' . $edit . '</td>'
                    . '<td>' . $delete . '</td>'
                    . '</tr>'
                    . '</table>';

            $data[] = $row;
        }

//        $output = array(
//            "draw" => $_POST['draw'],
//            "recordsTotal" => $this->customers->count_all(),
//            "recordsFiltered" => $this->customers->count_filtered(),
//            "data" => $data,
//        );

        $output = array(
            "draw" => "",
            "recordsTotal" => $hotels->num_rows(),
            "recordsFiltered" => $hotels->num_rows(),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }

    function savehotels() {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('name', 'Name', 'trim|required|xss_clean|max_length[256]|min_length[2]');
        $this->form_validation->set_rules('description', 'Description', 'trim|required|xss_clean|min_length[2]');
        $this->form_validation->set_rules('ubication', 'Ubication', 'trim|required|xss_clean|max_length[45]|min_length[2]');
        $this->form_validation->set_rules('city_idcity', 'City', 'trim|required|xss_clean|max_length[10]|min_length[1]|numeric');
        $this->form_validation->set_rules('alloment_single', 'Alloment Single', 'trim|required|xss_clean|max_length[10]|min_length[1]|numeric');
        $this->form_validation->set_rules('alloment_double', 'Alloment Double', 'trim|required|xss_clean|max_length[10]|min_length[1]|numeric');
        $this->form_validation->set_rules('alloment_family', 'Alloment Family', 'trim|required|xss_clean|max_length[10]|min_length[1]|numeric');
        $this->form_validation->set_rules('alloment_multifold', 'Alloment', 'trim|required|xss_clean|max_length[10]|min_length[1]|numeric');
        $this->form_validation->set_rules('category_idcategory', 'Category', 'trim|required|xss_clean|max_length[10]|min_length[1]|numeric');
        $this->form_validation->set_rules('active', 'Active', 'trim|required|xss_clean|max_length[15]|min_length[1]');
        if (!$this->input->post('idhotel')) {
            $this->form_validation->set_rules('logo', 'Front', 'callback_file_selected_test');
        }

        $this->form_validation->set_message('required', 'The field  %s is required');
        $this->form_validation->set_message('valid_email', 'The %s is not valid');
        $this->form_validation->set_message('min_length', 'Field %s must have at least %s characters');
        $this->form_validation->set_message('max_length', 'Field %s must have less %s characters');
        $this->form_validation->set_message('numeric', 'The field  %s is numeric');

        if (!$this->form_validation->run()) {
            $return = $this->input->post();
            $return['act'] = '';
            $return['ina'] = '';
            (1 == $this->input->post('active')) ? $return['act'] = 'checked' : (0 == $this->input->post('active')) ? $return['act'] = 'checked' : "";
            $this->newhotels($return);
        } else {
            $data['mesg'] = 'Ups! An error has occurred';
            if ($this->input->post('idhotel') != '') {
                $data = $this->input->post();
                $data['act'] = '';
                $data['ina'] = '';
                (1 == $this->input->post('active')) ? $data['act'] = 'checked' : $data['ina'] = 'checked';

                if ($this->Hotels_model->updatehotels($data)) {
                    $this->session->set_flashdata('success', 'Correctly Updated hotel');
                } else {
                    $this->session->set_flashdata('error', 'Ups! An error has occurred');
                }
            } else {
                $data = $this->input->post();
                $data['act'] = '';
                $data['ina'] = '';

                (1 == $this->input->post('active')) ? $data['act'] = 'checked' : $data['ina'] = 'checked';
                $this->session->set_flashdata('success', 'Hotel Created Correctly');

                $data['idhotel'] = $this->Hotels_model->inserthotels($data);
                if (!$data['idhotel']) {
                    $this->session->set_flashdata('error', 'Ups! An error has occurred');
                }
            }
            if ($data['idhotel']) {
                $this->Hotels_model->hotelfeacilities($data['idhotel'], $data['facilities']);
                /* Logo */
                if (!empty($_FILES['logo']['name'])) {
                    $config["upload_path"] = "web/images/gallery/hotels/";
                    $config["allowed_types"] = "gif|jpg|jpeg|png";
                    $config["max_size"] = "2000";
                    $config["remove_spaces"] = TRUE;
                    $config["file_name"] = date('Ymdhis') . rand(1, 10000);
                    $this->load->library('upload', $config);
                    $this->upload->initialize($config);
                    if ($this->upload->do_upload('logo')) {
                        $fileData = $this->upload->data();
                        $this->Hotels_model->insertimages($config["file_name"], $data['idhotel'], 0);
                    } else {
                        $errors = array('error' => $this->upload->display_errors());
                        die(json_encode($errors));
                    }
                }
                /* Logo */
                /* Front */
                if (!empty($_FILES['front']['name'])) {
                    $config["upload_path"] = "web/images/gallery/hotels/";
                    $config["allowed_types"] = "gif|jpg|jpeg|png";
                    $config["max_size"] = "2000";
                    $config["remove_spaces"] = TRUE;
                    $config["file_name"] = date('Ymdhis') . rand(1, 10000);
                    $this->load->library('upload', $config);
                    $this->upload->initialize($config);
                    if ($this->upload->do_upload('front')) {
                        $fileData = $this->upload->data();
                        $this->Hotels_model->insertimages($config["file_name"], $data['idhotel'], 1);
                    } else {
                        $errors = array('error' => $this->upload->display_errors());
                        die(json_encode($errors));
                    }
                }
                /* Front */
                if (!empty($_FILES['images']['name'])) {
                    $filesCount = count($_FILES['images']['name']);
                    for ($i = 0; $i < $filesCount; $i++) {
                        if ($_FILES['images']['name'][$i] != '') {
                            $_FILES['userFile']['name'] = $_FILES['images']['name'][$i];
                            $_FILES['userFile']['type'] = $_FILES['images']['type'][$i];
                            $_FILES['userFile']['tmp_name'] = $_FILES['images']['tmp_name'][$i];
                            $_FILES['userFile']['error'] = $_FILES['images']['error'][$i];
                            $_FILES['userFile']['size'] = $_FILES['images']['size'][$i];
                            $config["upload_path"] = "web/images/gallery/hotels/";
                            $config["allowed_types"] = "gif|jpg|jpeg|png";
                            $config["max_size"] = "2000";
                            $config["remove_spaces"] = TRUE;
                            $config["file_name"] = date('Ymdhis') . rand(1, 10000);
                            $this->load->library('upload', $config);
                            $this->upload->initialize($config);
                            if ($this->upload->do_upload('userFile')) {
                                $fileData = $this->upload->data();
                                $this->Hotels_model->insertimages($fileData['file_name'], $data['idhotel'], 2);
                            } else {
                                $errors = array('error' => $this->upload->display_errors());
                                die(json_encode($errors));
                            }
                        }
                    }
                }
            }
            $this->newhotels($data);
        }
    }

    function deletehotels() {//       
        $data = $this->input->post();
        if ($data['active'] == 0) {
            $data['active'] = 1;
        } else {
            $data['active'] = 0;
        }
        if ($this->Hotels_model->deletehotels($data)) {
            $this->session->set_flashdata('success', 'Correctly Updated Hotel');
        } else {
            $this->session->set_flashdata('error', 'Ups! An error has occurred');
        }
        $this->listhotels();
    }

    function autocomplete() {
        if ($this->input->is_ajax_request() && $this->input->post('info') && strlen($this->input->post('info')) > 3) {
            $condition = " name like '" . $this->input->post('info') . "%'";
            $search = $this->Planetresmodel->city($condition, " limit 10 ", " order by abbre ");
            if ($search !== FALSE) {
                foreach ($search->result() as $row) {
                    ?>
                    <a onclick="javascript:asignar(<?= $row->idcity . ',\'' . $row->name . '\''; ?>)" ><?php echo $row->name . ' ' . $row->abbre ?></a><br>
                    <?php
                }
            } else {
                ?>

                <p><?php echo 'No results' ?></p>

                <?php
            }
        }
    }

    function file_selected_test() {

        $this->form_validation->set_message('file_selected_test', 'Please select file.');
        if (empty($_FILES['logo']['name'])) {
            return false;
        } else {
            return true;
        }
    }

}
