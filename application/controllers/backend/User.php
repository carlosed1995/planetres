<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of User
 *
 * @author carlos
 */
class User extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('Axcs_model');
        $this->load->model('Planetresmodel');
        $this->load->model('Users_model');
        $this->load->library('Session');
    }

    function newuser($data = NULL) {

        if ($this->input->post('iduser') != NULL) {
            $user = $this->Users_model->selectuser(" iduser=" . $this->input->post('iduser'));
            $data = get_object_vars($user->result()[0]);

            $city = $search = $this->Planetresmodel->city(" idcity =" . $data['city_idcity'], " limit 10 ", " order by abbre ");
            $data['citysearch'] = get_object_vars($city->result()[0])['name'];
            $data['female'] = '';
            $data['male'] = '';
            ('MALE' == strtoupper($data['genere'])) ? $data['male'] = 'checked' : $data['female'] = 'checked';
            (1 == $data['active']) ? $data['act'] = 'checked' : $data['ina'] = 'checked';
            $data['cyear'] = explode("-", $data['birth'])[0];
            $data['cmonth'] = explode("-", $data['birth'])[1];
            $data['cday'] = explode("-", $data['birth'])[2];
            $data['ccpyear'] = explode("-", $data['expedition_passport'])[0];
            $data['ccpmonth'] = explode("-", $data['expedition_passport'])[1];
            $data['ccpday'] = explode("-", $data['expedition_passport'])[2];
            $data['cepyear'] = explode("-", $data['expire_passport'])[0];
            $data['cepmonth'] = explode("-", $data['expire_passport'])[1];
            $data['cepday'] = explode("-", $data['expire_passport'])[2];
            $data['pwd'] = NULL;
        }
        $rol = $this->Axcs_model->rol(" true ");
        if ($data == NULL) {
            $data['iduser'] = NULL;
            $data['name'] = NULL;
            $data['last_name'] = NULL;
            $data['middle_name'] = NULL;
            $data['passport'] = NULL;
            $data['pwd'] = NULL;
            $data['phone'] = NULL;
            $data['genere'] = NULL;
            $data['birth'] = "0000-00-00";
            $data['cyear'] = "0000";
            $data['cmonth'] = "00";
            $data['cday'] = "00";
            $data['msg'] = NULL;
            $data['email'] = NULL;
            $data['city_idcity'] = NULL;
            $data['citysearch'] = NULL;
            $data['zip_code'] = NULL;
            $data['mesg'] = NULL;
            $data['male'] = NULL;
            $data['female'] = NULL;
            $data['nationality'] = NULL;
            $data['expedition_passport'] = "0000-00-00";
            $data['expire_passport'] = "0000-00-00";
            $data['active'] = NULL;
            $data['gps'] = NULL;
            $data['location'] = NULL;
            $data['password'] = NULL;
            $data['act'] = '';
            $data['ina'] = '';
            $data['image'] = '';
            $data['url'] = '';
            $data['link'] = '';
        }

        if (isset($data['year'])) {
            $data['cyear'] = $data['year'];
        }
        if (isset($data['month'])) {
            $data['cmonth'] = $data['month'];
        }
        if (isset($data['day'])) {
            $data['cday'] = $data['day'];
        }


        $data['select'] = NULL;
        $data['year'] = '<select name="year" class="form-control"><option value="">Year...</option>';
        foreach (range(date('Y') - 100, date('Y')) as $numero) {
            $data['year'] .= '<option value="' . $numero . '" ' . (isset($data['cyear']) && $data['cyear'] == $numero ? "selected" : "") . '>' . $numero . '</option>';
        }
        $data['year'] .= '</select>';
        $data['month'] = '<select name="month" class="form-control"><option value="">Month...</option>';
        foreach (range(1, 12) as $numero) {
            $numero = str_pad($numero, "2", "0", STR_PAD_LEFT);
            $data['month'] .= '<option value="' . $numero . '" ' . (isset($data['cmonth']) && $data['cmonth'] == $numero ? "selected" : "") . '>' . $numero . '</option>';
        }
        $data['month'] .= '</select>';
        $data['day'] = '<select name="day" class="form-control"><option value="">Day...</option>';
        foreach (range(1, 31) as $numero) {
            $numero = str_pad($numero, "2", "0", STR_PAD_LEFT);
            $data['day'] .= '<option value="' . $numero . '" ' . (isset($data['cday']) && $data['cday'] == $numero ? "selected" : "") . '>' . $numero . '</option>';
        }$data['day'] .= '</select>';

        foreach ($rol->result() as $row) {
            if (isset($data['role_idrole']) && $data['role_idrole'] == $row->idrole) {
                $data['select'] .= '<option value="' . $row->idrole . '" selected>' . $row->name . '</option>';
            } else {
                $data['select'] .= '<option value="' . $row->idrole . '">' . $row->name . '</option>';
            }
        }
        $data['cpyear'] = '<select name="cpyear" class="form-control"><option value="0000">Year...</option>';
        foreach (range(date('Y') - 100, date('Y')) as $numero) {
            $data['cpyear'] .= '<option value="' . $numero . '" ' . (isset($data['ccpyear']) && $data['ccpyear'] == $numero ? "selected" : "") . '>' . $numero . '</option>';
        }
        $data['cpyear'] .= '</select>';
        $data['cpmonth'] = '<select name="cpmonth" class="form-control"><option value="00">Month...</option>';
        foreach (range(1, 12) as $numero) {
            $numero = str_pad($numero, "2", "0", STR_PAD_LEFT);
            $data['cpmonth'] .= '<option value="' . $numero . '" ' . (isset($data['ccpmonth']) && $data['ccpmonth'] == $numero ? "selected" : "") . '>' . $numero . '</option>';
        }
        $data['cpmonth'] .= '</select>';
        $data['cpday'] = '<select name="cpday" class="form-control"><option value="00">Day...</option>';
        foreach (range(1, 31) as $numero) {
            $numero = str_pad($numero, "2", "0", STR_PAD_LEFT);
            $data['cpday'] .= '<option value="' . $numero . '" ' . (isset($data['ccpday']) && $data['ccpday'] == $numero ? "selected" : "") . '>' . $numero . '</option>';
        }$data['cpday'] .= '</select>';
        /**/
        $data['epyear'] = '<select name="epyear" class="form-control"><option value="0000">Year...</option>';
        foreach (range(date('Y') - 100, date('Y')) as $numero) {
            $data['epyear'] .= '<option value="' . $numero . '" ' . (isset($data['cepyear']) && $data['cepyear'] == $numero ? "selected" : "") . '>' . $numero . '</option>';
        }
        $data['epyear'] .= '</select>';
        $data['epmonth'] = '<select name="epmonth" class="form-control"><option value="00">Month...</option>';
        foreach (range(1, 12) as $numero) {
            $numero = str_pad($numero, "2", "0", STR_PAD_LEFT);
            $data['epmonth'] .= '<option value="' . $numero . '" ' . (isset($data['cepmonth']) && $data['cepmonth'] == $numero ? "selected" : "") . '>' . $numero . '</option>';
        }
        $data['epmonth'] .= '</select>';
        $data['epday'] = '<select name="epday" class="form-control"><option value="00">Day...</option>';
        foreach (range(1, 31) as $numero) {
            $numero = str_pad($numero, "2", "0", STR_PAD_LEFT);
            $data['epday'] .= '<option value="' . $numero . '" ' . (isset($data['cepday']) && $data['cepday'] == $numero ? "selected" : "") . '>' . $numero . '</option>';
        }$data['epday'] .= '</select>';

        $data['selectnationality'] = '<select class="form-control" id="nationality" name="nationality" placeholder="Enter nationality">
                                    <option value="">Select ... </option>';


        $nationality = $this->Planetresmodel->nationality();
        foreach ($nationality->result() as $row) {
            $data['selectnationality'] .= '<option value="' . $row->idcountry . '" ' . (isset($data['nationality']) && $data['nationality'] == $row->idcountry ? "selected" : "") . '>' . $row->abbre . '</option>';
        }
        $data['selectnationality'] .= '</select>';
        $this->load->view('backend/newuser', $data);
    }

    function listuser($data = NULL) {

        $this->load->view('backend/listusers', $data);
    }

    function ajax_listuser() {

        $condi = "TRUE";
        $orderby = " order by iduser desc ";
        $limit = "";
        $fields = ' *,(SELECT r.`name` FROM `roles` as r WHERE r.`idrole`=`role_idrole`) as `rol`,(SELECT concat(cit.name," ",cit.abbre) as city FROM city as cit WHERE cit.idcity=city_idcity) as city,(select `name` from `images` where `idimages` in(select `images_idimages` from `user_images` where  `images_idimages`=`idimages` and `user_iduser`=`user`.`iduser` ))as  `image` ';
        $user = $this->Users_model->selectuser($condi, $orderby, $limit, $fields);
        $data = array();
        $no = 0;
        foreach ($user->result() as $us) {
            $no++;
            $row = array();
            $row[] = $no;            
            if ($us->image != NULL) {
                $imagen = $us->image;
            } else {
                $imagen = 'nopicture.jpg';
            }

            $row[] = '<img src="web/images/profiles/' . $imagen . '" class="img img-responsive" style="width: 50px;height: 50px;"/>';
            $row[] = $us->name;
            $row[] = $us->last_name;
            $row[] = ucfirst($us->rol);
            $row[] = $us->email;
            $row[] = $us->city;
            $row[] = ucfirst($us->genere);
            $row[] = $us->zip_code;
            $seemore = '<span class="cssToolTip">
                            <button class="btn btn-success btn-xs fa fa-search-plus"></button>
                            <span>User: ' . $no . '<br>Passport: ' . $us->passport . '<br>Phone:' . $us->phone . '<br>Birthdate: ' . $us->birth . ''
                    . '</span>'
                    . '</span>';
            $hidden = '<input type="hidden" name="iduser" value="' . $us->iduser . '">';
            $edit = '<div>' . $hidden . '<button id="updateuser" class="btn btn-warning btn-xs fa fa-edit" title="Edit user" onclick="udpate(' . $us->iduser . ',\'' . base_url() . '\')"></button>';
            $color = ($us->active == 1) ? "btn-danger" : "btn-default";
            $delete = '<div>' . $hidden . '<button class="btn ' . $color . ' btn-xs fa fa-trash-o" title="Delete user" onclick="deleteuser(' . $us->iduser . ',' . $us->active . ',\'' . base_url() . '\')"></button></div>';

            $row[] = '<table style="border-collapse: separate; border-spacing:  5px;">'
                    . '<tr>'
                    . '<td>' . $seemore . '</td>'
                    . '<td>' . $edit . '</td>'
                    . '<td>' . $delete . '</td>'
                    . '</tr>'
                    . '</table>';

            $data[] = $row;
        }

//        $output = array(
//            "draw" => $_POST['draw'],
//            "recordsTotal" => $this->customers->count_all(),
//            "recordsFiltered" => $this->customers->count_filtered(),
//            "data" => $data,
//        );

        $output = array(
            "draw" => "",
            "recordsTotal" => $user->num_rows(),
            "recordsFiltered" => $user->num_rows(),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }

    function saveuser() {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('name', 'Name', 'trim|required|xss_clean|max_length[256]|min_length[2]');
        $this->form_validation->set_rules('middle_name', 'Lastname', 'trim|required|xss_clean|max_length[256]|min_length[2]');
        $this->form_validation->set_rules('last_name', 'Lastname', 'trim|required|xss_clean|max_length[256]|min_length[2]');
        $this->form_validation->set_rules('passport', 'Passport', 'trim|xss_clean|max_length[45]|min_length[2]');
        $this->form_validation->set_rules('role_idrole', 'Role', 'trim|required|xss_clean|max_length[5]|min_length[1]');
        $this->form_validation->set_rules('gps', 'GPS', 'trim|xss_clean|max_length[45]|min_length[1]');
        $this->form_validation->set_rules('location', 'Location', 'trim|xss_clean|max_length[45]|min_length[1]');
        $this->form_validation->set_rules('email', 'Email', 'trim|valid_email|required|xss_clean');
        $this->form_validation->set_rules('phone', 'Phone', 'trim|required|xss_clean|max_length[15]|min_length[2]');
        $this->form_validation->set_rules('password', 'Password', 'trim|required|xss_clean|max_length[10]|min_length[6]');
        $this->form_validation->set_rules('password1', 'Confirmation Password', 'trim|required|xss_clean|max_length[10]|min_length[6]');
        $this->form_validation->set_rules('city_idcity', 'City', 'trim|required|xss_clean|max_length[10]|min_length[1]');
        $this->form_validation->set_rules('genere', 'Gender', 'trim|required|xss_clean|max_length[15]|min_length[1]');
        $this->form_validation->set_rules('active', 'Active', 'trim|required|xss_clean|max_length[1]|min_length[1]|numeric');
        $this->form_validation->set_rules('nationality', 'Nationality', 'trim|xss_clean|max_length[11]|min_length[1]|numeric');
        $this->form_validation->set_rules('image', 'Image', 'trim|xss_clean|max_length[45]|min_length[2]');
        $this->form_validation->set_rules('linl', 'Link', 'trim|xss_clean|max_length[45]|min_length[2]');

        $this->form_validation->set_rules('year', 'Year', 'trim|required|xss_clean|max_length[4]|min_length[4]|numeric');
        $this->form_validation->set_rules('month', 'Month', 'trim|required|xss_clean|max_length[2]|min_length[2]|numeric');
        $this->form_validation->set_rules('day', 'Day', 'trim|required|xss_clean|max_length[2]|min_length[2]|numeric|callback_checkDay[' . $this->input->post('month') . ',' . $this->input->post('day') . ']');

        $this->form_validation->set_rules('cpyear', 'Year', 'trim|xss_clean|max_length[4]|min_length[4]|numeric');
        $this->form_validation->set_rules('cpmonth', 'Month', 'trim|xss_clean|max_length[2]|min_length[2]|numeric');
        $this->form_validation->set_rules('cpday', 'Day', 'trim|xss_clean|max_length[2]|min_length[2]|numeric|callback_checkDay[' . $this->input->post('month') . ',' . $this->input->post('day') . ']');

        $this->form_validation->set_rules('epyear', 'Year', 'trim|xss_clean|max_length[4]|min_length[4]|numeric');
        $this->form_validation->set_rules('epmonth', 'Month', 'trim|xss_clean|max_length[2]|min_length[2]|numeric');
        $this->form_validation->set_rules('epday', 'Day', 'trim|xss_clean|max_length[2]|min_length[2]|numeric|callback_checkDay[' . $this->input->post('month') . ',' . $this->input->post('day') . ']');

        $this->form_validation->set_rules('zip_code', 'Zip Code', 'trim|xss_clean|max_length[10]|min_length[1]');

        $this->form_validation->set_message('required', 'The field  %s is required');

        $this->form_validation->set_message('valid_email', 'The %s is not valid');

        $this->form_validation->set_message('min_length', 'Field %s must have at least %s characters');

        $this->form_validation->set_message('max_length', 'Field %s must have less %s characters');
        $this->form_validation->set_message('numeric', 'The field  %s is numeric');

        $this->form_validation->set_message('checkDay', '%s Does not match the date format');

        $pass = 0;
        if (!$this->checkPassword($this->input->post('pwd'), $this->input->post('pwd1'))) {
            $pass = 1;
            //comprbamos que los password coicidan
            $this->form_validation->set_message('errorpass', 'the two passwords do not match');
        }
        if (!$this->form_validation->run() || $pass == 1) {
            $return = $this->input->post();
            $return['male'] = '';
            $return['female'] = '';
            $return['cyear'] = $this->input->post('year');
            $return['cmonth'] = $this->input->post('month');
            $return['ccpyear'] = $this->input->post('cpyear');
            $return['ccpmonth'] = $this->input->post('cpmonth');
            $return['ccpday'] = $this->input->post('cpday');
            $return['cepyear'] = $this->input->post('epyear');
            $return['cepmonth'] = $this->input->post('epmonth');
            $return['cepday'] = $this->input->post('epday');
            ('male' == strtolower($this->input->post('genere'))) ? $return['male'] = 'checked' : ('female' == strtolower($this->input->post('genere'))) ? $return['female'] = 'checked' : "";
            $this->newuser($return);
        } else {
            $data['mesg'] = 'Ups! An error has occurred';
            if ($this->input->post('iduser') != '') {
                $data = $this->input->post();
                $data['male'] = '';
                $data['female'] = '';
                $return['cyear'] = $this->input->post('year');
                $return['cmonth'] = $this->input->post('month');
                $return['ccpyear'] = $this->input->post('cpyear');
                $return['ccpmonth'] = $this->input->post('cpmonth');
                $return['ccpday'] = $this->input->post('cpday');
                $return['cepyear'] = $this->input->post('epyear');
                $return['cepmonth'] = $this->input->post('epmonth');
                $return['cepday'] = $this->input->post('epday');
                ('male' == strtolower($this->input->post('genere'))) ? $data['male'] = 'checked' : ('female' == strtolower($this->input->post('genere'))) ? $data['female'] = 'checked' : "";

                if ($this->Users_model->updateuser($data)) {
                    $this->session->set_flashdata('success', 'Correctly Updated User');
                } else {
                    $this->session->set_flashdata('error', 'Ups! An error has occurred');
                }
            } else {
                $data = $this->input->post();
                $data['male'] = '';
                $data['female'] = '';
                $return['cyear'] = $this->input->post('year');
                $return['cmonth'] = $this->input->post('month');
                $return['ccpyear'] = $this->input->post('cpyear');
                $return['ccpmonth'] = $this->input->post('cpmonth');
                $return['ccpday'] = $this->input->post('cpday');
                $return['cepyear'] = $this->input->post('epyear');
                $return['cepmonth'] = $this->input->post('epmonth');
                $return['cepday'] = $this->input->post('epday');
                ('male' == strtolower($this->input->post('genere'))) ? $data['male'] = 'checked' : ('female' == strtolower($this->input->post('genere'))) ? $data['female'] = 'checked' : "";
                $this->session->set_flashdata('success', 'User Created Correctly');

                $data['iduser'] = $this->Users_model->insertuser($data);
                if (!$data['iduser']) {
                    $this->session->set_flashdata('error', 'Ups! An error has occurred');
                } else {
                    $this->session->set_flashdata('success', 'User Created Correctly');
                }
            }
            $data['type'] = 0;
            $data['idimage'] = $this->Users_model->selectimage($data['iduser'])['images_idimages'];
            $data['imagename'] = $this->Users_model->selectimage($data['iduser'])['name'];
            $data['id'] = $data['iduser'];
            if ($data['role_idrole'] != 5 && $data['role_idrole'] != 6) {
                $this->uploadfile($data);                
            } else {
                $bus = $this->Users_model->selectbussinesinformation($data['iduser']);                
                $data['update']=($bus['paypal_account']!=NULL)?'update':'insert';
                $data['paypal_account'] = $bus['paypal_account'];
                $data['bank_name'] = $bus['bank_name'];
                $data['bank_account_name'] = $bus['bank_account_name'];
                $data['bank_account_number'] = $bus['bank_account_number'];
                $data['bank_account_swift'] = $bus['bank_account_swift'];
                $data['idbussines_information'] = $bus['idbussines_information'];
                $data['active'] = $bus['active'];
                $data['ina']=($bus['active']==0)?'checked':'';
                $data['act']=($bus['active']==1)?'checked':'';
                
                $this->bussinesinformation($data);
            }
        }
    }

    function bussinesinformation($data) {        
        $this->load->view('backend/user/bussines_information', $data);
    }

    function savebussinesinformation() {
        $data = $this->input->post();
        $this->load->library('form_validation');
        $this->form_validation->set_rules('paypal_account', 'Paypal Account', 'trim|required|xss_clean|max_length[256]|min_length[2]');
        $this->form_validation->set_rules('bank_name', 'Bank Name', 'trim|required|xss_clean|max_length[256]|min_length[2]');
        $this->form_validation->set_rules('bank_account_name', 'Bank Account Name', 'trim|required|xss_clean|max_length[256]|min_length[2]');
        $this->form_validation->set_rules('bank_account_number', 'Bank Account Number', 'trim|required|xss_clean|max_length[45]|min_length[2]');
        $this->form_validation->set_rules('bank_account_swift', 'Bank Account Swift', 'trim|required|xss_clean|max_length[5]|min_length[1]');
        $this->form_validation->set_rules('active', 'Active', 'trim|required|xss_clean|max_length[1]|min_length[1],numeric');
        $this->form_validation->set_message('required', 'The field  %s is required');
        $this->form_validation->set_message('valid_email', 'The %s is not valid');
        $this->form_validation->set_message('min_length', 'Field %s must have at least %s characters');
        $this->form_validation->set_message('max_length', 'Field %s must have less %s characters');
        $this->form_validation->set_message('numeric', 'The field  %s is numeric');
        if (!$this->form_validation->run()) {
            $this->load->view('backend/user/bussines_information', $data);
        } else {
            if($this->Users_model->savebussinesinformation($data)){
                $this->session->set_flashdata('success', 'Bussines Information Correctly');
                $this->uploadfile($data);
            }else{
                $this->session->set_flashdata('error', 'Ups! An error has occurred');
            }
        }
    }

    function uploadfile($data) {
        $this->load->view('backend/genericos/imageupload', $data);
    }

    function deleteuser() {    
        $data = $this->input->post();
        if ($data['active'] == 0) {
            $data['active'] = 1;
        } else {
            $data['active'] = 0;
        }
        if ($this->Users_model->deleteuser($data)) {
            $this->session->set_flashdata('success', 'Correctly Updated User');
        } else {
            $this->session->set_flashdata('error', 'Ups! An error has occurred');
        }
        $this->listuser();
    }

    function autocomplete() {
        if ($this->input->is_ajax_request() && $this->input->post('info') && strlen($this->input->post('info')) > 3) {
            $condition = " name like '" . $this->input->post('info') . "%'";
            $search = $this->Planetresmodel->city($condition, " limit 10 ", " order by abbre ");
            if ($search !== FALSE) {
                foreach ($search->result() as $row) {
                    ?>
                    <a onclick="javascript:asignar(<?= $row->idcity . ',\'' . $row->name . '\''; ?>)" ><?php echo $row->name . ' ' . $row->abbre ?></a><br>
                    <?php
                }
            } else {
                ?>

                <p><?php echo 'No results' ?></p>

                <?php
            }
        }
    }

    function checkDateFormat($date) {
        if (preg_match("/[0-9]{4}\/[0-12]{2}\/[0-31]{2}/", $date)) {
            if (checkdate(substr($date, 3, 2), substr($date, 0, 2), substr($date, 6, 4)))
                return true;
            else
                return false;
        } else {
            return false;
        }
    }

    function checkDay($month, $day) {
        if ($month == 2) {
            if ($day <= 29) {
                return true;
            } else {
                return false;
            }
        } else if (($month - 1) % 7 % 2) {
            if ($day <= 30) {
                return true;
            } else {
                return false;
            }
        } else {
            return true;
        }
//        $month == 2 ? ($year % 4 ? 28 : ($year % 100 ? 29 : ($year % 400 ? 28 : 29))) : (($month - 1) % 7 % 2 ? 30 : 31); 
    }

    function checkPassword($pass1, $pass2) {
        if ($pass1 == $pass2) {
            return true;
        } else {
            return false;
        }
    }

}
