<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of User
 *
 * @author carlos
 */
class Travelagent extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('Axcs_model');
        $this->load->model('Planetresmodel');
        $this->load->model('Travelagent_model');
        $this->load->library('Session');
    }

    function newtravelagent($data = NULL) {

        if ($this->input->post('idtravel_agent') != NULL) {
            $user = $this->Travelagent_model->selecttravelagent(" idtravel_agent=" . $this->input->post('idtravel_agent'));
            $data = get_object_vars($user->result()[0]);
            $data['city'] = $data['id_city'];
            $city = $search = $this->Planetresmodel->city(" idcity =" . $data['city'], " limit 10 ", " order by abbre ");
            $data['citysearch'] = get_object_vars($city->result()[0])['name'];
            $data['act'] = '';
            $data['ina'] = '';
            (1 == $data['status']) ? $data['act'] = 'checked' : $data['ina'] = 'checked';            
            $data['pwd'] = NULL;
        }
        $rol = $this->Axcs_model->rol(" idrole not in('3') ");
        if ($data == NULL) {
            $data['idtravel_agent'] = NULL;
            $data['name'] = NULL;
            $data['location'] = NULL;
            $data['gps'] = NULL;
            $data['pwd'] = NULL;
            $data['phone'] = NULL;
            $data['status'] = NULL;
            $data['email'] = NULL;
            $data['city'] = NULL;
            $data['citysearch'] = NULL;
            $data['zip_code'] = NULL;
            $data['act'] = '';
            $data['ina'] = '';
            $data['arc_number'] = '';
        }
        $data['role'] = '3';

        $this->load->view('backend/travelagent/newtravelagent', $data);
    }

    function listtravelagent($data = NULL) {
        $this->load->view('backend/travelagent/listtravelagent', $data);
    }

    function ajax_listtravelagent() {

        $condi = "TRUE";
        $orderby = " order by idtravel_agent desc ";
        $limit = "";
        $fields = ' *,(SELECT r.role FROM roles as r WHERE r.idrole=idrole) as role,(SELECT concat(cit.name," ",cit.abbre) as city FROM city as cit WHERE cit.idcity=id_city) as city ';
        $user = $this->Travelagent_model->selecttravelagent($condi, $orderby, $limit, $fields);

        $data = array();
        $no = 0;
        foreach ($user->result() as $us) {
            $no++;
            $row = array();
            $row[] = $no;            
            $row[] = $us->name;
            $row[] = $us->email;
            $row[] = $us->phone;
            $row[] = $us->city;
            $row[] = $us->location;
            $row[] = $us->gps;
            $row[] = $us->zip_code;
            $row[] = $us->arc_number;
            $row[] = (1 == $us->status) ? "Active" : "Inactive";
            
            $seemore = '<span class="cssToolTip">
                            <button class="btn btn-success btn-xs fa fa-search-plus"></button>
                            <span>User: ' . $no  . ''
                    . '</span>'
                    . '</span>';
            $hidden = '<input type="hidden" name="idtravel_agent" value="' . $us->idtravel_agent . '">';
            $edit = '<div>' . $hidden . '<button id="updatetarvelagent" class="btn btn-warning btn-xs fa fa-edit" title="Edit user" onclick="udpatetravelagent(' . $us->idtravel_agent . ',\'' . base_url() . '\')"></button>';
            $color = ($us->status == 1) ? "btn-danger" : "btn-default";
            $delete = '<div>' . $hidden . '<button class="btn ' . $color . ' btn-xs fa fa-trash-o" title="Delete user" onclick="deletetravelagent(' . $us->idtravel_agent . ',' . $us->status . ',\'' . base_url() . '\')"></button></div>';

            $row[] = '<table style="border-collapse: separate; border-spacing:  5px;">'
                    . '<tr>'
                    . '<td>' . $seemore . '</td>'
                    . '<td>' . $edit . '</td>'
                    . '<td>' . $delete . '</td>'
                    . '</tr>'
                    . '</table>';

            $data[] = $row;
        }

//        $output = array(
//            "draw" => $_POST['draw'],
//            "recordsTotal" => $this->customers->count_all(),
//            "recordsFiltered" => $this->customers->count_filtered(),
//            "data" => $data,
//        );

        $output = array(
            "draw" => "",
            "recordsTotal" => $user->num_rows(),
            "recordsFiltered" => $user->num_rows(),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }

    function savetravelagent() {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('name', 'Name', 'trim|required|xss_clean|max_length[256]|min_length[2]');
        $this->form_validation->set_rules('location', 'Location', 'trim|required|xss_clean|max_length[256]|min_length[2]');
        $this->form_validation->set_rules('gps', 'Gps', 'trim|required|xss_clean|max_length[45]|min_length[2]');
        $this->form_validation->set_rules('arc_number', 'Arc Number', 'trim|required|xss_clean|max_length[45]|min_length[1]');
        $this->form_validation->set_rules('email', 'Email', 'trim|valid_email|required|xss_clean');
        $this->form_validation->set_rules('phone', 'Phone', 'trim|required|xss_clean|max_length[15]|min_length[2]');
        $this->form_validation->set_rules('pwd', 'Password', 'trim|required|xss_clean|max_length[10]|min_length[6]');
        $this->form_validation->set_rules('pwd1', 'Confirmation Password', 'trim|required|xss_clean|max_length[10]|min_length[6]');
        $this->form_validation->set_rules('city', 'City', 'trim|required|xss_clean|max_length[10]|min_length[1]');
        $this->form_validation->set_rules('status', 'Status', 'trim|required|xss_clean|max_length[15]|min_length[1]');
        $this->form_validation->set_rules('zip_code', 'Zip Code', 'trim|required|xss_clean|max_length[10]|min_length[1]');

        $this->form_validation->set_message('required', 'The field  %s is required');
        $this->form_validation->set_message('valid_email', 'The %s is not valid');
        $this->form_validation->set_message('min_length', 'Field %s must have at least %s characters');
        $this->form_validation->set_message('max_length', 'Field %s must have less %s characters');
        $this->form_validation->set_message('numeric', 'The field  %s is numeric');



        $pass = 0;
        if (!$this->checkPassword($this->input->post('pwd'), $this->input->post('pwd1'))) {
            $pass = 1;
            //comprbamos que los password coicidan
            $this->form_validation->set_message('errorpass', 'the two passwords do not match');
        }
        if (!$this->form_validation->run() || $pass == 1) {
            $return = $this->input->post();
            $return['ina'] = '';
            $return['sta'] = '';

            (1 == $this->input->post('status')) ? $return['act'] = 'checked' : $return['ina'] = 'checked';
            $this->newtravelagent($return);
        } else {
            $data['mesg'] = 'Ups! An error has occurred';
            if ($this->input->post('idtravel_agent') != '') {
                $data = $this->input->post();
                $data['act'] = '';
                $data['ina'] = '';
                (1 == $this->input->post('status')) ? $data['act'] = 'checked' : $data['ina'] = 'checked';

                if ($this->Travelagent_model->updatetravelagent($data)) {
                    $this->session->set_flashdata('success', 'Correctly Updated Travel Agent');
                } else {
                    $this->session->set_flashdata('error', 'Ups! An error has occurred');
                }
            } else {
                $data = $this->input->post();
                $data['act'] = '';
                $data['ina'] = '';

                (1 == $this->input->post('status')) ? $data['act'] = 'checked' : $data['ina'] = 'checked';
                $this->session->set_flashdata('success', 'Travel Agent Created Correctly');

                $data['idtravel_agent'] = $this->Travelagent_model->inserttravelagent($data);
                if (!$data['idtravel_agent']) {
                    $this->session->set_flashdata('error', 'Ups! An error has occurred');
                }
            }
            $this->newtravelagent($data);
        }
    }

    function deletetravelagent() {//       
        $data = $this->input->post();
        if ($data['status'] == 0) {
            $data['status'] = 1;
        } else {
            $data['status'] = 0;
        }
        if ($this->Travelagent_model->deletetravelagent($data)) {
            $this->session->set_flashdata('success', 'Correctly Updated Travel Agent');
        } else {
            $this->session->set_flashdata('error', 'Ups! An error has occurred');
        }
        $this->listtravelagent();
    }

    function autocomplete() {
        if ($this->input->is_ajax_request() && $this->input->post('info') && strlen($this->input->post('info')) > 3) {
            $condition = " name like '" . $this->input->post('info') . "%'";
            $search = $this->Planetresmodel->city($condition, " limit 10 ", " order by abbre ");
            if ($search !== FALSE) {
                foreach ($search->result() as $row) {
                    ?>
                    <a onclick="javascript:asignar(<?= $row->idcity . ',\'' . $row->name . '\''; ?>)" ><?php echo $row->name . ' ' . $row->abbre ?></a><br>
                    <?php
                }
            } else {
                ?>

                <p><?php echo 'No results' ?></p>

                <?php
            }
        }
    }

    function checkDateFormat($date) {
        if (preg_match("/[0-9]{4}\/[0-12]{2}\/[0-31]{2}/", $date)) {
            if (checkdate(substr($date, 3, 2), substr($date, 0, 2), substr($date, 6, 4)))
                return true;
            else
                return false;
        } else {
            return false;
        }
    }

    function checkDay($month, $day) {
        if ($month == 2) {
            if ($day <= 29) {
                return true;
            } else {
                return false;
            }
        } else if (($month - 1) % 7 % 2) {
            if ($day <= 30) {
                return true;
            } else {
                return false;
            }
        } else {
            return true;
        }
//        $month == 2 ? ($year % 4 ? 28 : ($year % 100 ? 29 : ($year % 400 ? 28 : 29))) : (($month - 1) % 7 % 2 ? 30 : 31); 
    }

    function checkPassword($pass1, $pass2) {
        if ($pass1 == $pass2) {
            return true;
        } else {
            return false;
        }
    }

    function searchcountry() {
        if ($this->input->is_ajax_request() && $this->input->post('idcity')) {
            $condition = " idcountry in(select id_country from city where idcity=" . $this->input->post('idcity') . ")";
            $search = $this->Planetresmodel->country($condition, " limit 10 ", " order by abbre ");
            if ($search !== FALSE) {
                foreach ($search->result() as $row) {
                    //echo $row->idcountry . '|' . $row->name;
                    echo $row->name;
                }
            } else {
                ?>
                <p><?php echo 'No results' ?></p>
                <?php
            }
        }
    }

}
