<?php

defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Passenger
 *
 * @author carlos
 */
class Typetransfer extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('Axcs_model');
        $this->load->model('Planetresmodel');
        $this->load->model('Typetransfer_model');
        $this->load->library('Session');
    }

    function newtypetransfer($data = NULL) {


        if ($this->input->post('idtype_transfers') != NULL) {
            $typetransfer = $this->Typetransfer_model->selecttypetransfer(" idtype_transfers=" . $this->input->post('idtype_transfers'));
            $data = get_object_vars($typetransfer->result()[0]);            
            $data['ina'] = '';
            $data['act'] = '';
            (1 == $data['active']) ? $data['act'] = 'checked' : $data['ina'] = 'checked';
        }
        if ($data == NULL) {
            $data['idtype_transfers'] = NULL;
            $data['name'] = NULL;
            $data['description'] = NULL;            
            $data['active'] = NULL;
            $data['ina'] = NULL;
            $data['act'] = NULL;            
            
        }

        $this->load->view('backend/typetransfer/newtypetransfer', $data);
    }

    function savetypetransfer() {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('name', 'Name', 'trim|required|xss_clean|max_length[256]|min_length[2]');
        $this->form_validation->set_rules('description', 'Description', 'trim|required|xss_clean|min_length[10]');
        $this->form_validation->set_rules('active', 'Active', 'trim|required|xss_clean|max_length[15]|min_length[1]');


        $this->form_validation->set_message('required', 'The field  %s is required');
        $this->form_validation->set_message('valid_email', 'The %s is not valid');
        $this->form_validation->set_message('min_length', 'Field %s must have at least %s characters');
        $this->form_validation->set_message('max_length', 'Field %s must have less %s characters');
        $this->form_validation->set_message('numeric', 'The field  %s is numeric');

        if (!$this->form_validation->run()) {
            $return = $this->input->post();
            $return['act'] = '';
            $return['ina'] = '';

            (1 == $this->input->post('active')) ? $return['act'] = 'checked' : $return['ina'] = 'checked';

            $this->newtypetransfer($return);
        } else {
            $data['mesg'] = 'Ups! An error has occurred';
            if ($this->input->post('idtype_transfers') != '') {
                $data = $this->input->post();
                $data['act'] = '';
                $data['ina'] = '';
                            (1 == $this->input->post('active')) ? $return['act'] = 'checked' : (0 == $this->input->post('active')) ? $return['ina'] = 'checked':$return['ina'] = '';

                if ($this->Typetransfer_model->updatetypetransfer($data)) {
                    $this->session->set_flashdata('success', 'Correctly Updated Type Transfer');
                } else {
                    $this->session->set_flashdata('error', 'Ups! An error has occurred');
                }
            } else {
                $data = $this->input->post();
                $data['act'] = '';
                $data['ina'] = '';
                (1 == $this->input->post('active')) ? $data['act'] = 'checked' : $data['ina'] = 'checked';
                $this->session->set_flashdata('success', 'Type Transfer Created Correctly');

                $data['idtype_transfers'] = $this->Typetransfer_model->inserttypetransfer($data);
                if (!$data['idtype_transfers']) {
                    $this->session->set_flashdata('error', 'Ups! An error has occurred');
                }
            }
            $this->newtypetransfer($data);
        }
    }

    function listtypetransfer($data = NULL) {
        $this->load->view('backend/typetransfer/listtypetransfer', $data);
    }

    function ajax_listtypetransfer() {


        $condi = "TRUE";
        $orderby = " order by idtype_transfers desc ";
        $limit = "";
        $fields = '*';
        $typetransfer = $this->Typetransfer_model->selecttypetransfer($condi, $orderby, $limit, $fields);

        $data = array();
        $no = 0;
        foreach ($typetransfer->result() as $ps) {
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $ps->name;
            $row[] = $ps->description;                        
            $row[] = (1==$ps->active)? "Active":"Inactive";            

            //$row[] = ;

            $seemore = '<span class="cssToolTip">
                            <button class="btn btn-success btn-xs fa fa-search-plus"></button>
                            <span>Type Transfer: ' . $no . '<br>'
                    . '</span>'
                    . '</span>';

            $hidden = '<input type="hidden" name="idtype_transfers" value="' . $ps->idtype_transfers . '">';
            $edit = '<div>' . $hidden . '<button id="updatetypetransfer" class="btn btn-warning btn-xs fa fa-edit" title="Edit typetransfer" onclick="udpatetypetransfer(' . $ps->idtype_transfers . ',\'' . base_url() . '\')"></button>';
            $color = ($ps->active == 1) ? "btn-danger" : "btn-default";
            $delete = '<div>' . $hidden . '<button class="btn ' . $color . ' btn-xs fa fa-trash-o" title="Delete typetransfer" onclick="deletetypetransfer(' . $ps->idtype_transfers . ',' . $ps->active . ',\'' . base_url() . '\')"></button></div>';

            $row[] = '<table style="border-collapse: separate; border-spacing:  5px;">'
                    . '<tr>'
                    . '<td>' . $seemore . '</td>'
                    . '<td>' . $edit . '</td>'
                    . '<td>' . $delete . '</td>'
                    . '</tr>'
                    . '</table>';

            $data[] = $row;
        }

//        $output = array(
//            "draw" => $_POST['draw'],
//            "recordsTotal" => $this->customers->count_all(),
//            "recordsFiltered" => $this->customers->count_filtered(),
//            "data" => $data,
//        );

        $output = array(
            "draw" => "",
            "recordsTotal" => $typetransfer->num_rows(),
            "recordsFiltered" => $typetransfer->num_rows(),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }

    function deletetypetransfer() {//       
        $data = $this->input->post();
        if ($data['active'] == 0) {
            $data['active'] = 1;
        } else {
            $data['active'] = 0;
        }
        if ($this->Typetransfer_model->deletetypetransfer($data)) {
            $this->session->set_flashdata('success', 'Correctly Updated Type Transfers');
        } else {
            $this->session->set_flashdata('error', 'Ups! An error has occurred');
        }
        $this->listtypetransfer();
    }

}
