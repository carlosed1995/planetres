<?php

defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Passenger
 *
 * @author carlos
 */
class Categories extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('Axcs_model');
        $this->load->model('Planetresmodel');
        $this->load->model('Categories_model');
        $this->load->library('Session');
    }

    function newcategories($data = NULL) {


        if ($this->input->post('idcategory') != NULL) {
            $categories = $this->Categories_model->selectcategories(" idcategory=" . $this->input->post('idcategory'));
            $data = get_object_vars($categories->result()[0]);            
            $data['ina'] = '';
            $data['act'] = '';
            (1 == $data['active']) ? $data['act'] = 'checked' : $data['ina'] = 'checked';
        }
        if ($data == NULL) {
            $data['idcategory'] = NULL;
            $data['name'] = NULL;
            $data['description'] = NULL;            
            $data['active'] = NULL;
            $data['ina'] = NULL;
            $data['act'] = "0000-00-00";            
            
        }

        $this->load->view('backend/categories/newcategories', $data);
    }

    function savecategories() {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('name', 'Name', 'trim|required|xss_clean|max_length[256]|min_length[2]');
        $this->form_validation->set_rules('description', 'Description', 'trim|required|xss_clean|min_length[10]');
        $this->form_validation->set_rules('active', 'Active', 'trim|required|xss_clean|max_length[15]|min_length[1]');


        $this->form_validation->set_message('required', 'The field  %s is required');
        $this->form_validation->set_message('valid_email', 'The %s is not valid');
        $this->form_validation->set_message('min_length', 'Field %s must have at least %s characters');
        $this->form_validation->set_message('max_length', 'Field %s must have less %s characters');
        $this->form_validation->set_message('numeric', 'The field  %s is numeric');

        if (!$this->form_validation->run()) {
            $return = $this->input->post();
            $return['act'] = '';
            $return['ina'] = '';

            (1 == $this->input->post('active')) ? $return['act'] = 'checked' : $return['ina'] = 'checked';

            $this->newcategories($return);
        } else {
            $data['mesg'] = 'Ups! An error has occurred';
            if ($this->input->post('idcategory') != '') {
                $data = $this->input->post();
                $data['act'] = '';
                $data['ina'] = '';
                            (1 == $this->input->post('active')) ? $return['act'] = 'checked' : (0 == $this->input->post('active')) ? $return['ina'] = 'checked':$return['ina'] = '';

                if ($this->Categories_model->updatecategories($data)) {
                    $this->session->set_flashdata('success', 'Correctly Updated Categories');
                } else {
                    $this->session->set_flashdata('error', 'Ups! An error has occurred');
                }
            } else {
                $data = $this->input->post();
                $data['act'] = '';
                $data['ina'] = '';
                (1 == $this->input->post('active')) ? $data['act'] = 'checked' : $data['ina'] = 'checked';
                $this->session->set_flashdata('success', 'Categories Created Correctly');

                $data['idcategory'] = $this->Categories_model->insertcategories($data);
                if (!$data['idcategory']) {
                    $this->session->set_flashdata('error', 'Ups! An error has occurred');
                }
            }
            $this->newcategories($data);
        }
    }

    function listcategories($data = NULL) {
        $this->load->view('backend/categories/listcategories', $data);
    }

    function ajax_listcategories() {


        $condi = "TRUE";
        $orderby = " order by idcategory desc ";
        $limit = "";
        $fields = '*';
        $categories = $this->Categories_model->selectcategories($condi, $orderby, $limit, $fields);

        $data = array();
        $no = 0;
        foreach ($categories->result() as $ps) {
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $ps->name;
            $row[] = $ps->description;                        
            $row[] = (1==$ps->active)? "Active":"Inactive";            

            //$row[] = ;

            $seemore = '<span class="cssToolTip">
                            <button class="btn btn-success btn-xs fa fa-search-plus"></button>
                            <span>Category: ' . $no . '<br>'
                    . '</span>'
                    . '</span>';

            $hidden = '<input type="hidden" name="idcategory" value="' . $ps->idcategory . '">';
            $edit = '<div>' . $hidden . '<button id="updatecategories" class="btn btn-warning btn-xs fa fa-edit" title="Edit categories" onclick="udpatecategories(' . $ps->idcategory . ',\'' . base_url() . '\')"></button>';
            $color = ($ps->active == 1) ? "btn-danger" : "btn-default";
            $delete = '<div>' . $hidden . '<button class="btn ' . $color . ' btn-xs fa fa-trash-o" title="Delete categories" onclick="deletecategories(' . $ps->idcategory . ',' . $ps->active . ',\'' . base_url() . '\')"></button></div>';

            $row[] = '<table style="border-collapse: separate; border-spacing:  5px;">'
                    . '<tr>'
                    . '<td>' . $seemore . '</td>'
                    . '<td>' . $edit . '</td>'
                    . '<td>' . $delete . '</td>'
                    . '</tr>'
                    . '</table>';

            $data[] = $row;
        }

//        $output = array(
//            "draw" => $_POST['draw'],
//            "recordsTotal" => $this->customers->count_all(),
//            "recordsFiltered" => $this->customers->count_filtered(),
//            "data" => $data,
//        );

        $output = array(
            "draw" => "",
            "recordsTotal" => $categories->num_rows(),
            "recordsFiltered" => $categories->num_rows(),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }

    function deletecategories() {//       
        $data = $this->input->post();
        if ($data['active'] == 0) {
            $data['active'] = 1;
        } else {
            $data['active'] = 0;
        }
        if ($this->Categories_model->deletecategories($data)) {
            $this->session->set_flashdata('success', 'Correctly Updated Categories');
        } else {
            $this->session->set_flashdata('error', 'Ups! An error has occurred');
        }
        $this->listcategories();
    }

}
