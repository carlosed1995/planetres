<?php

defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Passenger
 *
 * @author carlos
 */
class Typeroom extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('Axcs_model');
        $this->load->model('Planetresmodel');
        $this->load->model('Typeroom_model');
        $this->load->library('Session');
    }

    function newtyperoom($data = NULL) {


        if ($this->input->post('idtype_room') != NULL) {
            $typeroom = $this->Typeroom_model->selecttyperoom(" idtype_room=" . $this->input->post('idtype_room'));
            $data = get_object_vars($typeroom->result()[0]);
            $data['ina'] = '';
            $data['act'] = '';
            (1 == $data['active']) ? $data['act'] = 'checked' : $data['ina'] = 'checked';
        }
        if ($data == NULL) {
            $data['idtype_room'] = NULL;
            $data['name'] = NULL;
            $data['description'] = NULL;
            $data['active'] = NULL;
            $data['ina'] = NULL;
            $data['act'] = NULL;
            $data['num_adults'] = NULL;
            $data['num_kids'] = NULL;
        }

        $this->load->view('backend/typeroom/newtyperoom', $data);
    }

    function savetyperoom() {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('name', 'Name', 'trim|required|xss_clean|max_length[256]|min_length[2]');
        $this->form_validation->set_rules('description', 'Description', 'trim|required|xss_clean|min_length[10]');
        $this->form_validation->set_rules('num_kids', 'Kids', 'trim|required|xss_clean|min_length[1]|numeric');
        $this->form_validation->set_rules('num_adults', 'Adults', 'trim|required|xss_clean|min_length[1]|numeric');
        $this->form_validation->set_rules('active', 'Active', 'trim|required|xss_clean|max_length[15]|min_length[1]');


        $this->form_validation->set_message('required', 'The field  %s is required');
        $this->form_validation->set_message('valid_email', 'The %s is not valid');
        $this->form_validation->set_message('min_length', 'Field %s must have at least %s characters');
        $this->form_validation->set_message('max_length', 'Field %s must have less %s characters');
        $this->form_validation->set_message('numeric', 'The field  %s is numeric');

        if (!$this->form_validation->run()) {
            $return = $this->input->post();
            $return['act'] = '';
            $return['ina'] = '';
            $data['num_adults'] = NULL;
            $data['num_kids'] = NULL;

            (1 == $this->input->post('active')) ? $return['act'] = 'checked' : $return['ina'] = 'checked';

            $this->newtyperoom($return);
        } else {
            $data['mesg'] = 'Ups! An error has occurred';
            if ($this->input->post('idtype_room') != '') {
                $data = $this->input->post();
                $data['act'] = '';
                $data['ina'] = '';
                
                (1 == $this->input->post('active')) ? $return['act'] = 'checked' : (0 == $this->input->post('active')) ? $return['ina'] = 'checked' : $return['ina'] = '';

                if ($this->Typeroom_model->updatetyperoom($data)) {
                    $this->session->set_flashdata('success', 'Correctly Updated Type room');
                } else {
                    $this->session->set_flashdata('error', 'Ups! An error has occurred');
                }
            } else {
                $data = $this->input->post();
                $data['act'] = '';
                $data['ina'] = '';
                
                (1 == $this->input->post('active')) ? $data['act'] = 'checked' : $data['ina'] = 'checked';
                $this->session->set_flashdata('success', 'Type Room Created Correctly');

                $data['idtype_room'] = $this->Typeroom_model->inserttyperoom($data);
                if (!$data['idtype_room']) {
                    $this->session->set_flashdata('error', 'Ups! An error has occurred');
                }
            }
            $this->newtyperoom($data);
        }
    }

    function listtyperoom($data = NULL) {
        $this->load->view('backend/typeroom/listtyperoom', $data);
    }

    function ajax_listtyperoom() {


        $condi = "TRUE";
        $orderby = " order by idtype_room desc ";
        $limit = "";
        $fields = '*';
        $typeroom = $this->Typeroom_model->selecttyperoom($condi, $orderby, $limit, $fields);

        $data = array();
        $no = 0;
        foreach ($typeroom->result() as $ps) {
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $ps->name;
            $row[] = $ps->description;
            $row[] = $ps->num_adults;
            $row[] = $ps->num_kids;
            $row[] = (1 == $ps->active) ? "Active" : "Inactive";

            //$row[] = ;

            $seemore = '<span class="cssToolTip">
                            <button class="btn btn-success btn-xs fa fa-search-plus"></button>
                            <span>Type Room: ' . $no . '<br>'
                    . '</span>'
                    . '</span>';

            $hidden = '<input type="hidden" name="idtype_room" value="' . $ps->idtype_room . '">';
            $edit = '<div>' . $hidden . '<button id="updatetyperoom" class="btn btn-warning btn-xs fa fa-edit" title="Edit typeroom" onclick="udpatetyperoom(' . $ps->idtype_room . ',\'' . base_url() . '\')"></button>';
            $color = ($ps->active == 1) ? "btn-danger" : "btn-default";
            $delete = '<div>' . $hidden . '<button class="btn ' . $color . ' btn-xs fa fa-trash-o" title="Delete typeroom" onclick="deletetyperoom(' . $ps->idtype_room . ',' . $ps->active . ',\'' . base_url() . '\')"></button></div>';

            $row[] = '<table style="border-collapse: separate; border-spacing:  5px;">'
                    . '<tr>'
                    . '<td>' . $seemore . '</td>'
                    . '<td>' . $edit . '</td>'
                    . '<td>' . $delete . '</td>'
                    . '</tr>'
                    . '</table>';

            $data[] = $row;
        }

//        $output = array(
//            "draw" => $_POST['draw'],
//            "recordsTotal" => $this->customers->count_all(),
//            "recordsFiltered" => $this->customers->count_filtered(),
//            "data" => $data,
//        );

        $output = array(
            "draw" => "",
            "recordsTotal" => $typeroom->num_rows(),
            "recordsFiltered" => $typeroom->num_rows(),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }

    function deletetyperoom() {//       
        $data = $this->input->post();
        if ($data['active'] == 0) {
            $data['active'] = 1;
        } else {
            $data['active'] = 0;
        }
        if ($this->Typeroom_model->deletetyperoom($data)) {
            $this->session->set_flashdata('success', 'Correctly Updated Type Room');
        } else {
            $this->session->set_flashdata('error', 'Ups! An error has occurred');
        }
        $this->listtyperoom();
    }

}
