<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Travel_agency_functions extends CI_Controller {

function __construct(){
        parent::__construct();
        $this->load->library('Session');
    $this->load->helper('form');
    $this->load->library('form_validation');
        if(!$this->session->userdata("login")){
            redirect(base_url('Planetres'));
        }
    }


      function index(){
      $this->load->view('profile/travelagent/headertravelagent');
      $this->load->view('profile/travelagent/trabelcreate');
      $this->load->view('profile/travelagent/footer');
    }

        


  function profile_travel_agent_create(){
    $this->load->view('profile/travelagent/headertravelagent');
    $this->load->view('profile/travelagent/trabelcreate');
    $this->load->view('profile/travelagent/footer');
  }


  public function profile_travel_agent_list(){


      $data = array(
        'list'=>$this->Travelagent_model->list_agent_trvl()
        );

       $this->load->view('profile/travelagent/headertravelagent');
       $this->load->view('profile/travelagent/trabel_agent_index', $data);
       $this->load->view('profile/travelagent/footer');

    
            
}

public function profile_travel_agent_edit($id){


    $idupdate = $this->Travelagent_model->id_update_travel_agent($id);

foreach ($idupdate->result() as $row) {
    $Name = $row->Name;
    $Web_Site = $row->Web_Site;
    $email = $row->email;
    $phone = $row->phone;
    $Agency_phone = $row->Agency_phone;
    $Slogan_travel = $row->Slogan_travel;
    $Logo = $row->Logo;
}

    $data =  array(
        'id'=> $id,
        'Name' => $Name ,
        'Web_Site' => $Web_Site,
        'email' =>  $email,
        'phone' => $phone ,
        'Agency_phone' => $Agency_phone,
        'Slogan_travel' =>   $Slogan_travel,
        'Logo' => $Logo);


     $this->load->view('profile/travelagent/headertravelagent');
     $this->load->view('profile/travelagent/trabeledit', $data);
     $this->load->view('profile/travelagent/footer');
}


public function update_agent_trvel($id){

  

     $this->form_validation->set_rules('name', 'Name','trim|required|xss_clean|alpha');
  $this->form_validation->set_rules('website', 'Website','trim|required|xss_clean|alpha');
  $this->form_validation->set_rules('email', 'Email','valid_email|required|is_unique[user.email]');
  $this->form_validation->set_rules('phone', 'Phone','required|integer');
  $this->form_validation->set_rules('agencyphon', 'Agency phone','integer|required');
  $this->form_validation->set_rules('slogan', 'Slogan','trim|required|xss_clean|alpha');

$this->form_validation->set_error_delimiters('<div class="text-danger">','</div>');



 if ($this->form_validation->run() == FALSE)
                {

     $idupdate = $this->Travelagent_model->id_update_travel_agent($id);

foreach ($idupdate->result() as $row) {
    $Name = $row->Name;
    $Web_Site = $row->Web_Site;
    $email = $row->email;
    $phone = $row->phone;
    $Agency_phone = $row->Agency_phone;
    $Slogan_travel = $row->Slogan_travel;
    $Logo = $row->Logo;
}

    $data =  array(
        'id'=> $id,
        'Name' => $Name ,
        'Web_Site' => $Web_Site,
        'email' =>  $email,
        'phone' => $phone ,
        'Agency_phone' => $Agency_phone,
        'Slogan_travel' =>   $Slogan_travel,
        'Logo' => $Logo);


     $this->load->view('profile/travelagent/headertravelagent');
     $this->load->view('profile/travelagent/trabeledit', $data);
     $this->load->view('profile/travelagent/footer');
  }else{
$data =  array(
   'Name' => $this->input->post('name'),
   'Web_Site' => $this->input->post('website'),
   'email' => $this->input->post('email'),
   'phone' => $this->input->post('phone'),
   'Agency_phone' => $this->input->post('agencyphon'),
   'Slogan_travel' => $this->input->post('slogan')
    //'Logo' => $this->input->post('logo')

    );
  $this->Travelagent_model->update_travel_agent($id, $data);



                  $this->session->set_flashdata('agent', '
Record edited correctly');
                    redirect(base_url('travelagency/Travel_agency_functions/profile_travel_agent_list'));

}
}

public function insert_agent_travel(){

  $this->form_validation->set_rules('name', 'Name','trim|required|xss_clean|alpha');
  $this->form_validation->set_rules('website', 'Website','trim|required|xss_clean|alpha');
  $this->form_validation->set_rules('email', 'Email','valid_email|required|is_unique[user.email]');
  $this->form_validation->set_rules('phone', 'Phone','required|integer');
  $this->form_validation->set_rules('agencyphon', 'Agency phone','required|integer');
  $this->form_validation->set_rules('slogan', 'Slogan','trim|required|xss_clean|alpha');
  $this->form_validation->set_rules('portrait', 'Image','trim|required|xss_clean|alpha');

$this->form_validation->set_error_delimiters('<div class="text-danger">','</div>');

 if ($this->form_validation->run() == FALSE)
                {
    $this->load->view('profile/travelagent/headertravelagent');
    $this->load->view('profile/travelagent/trabelcreate');
    $this->load->view('profile/travelagent/footer');
                }else{
                
                     
                

  $nameimg = $_FILES['portrait']['name'];
    
    $config = array();
        $config["upload_path"] = "web/images/profiles/";
        $config["allowed_types"] = "jpg|jpeg|png";
        $config["max_size"] = "2000";
        $config["remove_spaces"] = TRUE;
        $config["file_name"] = $nameimg;
        $this->load->library('upload', $config);
        if (!$this->upload->do_upload('portrait')){
            $errors = array('error' => $this->upload->display_errors());
            die(json_encode($errors));            
        }else{
            $data = array('upload_data' => $this->upload->data());
         
}


$param['Name'] = $this->input->post('name');
    $param['Web_Site'] = $this->input->post('website');
    $param['email'] = $this->input->post('email');
    $param['phone'] = $this->input->post('phone');
    $param['Agency_phone'] = $this->input->post('agencyphon');
    $param['Slogan_travel'] = $this->input->post('slogan');
    $param['Logo'] =  $config["file_name"];
     $insert = $this->Travelagent_model->insert_agent($param);


  
               

                  $this->session->set_flashdata('agent', '
Successful registration');
                    redirect(base_url('travelagency/Travel_agency_functions/profile_travel_agent_list'));
                

}

}



}
	














  


    


     




