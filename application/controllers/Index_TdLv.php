<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Index_TdLv extends CI_Controller {

	function __construct(){
		parent::__construct();
	}

	function subtractdays_global($date1,$date2) {
        $datetime1 = new DateTime($date1);
        $datetime2 = new DateTime($date2);
        $interval = $datetime1->diff($datetime2);
        return $interval->format('%r%a');
    }

	public function busqueda_de_hoteles(){	
		$_SESSION['nights'] = $this->subtractdays_global($this->input->post('start_hotel'),$this->input->post('end_hotel'));

		$data = array();	 		
		
		$countroom = $this->Tdlvmodel->count_room("type_room_idtype_room = ".$this->input->post('guests_hotel'));

		if($countroom){
			foreach ($countroom->result() as $key) {           
					$count_room = $key->count_room; 
			}    

			$capture_room = $this->Tdlvmodel->capture_room(trim($this->input->post('guests_hotel')), $this->input->post('room'), $count_room, $this->input->post('adult'), $this->input->post('kids'));

		}

		if($capture_room){
			foreach ($capture_room->result() as $key) {			
				$guests_hotel = $key->idtype_room;	
			}
			$result= $this->Tdlvmodel->busqueda_hoteles(trim($this->input->post('city_hotel')), trim($this->input->post('country_hotel')), $guests_hotel);
		}else{			
			$result = FALSE;
		}					
		
		if($result){
					
			foreach ($result->result() as $key) {
				$count = $key->category_idcategory;	

				$count--;

				for($i=0;$i <= $count ;$i++){
					$star_category[$i] = "<img src='".base_url()."web/images/icon-star.png' width='12' height='12'>";
				}
			}
			
			foreach ($result->result() as $row) {	
				$data = "<div class='r-row rounded'>
		        				<a href='#'><img src='".base_url()."web/images/thumbnail.jpg' class='thumb rounded'></a>
		         				<div class='description'>
		           					<h3>".$row->name."</h3>
		         					<p>".$row->description."</p>
		          					<p>".implode($star_category)."</p>
		         				</div>
		          				<div class='price rounded'>
						        	<strong>$????????</strong>
						          	<p>Per person</p>
						          	<a href='".base_url()."Planetres/reservation_hotel?id=".$row->idhotel."&category=".$row->category_idcategory."&guests_hotel=".$this->input->post('guests_hotel')."&start_hotel=".$this->input->post('start_hotel')."&end_hotel=".$this->input->post('end_hotel')."&room=".$this->input->post('room')."&adult=".$this->input->post('adult')."&kids=".$this->input->post('kids')."&adult1=".$this->input->post('adult1')."&adult2=".$this->input->post('adult2')."&adult3=".$this->input->post('adult3')."&adult4=".$this->input->post('adult4')."&kids1=".$this->input->post('kids1')."&kids2=".$this->input->post('kids2')."&kids3=".$this->input->post('kids3')."&kids4=".$this->input->post('kids4')."' class='rounded'>Reserve</a>
						        </div>
						    </div>";	
				echo $data;	
			}
		}else{
			echo "Was not found";
		}	

		
	}

	public function busqueda_de_tours(){
		$data = array();						
		$result= $this->Tdlvmodel->busqueda_tours(trim($this->input->post('destinations')));
		if($result){
			foreach ($result->result() as $row) {						
									
				$data = "<div class='r-row rounded'>
								<a href='#'><img src='".base_url()."web/images/thumbnail.jpg' class='thumb rounded'></a>
								<div class='description'>
									<h3>".$row->name_tours."</h3>
									<p>Space available: ".$row->max_persons."</p>
									<p><img src='".base_url()."web/images/icon-star.png' width='12' height='12'>
									<img src='".base_url()."web/images/icon-star.png' width='12' height='12'>
									<img src='".base_url()."web/images/icon-star.png' width='12' height='12'></p>
								</div>
								<div class='price rounded'>
									<strong>$255.31</strong>
									<p>Per person</p>
									<a href='".base_url()."Planetres/reservation_tour?id=".$row->idtours."&type_tours=".$row->idtours_type."&destinations=".$this->input->post('destinations')."&guests_tours=".$this->input->post('guests_tours')."&room_tours=".$this->input->post('room_tours')."&adult_tours=".$this->input->post('adult_tours')."&kids_tours=".$this->input->post('kids_tours')."&adult_tours1=".$this->input->post('adult_tours1')."&adult_tours2=".$this->input->post('adult_tours2')."&adult_tours3=".$this->input->post('adult_tours3')."&kids_tours1=".$this->input->post('kids_tours1')."&kids_tours2=".$this->input->post('kids_tours2')."&kids_tours3=".$this->input->post('kids_tours3')."' class='rounded'>Reserve</a>
								</div>
							</div>";	
				echo $data;	
			}
		}else{
			echo "Was not found";
		}		
	}

	public function busqueda_de_excursions(){
			
		$data = array();		

  		$result= $this->Tdlvmodel->busqueda_excursions(trim($this->input->post('city_excursions')), trim($this->input->post('country_excursions')),trim($this->input->post('start_excursions')),trim($this->input->post('end_excursions')),trim($this->input->post('guests_excursions')),trim($this->input->post('guests_kids')));

		if($result){
			foreach ($result->result() as $row) {						
				$max_persons=$row->guests_adults+$row->guests_kids;	
				$data = "<div class='r-row rounded'>
								<a href='#'><img src='".base_url()."web/images/thumbnail.jpg' class='thumb rounded'></a>
								<div class='description'>
									<h3>".$row->name."</h3>
									<p>Space available: ".$max_persons."</p>
									<p><img src='".base_url()."web/images/icon-star.png' width='12' height='12'>
									<img src='".base_url()."web/images/icon-star.png' width='12' height='12'>
									<img src='".base_url()."web/images/icon-star.png' width='12' height='12'></p>
								</div>
								<div class='price rounded'>
									<strong></strong>
									<p>Per person</p>
									<a href='".base_url()."Planetres/reservation_excursion?id=".$row->idexcursions."&start_excursions=".$this->input->post('start_excursions')."&end_excursions=".$this->input->post('end_excursions')."&city_excursions=".$this->input->post('city_excursions')."&guests_excursions=".$this->input->post('guests_excursions')."&guests_kids=".$this->input->post('guests_kids')."&guests_total=".$this->input->post('guests_total')."' class='rounded'>Reserve</a>
								</div>
							</div>";	
				echo $data;	
			}
		}else{
			echo "was not found";
		}	
	}

	public function busqueda_de_transfer(){	

		$data = array();			
		
	$result=$this->Tdlvmodel->busqueda_transfer( trim($this->input->post('citytransf')), trim($this->input->post('country_transfer')), trim($this->input->post('guests_transfer')));


		if($result){
			foreach ($result->result() as $row) {						
									
				$data = "<div class='r-row rounded'>
								<a href='#'><img src='".base_url()."web/images/thumbnail.jpg' class='thumb rounded'></a>
								<div class='description'>
									<h3>".$row->name."</h3>
									<p>".$row->description."</p>
									<p></p>
								</div>
								<div class='price rounded'>
									<strong>??????$</strong>
									<p>Per person</p>
									<a href='".base_url()."Planetres/reservation_transfers?id=".$row->idtransfers."&start_transfer=".$this->input->post('start_transfer')."&end_transfer=".$this->input->post('end_transfer')."&citytransf=".$this->input->post('citytransf')."&guests_transfer=".$this->input->post('guests_transfer')."&count_transport=".$this->input->post('count_transport')."&person=".$this->input->post('person')."' class='rounded'>Reserve</a>
								</div>
							</div>";	
				echo $data;	
			}
		}else{
			echo "Was not found";
		}		
	}
}