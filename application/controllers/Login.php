<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

    function __construct(){
        parent::__construct();
    }

    public function index(){
        
        $this->load->view('frontend/login');
        
    }

    function validate($email, $password) {
        if ($email == "" && $password == "") {
            return false;
        } else {
            return true;
        }
    }
    
public function signin() {

    	$email = $this->input->post('email');
    	$password = $this->input->post('password');
        $validate = $this->validate($email, $password);
        if($validate != false){
        	$response = $this->Axcs_model->login_user($email,$password);   
                    
        	if($response){
        		$data = [

        			"iduser" => $response->iduser,
        			"name" => $response->name,
        			"id_role" => $response->role_idrole,
        			"login" => TRUE
        		];

        		$this->session->set_userdata($data);

        	}else{
        		echo "error";
        	}
        }else{
            echo "Empty fields";
        }
    }

    function destroySession() {
        session_destroy();
        header('location:' . base_url('Planetres'));
    }

}