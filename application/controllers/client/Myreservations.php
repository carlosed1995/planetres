
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Myreservations extends CI_Controller {

    function __construct(){
        parent::__construct();
        if(!$this->session->userdata("login")){
            redirect(base_url('Planetres'));
        }
    }

    function index(){
        $this->load->view('profile/client/headerclient');
        $this->load->view('profile/client/my_reservations');
        $this->load->view('profile/client/footerclient');
    }

function ajax_list_myreservations() {

        $fields = "reservations.* FROM `reservations` inner join user as us on us.iduser=reservations.user_iduser where us.iduser = ".$this->session->userdata('iduser');
        $reservations = $this->Reservations_model->selectreservations_client($fields);

        $data = array();
        $no = 0;
        foreach ($reservations->result() as $us) {
            if($us->active == 0){

                $row = array();
                $row[] = 'No results';
                $row[] = 'No results';
                $row[] = 'No results';
                $row[] = 'No results';
                $row[] = 'No results';

                
                $hidden = '<input type="hidden" name="idreservations" value="' . $us->idreservations . '">';
                
                $color = ($us->active == 1) ? "btn-danger" : "btn-default";

                $delete = '<div>' . $hidden . '<button class="btn ' . $color . ' btn-xs fa fa-trash-o" title="Delete Reservations" onclick="deletereservations(' . $us->idreservations . ',' . $us->active . ',\'' . base_url() . '\')"><i class="glyphicon glyphicon-trash"></i></button></div>';

                $row[] = 'No results';

                $data[] = $row;
                
            }else{
                $no++;
                $row = array();
                $row[] = $no;
                $row[] = explode(" ",$us->date)[0];
                $row[] = $us->cost;
                $row[] = $us->pay;
                $row[] = (1 == $us->active) ? "Active" : "Inactive";

                
                $hidden = '<input type="hidden" name="idreservations" value="' . $us->idreservations . '">';
                
                $color = ($us->active == 1) ? "btn-danger" : "btn-default";
                $delete = '<div>' . $hidden . '<button class="btn ' . $color . ' btn-xs fa fa-trash-o" title="Delete Reservations" onclick="deletereservations(' . $us->idreservations . ',' . $us->active . ',\'' . base_url() . '\')"><i class="glyphicon glyphicon-trash"></i></button></div>';

                $row[] = '<table style="border-collapse: separate; border-spacing:  5px;">'
                        . '<tr>'
                        . '<td>' . $delete . '</td>'
                        . '</tr>'
                        . '</table>';

                $data[] = $row;
            }
            
        }

        $output = array(
            "draw" => "",
            "recordsTotal" => $reservations->num_rows(),
            "recordsFiltered" => $reservations->num_rows(),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }

    function deletereservations() {//       
        $data = $this->input->post();
        if ($data['active'] == 0) {
            $data['active'] = 1;
        } else {
            $data['active'] = 0;
        }
        if ($this->Reservations_model->deletereservations($data)) {
            $this->session->set_flashdata('success', 'Correctly Updated Reservations');
        } else {
            $this->session->set_flashdata('error', 'Ups! An error has occurred');
        }
        $this->index();
    }


 

}