<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profile extends CI_Controller {

	function __construct(){
        parent::__construct();
        if(!$this->session->userdata("login")){
            redirect(base_url('Planetres'));
        }
    }

    function index(){
        $data = array();
        $iduser = $this->session->userdata("iduser");
        

        if($this->session->userdata('id_role') == 7){ //client
            
            $data['user_information'] = $this->Users_model->selectuser("iduser = ".$iduser);
            $this->load->view('profile/client/headerclient');
            $this->load->view('profile/client/profile_personal_client',$data);
            $this->load->view('profile/client/footerclient');

        }elseif($this->session->userdata('id_role') == 6){ //travel_agent


             $data['user_information'] = $this->Users_model->selectuser("iduser = ".$iduser);
        $this->load->view('profile/travelagent/headertravelagent');
        $this->load->view('profile/travelagent/profile_travel_agent',$data);
        $this->load->view('profile/travelagent/footer');

            

        }elseif($this->session->userdata('id_role') == 5){ //supplier
            
            $this->load->view('profile/supplier/header');
            $this->load->view('profile/supplier/index_supplier');
            $this->load->view('profile/supplier/footer');

        }
        elseif($this->session->userdata('id_role') == 4){ //support_client

                  

        }elseif($this->session->userdata('id_role') == 3){ //support_travel_agent
    
     $data['user_information'] = $this->Users_model->selectuser("iduser = ".$iduser);
        $this->load->view('profile/support_travel_agent/headersupportravelagn');
        $this->load->view('profile/support_travel_agent/profile_support',$data);
        $this->load->view('profile/travelagent/footer');

            







        }elseif($this->session->userdata('id_role') == 2){ //support_supplier

           
            
        }elseif($this->session->userdata('id_role') == 1){ //admin
           

        }else{ //usuario index
           
        }   
        
    }

    function city(){

                $city=$this->Planetresmodel->city("country_idcountry=".$_POST['nationality']);

                foreach ($city->result() as $row) { 

                echo "<option value='".$row->idcity."'>".$row->name."</>";
            
            }
        }

    function edit_profile_personal_client(){
        
       
            $data['data'] = $this->Users_model->selectuser("iduser = ".$this->session->userdata('iduser'));;
            $data['name'] = NULL;
            $data['middle_name'] = NULL;
            $data['last_name'] = NULL;
            $data['passport'] = NULL;
            $data['phone'] = NULL;
            $data['genere'] = NULL;
            $data['birth'] = "0000-00-00";
            $data['cyear'] = "0000";
            $data['cmonth'] = "00";
            $data['cday'] = "00";
            $data['expedition_passport'] = "0000-00-00";
            $data['expedition_year'] = "0000";
            $data['expedition_month'] = "00";
            $data['expedition_day'] = "00";
            $data['expire_passport'] = "0000-00-00";
            $data['expire_year'] = "0000";
            $data['expire_month'] = "00";
            $data['expire_day'] = "00";
            $data['nationality'] = NULL;
            $data['city'] = NULL;
            $data['citysearch'] = NULL;
            $data['url'] = NULL;
            $data['zip_code'] = NULL;
            $data['male'] = NULL;
            $data['female'] = NULL;
 
        if (isset($data['year'])) {
            $data['cyear'] = $data['year'];
        }
        if (isset($data['month'])) {
            $data['cmonth'] = $data['month'];
        }
        if (isset($data['day'])) {
            $data['cday'] = $data['day'];
        }

        if (isset($data['year_expedition'])) {
            $data['expedition_year'] = $data['year_expedition'];
        }
        if (isset($data['month_expedition'])) {
            $data['expedition_month'] = $data['month_expedition'];
        }
        if (isset($data['day_expedition'])) {
            $data['expedition_day'] = $data['day_expedition'];
        }

        if (isset($data['year_expire'])) {
            $data['expire_year'] = $data['year_expire'];
        }
        if (isset($data['month_expire'])) {
            $data['expire_month'] = $data['month_expire'];
        }
        if (isset($data['day_expire'])) {
            $data['expire_day'] = $data['day_expire'];
        }

        $data['select'] = NULL;
        $data['year'] = '<select name="year" class="form-control"><option value="">Year...</option>';
        foreach (range(date('Y') - 100, date('Y')) as $numero) {
            $data['year'] .= '<option value="' . $numero . '" ' . (isset($data['cyear']) && $data['cyear'] == $numero ? "selected" : "") . '>' . $numero . '</option>';
        }
        $data['year'] .= '</select>';
        $data['month'] = '<select name="month" class="form-control"><option value="">Month...</option>';
        foreach (range(1, 12) as $numero) {
            $numero = str_pad($numero, "2", "0", STR_PAD_LEFT);
            $data['month'] .= '<option value="' . $numero . '" ' . (isset($data['cmonth']) && $data['cmonth'] == $numero ? "selected" : "") . '>' . $numero . '</option>';
        }
        $data['month'] .= '</select>';
        $data['day'] = '<select name="day" class="form-control"><option value="">Day...</option>';
        foreach (range(1, 31) as $numero) {
            $numero = str_pad($numero, "2", "0", STR_PAD_LEFT);
            $data['day'] .= '<option value="' . $numero . '" ' . (isset($data['cday']) && $data['cday'] == $numero ? "selected" : "") . '>' . $numero . '</option>';
        }$data['day'] .= '</select>';
        /* ----------------------------------------  */
        $data['select_expedition'] = NULL;
        $data['year_expedition'] = '<select name="year_expedition" class="form-control"><option value="">Year...</option>';
        foreach (range(date('Y') - 100, date('Y')) as $numero) {
            $data['year_expedition'] .= '<option value="' . $numero . '" ' . (isset($data['expedition_year']) && $data['expedition_year'] == $numero ? "selected" : "") . '>' . $numero . '</option>';
        }
        $data['year_expedition'] .= '</select>';
        $data['month_expedition'] = '<select name="month_expedition" class="form-control"><option value="">Month...</option>';
        foreach (range(1, 12) as $numero) {
            $numero = str_pad($numero, "2", "0", STR_PAD_LEFT);
            $data['month_expedition'] .= '<option value="' . $numero . '" ' . (isset($data['expedition_month']) && $data['expedition_month'] == $numero ? "selected" : "") . '>' . $numero . '</option>';
        }
        $data['month_expedition'] .= '</select>';
        $data['day_expedition'] = '<select name="day_expedition" class="form-control"><option value="">Day...</option>';
        foreach (range(1, 31) as $numero) {
            $numero = str_pad($numero, "2", "0", STR_PAD_LEFT);
            $data['day_expedition'] .= '<option value="' . $numero . '" ' . (isset($data['expedition_day']) && $data['expedition_day'] == $numero ? "selected" : "") . '>' . $numero . '</option>';
        }$data['day_expedition'] .= '</select>';
        /* ----------------------------------------*/
        $data['select_expire'] = NULL;
        $data['year_expire'] = '<select name="year_expire" class="form-control"><option value="">Year...</option>';
        foreach (range(date('Y') - 100, date('Y')) as $numero) {
            $data['year_expire'] .= '<option value="' . $numero . '" ' . (isset($data['expire_year']) && $data['expire_year'] == $numero ? "selected" : "") . '>' . $numero . '</option>';
        }
        $data['year_expire'] .= '</select>';
        $data['month_expire'] = '<select name="month_expire" class="form-control"><option value="">Month...</option>';
        foreach (range(1, 12) as $numero) {
            $numero = str_pad($numero, "2", "0", STR_PAD_LEFT);
            $data['month_expire'] .= '<option value="' . $numero . '" ' . (isset($data['expire_month']) && $data['expire_month'] == $numero ? "selected" : "") . '>' . $numero . '</option>';
        }
        $data['month_expire'] .= '</select>';
        $data['day_expire'] = '<select name="day_expire" class="form-control"><option value="">Day...</option>';
        foreach (range(1, 31) as $numero) {
            $numero = str_pad($numero, "2", "0", STR_PAD_LEFT);
            $data['day_expire'] .= '<option value="' . $numero . '" ' . (isset($data['expire_day']) && $data['expire_day'] == $numero ? "selected" : "") . '>' . $numero . '</option>';
        }$data['day_expire'] .= '</select>';

        $data['selectnationality'] = '<select class="form-control" id="nationality" name="nationality" placeholder="Enter nationality">
                                    <option value="">Select ... </option>';


        $nationality = $this->Planetresmodel->nationality();
        foreach ($nationality->result() as $row) {
            $data['selectnationality'] .= '<option value="' . $row->idcountry . '" ' . (isset($data['nationality']) && $data['nationality'] == $row->idcountry ? "selected" : "") . '>' .$row->name .", ". $row->abbre . '</option>';
        }
        $data['selectnationality'] .= '</select>';

    	$this->load->view('profile/client/headerclient');
    	$this->load->view('profile/client/edit_profile_personal_client',$data);
    	$this->load->view('profile/client/footerclient');
        //$this->load->view('profile/client/Scriptfunctionmenuclient');
    }

    function save_changes_personal_client(){
        /*echo "<pre>";
        print_r($this->input->post());
        echo "</pre>";
        die();*/
        /*$this->load->library('form_validation');
        $this->form_validation->set_rules('name', 'Name', 'trim|required|xss_clean|max_length[256]|min_length[2]');
        $this->form_validation->set_rules('lastname', 'Lastname', 'trim|required|xss_clean|max_length[256]|min_length[2]');
        $this->form_validation->set_rules('passport', 'Passport', 'trim|required|xss_clean|max_length[45]|min_length[2]');
        $this->form_validation->set_rules('role', 'Role', 'trim|required|xss_clean|max_length[5]|min_length[1]');
        $this->form_validation->set_rules('phone', 'Phone', 'trim|required|xss_clean|max_length[15]|min_length[2]');
        $this->form_validation->set_rules('city', 'City', 'trim|required|xss_clean|max_length[10]|min_length[1]');
        $this->form_validation->set_rules('genere', 'Gender', 'trim|required|xss_clean|max_length[15]|min_length[1]');

        $this->form_validation->set_rules('year', 'Year', 'trim|required|xss_clean|max_length[4]|min_length[4]|numeric');
        $this->form_validation->set_rules('month', 'Month', 'trim|required|xss_clean|max_length[2]|min_length[2]|numeric');
        $this->form_validation->set_rules('day', 'Day', 'trim|required|xss_clean|max_length[2]|min_length[2]|numeric|callback_checkDay[' . $this->input->post('month') . ',' . $this->input->post('day') . ']');
        
        $this->form_validation->set_message('required', 'The field  %s is required');
        
        $this->form_validation->set_message('min_length', 'Field %s must have at least %s characters');
        
        $this->form_validation->set_message('max_length', 'Field %s must have less %s characters');
        $this->form_validation->set_message('numeric', 'The field  %s is numeric');

        $this->form_validation->set_message('checkDay', '%s Does not match the date format');*/

        $data = $this->input->post();

        if ($this->Users_model->update_data_client($data)) {
            $this->session->set_flashdata('success', 'Correctly Updated User');
        } else {
            $this->session->set_flashdata('error', 'Ups! An error has occurred');
        }
            $this->index();
    }

    

   /*

    function insert_supplier(){


            $datos['first_name']=$_POST['first_name'];
            $datos['middle_name']=$_POST['middle_name'];
            $datos['last_name']=$_POST['last_name'];
            $datos['address']=$_POST['address'];
            $datos['city']=$_POST['city'];
            $datos['state']=$_POST['state'];
            $datos['country']=$_POST['country'];
            $datos['zip']=$_POST['zip'];
            $datos['email']=$_POST['email'];
            $datos['phone']=$_POST['phone'];
            $datos['skype']=$_POST['skype'];
            $datos['whatsapp']=$_POST['whatsapp'];
            $datos['business_name']=$_POST['business_name'];
            $datos['registration_name']=$_POST['registration_name'];
            $datos['website']=$_POST['website'];
            $datos['business_address']=$_POST['business_address'];
            $datos['business_city']=$_POST['business_city'];
            $datos['business_state']=$_POST['business_state'];
            $datos['business_country']=$_POST['business_country'];
            $datos['business_zip']=$_POST['business_zip'];
            $datos['business_email']=$_POST['business_email'];
            $datos['business_phone']=$_POST['business_phone'];
            $datos['business_skype']=$_POST['business_skype'];
            $datos['business_whatsapp']=$_POST['business_whatsapp'];
            $datos['tours']=$_POST['tours'];
            $datos['excursions']=$_POST['excursions'];
            $datos['transfer']=$_POST['transfer'];
            $datos['hotel']=$_POST['hotel'];
            $datos['paypal']=$_POST['paypal'];
            $datos['bank_name']=$_POST['bank_name'];
            $datos['bank_acc_name']=$_POST['bank_acc_name'];
            $datos['acc_num']=$_POST['acc_num'];
            $datos['swift_num']=$_POST['swift_num'];
        

            $this->Planetresmodel->insert_supplier_person($datos);
            $this->Planetresmodel->insert_supplier_business($datos);

            echo "<div class=\"info\">Registro exitoso</div>";
            redirect(("Planetres/profile_supplier"), "refresh");

            
        }

        function insert_hotel(){

            $datos['name']=$_POST['name'];
            $datos['address']=$_POST['address'];
            $datos['description']=$_POST['description'];
            $datos['information']=$_POST['information'];
            $datos['map']=$_POST['map'];
            $datos['location']=$_POST['location'];
            $datos['city']=$_POST['city'];
            $datos['category']=$_POST['category'];
            $datos['status']=$_POST['status'];

                $this->Planetresmodel->insert_hotel($datos);

            echo "<div class=\"info\">Registro exitoso</div>";
            redirect(("Planetres/profile_supplier"), "refresh");

        }

        function city_hotel(){

                $city=$this->Planetresmodel->city_hotel($_POST['country']);

                foreach ($city->result() as $row) { 

                echo "<option value='".$row->idcity."'>".$row->name."</>";
            
            }
        }

        function insert_tours(){

            $datos['name']=$_POST['name'];
            $datos['code']=$_POST['code'];
            $datos['location']=$_POST['location'];
            $datos['highlights']=$_POST['highlights'];
            $datos['max_persons']=$_POST['max_persons'];
            $datos['type']=$_POST['type'];
            $datos['status']=$_POST['status'];

            $lastId=$this->Planetresmodel->insert_tours($datos);

            echo "<div class=\"info\">Registro exitoso</div>";
            redirect(("Planetres/profile_supplier"), "refresh");


        }


    public function profile_supplier_busin()
    {
        $this->load->view('profile/supplier/header');
        $this->load->view('profile/supplier/index_busin');
        $this->load->view('profile/supplier/footer');

    }

    public function profile_supplier_hotel_create()
    {


            $data['category'] = $this->Planetresmodel->category_hotel();
            $data['country'] = $this->Planetresmodel->country_hotel();
            //$data['city'] = $this->Planetresmodel->city_hotel($_POST['country']);

            $this->load->view('profile/supplier/header');
            $this->load->view('profile/supplier/hotelproduct',$data);
            $this->load->view('profile/supplier/footer');

            

    }

    public function profile_supplier_excursion_index()
    {
        $this->load->view('profile/supplier/header');
                $this->load->view('profile/supplier/excursions_index');
                $this->load->view('profile/supplier/footer');


    }

    public function profile_supplier_excursion_update()
    {
        $this->load->view('profile/supplier/header');
                $this->load->view('profile/supplier/excursions_update');
                $this->load->view('profile/supplier/footer');


    }

    public function profile_supplier_excursion_create()
    {
        $this->load->view('profile/supplier/header');
            $this->load->view('profile/supplier/excursions_create');
            $this->load->view('profile/supplier/footer');

    }


    public function profile_supplier_tour_update()
    {
        $this->load->view('profile/supplier/header');
            $this->load->view('profile/supplier/tours_edit');
            $this->load->view('profile/supplier/footer');

    }

    public function profile_supplier_tour_index()
    {
        $this->load->view('profile/supplier/header');
            $this->load->view('profile/supplier/tours_index');
            $this->load->view('profile/supplier/footer');

    }

        public function profile_supplier_tour_creat()
    {

            $data['tours_type'] = $this->Planetresmodel->type_tours();
            $this->load->view('profile/supplier/header');
            $this->load->view('profile/supplier/tours_create',$data);
            $this->load->view('profile/supplier/footer');

    }

        public function profile_supplier_create()
    {
            $this->load->view('profile/supplier/header');
            $this->load->view('profile/supplier/create');
            $this->load->view('profile/supplier/footer');

    }

        public function profile_supplier_update_personal()
    {
            $this->load->view('profile/supplier/header');
            $this->load->view('profile/supplier/personaledit');
            $this->load->view('profile/supplier/footer');

    }

    public function profile_supplier_update_business()
    {
            $this->load->view('profile/supplier/header');
            $this->load->view('profile/supplier/businessedit');
            $this->load->view('profile/supplier/footer');

    }

  






   public function profile_travel_agent_seller()
    {
    $this->load->view('profile/travelagent/header');
    $this->load->view('profile/travelagent/trabel_agent');
    $this->load->view('profile/travelagent/footer');

    }

    


public function profile_travel_agent_clients()
    {
  $this->load->view('profile/travelagent/header');
  $this->load->view('profile/travelagent/trabelclients');
  $this->load->view('profile/travelagent/footer');

    }
  
public function profile_travel_agent_config()
    {
  $this->load->view('profile/travelagent/header');
  $this->load->view('profile/travelagent/config');
  $this->load->view('profile/travelagent/footer');

    }*/



}