<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Config extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model('Users_model');
		$this->load->library('Session');
		$this->load->helper('form');
		$this->load->library('form_validation');
		if(!$this->session->userdata("login")){
            redirect(base_url('Planetres'));
        }
        $data=array();
	}

	function index(){

		if($this->session->userdata('id_role') == 7){ // client
			
			$data['data']= $this->Users_model->selectuser("iduser = ".$this->session->userdata('iduser'));
			$data['new_email'] = NULL;
	        $data['old_password'] = NULL;
	        $data['confirm_email'] = NULL;
	        $data['new_password'] = NULL;
	        $data['confirm_password'] = NULL;
			
            $this->load->view('profile/client/headerclient');
			$this->load->view('profile/client/config_client',$data);
			$this->load->view('profile/client/footerclient');
			
        }elseif($this->session->userdata('id_role') == 6){ //travel_agent
        	
        $data['data']= $this->Users_model->selectuser("iduser = ".$this->session->userdata('iduser'));
            $data['new_email'] = NULL;
            $data['old_password'] = NULL;
            $data['confirm_email'] = NULL;
            $data['new_password'] = NULL;
            $data['confirm_password'] = NULL;
            
            $this->load->view('profile/travelagent/headertravelagent');
            $this->load->view('profile/travelagent/config',$data);
            $this->load->view('profile/travelagent/footer');
            
        }elseif($this->session->userdata('id_role') == 5){ //supplier
        	
        }
        elseif($this->session->userdata('id_role') == 4){
        	
        	$data['data']= $this->Users_model->selectuser("iduser = ".$this->session->userdata('iduser'));	
	        $data['new_email'] = NULL;
	        $data['old_password'] = NULL;
	        $data['confirm_email'] = NULL;
	        $data['new_password'] = NULL;
	        $data['confirm_password'] = NULL;

	        $this->load->view('profile/support_client/headersupportclient');
			$this->load->view('profile/support_client/config_supportclient',$data);
			$this->load->view('profile/support_client/footersupportclient');

        }elseif($this->session->userdata('id_role') == 3){ 

$data['data']= $this->Users_model->selectuser("iduser = ".$this->session->userdata('iduser'));
            $data['new_email'] = NULL;
            $data['old_password'] = NULL;
            $data['confirm_email'] = NULL;
            $data['new_password'] = NULL;
            $data['confirm_password'] = NULL;
            
            $this->load->view('profile/support_travel_agent/headersupportravelagn');
            $this->load->view('profile/support_travel_agent/config_travel',$data);
            $this->load->view('profile/travelagent/footer');

        }elseif($this->session->userdata('id_role') == 2){ //support_supplier
        	
        }elseif($this->session->userdata('id_role') == 1){ //admin
        	
        }else{ //usuario index

        }
	}

	public function check_password_ajax() {
        $old_password = $this->input->post('old_password');
        $check_old_password = $this->Users_model->selectuser("password = ".$old_password. " and iduser = ".$this->session->userdata('iduser'));
        if ($check_old_password) {
            $this->form_validation->set_message('check_password_ajax', '%s: already exist in the db');
        } else {
            echo '<div style="display:none">1</div>';
        }
    }
 
	//validamos el email con ajax
    public function check_email_ajax() {
        $new_email = $this->input->post('new_email');
        if($new_email){
        	$check_new_email = $this->Users_model->selectuser("email = '".$new_email."'");
	        if ($check_new_email) {
	            $this->form_validation->set_message('check_email_ajax', '%s: already exist in the db');
	        } else {
	            echo '<div style="display:none">1</div>';
	        }
	    }
    }

	 function save_config_general() {
	 	
	 	/*echo "<pre>";
	 	print_r($this->input->post());
	 	echo "</pre>";
	 	die();*/
	 	if($this->input->post('submit1')){
	 		
	 		/*$this->load->library('form_validation');
	        $this->form_validation->set_rules('new_email', 'New Email', 'min_length[6]|max_length[100]|trim|valid_email|callback_check_email_ajax|required|xss_clean');

	        $this->form_validation->set_rules('confirm_email', 'Confirm Email', 'min_length[6]|max_length[100]|trim|valid_email|required|xss_clean');
	        
	        $this->form_validation->set_message('min_length', 'Field %s must have at least %s characters');
       		$this->form_validation->set_message('max_length', 'Field %s must have less %s characters');
	        $this->form_validation->set_message('valid_email', 'The %s is not valid');
	        $this->form_validation->set_message('required', 'The field  %s is required');*/
	        
	        $chkemail=0;
	        if(!$this->checkEmail($this->input->post('new_email'), $this->input->post('confirm_email'))){
        		/*$this->form_validation->set_message('erroremail', 'the two email do not match');*/
        		$chkemail=1;
        	}
        	if ($chkemail==1) {
                $this->index();
            } else {
                $data = $this->input->post();
                $update = $this->Users_model->update_email_config_general_client($data);
                if ($update) {
                    $this->session->set_flashdata('email_update', 'the email is change');
                    redirect(base_url('travelagency_support/Config'));
                }else{
                	$this->session->set_flashdata('error', 'Ups! An error has occurred');
                	redirect(base_url('travelagency_support/Config'));
                }
            }
	 	}

		if($this->input->post('submit2')){
	 		/*$this->form_validation->set_rules('new_password', 'New Password', 'trim|required|xss_clean|max_length[10]|min_length[6]');
        	$this->form_validation->set_rules('confirm_password', 'Confirmation Password', 'trim|required|xss_clean|max_length[10]|min_length[6]');
        	$this->form_validation->set_message('required', 'The field  %s is required');
        	$this->form_validation->set_message('min_length', 'Field %s must have at least %s characters');
        	$this->form_validation->set_message('max_length', 'Field %s must have less %s characters');*/
        	$pass=0;
        	if (!$this->checkPassword($this->input->post('new_password'), $this->input->post('confirm_password'))) {
	            //comprbamos que los password coicidan
	            /*
	            $this->form_validation->set_message('errorpass', 'the two passwords do not match');*/
	            $pass=1;
        	}
        	
        	if ($pass==1) {
                $this->index();
            } else {
                $data = $this->input->post();
                $update = $this->Users_model->update_password_config_general_client($data);
                if ($update) {
                    $this->session->set_flashdata('success', 'the password is change');
                    redirect(base_url('travelagency_support/Config'));
                }else{
                	$this->session->set_flashdata('error', 'Ups! An error has occurred');
                	redirect(base_url('travelagency_support/Config'));
                }
            }

    	}
	}

    function checkPassword($pass1, $pass2) {
		if ($pass1 == $pass2) {
	        return true;
	    } else {
	        return false;
	    }
	}

    function checkEmail($email1, $email2) {
	        if ($email1 == $email2) {
	            return true;
	        } else {
	            return false;
	        }
    	
    }
}