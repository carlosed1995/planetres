<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Travel_agency_support_functions extends CI_Controller {

	function __construct(){
        parent::__construct();
        if(!$this->session->userdata("login")){
            redirect(base_url('Planetres'));
        }
    }


    function index(){
      $this->load->view('profile/support_travel_agent/headersupportravelagn');
      $this->load->view('profile/support_travel_agent/profile_support');
      $this->load->view('profile/support_travel_agent/footer');
    }

    public function profile_support_create(){
	    $this->load->view('profile/support_travel_agent/headersupportravelagn');
	    $this->load->view('profile/support_travel_agent/create_travel_agent');
	    $this->load->view('profile/travelagent/footer');
	}

    function reservations_travel_support_agency(){
        $this->load->view('profile/support_travel_agent/headersupportravelagn');
        $this->load->view('profile/support_travel_agent/my_reservations_agency');
        $this->load->view('profile/travelagent/footer');
    }

    public function profile_travel_agent_listsup(){
     
      $data = array(
        'list'=>$this->Travelagent_model->list_agent_supp()
        );

       $this->load->view('profile/support_travel_agent/headersupportravelagn');
       $this->load->view('profile/support_travel_agent/profile_support', $data);
       $this->load->view('profile/travelagent/footer');
        
}



public function off_travel_reservation($id){
      $data =  array(
   'active' => 0
   
    );
  $this->Travelagent_model->on_off_travelsupp($id, $data);


  redirect('travelagency_support/Travel_agency_support_functions/reservations_travel_support_agency');
}

public function on_travel_reservation($id){
  $data =  array(
   'active' => 1
   
    );
  $this->Travelagent_model->on_off_travelsupp($id, $data);


  redirect('travelagency_support/Travel_agency_support_functions/reservations_travel_support_agency');
}




    function ajax_list_travel_support() {

        $fields = "reservations.* FROM `reservations` inner join user as us on us.iduser=reservations.user_iduser where us.iduser = "."'3'";
        $reservations = $this->Reservations_model->selectreservations_client_travel($fields);

        $data = array();
        $no = 0;
        foreach ($reservations->result() as $us) {
            if($us->active == 0){

                $no++;
                $row = array();
                $row[] = $no;
                $row[] = explode(" ",$us->date)[0];
                $row[] = $us->cost;
                $row[] = $us->pay;
                $row[] = (1 == $us->active) ? "<a class='btn btn-success' href='".base_url().'travelagency_support/Travel_agency_support_functions/off_travel_reservation/'.$us->idreservations."'>".'to decline'."</a>" : "<a class='btn btn-default' href='".base_url().'travelagency_support/Travel_agency_support_functions/on_travel_reservation/'.$us->idreservations."'>".'to accept'."</a>";

                
                $hidden = '<input type="hidden" name="idreservations" value="' . $us->idreservations . '">';
                
                $color = ($us->active == 1) ? "btn-danger" : "btn-default";
                $delete = '<div>' . $hidden /*. '<button class="btn ' . $color . ' btn-xs fa fa-trash-o" title="Delete Reservations" onclick="deletereservations(' . $us->idreservations . ',' . $us->active . ',\'' . base_url() . '\')"><i class="glyphicon glyphicon-trash"></i></button></div>'*/;

                $row[] = '<table style="border-collapse: separate; border-spacing:  5px;">'
                        . '<tr>'
                        . '<td>' . $delete . '</td>'
                        . '</tr>'
                        . '</table>';

                $data[] = $row;
                
            }else{
                $no++;
                $row = array();
                $row[] = $no;
                $row[] = explode(" ",$us->date)[0];
                $row[] = $us->cost;
                $row[] = $us->pay;
                $row[] = (1 == $us->active) ? "<a class='btn btn-success' href='".base_url().'travelagency_support/Travel_agency_support_functions/off_travel_reservation/'.$us->idreservations."'>".'to decline'."</a>" : "<a class='btn btn-default' href='".base_url().'travelagency_support/Travel_agency_support_functions/on_travel_reservation/'.$us->idreservations."'>".'to accept'."</a>";

                
                $hidden = '<input type="hidden" name="idreservations" value="' . $us->idreservations . '">';
                
                $color = ($us->active == 1) ? "btn-danger" : "btn-default";
                $delete = '<div>' . $hidden /*. '<button class="btn ' . $color . ' btn-xs fa fa-trash-o" title="Delete Reservations" onclick="deletereservations(' . $us->idreservations . ',' . $us->active . ',\'' . base_url() . '\')"><i class="glyphicon glyphicon-trash"></i></button></div>'*/;

                $row[] = '<table style="border-collapse: separate; border-spacing:  5px;">'
                        . '<tr>'
                        . '<td>' . $delete . '</td>'
                        . '</tr>'
                        . '</table>';

                $data[] = $row;
            }
            
        }

        $output = array(
            "draw" => "",
            "recordsTotal" => $reservations->num_rows(),
            "recordsFiltered" => $reservations->num_rows(),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }
}