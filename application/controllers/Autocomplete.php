<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Autocomplete extends CI_Controller {

	function __construct(){
		parent::__construct();
	}

	function autocomplete_hotel() {
        if ($this->input->is_ajax_request() && $this->input->post('info') && strlen($this->input->post('info')) > 2) {
            $condition = " country.name like '" . trim($this->input->post('info')) . "%' or city.name like '" . trim($this->input->post('info')) . "%'";
            $search = $this->Planetresmodel->city_country($condition, " limit 7 ", " group by city.abbre ");
            if ($search !== FALSE) {
                foreach ($search->result() as $row) {
                    ?>
                    <a onclick="javascript:asignar_hotel(<?='\''.$row->idcity.'\',\''.$row->idcountry.'\',\''.$row->city.'\',\'' .$row->country.'\',\''.$row->abbre.'\'';?>)" ><?php echo $row->city .', '.$row->country.' '.$row->abbre ?></a><br>
                    <?php
                }
            } else {
                ?>

                <p><?php echo 'No results' ?></p>

                <?php
            }
        }
    }		   

	function autocomplete_excursion() {
        if ($this->input->is_ajax_request() && $this->input->post('info') && strlen($this->input->post('info')) > 2) {
            $condition = " country.name like '" . trim($this->input->post('info')) . "%' or city.name like '" . trim($this->input->post('info')) . "%'";
            $search = $this->Planetresmodel->city_country($condition, " limit 7 ", " group by city.abbre ");
            if ($search !== FALSE) {
                foreach ($search->result() as $row) {
                    ?>
                    <a onclick="javascript:asignar_excursion(<?='\''.$row->idcity.'\',\''.$row->idcountry.'\',\''.$row->city.'\',\'' .$row->country.'\',\''.$row->abbre.'\'';?>)" ><?php echo $row->city .', '.$row->country.' '.$row->abbre ?></a><br>
                    <?php
                }
            } else {
                ?>

                <p><?php echo 'No results' ?></p>

                <?php
            }
        }
    }

    function autocomplete_transfer() {
       if ($this->input->is_ajax_request() && $this->input->post('info') && strlen($this->input->post('info')) > 2) {
            $condition = " country.name like '" . trim($this->input->post('info')) . "%' or city.name like '" . trim($this->input->post('info')) . "%'";
            $search = $this->Planetresmodel->city_country($condition, " limit 7 ", " group by city.abbre ");
            if ($search !== FALSE) {
                foreach ($search->result() as $row) {
                    ?>
                    <a onclick="javascript:asignar_transfer(<?='\''.$row->idcity.'\',\''.$row->idcountry.'\',\''.$row->city.'\',\'' .$row->country.'\',\''.$row->abbre.'\'';?>)" ><?php echo $row->city .', '.$row->country.' '.$row->abbre ?></a><br>
                    <?php
                }
            } else {
                ?>

                <p><?php echo 'No results' ?></p>

                <?php
            }
        }
    }
}