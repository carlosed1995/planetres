var portrait_uploader = {

    reader: null,
    form_data: null,
    file_data: null,
    callbacks: {
        progress: false,
        loaded: false,
        uploaded: false,
        error: false
    },

    init: function ()
    {
        this.reader = new FileReader();
        this.form_data = new FormData();

        this.handle_file_upload();

        return this;
    },

    on: function (type, callback)
    {
        var _undefined;

        if (this.callbacks[type] === _undefined || typeof callback !== 'function')
            return this;

        portrait_uploader.callbacks[type] = callback;

        return this;
    },

    upload_file: function (submit_btn)
    {
        this.file_data = $('#file').prop('files')[0];
        this.id=$('#id').val();
        this.type=$('#type').val();
        this.form_data.append('portrait', this.file_data);
        this.form_data.append('id', this.id);
        this.form_data.append('type', this.type);
        $.ajax({
            url: global_data.portrait_upload_url,
            dataType: 'json',
            cache: false,
            contentType: false,
            processData: false,
            data: portrait_uploader.form_data,
            type: 'post',
            xhr: function ()
            {
                var xhr = $.ajaxSettings.xhr();

                if (xhr.upload)
                {
                    xhr.upload.addEventListener(
                            'progress',
                            portrait_uploader.handle_progress,
                            false
                            );
                }

                return xhr;
            },
            complete: function (data)
            {
                if (portrait_uploader.callbacks.loaded)
                    portrait_uploader.callbacks.loaded.call(null, data);
            },
            success: function (data)
            {
                if (data.error)
                {
                    if (portrait_uploader.callbacks.error)
                        portrait_uploader.callbacks.error.call(null, data.error, data);

                    return false;
                }

                if (portrait_uploader.callbacks.uploaded)
                    portrait_uploader.callbacks.uploaded.call(null, data);
            },
            error: function (data)
            {
                if (portrait_uploader.callbacks.error)
                    portrait_uploader.callbacks.error.call(null, '404 not found', data);
            }
        })

        return false; // prevent anchor action
    },

    handle_progress: function (event)
    {
        console.log('loading');

        if (event.lengthComputable)
        {

            var percentage = (event.loaded / event.total) * 100;

            if (portrait_uploader.callbacks.progress)
                portrait_uploader.callbacks.progress.call(null, percentage, this.file_data);

        }
    },

    handle_file_upload: function ()
    {
        $('#file').change(function () {

            var file = (this.files[0].name).toString();

            $('#file-info').empty().text(file);

            portrait_uploader.reader.onload = function (e)
            {
                $('#preview img').attr('src', e.target.result);
            }

            portrait_uploader.reader.readAsDataURL(this.files[0]);
        });

        $('#file-save').on('click', null, {
            reader: this.reader
        }, function () {
            return portrait_uploader.upload_file(this);
        });
    }

};

$(document).ready(function () {

    $('#preview').on('mouseenter mouseleave', null, {can_animate: true}, function (event) {

        var desired_event = {
            mouseleave: 'fadeOut',
            mouseenter: 'fadeIn'
        };

        event.data.can_animate = false;

        $("#file-select").stop(true, true)[desired_event[event.type]]("fast", function () {
            event.data.can_animate = true;
        });

    });

    $('#file-select').on('click', function (e) {
        e.preventDefault();
        $('#file').click();
    });

    $('#file').val(null);

    portrait_uploader
            .init()
            .on('progress', function (percentage) {
                // en este evento obtenemos el porcentaje de subida C:

                console.log('progreso: ', percentage);
            })
            .on('uploaded', function (data) {
                $("#file-info").removeClass('text-info');
                $("#file-info").removeClass('text-danger');
                $("#file-info").addClass('text-success');
                $("#file-info").html("Success1");                                
            })
            .on('error', function (msg, data) {
                $("#file-info").removeClass('text-info');
                $("#file-info").removeClass('text-success');
                $("#file-info").addClass('text-danger');
                $("#file-info").html(msg);
            });

});