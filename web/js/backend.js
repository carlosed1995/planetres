/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/*Menu*/
$("#newpassenger").click(function () {
    $(".content-header small").html("New Passenger");
    $('#maincontent').load('backend/Passenger/newpassenger');
});
$("#listpassenger").click(function () {
    $(".content-header small").html("Passengers List");
    $('#maincontent').load('backend/Passenger/listpassenger');
});
$("#newtransport").click(function () {
    $(".content-header small").html("New Transport");
    $('#maincontent').load('backend/Transport/newtransport');
});
$("#listtransport").click(function () {
    $(".content-header small").html("Transport List");
    $('#maincontent').load('backend/Transport/listtransport');
});
$("#newtravelagent").click(function () {
    $(".content-header small").html("New Travel Agent");
    //$("#maincontent").html("New Travel Agent Working");   
    $('#maincontent').load('backend/Travelagent/newtravelagent');
});
$("#listtravelagent").click(function () {
    $(".content-header small").html("Travel Agent List");
    //$("#maincontent").html("Travel Agent List Working");   
    $('#maincontent').load('backend/Travelagent/listtravelagent');
});
$("#newexcursions").click(function () {
    $(".content-header small").html("New Excursions");
    $('#maincontent').load('backend/Excursions/newexcursions');
});
$("#listexcursions").click(function () {
    $(".content-header small").html("Excursions List");
    $('#maincontent').load('backend/Excursions/listexcursions');
});
$("#newhotel").click(function () {
    $(".content-header small").html("New Hotel");
    $('#maincontent').load('backend/Hotels/newhotels');
});
$("#listhotel").click(function () {
    $(".content-header small").html("Hotels List");
    $('#maincontent').load('backend/Hotels/listhotels');
});
$("#newtour").click(function () {
    $(".content-header small").html("New Tour");
    $('#maincontent').load('backend/Tours/newtours');
});
$("#listtour").click(function () {
    $(".content-header small").html("Tours List");
    $('#maincontent').load('backend/Tours/listtours');
});
$("#newreservation").click(function () {
    $(".content-header small").html("New Reservation");
    $('#maincontent').load('backend/Reservations/newreservations');
});
$("#listreservation").click(function () {
    $(".content-header small").html("Reservation List");
    $('#maincontent').load('backend/Reservations/listreservations');
});
$("#newcategory").click(function () {
    $(".content-header small").html("New Category");
    $('#maincontent').load('backend/Categories/newcategories');
});
$("#listcategory").click(function () {
    $(".content-header small").html("Categories List");
    $('#maincontent').load('backend/Categories/listcategories');
});
$("#newfacilities").click(function () {
    $(".content-header small").html("New Facility");
    $('#maincontent').load('backend/Facilities/newfacilities');
});
$("#listfacilities").click(function () {
    $(".content-header small").html("Features List");
    $('#maincontent').load('backend/Facilities/listfacilities');
});
$("#newtypetransfer").click(function () {
    $(".content-header small").html("New Type Transfer");
    $('#maincontent').load('backend/Typetransfer/newtypetransfer');
});
$("#listtypetransfer").click(function () {
    $(".content-header small").html("Type Transfer List");
    $('#maincontent').load('backend/Typetransfer/listtypetransfer');
});
$("#newtourstype").click(function () {
    $(".content-header small").html("New Tours Type");
    $('#maincontent').load('backend/Tourstype/newtourstype');
});
$("#listtourstype").click(function () {
    $(".content-header small").html("Tours Type List");
    $('#maincontent').load('backend/Tourstype/listtourstype');
});
$("#newtyperoom").click(function () {
    $(".content-header small").html("New Type Room");
    $('#maincontent').load('backend/Typeroom/newtyperoom');
});
$("#listtyperoom").click(function () {
    $(".content-header small").html("Type Room List");
    $('#maincontent').load('backend/Typeroom/listtyperoom');
});
/*fin menu*/
function udpatepassenger(id, url) {
    $.ajax({
        data: {idpassengers: id},
        url: url + "backend/Passenger/newpassenger",
        type: "POST",
        success: function (datos)
        {
            $(".content-header small").html("Update Passeneger");
            $('#maincontent').html(datos);
        }
    });
}
function deletepassenger(id, active, url) {
    $.ajax({
        data: {idpassengers: id, active: active},
        url: url + "backend/Passenger/deletepassenger",
        type: "POST",
        success: function (datos)
        {
            $('#maincontent').html(datos);
        }
    });
}
function udpatetransport(id, url) {
    $.ajax({
        data: {idtransport: id},
        url: url + "backend/Transport/newtransport",
        type: "POST",
        success: function (datos)
        {
            $(".content-header small").html("Update Transport");
            $('#maincontent').html(datos);
        }
    });
}
function deletetransport(id, status, url) {
    $.ajax({
        data: {idtransport: id, active: status},
        url: url + "backend/Transport/deletetransport",
        type: "POST",
        success: function (datos)
        {
            $('#maincontent').html(datos);
        }
    });
}
function udpatetravelagent(id, url) {
    $.ajax({
        data: {idtravel_agent: id},
        url: url + "backend/Travelagent/newtravelagent",
        type: "POST",
        success: function (datos)
        {
            $(".content-header small").html("Update Travel Agent");
            $('#maincontent').html(datos);
        }
    });
}
function deletetravelagent(id, status, url) {
    $.ajax({
        data: {idtravel_agent: id, status: status},
        url: url + "backend/Travelagent/deletetravelagent",
        type: "POST",
        success: function (datos)
        {
            $('#maincontent').html(datos);
        }
    });
}
function udpateexcursions(id, url) {
    $.ajax({
        data: {idexcursions: id},
        url: url + "backend/Excursions/newexcursions",
        type: "POST",
        success: function (datos)
        {
            $(".content-header small").html("Update Excursions");
            $('#maincontent').html(datos);
        }
    });
}
function deleteexcursions(id, status, url) {
    $.ajax({
        data: {idexcursions: id, active: status},
        url: url + "backend/Excursions/deleteexcursions",
        type: "POST",
        success: function (datos)
        {
            $('#maincontent').html(datos);
        }
    });
}
function udpatereservations(id, url) {
    $.ajax({
        data: {idreservations: id},
        url: url + "backend/Reservations/newreservations",
        type: "POST",
        success: function (datos)
        {
            $(".content-header small").html("Update Reservations");
            $('#maincontent').html(datos);
        }
    });
}
function deletereservations(id, status, url) {
    $.ajax({
        data: {idreservations: id, active: status},
        url: url + "backend/Reservations/deletereservations",
        type: "POST",
        success: function (datos)
        {
            $('#maincontent').html(datos);
        }
    });
}
function udpatehotels(id, url) {
    $.ajax({
        data: {idhotel: id},
        url: url + "backend/Hotels/newhotels",
        type: "POST",
        success: function (datos)
        {
            $(".content-header small").html("Update Hotel");
            $('#maincontent').html(datos);
        }
    });
}
function deletehotels(id, status, url) {
    $.ajax({
        data: {idhotel: id, active: status},
        url: url + "backend/Hotels/deletehotels",
        type: "POST",
        success: function (datos)
        {
            $('#maincontent').html(datos);
        }
    });
}
function udpatetours(id, url) {
    $.ajax({
        data: {idtours: id},
        url: url + "backend/Tours/newtours",
        type: "POST",
        success: function (datos)
        {
            $(".content-header small").html("Update Tour");
            $('#maincontent').html(datos);
        }
    });
}
function deletetours(id, status, url) {
    $.ajax({
        data: {idtours: id, active: status},
        url: url + "backend/Tours/deletetours",
        type: "POST",
        success: function (datos)
        {
            $('#maincontent').html(datos);
        }
    });
}
function udpatecategories(id, url) {
    $.ajax({
        data: {idcategory: id},
        url: url + "backend/Categories/newcategories",
        type: "POST",
        success: function (datos)
        {
            $(".content-header small").html("Update Category");
            $('#maincontent').html(datos);
        }
    });
}
function deletecategories(id, status, url) {
    $.ajax({
        data: {idcategory: id, active: status},
        url: url + "backend/Categories/deletecategories",
        type: "POST",
        success: function (datos)
        {
            $('#maincontent').html(datos);
        }
    });
}
function udpatefacilities(id, url) {
    $.ajax({
        data: {idfacilities: id},
        url: url + "backend/Facilities/newfacilities",
        type: "POST",
        success: function (datos)
        {
            $(".content-header small").html("Update Feature");
            $('#maincontent').html(datos);
        }
    });
}
function deletefacilities(id, status, url) {
    $.ajax({
        data: {idfacilities: id, active: status},
        url: url + "backend/Facilities/deletefacilities",
        type: "POST",
        success: function (datos)
        {
            $('#maincontent').html(datos);
        }
    });
}
function udpatetypetransfer(id, url) {
    $.ajax({
        data: {idtype_transfers: id},
        url: url + "backend/Typetransfer/newtypetransfer",
        type: "POST",
        success: function (datos)
        {
            $(".content-header small").html("Update Type Trasnfer");
            $('#maincontent').html(datos);
        }
    });
}
function deletetypetransfer(id, status, url) {
    $.ajax({
        data: {idtype_transfers: id, active: status},
        url: url + "backend/Typetransfer/deletetypetransfer",
        type: "POST",
        success: function (datos)
        {
            $('#maincontent').html(datos);
        }
    });
}
function udpatetourstype(id, url) {
    $.ajax({
        data: {idtours_type: id},
        url: url + "backend/Tourstype/newtourstype",
        type: "POST",
        success: function (datos)
        {
            $(".content-header small").html("Update Tours Type");
            $('#maincontent').html(datos);
        }
    });
}
function deletetourstype(id, status, url) {
    $.ajax({
        data: {idtours_type: id, active: status},
        url: url + "backend/Tourstype/deletetourstype",
        type: "POST",
        success: function (datos)
        {
            $('#maincontent').html(datos);
        }
    });
}
function udpatetyperoom(id, url) {
    $.ajax({
        data: {idtype_room: id},
        url: url + "backend/Typeroom/newtyperoom",
        type: "POST",
        success: function (datos)
        {
            $(".content-header small").html("Update Tours Type");
            $('#maincontent').html(datos);
        }
    });
}
function deletetyperoom(id, status, url) {
    $.ajax({
        data: {idtype_room: id, active: status},
        url: url + "backend/Typeroom/deletetyperoom",
        type: "POST",
        success: function (datos)
        {
            $('#maincontent').html(datos);
        }
    });
}