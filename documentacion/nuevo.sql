ALTER TABLE `excursions` ADD `user_iduser` INT NOT NULL ;
ALTER TABLE `tours` ADD `user_iduser` INT NOT NULL ;
ALTER TABLE `hotel` ADD `user_iduser` INT NOT NULL ;
ALTER TABLE `reservations` ADD `user_iduser` INT NOT NULL ;
ALTER TABLE `trasnfers` ADD `user_iduser` INT NOT NULL ;
ALTER TABLE `user` ADD `user_iduser` INT NOT NULL ;
CREATE TABLE IF NOT EXISTS `mydb`.`history` (
  `idhistory` INT NOT NULL AUTO_INCREMENT,
  `date` DATETIME NULL,
  `table` VARCHAR(256) NULL,
  `description` TEXT NULL,
  `change` TEXT NULL,
  `iduser` INT NULL,
  PRIMARY KEY (`idhistory`))
ENGINE = InnoDB


